<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Indice de Rutas
|--------------------------------------------------------------------------
|
| Raiz (/)
| Auth Routes
| Home
| Rutas de Recursos
| Rutas Champions Tools
| Rutas Ajax
|
*/

Route::get('/', function () {
	//return view('welcome');
	return redirect()->route('home');
});

Route::get('/vw_info_php', function(){
return view('info');
});




Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth','verified','web']);
Route::get('/sitio/en/construccion', 'HomeController@en_construccion')->name('web.en.construccion')->middleware(['auth','verified','web']);

Route::resource('roles', 'RoleController')->middleware(['auth','verified','web']);
Route::get('/roles/permisos/{id_role}','RoleController@permisos')
            ->name('roles.permisos')
            ->middleware(['auth','verified','web']);

Route::post('/roles/asignar/permiso','RoleController@AsociarPermiso')
            ->name('roles.asignar.permiso')
            ->middleware(['auth','verified','web']);

Route::get('permitir','RoleController@permitir')->name('permitir');
Route::resource('permisos', 'PermissionController')->middleware(['auth','verified','web']);
Route::resource('paises', 'CountryController')->middleware(['auth','verified','web']);

Route::resource('empleados', 'EmployeeController')->middleware(['auth','verified','web']);
Route::post('/empleados/filtros','EmployeeController@filtros')
			->name('empleados.filtros')
			->middleware(['auth','verified','web']);

Route::resource('lideres', 'HeadController')->middleware(['auth','verified','web']);
Route::resource('cargos', 'PositionController')->middleware(['auth','verified','web']);
Route::resource('usuarios', 'UserController')->middleware(['auth','verified','web']);
Route::resource('canales', 'ChannelController')->middleware(['auth','verified','web']);
Route::resource('territorios', 'TerritoryController')->middleware(['auth','verified','web']);
Route::resource('regionales', 'RegionalController')->middleware(['auth','verified','web']);

Route::resource('admins', 'AdminsController')->middleware(['auth','verified','web']);

Route::post('/admins/buscar','AdminsController@buscar_empleado')
			->name('admins.buscar.empleado')
			->middleware(['auth','verified','web']);

Route::get('/admins/ver/buscar','AdminsController@ver_buscar_empleado')
			->name('admins.ver.buscar.empleado')
            ->middleware(['auth','verified','web']);

Route::resource('logs', 'LogController')->middleware(['auth','verified','web']);

Route::get('logs/export/accesos/{rango}/{cedula}', 'LogController@exportar_accesos')
            ->name('log.exportar.accesos')
            ->middleware(['auth','verified','web']);

Route::get('logs/export/accesos/resumen/{rango}/{cedula}', 'LogController@exportar_accesos_resumen')
            ->name('log.exportar.accesos.resumen')
            ->middleware(['auth','verified','web']);

Route::post('/logs/traer_cedula','LogController@traer_cedula')
            ->name('log.traer.cedula')
            ->middleware(['auth','verified','web']);

Route::post('/visits/register','VisitsController@registrar_visita')
            ->name('visits.register')
            ->middleware(['auth','verified','web']);

Route::post('/visits/close','VisitsController@cerrar_visita')
            ->name('visits.close')
            ->middleware(['auth','verified','web']);


//ENCUESTAS
// =============================================================================================
Route::get('encuestas/formularios/{visita}/{form}', 'EncuestasController@formularios_encuestas')
            ->name('encuestas.formularios')
            ->middleware(['auth','verified','web']);

Route::get('encuestas/oficiales/{id_encuesta}/{version}/{idpdv}/{doc_acomp?}','EncuestasController@encuestas_oficiales')
            ->name('encuestas.oficiales')
            ->middleware(['auth','verified','web']);

Route::get('encuestas/preguntas/constructor/{encuesta_visita}/{encuesta}/{version}/{completada}/{pregunta}/{anterior}/{doc_acomp?}/{atras?}/{id_opc_resp?}/{txt_opc_resp?}/{txt_resp?}/{calif_resp?}','EncuestasController@encuestasPreguntasConstructor')
            ->name('encuestas.preguntas.constructor')
            ->middleware(['auth','verified','web']);


Route::post('encuestas/guardar','EncuestasController@encuestas_guardar')
            ->name('encuestas.validar.guardar')
            ->middleware(['auth','verified','web']);

Route::post('encuestas/regresar','EncuestasController@encuestas_regresar')
            ->name('encuestas.pregunta.anterior')
            ->middleware(['auth','verified','web']);

Route::get('/checklist/ver/salida','EncuestasController@ver_check_ruta')
			->name('encuestas.ver.check.salida')->middleware(['auth','verified','web']);

Route::post('/checklist/crear/salida','EncuestasController@crear_check_ruta')
			->name('encuestas.crear.check.salida')->middleware(['auth','verified','web']);

Route::get('/coaching/ver','EncuestasController@ver_coachings')
			->name('encuestas.ver.coachings')->middleware(['auth','verified','web']);

Route::get('/coaching/finalizar/{id}','EncuestasController@finalizar_coachings')
            ->name('encuestas.finalizar.coachings')->middleware(['auth','verified','web']);

Route::get('/tracking/ver/salida','EncuestasController@iniciar_tracking')
            ->name('encuestas.iniciar.tracking')->middleware(['auth','verified','web']);

Route::post('/tracking/crear','EncuestasController@crear_tracking_competencia')
			->name('encuestas.crear.tracking.competencia')->middleware(['auth','verified','web']);

// =============================================================================================

//RUTAS CHAMPIONS TOOLS
Route::get('/dash_pospago', function () {
	return view('bi.ctools.dash_movil');
})->name('bi.ctools.movil')
  ->middleware(['auth','verified','web']);

Route::get('/dash_home', function () {
	return view('bi.ctools.dash_home');
})->name('bi.ctools.home')
  ->middleware(['auth','verified','web']);

// *******************************************************************************************
// *******************************************************************************************
//RUTAS PARA GESTION EN PUNTO DE VENTA
Route::group(['prefix' => 'gestion'], function () {

    Route::get('/buscar_punto','GestionController@buscarPunto')->name('gestion.buscar.punto')->middleware(['auth','verified','web']);
    Route::post('/buscar_punto','GestionController@traerPunto')->name('gestion.traer.punto')->middleware(['auth','verified','web']);
    Route::post('/buscar_nom_punto','GestionController@traerNombrePunto')->name('gestion.traer.nombre.puntos')->middleware(['auth','verified','web']);
    Route::post('/buscar_cc_owner','GestionController@traerCedulaPropietario')->name('gestion.traer.cedula.propietario')->middleware(['auth','verified','web']);
	Route::post('/buscar_pos_code','GestionController@traerCodigoPos')->name('gestion.traer.pos.code')->middleware(['auth','verified','web']);
    Route::post('/buscar_circuito_estado','GestionController@traerCircuitoEstado')->name('gestion.traer.circuito.estado')->middleware(['auth','verified','web']);
    Route::post('/buscar_puntos_ruta_dia','GestionController@traerPuntosRutaDia')->name('gestion.puntos.ruta.dia')->middleware(['auth','verified','web']);
    Route::post('/confidencialidad','GestionController@confidencialidad')->name('gestion.guardar.confidencialidad')->middleware(['auth','verified','web']);
    Route::post('/gestion_fotos','GestionController@foto_punto_venta')->name('gestion.cargar.foto')->middleware(['auth','verified','web']);

	Route::post('/gestion_correos','GestionController@email_pv')->name('gestion.correo.pv')->middleware(['auth','verified','web']);

});

// *******************************************************************************************

// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------
//RUTAS PARA PLANEACION
Route::group(['prefix' => 'planeacion'], function () {
    Route::get('/ver_planeador','PlaneadorController@verPlaneador')->name('planeacion.ver.planeador')->middleware(['auth','verified','web']);
});
// *******************************************************************************************

//RUTAS PARA REPORTE DE TIENDAS
Route::group(['prefix' => 'tiendas'], function () {
	Route::get('/ns/ver/{aliado?}/{regional?}','ReporteTiendasController@ver_ns')->name('tiendas.ver.ns')->middleware(['auth','verified','web']);
	Route::get('/nps/ver/{canal?}/{negocio?}/{regional?}/{tienda?}','ReporteTiendasController@ver_nps')->name('tiendas.ver.nps')->middleware(['auth','verified','web']);
});


//RUTAS PARA SEGUIMIENTO RETAIL
Route::group(['prefix' => 'retail'], function () {
	Route::get('/ver_todo','RetailController@ver_todo')->name('retail.ver.todo')->middleware(['auth','verified','web']);
	Route::get('/ver_regional/{regional}','RetailController@ver_regional')->name('retail.ver.regional')->middleware(['auth','verified','web']);
	Route::get('/ver_regional/{regional}/{departamento}','RetailController@ver_departamento')->name('retail.ver.departamento')->middleware(['auth','verified','web']);

	Route::get('/export/nacional', 'RetailController@exportar_puntos_nacional')->name('retail.exportar.nacional')->middleware(['auth','verified','web']);
	Route::get('/export/regional/{regional}', 'RetailController@exportar_puntos_regional')->name('retail.exportar.regional')->middleware(['auth','verified','web']);
	Route::get('/export/departamento/{regional}/{departamento}', 'RetailController@exportar_puntos_departamento')->name('retail.exportar.departamento')->middleware(['auth','verified','web']);
});

//RUTAS PARA MAPA DE GESTORES
Route::group(['prefix' => 'mapas'], function () {
    Route::get('/georeferenciar','MapaGestorController@entrar')->name('mapas.gestores.entrar');
    Route::post('/validar_gestores','MapaGestorController@validar')->name('mapas.gestores.validar');//->middleware(['auth','verified','web']);

//RUTAS PARA MAPA MOC (mapa oportunidades comerciales)
    Route::post('/ver_cve','MapaGestorController@ver_cve')->name('mapas.ver.cve')->middleware(['auth','verified','web']);
    Route::get('/ver_opciones','MapaGestorController@verOpciones')->name('mapas.ver.opciones.moc')->middleware(['auth','verified','web']);

//RUTAS PARA MAPA CENSO (mapa censo puntos propios)
	Route::get('/ver_censo/{c_cves}/{c_antenas}/{c_cciales}/{c_puntos}','MapaGestorController@verCenso')->name('mapas.ver.censo.propios')->middleware(['auth','verified','web']);
	Route::post('/actualizar_censo','MapaGestorController@ActualizarCenso')->name('mapas.actualizar.censo.propios')->middleware(['auth','verified','web']);

//RUTAS PARA MAPA CENSO BOC (mapa censo BOC Bolivia)
	Route::get('/ver_censo_boc/{c_cves}/{c_circuitos}/{c_puntos}','MapaGestorController@verCensoBOC')->name('mapas.ver.censo.boc')->middleware(['auth','verified','web']);
	Route::post('/actualizar_censo_boc','MapaGestorController@ActualizarCensoBOC')->name('mapas.actualizar.censo.boc')->middleware(['auth','verified','web']);


//PARA EXPORTAR UN EXCEL DE LOS PUNTOS DEL CVE
    Route::get('/export/puntos/{cve}', 'MapaGestorController@exportar_puntos_cve')
            ->name('mapas.exportar.puntos')->middleware(['auth','verified','web']);
});

//RUTAS PARA TRADE MARKETING
Route::group(['prefix' => 'trade'], function () {
    /*
        GET /trade/Path             index()     tradePathController@index   name('trade.path.index')
        GET /trade/Path/create      create()    tradePathController@create  name('trade.path.create')
        POST /trade/Path            store()     tradePathController@store   name('trade.path.store')
        GET /trade/Path/{id}        show()      tradePathController@show    name('trade.path.show')
        GET /trade/Path/{id}/edit   edit()      tradePathController@edit    name('trade.path.edit')
        PUT/PATCH /trade/Path/{id}  update()    tradePathController@update  name('trade.path.update')
        DELETE /trade/Path/{id}     destroy()   tradePathController@destroy name('trade.path.destroy')
    */
    Route::get('/seleccionar_implementacion','TradePiezasController@seleccionaImplementacion')->name('trade.selecciona.implementacion')->middleware(['auth','verified','web']);
    Route::post('/implementacion_seleccionada','TradePiezasController@implementacionSeleccionada')->name('trade.implementacion.seleccionada')->middleware(['auth','verified','web']);
	Route::get('/asociar_piezas/{impl}/{pieza}','TradePiezasController@asociarPiezas')->name('trade.piezas.asociado')->middleware(['auth','verified','web']);
	Route::get('/desasociar_piezas/{impl}/{pieza}','TradePiezasController@desasociarPiezas')->name('trade.piezas.desasociado')->middleware(['auth','verified','web']);

	Route::post('/implementacion_auditada','TradePiezasController@implementacionAuditada')->name('trade.implementacion.auditada')->middleware(['auth','verified','web']);
	Route::get('/ver_auditorias/{impl}/{idpdv}','TradePiezasController@verAuditoria')->name('trade.implementacion.ver.auditorias')->middleware(['auth','verified','web']);

	Route::get('/implementacion/reactivar/{id}','TradeImplementacionController@reactivate')->name('trade.implementacion.reactivar')->middleware(['auth','verified','web']);

	Route::post('/colocacion_seleccionada','TradePiezasController@ColocacionSeleccionada')->name('trade.colocacion.seleccionada')->middleware(['auth','verified','web']);
	Route::get('/colocar_piezas/{idpdv}/{impl}/{pieza}','TradePiezasController@colocarPiezas')->name('trade.piezas.colocar')->middleware(['auth','verified','web']);
    Route::get('/quitar_piezas/{idpdv}/{impl}/{pieza}','TradePiezasController@quitarPiezas')->name('trade.piezas.quitar')->middleware(['auth','verified','web']);

    Route::get('/inventario/ver/{impl_pieza}','TradePiezasController@verInventario')->name('trade.inventario.ver')->middleware(['auth','verified','web']);
    Route::post('/inventario/ajustar/inventario','TradePiezasController@ajustarInventario')->name('trade.inventario.ajustar')->middleware(['auth','verified','web']);

    Route::get('/dash/ver','TradePiezasController@dashVer')->name('trade.dash.ver')->middleware(['auth','verified','web']);
    Route::get('/dash/ver/implementacion/{implementacion}','TradePiezasController@dashVerImplementacion')->name('trade.dash.ver.implementacion')->middleware(['auth','verified','web']);
	Route::get('/dash/ver/regional/{implementacion}/{regional}','TradePiezasController@dashVerRegional')->name('trade.dash.ver.regional')->middleware(['auth','verified','web']);

    //Reporte de Excel
    Route::get('implementacion/export', 'TradePiezasController@exportar_reporte_basico')->name('trade.exportar.reporte.basico')->middleware(['auth','verified','web']);
    //Resource
    Route::resource('clases', 'TradeClaseController')->middleware(['auth','verified','web']);
    Route::resource('contenidos', 'TradeContenidoController')->middleware(['auth','verified','web']);
    Route::resource('objetivos', 'TradeObjetivoController')->middleware(['auth','verified','web']);
    Route::resource('implementaciones', 'TradeImplementacionController')->middleware(['auth','verified','web']);
    Route::resource('tipos', 'TradeTipoController')->middleware(['auth','verified','web']);
    Route::resource('piezas', 'TradePiezasController')->middleware(['auth','verified','web']);
    Route::resource('clasehallazgo', 'TradeClaseHallazgoController')->middleware(['auth','verified','web']);
    Route::resource('hallazgos', 'TradeHallazgosController')->middleware(['auth','verified','web']);
    /*
    Route::get('/ver_piezas','TradePiezas@verPiezas')->name('trade.piezas.ver')->middleware(['auth','verified','web']);
    */
});

/***************************************************************************************************
*** PRUEBA PARA GEOCODER EN INFOPOS
***************************************************************************************************/
Route::get('/cercanos',function(){
    return view('cercanos.ver');
})->name('cercanos.ver')->middleware(['auth','verified','web']);
//********************************************************************************************** */


/***************************************************************************************************
*** PRUEBA PARA ADMINISTRACION DE ARCHIVOS EN INFOPOS
***************************************************************************************************/
Route::get('/verArchivos','ArchivosController@verArchivos')->name('archivos.verArchivos')
                                                           ->middleware(['auth','verified','web']);

Route::get('/{archivo}','ArchivosController@descargarArchivos')->name('archivos.descargarArchivos')
                                                           ->middleware(['auth','verified','web']);

//********************************************************************************************** */


//  Route::view('error', 'error', ['error' => $error])->name('error');

/*
|--------------------------------------------------------------------------
| Rutas Ajax
|--------------------------------------------------------------------------
|
| Nos van a ayudar a generar iteraccion con ajax
| queda pendiente evaluar reemplazar con VUE JS
| de momento queda este parche
|
*/

Route::get( '/ajax/pais/{id}',
			'JqueryController@ajaxPaisRegiones')->name('ajax.pais.regiones')
												->middleware(['web']);      //->middleware(['auth','verified','web'])

Route::get( '/ajax/region/{id}',
			'JqueryController@ajaxRegionTerritorios')->name('ajax.region.territorios')
												     ->middleware(['web']);  //->middleware(['auth','verified','web'])

Route::get( '/ajax/empleado/{cedula}',
			'JqueryController@ajaxCedulaEmpleado')->name('ajax.cedula.empleado')
												     ->middleware(['web']);  //->middleware(['auth','verified','web'])


Route::get( '/ajax/circuito/autocompletar',
            'JqueryController@ajaxAutocompleteCircuits')->name('ajax.circuito.autocompletar')
                                                        ->middleware(['web']);

Route::get( '/ajax/cercanos/puntos/lat/{lat}/lng/{lng}',
            'JqueryController@ajaxCercanosPuntos')->name('ajax.cercanos.puntos')
                                                        ->middleware(['web']);  //->middleware(['auth','verified','web'])

Route::get( '/ajax/trade/objetivo/{fuente}',
            'JqueryController@ajaxTradeObjetivos')->name('ajax.trade.objetivos')
                                                       ->middleware(['web']);   //->middleware(['auth','verified','web'])

Route::get( '/ajax/cve/municipios/{departamento}',
			'JqueryController@ajaxMunicipios_cve')->name('ajax.cve.municipios')
												     ->middleware(['web']);  //->middleware(['auth','verified','web'])
Route::get( '/ajax/cve/departamentos',
			'JqueryController@ajaxDepartamentos_cve')->name('ajax.cve.departamentos')
												     ->middleware(['web']);  //->middleware(['auth','verified','web'])

Route::get( '/ajax/cve/cves/{departamento}/{municipio}',
			'JqueryController@ajaxCves_cve')->name('ajax.cve.cves')
											->middleware(['web']);  //->middleware(['auth','verified','web'])

Route::get( '/ajax/tracking/{municipio}',
            'JqueryController@ajaxTrackingMunicipio')->name('ajax.tracking.municipio')
                                                  ->middleware(['web']);  //->middleware(['auth','verified','web'])

/*
|--------------------------------------------------------------------------
| Fin Rutas Ajax
|--------------------------------------------------------------------------
*/
