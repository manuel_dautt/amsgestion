@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-12">
        <div class="pull-left">
            <h6>PUNTOS HARD-DISCOUNTER</h6>
        </div>
        <div class="pull-right">
			REGIONALES
			@foreach($regionales as $regional)
			<a class="btn btn-sm btn-primary" href="{{ route('retail.ver.regional',['regional' => $regional->regional ]) }}">{{ $regional->regional }}</a>
			@endforeach
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-hover table-sm">
	<caption>Reporte de Puntos</caption>
	<h6 class="row center"><strong>Listado Puntos de Venta</strong><a class="btn btn-sm btn-primary" href="{{ route('retail.exportar.nacional') }}">Listar xlsx</a></h6>
        <thead>
            <tr>
                <th scope="col">CADENA</th>
                <th scope="col">NOMBRE_PUNTO</th>
                <th scope="col">REGIONAL</th>
                <th scope="col">DEPARTAMENTO</th>
                <th scope="col">CIUDAD</th>
                <th scope="col">DIRECCION</th>
                <th scope="col">POS_PADRE</th>
                <th scope="col">POS_CODE</th>
				<th scope="col">EJECUTIVO</th>
            </tr>
        </thead>
        <tbody>
            @foreach($puntos as $pv)
            <tr>
                <td>{{ $pv->cadena }}
				<form action="{{ route('gestion.traer.punto') }}" method="POST">
					@csrf
					<input type="hidden" id="idpdv" name="idpdv" value="{{ $pv->idpdv }}">
					<button type="submit" class="btn btn-sm btn-primary">VER</button>
				</form>
				</td>
                <td>{{ $pv->nombre_punto }}</td>
				<td>{{ $pv->regional }}</td>
				<td>{{ $pv->departamento }}</td>
				<td>{{ $pv->ciudad }}</td>
				<td>{{ $pv->direccion }}</td>
				<td>{{ $pv->pos_padre }}</td>
				<td>{{ $pv->cod_pos }}</td>
				<td>{{ $pv->ejecutivo }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>

@endsection