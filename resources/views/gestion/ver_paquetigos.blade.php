<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">TOP VENTA PAQUETES</h5>
</div>
<div class="row contenedorTitulo text-center">
        <div class="col-4 tituloMes ColumnaCuatro text-center">
        Paquete
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_1}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_2}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_3}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_4}}
    </div>
</div>
@if($status['paquetes_recargas'])
    @foreach ($paquetes_recargas as $paquete)
        <div class="row text-dark bg-dark-50 border border-white text-center">
            <div class="col-4 datosPaquete">
                {{$paquete}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_1,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_1]['monto']) : 0}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_2,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_2]['monto']) : 0}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_3,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_3]['monto']) : 0}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_4,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_4]['monto']) : 0}}
            </div>

            <div class="col-4 datosPaquete">
			(monto)
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_1,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_1]['cantidad']) : 0}})
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_2,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_2]['cantidad']) : 0}})
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_3,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_3]['cantidad']) : 0}})
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_4,$mix_rec_paquetes[$paquete]) ) ? number_format($mix_rec_paquetes[$paquete][$mes_4]['cantidad']) : 0}})
            </div>
        </div>
    @endforeach
@else
    <div class="col-4 border border-white">
        SIN INFORMACION EN FUENTE PAQUETIGOS
    </div>
@endif
