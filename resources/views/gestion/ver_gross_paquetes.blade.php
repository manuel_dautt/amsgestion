<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">GROSS CON PAQUETES</h5>
</div>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>

<div class="row tituloKPI text-center">
    GROSS PREPAGO
</div>
@if($status['activaciones'])
    <div class="row text-center">
        <div class="col-3 datos">
            {{( array_key_exists($mes_1,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_1] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_2,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_2] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_3,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_3] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_4,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_4] : 0}}
        </div>
    </div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif

<div class="row tituloKPI text-center">
    GROSS PAQ. FOCO / % VS GROSS
</div>
@if($status['gross_paquetes'])
<div class="row  text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['gross_pq_foco']) ) ? $gross_paquetes['gross_pq_foco'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['gross_pq_foco']) ) ? $gross_paquetes['gross_pq_foco'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['gross_pq_foco']) ) ? $gross_paquetes['gross_pq_foco'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['gross_pq_foco']) ) ? $gross_paquetes['gross_pq_foco'][$mes_4] : 0}}
    </div>
</div>
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['porc_pq_foco']) ) ? $gross_paquetes['porc_pq_foco'][$mes_1] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['porc_pq_foco']) ) ? $gross_paquetes['porc_pq_foco'][$mes_2] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['porc_pq_foco']) ) ? $gross_paquetes['porc_pq_foco'][$mes_3] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['porc_pq_foco']) ) ? $gross_paquetes['porc_pq_foco'][$mes_4] : 0}}%
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
<div class="row tituloKPI text-center">
    GROSS PAQ. $6 mil / % VS GROSS
</div>
@if($status['gross_paquetes'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['gross_pq_6k']) ) ? $gross_paquetes['gross_pq_6k'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['gross_pq_6k']) ) ? $gross_paquetes['gross_pq_6k'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['gross_pq_6k']) ) ? $gross_paquetes['gross_pq_6k'][$mes_3] : 0}}
    </div>
    <div class="col-3  datos">
        {{( array_key_exists($mes_4,$gross_paquetes['gross_pq_6k']) ) ? $gross_paquetes['gross_pq_6k'][$mes_4] : 0}}
    </div>
</div>
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['porc_pq_6k']) ) ? $gross_paquetes['porc_pq_6k'][$mes_1] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['porc_pq_6k']) ) ? $gross_paquetes['porc_pq_6k'][$mes_2] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['porc_pq_6k']) ) ? $gross_paquetes['porc_pq_6k'][$mes_3] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['porc_pq_6k']) ) ? $gross_paquetes['porc_pq_6k'][$mes_4] : 0}}%
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif

<div class="row tituloKPI text-center">
    GROSS PAQ. TODO CONTIGO / % VS GROSS
</div>

@if($status['gross_paquetes'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['gross_pq_todo']) ) ? $gross_paquetes['gross_pq_todo'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['gross_pq_todo']) ) ? $gross_paquetes['gross_pq_todo'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['gross_pq_todo']) ) ? $gross_paquetes['gross_pq_todo'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['gross_pq_todo']) ) ? $gross_paquetes['gross_pq_todo'][$mes_4] : 0}}
    </div>
</div>
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['porc_pq_todo']) ) ? $gross_paquetes['porc_pq_todo'][$mes_1] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['porc_pq_todo']) ) ? $gross_paquetes['porc_pq_todo'][$mes_2] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['porc_pq_todo']) ) ? $gross_paquetes['porc_pq_todo'][$mes_3] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['porc_pq_todo']) ) ? $gross_paquetes['porc_pq_todo'][$mes_4] : 0}}%
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
