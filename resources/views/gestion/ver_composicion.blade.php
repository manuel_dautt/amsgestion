<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">COMPOSICION DEL GROSS</h5>
</div>
<div class="row contenedorTitulo text-center">
        <div class="col-4 tituloMes ColumnaCuatro  text-center">
        Paquete
    </div>
    <div class="col-2 tituloMes ColumnaCuatro  text-center">
        {{$mes_1}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro  text-center">
        {{$mes_2}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro  text-center">
        {{$mes_3}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro  text-center">
        {{$mes_4}}
    </div>
</div>
@if($status['paquetes_composicion'])
@foreach ($paquetes_composicion as $paquete)
<div class="row text-dark bg-dark-50 border border-white text-center">
    <div class="col-4 datos">
        {{$paquete}}
    </div>
    <div class="col-2 border">
        {{( array_key_exists($mes_1,$composicion_gross[$paquete]) ) ? $composicion_gross[$paquete][$mes_1] : 0}}
    </div>
    <div class="col-2 border">
        {{( array_key_exists($mes_2,$composicion_gross[$paquete]) ) ?  $composicion_gross[$paquete][$mes_2] : 0}}
    </div>
    <div class="col-2 border">
        {{( array_key_exists($mes_3,$composicion_gross[$paquete]) ) ?  $composicion_gross[$paquete][$mes_3] : 0}}
    </div>
    <div class="col-2 border">
        {{( array_key_exists($mes_4,$composicion_gross[$paquete]) ) ?  $composicion_gross[$paquete][$mes_4] : 0}}
    </div>
</div>
@endforeach

<div class="row text-dark bg-dark-50 border border-white text-center">
    <div class="col-4 datos">
        T. Gross Pre
    </div>
    <div class="col-2 datos border">
        {{( array_key_exists($mes_1,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_1] : 0}}
    </div>
    <div class="col-2 datos border">
        {{( array_key_exists($mes_2,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_2] : 0}}
    </div>
    <div class="col-2 datos border">
        {{( array_key_exists($mes_3,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_3] : 0}}
    </div>
    <div class="col-2 datos border">
        {{( array_key_exists($mes_4,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_4] : 0}}
    </div>
</div>

@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
