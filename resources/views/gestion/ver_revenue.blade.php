<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">R E V E N U E</h5> <br>
</div>
<h6 class="center h6">La informacion de revenue es de referencia y depende de las actualizaciones por parte de Comisiones</h6>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>
<div class="row tituloKPI text-center">
    REVENUE PREPAGO
</div>
@if($status['rev'])
<div class="row  text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$rev['rev_pre']) ) ? number_format($rev['rev_pre'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$rev['rev_pre']) ) ? number_format($rev['rev_pre'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$rev['rev_pre']) ) ? number_format($rev['rev_pre'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$rev['rev_pre']) ) ? number_format($rev['rev_pre'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE REVENUE
</div>
@endif

<div class="row tituloKPI text-center">
    REVENUE POSPAGO
</div>
@if($status['rev'])
<div class="row  text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$rev['rev_pos']) ) ? number_format($rev['rev_pos'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$rev['rev_pos']) ) ? number_format($rev['rev_pos'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$rev['rev_pos']) ) ? number_format($rev['rev_pos'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$rev['rev_pos']) ) ? number_format($rev['rev_pos'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE REVENUE
</div>
@endif

<div class="row tituloKPI text-center">
    REVENUE PORTACIONES
</div>
@if($status['rev'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$rev['rev_porta']) ) ? number_format($rev['rev_porta'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$rev['rev_porta']) ) ? number_format($rev['rev_porta'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$rev['rev_porta']) ) ? number_format($rev['rev_porta'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$rev['rev_porta']) ) ? number_format($rev['rev_porta'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE REVENUE
</div>
@endif

<div class="row tituloKPI text-center">
    REVENUE MIGRACIONES
</div>
@if($status['rev'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$rev['rev_migra']) ) ? number_format($rev['rev_migra'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$rev['rev_migra']) ) ? number_format($rev['rev_migra'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$rev['rev_migra']) ) ? number_format($rev['rev_migra'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$rev['rev_migra']) ) ? number_format($rev['rev_migra'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE REVENUE
</div>
@endif

<div class="row tituloKPI text-center">
    REVENUE TOTAL
</div>
@if($status['rev'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$rev['rev_total']) ) ? number_format($rev['rev_total'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$rev['rev_total']) ) ? number_format($rev['rev_total'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$rev['rev_total']) ) ? number_format($rev['rev_total'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$rev['rev_total']) ) ? number_format($rev['rev_total'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE REVENUE
</div>
@endif
