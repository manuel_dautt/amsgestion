<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">TIPO DE RECARGAS (Cant.-Monto-TicketProm.)</h5>
</div>
<div class="row contenedorTitulo text-center">
        <div class="col-4 tituloMes ColumnaCuatro text-center">
        Tipo Rec.
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_1}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_2}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_3}}
    </div>
    <div class="col-2 tituloMes ColumnaCuatro text-center">
        {{$mes_4}}
    </div>
</div>
@if($status['ticket'])
    @foreach ($tipos_recargas as $tipo)
        <div class="row text-dark bg-dark-50 border border-white text-center">
            <div class="col-4 datosPaquete">
                {{$tipo}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_1,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_1]['cantidad']) : 0}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_2,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_2]['cantidad']) : 0}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_3,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_3]['cantidad']) : 0}}
            </div>
            <div class="col-2 ventaPaq">
                {{( array_key_exists($mes_4,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_4]['cantidad']) : 0}}
            </div>

            <div class="col-4 datosPaquete">
			(monto)
            </div>
            <div class="col-2 ventaPaq ">
                (${{( array_key_exists($mes_1,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_1]['monto']) : 0}})
            </div>
            <div class="col-2 ventaPaq ">
                (${{( array_key_exists($mes_2,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_2]['monto']) : 0}})
            </div>
            <div class="col-2 ventaPaq ">
                (${{( array_key_exists($mes_3,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_3]['monto']) : 0}})
            </div>
            <div class="col-2 ventaPaq ">
                (${{( array_key_exists($mes_4,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_4]['monto']) : 0}})
            </div>

            <div class="col-4 datosPaquete">
			(tckt.Prom)
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_1,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_1]['ticket']) : 0}})
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_2,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_2]['ticket']) : 0}})
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_3,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_3]['ticket']) : 0}})
            </div>
            <div class="col-2 ventaPaq top">
                (${{( array_key_exists($mes_4,$ticket[$tipo]) ) ? number_format($ticket[$tipo][$mes_4]['ticket']) : 0}})
            </div>
        </div>
    @endforeach
@else
    <div class="col-4 border border-white">
        SIN INFORMACION EN FUENTE PAQUETIGOS
    </div>
@endif
