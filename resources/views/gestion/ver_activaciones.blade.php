<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">ACT. BRUTAS / GROSS</h5>
</div>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>
<div class="row tituloKPI text-center">
    ACTIVACIONES BRUTAS
</div>
@if($status['activaciones'])
    <div class="row text-center">
        <div class="col-3 datos">
            {{( array_key_exists($mes_1,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_1] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_2,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_2] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_3,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_3] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_4,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_4] : 0}}
        </div>
    </div>
@else
    <div class="col-4 border border-white">
        SIN INFORMACION EN FUENTE ACTIVACIONES
    </div>
@endif

<div class="row tituloKPI text-center">
    GROSS PREPAGO
</div>
@if($status['activaciones'])
    <div class="row text-center">
        <div class="col-3 datos">
            {{( array_key_exists($mes_1,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_1] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_2,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_2] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_3,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_3] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_4,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_4] : 0}}
        </div>
    </div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif

<div class="row tituloKPI text-center">
    GROSS POSPAGO
</div>
@if($status['activaciones'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$activaciones['gross_pos']) ) ? $activaciones['gross_pos'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$activaciones['gross_pos']) ) ? $activaciones['gross_pos'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$activaciones['gross_pos']) ) ? $activaciones['gross_pos'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$activaciones['gross_pos']) ) ? $activaciones['gross_pos'][$mes_4] : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif

<div class="row tituloKPI text-center">
    GROSS TOTAL
</div>
@if($status['activaciones'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$activaciones['gross_total']) ) ? $activaciones['gross_total'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$activaciones['gross_total']) ) ? $activaciones['gross_total'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$activaciones['gross_total']) ) ? $activaciones['gross_total'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$activaciones['gross_total']) ) ? $activaciones['gross_total'][$mes_4] : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif

<div class="row tituloKPI text-center">
<h6 class="center">La informacion de Portaciones y Migraciones ya esta contenida en los valores de Gross Pre y Pos</h6>    
</div>

<div class="row tituloKPI text-center">
    PORTACIONES (Pre y Pos)
</div>
@if($status['activaciones'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$activaciones['gross_porta']) ) ? $activaciones['gross_porta'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$activaciones['gross_porta']) ) ? $activaciones['gross_porta'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$activaciones['gross_porta']) ) ? $activaciones['gross_porta'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$activaciones['gross_porta']) ) ? $activaciones['gross_porta'][$mes_4] : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif


<div class="row tituloKPI text-center">
    MIGRACIONES
</div>
@if($status['activaciones'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$activaciones['gross_migra']) ) ? $activaciones['gross_migra'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$activaciones['gross_migra']) ) ? $activaciones['gross_migra'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$activaciones['gross_migra']) ) ? $activaciones['gross_migra'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$activaciones['gross_migra']) ) ? $activaciones['gross_migra'][$mes_4] : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
