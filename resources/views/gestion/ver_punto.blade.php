@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h6>{{$rpoint['idpdv']}}-{{ $rpoint['nombre_punto'] }}
			@if(!empty($PV_Fidel))
				<span class="bg-success text-white">PLAN FIDELIZACION</span>
			@endif
			</h6>
        </div>
		<div class="pull-right">
			<a class="btn btn-sm btn-primary" href="{{ route('gestion.buscar.punto') }}"> Regresar</a>
			<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Fecha_Cubos_Modal">
				Fechas Cubos
			</button>
		</div>
		@if($tiene_chequeos['status'])
		<div class="pull-right">
			<small><span class='bg-danger text-white'>
			<span><sub>{{ $tiene_chequeos['mensaje']}}</sub></span><br>
			<span><sub>{{ $tiene_chequeos['cc'].'-'.$tiene_chequeos['nombre']}}</sub></span><br>
			</span></small>
			<a class="btn btn-sm btn-danger" href="{{ route('encuestas.ver.coachings') }}">Cerrar</a>
		</div>
		@endif
    </div>
</div>

<small>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link text-black-50 gris" id="Visita-tab" data-toggle="tab" href="#Visita" role="tab" aria-controls="Visita" aria-selected="true"><small>Visitas</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 gris" id="Info-tab" data-toggle="tab" href="#Info" role="tab" aria-controls="Info" aria-selected="true"><small>Info</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 gris" id="Fotos-tab" data-toggle="tab" href="#Fotos" role="tab" aria-controls="Fotos" aria-selected="true"><small>Fotos</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 azultigo" id="Activaciones-tab" data-toggle="tab" href="#Activaciones" role="tab" aria-controls="Activaciones" aria-selected="false"><small>Activac.</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 azultigo" id="Calidad-tab" data-toggle="tab" href="#Calidad" role="tab" aria-controls="Calidad" aria-selected="false"><small>Calidad</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 azultigo" id="GrossPaquete-tab" data-toggle="tab" href="#GrossP" role="tab" aria-controls="GrossP" aria-selected="false"><small>Grs Paq.</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 azultigo" id="ComposicionGross-tab" data-toggle="tab" href="#ComposGross" role="tab" aria-controls="ComposGross" aria-selected="false"><small>Comp. Grs</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 azultigo" id="Revenue-tab" data-toggle="tab" href="#Revenue" role="tab" aria-controls="Revenue" aria-selected="false"><small>Revenue</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 azultigo" id="NuevaSeg-tab" data-toggle="tab" href="#NuevaSegmentacion" role="tab" aria-controls="NuevaSeg" aria-selected="false"><small>Segment.</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 amarillo" id="Recargas-tab" data-toggle="tab" href="#Recargas" role="tab" aria-controls="Recargas" aria-selected="false"><small>Recargas</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 amarillo" id="TicketRec-tab" data-toggle="tab" href="#TicketRec" role="tab" aria-controls="TicketRec" aria-selected="false"><small>Tick. Rec.</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 amarillo" id="RecPaquetes-tab" data-toggle="tab" href="#RecPaq" role="tab" aria-controls="RecPaq" aria-selected="false"><small>Paq. Foco</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 amarillo" id="TopPaquetes-tab" data-toggle="tab" href="#TopPaq" role="tab" aria-controls="TopPaq" aria-selected="false"><small>Top Paq.</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 gris" id="TradeVisibilidad-tab" data-toggle="tab" href="#TradeVisibilidad" role="tab" aria-controls="TradeVisibilidad" aria-selected="true"><small>Visibilidad</small></a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-black-50 gris" id="Encuestas-tab" data-toggle="tab" href="#Encuestas" role="tab" aria-controls="Encuestas" aria-selected="true"><small>Encuestas</small></a>
    </li>
</ul>
</small>

<small>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="Visita" role="tabpanel" aria-labelledby="Visita-tab">
        <p class="text-primary"><sub>Permite Iniciar Visitas y Realizar Marcaciones en el punto de venta</sub></p>
		@if(Auth::user()->employee->document != 1010101010)
			@include('visitas.ver_visitas')
		@endif
    </div>
    <div class="tab-pane fade show" id="Info" role="tabpanel" aria-labelledby="Info-tab">
        <p class="text-primary"><sub>Presenta informacion basica relacionada con el punto de venta</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_info')
		@endif
    </div>
    <div class="tab-pane fade show" id="Fotos" role="tabpanel" aria-labelledby="Fotos-tab">
        <p class="text-primary"><sub>Muestra Imagenes del Punto de Venta Capturadas por nuestros Funcionarios</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_fotos')
		@endif
    </div>
    <div class="tab-pane fade" id="Activaciones" role="tabpanel" aria-labelledby="Activaciones-tab">
        <p class="text-primary"><sub>Muestra Informacion de Activaciones y Gross realizados por el PV</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_activaciones')
		@endif
    </div>
    <div class="tab-pane fade" id="Calidad" role="tabpanel" aria-labelledby="Calidad-tab">
        <p class="text-primary"><sub>Activaciones recargadas el mismo dia y cuantas recargaron > $3K</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_calidad')
		@endif
    </div>
    <div class="tab-pane fade" id="GrossP" role="tabpanel" aria-labelledby="GrossPaquete-tab">
        <p class="text-primary"><sub>Gross Realizado con Paquetes Especiales</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_gross_paquetes')
		@endif
    </div>
    <div class="tab-pane fade" id="Revenue" role="tabpanel" aria-labelledby="Revenue-tab">
        <p class="text-primary"><sub>Presenta el revenue activaciones via Comisiones</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_revenue')
		@endif
    </div>
    <div class="tab-pane fade" id="NuevaSegmentacion" role="tabpanel" aria-labelledby="NuevaSeg-tab">
        <p class="text-primary"><sub>Segmentacion Por Gross (Nuevo)</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_nueva_segmentacion')
		@endif
    </div>
    <div class="tab-pane fade" id="ComposGross" role="tabpanel" aria-labelledby="ComposicionGross-tab">
        <p class="text-primary"><sub>Muestra Mix de Paqutes con que realizaron Gross</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_composicion')
		@endif
    </div>
    <div class="tab-pane fade" id="Recargas" role="tabpanel" aria-labelledby="Recargas-tab">
        <p class="text-primary"><sub>Composicion de la Recarga</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_recargas')
		@endif
    </div>
    <div class="tab-pane fade" id="TicketRec" role="tabpanel" aria-labelledby="TicketRec-tab">
        <p class="text-primary"><sub>Muestra cual es el Ticket Promedio de recarga por tipologia</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_ticket')
		@endif
    </div>
    <div class="tab-pane fade" id="RecPaq" role="tabpanel" aria-labelledby="RecPaquetes-tab">
        <p class="text-primary"><sub>Recarga Generada con paquetes espaciales</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_recarga_paquetigos')
		@endif
    </div>
    <div class="tab-pane fade" id="TopPaq" role="tabpanel" aria-labelledby="TopPaquetes-tab">
        <p class="text-primary"><sub>Composicion de la recarga via Paquetes</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('gestion.ver_paquetigos')
		@endif
    </div>
    <div class="tab-pane fade show" id="TradeVisibilidad" role="tabpanel" aria-labelledby="TradeVisibilidad-tab">
        <p class="text-primary"><sub>Campañas de Implementacion de Visibilidad en el punto de venta</sub></p>
        @if(true)
			@include('trade.ver_implementacion')
		@endif
    </div>
    <div class="tab-pane fade show" id="Encuestas" role="tabpanel" aria-labelledby="Encuestas-tab">
        <p class="text-primary"><sub>Permite la marcacion de encuestas en el desarrollo de la visita</sub></p>
        @if(Auth::user()->employee->document != 1010101010)
			@include('encuestas.ver_encuestas')
		@endif
    </div>
</div>
</small>
</div>

<!-- Modal -->
<div class="modal fade" id="Fecha_Cubos_Modal" tabindex="-1" role="dialog" aria-labelledby="Fecha_Cubos_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel">Fechas Actualizacion INFOPOS <b class="text-danger">{{$fecha_actualizacion_infopos}}</b></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if($error_actualizacion_infopos == 'S')<p class="text-danger">{{$obs_actualizacion_infopos}}</p>@endif
            <ul class="list-group">
            <li class="list-group-item">Corte Activaciones Brutas: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_act_pre'])) }}</strong></li>
                <li class="list-group-item">Corte Gross Prepago: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_gross_pre'])) }}</strong></li>
                <li class="list-group-item">Corte Gross Pospago: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_gross_pos'])) }}</strong></li>
                <li class="list-group-item">Corte Revenue Prepago: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_rev_pre'])) }}</strong></li>
                <li class="list-group-item">Corte Revenue Pospago: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_rev_pos'])) }}</strong></li>
                <li class="list-group-item">Corte Recarga Pura: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_recarga_pura'])) }}</strong></li>
                <li class="list-group-item">Corte Paquetigo: <strong>{{ date('Y-m-d',strtotime($fechas_cubos['fecha_paquetigo'])) }}</strong></li>
            </ul>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- FinModal -->
@endsection
