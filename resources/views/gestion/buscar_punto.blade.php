@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Buscar Punto Venta</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('home') }}"> Regresar</a>
    </div>

  </div>
  
  

  
</div>

<div class="row">
    @if ($message = Session::get('success'))
        @include('layouts.alert_success')
    @endif

    @if ($message = Session::get('error'))
        @include('layouts.alert_error')
    @endif
</div>

<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>BUSCAR IDPDV</strong></h6>
        <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
            @csrf

            <div class="row">
              <div class="col-xs-8 col-sm-8 col-md-8">
                  <input type="number" id="idpdv" name="idpdv" value="{{ old('idpdv') }}" class="form-control @error('idpdv') is-invalid @enderror" placeholder="Ingrese IDPDV">
                  @error('idpdv')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror

              </div>

              <div class="col-xs-3 col-sm-3 col-md-3">
                <button type="submit" class="btn btn-md btn-primary">Buscar</button>
              </div>
            </div>
          </form>
    </div>
  </div> <!-- FIN CARD buscar x idpdv -->
<br/>

@if(Auth::user()->employee->document != 1010101010)
<div class="card-body">
    <h6 class="card-title"><strong>BUSCAR PUNTOS RUTA DEL DIA</strong></h6>
    <form class="form-inline" action="{{ route('gestion.puntos.ruta.dia') }}" method="POST">
        @csrf

        <div class="row">
          <div class="col-xs-8 col-sm-8 col-md-8">
            <select name="dia_semana" id="dia_semana" class="form-control @error('dia_semana') is-invalid @enderror">
                <option value=X selected>HOY {{ $dias_semana[$dia_hoy] }}</option>
                @foreach ($dias_semana as $id => $dia_sem)
                    <option value="{{ $id }}">{{ $dia_sem }}</option>
                @endforeach
            </select>
            <input type="hidden" id="dia_hoy" name="dia_hoy" value="{{$dia_hoy}}">
            @error('dia_semana')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <div class="col-xs-3 col-sm-3 col-md-3">
            <button type="submit" class="btn btn-md btn-primary">Buscar</button>
          </div>
        </div>
      </form>
</div> <!-- FIN CARD buscar puntos de la ruta del dia -->
@endif
<br/>

@if(Auth::user()->employee->document != 1010101010)
<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>BUSCAR NOMBRE PV</strong></h6>
        <form class="form-inline" action="{{ route('gestion.traer.nombre.puntos') }}" method="POST">
            @csrf

            <div class="row">
                <div class="col-xs-8 col-sm-8 col-md-8">
                    <input type="text" id="nombre" name="nombre" value="{{ old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror" placeholder="Ingrese Nombre">
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <button type="submit" class="btn btn-md btn-primary">
                        Buscar
                    </button>
                </div>
            </div>
        </form>
    </div>  <!-- FIN CARD BODY -->
</div> <!-- FIN CARD buscar x Nombre -->
@endif
<br/>


@if(Auth::user()->employee->document != 1010101010)
<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>BUSCAR COD POS/SUB</strong></h6>
        <form class="form-inline" action="{{ route('gestion.traer.pos.code') }}" method="POST">
            @csrf

            <div class="row">
                <div class="col-xs-8 col-sm-8 col-md-8">
                    <input type="text" id="poscode" name="poscode" value="{{ old('poscode') }}" class="form-control @error('poscode') is-invalid @enderror" placeholder="Ingrese Pos/Sub Code">
                    @error('poscode')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <button type="submit" class="btn btn-md btn-primary">
                        Buscar
                    </button>
                </div>
            </div>
        </form>
    </div>  <!-- FIN CARD BODY -->
</div> <!-- FIN CARD buscar x CodigoPos o Sub -->
@endif
<br/>



@if(Auth::user()->employee->document != 1010101010)
<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>BUSCAR CC PROPIETARIO</strong></h6>
        <form class="form-inline" action="{{ route('gestion.traer.cedula.propietario') }}" method="POST">
            @csrf

            <div class="row">
                <div class="col-xs-8 col-sm-8 col-md-8">
                    <input type="text" id="cc_owner" name="cc_owner" value="{{ old('cc_owner') }}" class="form-control @error('cc_owner') is-invalid @enderror" placeholder="Ingrese Documento Propietario">
                    @error('cc_owner')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <button type="submit" class="btn btn-md btn-primary">
                        Buscar
                    </button>
                </div>
            </div>
        </form>
    </div>  <!-- FIN CARD BODY -->
</div> <!-- FIN CARD buscar x Nombre -->
@endif
<br/>

@if(Auth::user()->employee->document != 1010101010)
<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>BUSCAR X CIRCUITO/ESTADO</strong></h6>
        <form class="form-inline" action="{{ route('gestion.traer.circuito.estado') }}" method="POST">
            @csrf

            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <input type="text" id="circuito" name="circuito" value="{{ old('circuito') }}" class="form-control @error('circuito') is-invalid @enderror" placeholder="Ingrese Circuito a Buscar">
                    @error('circuito')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-4 offset-md-1 col-sm-4 offset-sm-1">
                    <select name="estado" id="estado" class="form-control @error('estado') is-invalid @enderror">
                        <option value='X' selected>Estados DMS</option>
                        @foreach ($estados_dms as $estado)
                            <option value="{{$estado['estado_dms']}}">{{$estado['estado_dms']}}</option>
                        @endforeach
                    </select>
                    @error('estado')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-2 offset-md-1 col-sm-2 offset-sm-1">
                    <button type="submit" class="btn btn-md btn-primary">
                        Buscar
                    </button>
                </div>
            </div>
        </form>
    </div>  <!-- FIN CARD BODY -->
</div> <!-- FIN CARD buscar x Circuito -->
@endif
</div>
 
@include('scripts.gestion_dealerstools.autocompletar_circuitos')


@endsection
