<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">CALIDAD DEL GROSS</h5>
</div>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>

<div class="row tituloKPI text-center">
    GROSS PRE. - GROSS PAQ. AUTO. - %GROSS AUTO.
</div>
@if($status['activaciones'])
    <div class="row text-center">
        <div class="col-3 datos">
            {{( array_key_exists($mes_1,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_1] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_2,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_2] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_3,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_3] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_4,$activaciones['gross_pre']) ) ? $activaciones['gross_pre'][$mes_4] : 0}}
        </div>
    </div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
@if($status['gross_paquetes'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['gross_auto']) ) ? $gross_paquetes['gross_auto'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['gross_auto']) ) ? $gross_paquetes['gross_auto'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['gross_auto']) ) ? $gross_paquetes['gross_auto'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['gross_auto']) ) ? $gross_paquetes['gross_auto'][$mes_4] : 0}}
    </div>
</div>
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['porc_gross_auto']) ) ? $gross_paquetes['porc_gross_auto'][$mes_1] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['porc_gross_auto']) ) ? $gross_paquetes['porc_gross_auto'][$mes_2] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['porc_gross_auto']) ) ? $gross_paquetes['porc_gross_auto'][$mes_3] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['porc_gross_auto']) ) ? $gross_paquetes['porc_gross_auto'][$mes_4] : 0}}%
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
<div class="row tituloKPI text-center">
    ACT. BRUTAS - ACT.C/RECARGA MISMO DIA - %ACT.C/REC.MISMO DIA
</div>
@if($status['activaciones'])
    <div class="row text-center">
        <div class="col-3 datos">
            {{( array_key_exists($mes_1,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_1] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_2,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_2] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_3,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_3] : 0}}
        </div>
        <div class="col-3 datos">
            {{( array_key_exists($mes_4,$activaciones['brutas']) ) ? $activaciones['brutas'][$mes_4] : 0}}
        </div>
    </div>
@else
    <div class="col-4 border border-white">
        SIN INFORMACION EN FUENTE ACTIVACIONES
    </div>
@endif

@if($status['gross_paquetes'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_4] : 0}}
    </div>
</div>
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['porc_rec_mismo_dia']) ) ? $gross_paquetes['porc_rec_mismo_dia'][$mes_1] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['porc_rec_mismo_dia']) ) ? $gross_paquetes['porc_rec_mismo_dia'][$mes_2] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['porc_rec_mismo_dia']) ) ? $gross_paquetes['porc_rec_mismo_dia'][$mes_3] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['porc_rec_mismo_dia']) ) ? $gross_paquetes['porc_rec_mismo_dia'][$mes_4] : 0}}%
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif

<div class="row tituloKPI text-center">
    ACT.C/REC.MISMO DIA - ACT.C/REC.MISMO DIA > $3K - %ACT.C/REC>$3K
</div>


@if($status['activaciones'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['rec_mismo_dia']) ) ? $gross_paquetes['rec_mismo_dia'][$mes_4] : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif


@if($status['gross_paquetes'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['rec_mayor_3k']) ) ? $gross_paquetes['rec_mayor_3k'][$mes_1] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['rec_mayor_3k']) ) ? $gross_paquetes['rec_mayor_3k'][$mes_2] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['rec_mayor_3k']) ) ? $gross_paquetes['rec_mayor_3k'][$mes_3] : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['rec_mayor_3k']) ) ? $gross_paquetes['rec_mayor_3k'][$mes_4] : 0}}
    </div>
</div>
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$gross_paquetes['porc_rec_mayor_3k']) ) ? $gross_paquetes['porc_rec_mayor_3k'][$mes_1] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$gross_paquetes['porc_rec_mayor_3k']) ) ? $gross_paquetes['porc_rec_mayor_3k'][$mes_2] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$gross_paquetes['porc_rec_mayor_3k']) ) ? $gross_paquetes['porc_rec_mayor_3k'][$mes_3] : 0}}%
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$gross_paquetes['porc_rec_mayor_3k']) ) ? $gross_paquetes['porc_rec_mayor_3k'][$mes_4] : 0}}%
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ACTIVACIONES
</div>
@endif
