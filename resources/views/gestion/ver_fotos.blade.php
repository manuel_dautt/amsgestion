<div class="row text-center bg-tigo bg-light border border-white">
    <h5 class="center tituloPrincipal">FOTOS DEL PUNTO DE VENTA </h5>
    <h6><small>{{$rpoint['idpdv']}}</small></h6>
</div>

imagen:
{{( array_key_exists(1,$imagen_pv) ) ? $imagen_pv[1]['nombre'] : 'Sin Imagen Cargada'}}
<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Imagen1_Modal">
    Ver Imagen
</button>
<form action="{{ route('gestion.cargar.foto') }}" method="POST" enctype="multipart/form-data">
  @csrf

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Foto Fachada:</strong>
          <input type="file" accept="image/jpeg,image/png,image/jpg" name="imagen" class="form-control" placeholder="Imagen..." required>

          <input type="hidden" name="idpdv" value="{{$rpoint['idpdv']}}">
          <input type="hidden" name="prefijo" value="foto_pv">
          <input type="hidden" name="numero" value="1">

          <button type="submit" class="btn btn-sm btn-primary">Crear</button>
        </div>
    </div>
</form>
<hr>
{{( array_key_exists(2,$imagen_pv) ) ? $imagen_pv[2]['nombre'] : 'Sin Imagen Cargada'}}
<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Imagen2_Modal">
    Ver Imagen
</button>
<form action="{{ route('gestion.cargar.foto') }}" method="POST" enctype="multipart/form-data">
    @csrf

      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <strong>Foto Interior:</strong>
            <input type="file" accept="image/jpeg,image/png,image/jpg" name="imagen" class="form-control" placeholder="Imagen..." required>

            <input type="hidden" name="idpdv" value="{{$rpoint['idpdv']}}">
            <input type="hidden" name="prefijo" value="foto_pv">
            <input type="hidden" name="numero" value="2">

            <button type="submit" class="btn btn-sm btn-primary">Crear</button>
          </div>
      </div>
  </form>


<!-- Modal1 -->
<div class="modal fade" id="Imagen1_Modal" tabindex="-1" role="dialog" aria-labelledby="Imagen1_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel">Imagen Fachada</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if(( array_key_exists(1,$imagen_pv)) )
            <img src="{{$imagen_pv[1]['url']}}" class="img-fluid" alt="Imagen1"/>
            @else
                Sin Imagen Cargada
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- FinModal1 -->

<!-- Modal2 -->
<div class="modal fade" id="Imagen2_Modal" tabindex="-1" role="dialog" aria-labelledby="Imagen2_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel2">Imagen Interior</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if(( array_key_exists(2,$imagen_pv)) )
            <img src="{{$imagen_pv[2]['url']}}" class="img-fluid" alt="Imagen1"/>
            @else
                Sin Imagen Cargada
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- FinModal2 -->
