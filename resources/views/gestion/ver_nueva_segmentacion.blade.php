<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">NUEVA SEGMENTACION X GROSS</h5>
</div>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>
<div class="row tituloKPI text-center">
    ARPU DEL GROSS SUCURSAL
</div>
<p class="text-center"><small>Para meses cerrados se extrae de comisiones, para el mes vigente es el promedio del ultimo trimestre: JUL = promedio(ABR, MAY, JUN)</small></p>
@if($status['rev'] && array_key_exists('arpu_nueva_segmentacion',$rev))
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$rev['arpu_nueva_segmentacion']) ) ? number_format($rev['arpu_nueva_segmentacion'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$rev['arpu_nueva_segmentacion']) ) ? number_format($rev['arpu_nueva_segmentacion'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$rev['arpu_nueva_segmentacion']) ) ? number_format($rev['arpu_nueva_segmentacion'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$rev['arpu_nueva_segmentacion']) ) ? number_format($rev['arpu_nueva_segmentacion'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-12 border border-white text-center">
    SIN INFORMACION DE ARPU EN FUENTE REVENUE/GROSS
</div>
@endif

<div class="row tituloKPI text-center">
    ARPU GROSS REAL PUNTO DE VENTA
</div>
<p class="text-center"><small>Para meses cerrados se extrae de comisiones, para el mes vigente es el promedio del ultimo trimestre: JUL = promedio(ABR, MAY, JUN)</small></p>
<div class="row text-center">
    <div class="col-3 datos">
        {{ number_format($arpu_pv['mes_1']) }}
    </div>
    <div class="col-3 datos">
        {{ number_format($arpu_pv['mes_2']) }}
    </div>
    <div class="col-3 datos">
        {{ number_format($arpu_pv['mes_3']) }}
    </div>
    <div class="col-3 datos">
        {{ number_format($arpu_pv['mes_actual']) }}
    </div>
</div>

<div class="row tituloKPI text-center">
    NUEVA SEGMENTACION X GROSS<span class="badge badge-secondary">New</span>
    <a href="#" type="button" data-toggle="modal" data-target="#gross_peid_modal">
        <i class="fas fa-question-circle"></i>
    </a>
</div>

@if($status['rev'] && array_key_exists('nueva_seg_gross',$rev) && array_key_exists('rank_segmentacion',$rev))
<div class="row text-center">
    <div class='col-3 datos {{( array_key_exists($mes_1,$rev['rank_segmentacion']) ) ? $rev['rank_segmentacion'][$mes_1] : '-'}}''>
        {{( array_key_exists($mes_1,$rev['nueva_seg_gross']) ) ? $rev['nueva_seg_gross'][$mes_1] : '-'}}<i class='{{( array_key_exists($mes_1,$rev['rank_segmentacion']) ) ? $rev['icono_segmentacion'][$mes_1] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_2,$rev['rank_segmentacion']) ) ? $rev['rank_segmentacion'][$mes_2] : '-'}}''>
        {{( array_key_exists($mes_2,$rev['nueva_seg_gross']) ) ? $rev['nueva_seg_gross'][$mes_2] : '-'}}<i class='{{( array_key_exists($mes_2,$rev['rank_segmentacion']) ) ? $rev['icono_segmentacion'][$mes_2] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_3,$rev['rank_segmentacion']) ) ? $rev['rank_segmentacion'][$mes_3] : '-'}}''>
        {{( array_key_exists($mes_3,$rev['nueva_seg_gross']) ) ? $rev['nueva_seg_gross'][$mes_3] : '-'}}<i class='{{( array_key_exists($mes_3,$rev['rank_segmentacion']) ) ? $rev['icono_segmentacion'][$mes_3] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_4,$rev['rank_segmentacion']) ) ? $rev['rank_segmentacion'][$mes_4] : '-'}}''>
        {{( array_key_exists($mes_4,$rev['nueva_seg_gross']) ) ? $rev['nueva_seg_gross'][$mes_4] : '-'}}<i class='{{( array_key_exists($mes_4,$rev['rank_segmentacion']) ) ? $rev['icono_segmentacion'][$mes_4] : '-'}}'></i>
    </div>
</div>
@else
<div class="col-12 border border-white text-center">
    SIN INFORMACION EN FUENTE REVENUE/GROSS
</div>
@endif

<div class="row tituloKPI text-center">
    EVOLUCION COMERCIAL<span class="badge badge-secondary">New</span>
    <a href="#" type="button" data-toggle="modal" data-target="#evolucion_cial_modal">
        <i class="fas fa-question-circle"></i>
    </a>
</div>
@if($status['rev'] && array_key_exists('evolucion_cial',$rev) && array_key_exists('rank_segmentacion',$rev))
<div class="row text-center">
    <div class='col-3 datos {{( array_key_exists($mes_1,$rev['color_evolucion']) ) ? $rev['color_evolucion'][$mes_1] : '-'}}''>
        {{( array_key_exists($mes_1,$rev['evolucion_cial']) ) ? $rev['evolucion_cial'][$mes_1] : '-'}}
        <i class='{{( array_key_exists($mes_1,$rev['rank_segmentacion']) ) ? $rev['icono_evolucion'][$mes_1] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_2,$rev['color_evolucion']) ) ? $rev['color_evolucion'][$mes_2] : '-'}}''>
        {{( array_key_exists($mes_2,$rev['evolucion_cial']) ) ? $rev['evolucion_cial'][$mes_2] : '-'}}
        <i class='{{( array_key_exists($mes_2,$rev['rank_segmentacion']) ) ? $rev['icono_evolucion'][$mes_2] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_3,$rev['color_evolucion']) ) ? $rev['color_evolucion'][$mes_3] : '-'}}''>
        {{( array_key_exists($mes_3,$rev['evolucion_cial']) ) ? $rev['evolucion_cial'][$mes_3] : '-'}}
        <i class='{{( array_key_exists($mes_3,$rev['rank_segmentacion']) ) ? $rev['icono_evolucion'][$mes_3] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_4,$rev['color_evolucion']) ) ? $rev['color_evolucion'][$mes_4] : '-'}}''>
        {{( array_key_exists($mes_4,$rev['evolucion_cial']) ) ? $rev['evolucion_cial'][$mes_4] : '-'}}
        <i class='{{( array_key_exists($mes_4,$rev['rank_segmentacion']) ) ? $rev['icono_evolucion'][$mes_4] : '-'}}'></i>
    </div>
</div>
@else
<div class="col-12 border border-white text-center">
    SIN INFORMACION DE EVOLUCION CIAL. EN FUENTE REVENUE/GROSS
</div>
@endif

<div class="row tituloKPI text-center">
    SEG X REVENUE (COMIS.)
    <a href="#" type="button" data-toggle="modal" data-target="#seg_revenue_modal">
        <i class="fas fa-question-circle"></i>
    </a>
</div>
@if($status['rev'] && array_key_exists('seg_x_rev_old',$rev) && array_key_exists('rank_segmentacion_old',$rev))

<div class="row text-center">
    <div class='col-3 datos {{( array_key_exists($mes_1,$rev['rank_segmentacion_old']) ) ? $rev['rank_segmentacion_old'][$mes_1] : '-'}}''>
        {{( array_key_exists($mes_1,$rev['seg_x_rev_old']) ) ? $rev['seg_x_rev_old'][$mes_1] : '-'}}<i class='{{( array_key_exists($mes_1,$rev['rank_segmentacion_old']) ) ? $rev['icono_segmentacion_old'][$mes_1] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_2,$rev['rank_segmentacion_old']) ) ? $rev['rank_segmentacion_old'][$mes_2] : '-'}}''>
        {{( array_key_exists($mes_2,$rev['seg_x_rev_old']) ) ? $rev['seg_x_rev_old'][$mes_2] : '-'}}<i class='{{( array_key_exists($mes_2,$rev['rank_segmentacion_old']) ) ? $rev['icono_segmentacion_old'][$mes_2] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_3,$rev['rank_segmentacion_old']) ) ? $rev['rank_segmentacion_old'][$mes_3] : '-'}}''>
        {{( array_key_exists($mes_3,$rev['seg_x_rev_old']) ) ? $rev['seg_x_rev_old'][$mes_3] : '-'}}<i class='{{( array_key_exists($mes_3,$rev['rank_segmentacion_old']) ) ? $rev['icono_segmentacion_old'][$mes_3] : '-'}}'></i>
    </div>
    <div class='col-3 datos {{( array_key_exists($mes_4,$rev['rank_segmentacion_old']) ) ? $rev['rank_segmentacion_old'][$mes_4] : '-'}}''>
        {{( array_key_exists($mes_4,$rev['seg_x_rev_old']) ) ? $rev['seg_x_rev_old'][$mes_4] : '-'}}<i class='{{( array_key_exists($mes_4,$rev['rank_segmentacion_old']) ) ? $rev['icono_segmentacion_old'][$mes_4] : '-'}}'></i>
    </div>
</div>
@else
<div class="col-12 border border-white text-center">
    SIN INFORMACION EN FUENTE REVENUE
</div>
@endif


<!-- Modal 1 -->
<div class="modal fade" id="seg_revenue_modal" tabindex="-1" role="dialog" aria-labelledby="seg_revenue_modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Segmentacion X Revenue</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive-sm">
                <table class="table table-striped table-hover table-sm">
                    <caption>La fuente de calculo es el Revenue de Comisiones</caption>
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Segmentacion</th>
                        <th scope="col">Revenue Min.</th>
                        <th scope="col">Revenue Max.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Principiantes</td>
                            <td>$ 1</td>
                            <td>$ 50.000</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>En Desarrollo</td>
                            <td>$ 50.001</td>
                            <td>$ 200.000</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Desarrollados</td>
                            <td>$ 200.001</td>
                            <td>$ 400.000</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Productivos</td>
                            <td>$ 400.001</td>
                            <td>$ En Adelante</td>
                        </tr>
                    </tbody>
                </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal 2 -->
<div class="modal fade" id="gross_peid_modal" tabindex="-1" role="dialog" aria-labelledby="gross_peid_modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Segmentacion X Gross</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive-sm">
                <table class="table table-striped table-hover table-sm">
                    <caption>El Calculo del Segmento se realiza via Gross y los rangos se ajustan por regional</caption>
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Segmentacion</th>
                        <th scope="col">Gross Min.</th>
                        <th scope="col">Gross Max.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($gross_peid as $peid)
                        <tr>
                            <th scope="row">{{ $peid['orden'] }}</th>
                            <td>{{ $peid['segmentacion'] }}</td>
                            <td>{{ $peid['min'] }}</td>
                            <td>{{ $peid['max'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal 3 -->
<div class="modal fade" id="evolucion_cial_modal" tabindex="-1" role="dialog" aria-labelledby="evolucion_cial_modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Evolucion Comercial PV</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive-sm">
                <table class="table table-striped table-hover table-sm">
                    <caption>Muestra la madurez comercial del punto de venta</caption>
                    <thead>
                        <tr>
                        <th scope="col">NIVEL</th>
                        <th scope="col">NOMBRE</th>
                        <th scope="col">KPI</th>
                        <th scope="col">VALOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">NIVEL_6</th>
                            <td>Pospago</td>
                            <td>GrossPos/GrossPre</td>
                            <td>> 2%</td>
                        </tr>
                        <tr>
                            <th scope="row">NIVEL_5</th>
                            <td>Portacion</td>
                            <td>GrossPorta/GrossPre</td>
                            <td>> 5%</td>
                        </tr>
                        <tr>
                            <th scope="row">NIVEL_4</th>
                            <td>Gross Paq.Foco</td>
                            <td>GrossPqFoco/GrossPre</td>
                            <td>> 20%</td>
                        </tr>
                        <tr>
                            <th scope="row">NIVEL_3</th>
                            <td>Gross Paq.</td>
                            <td>GrossAuto./GrossPre</td>
                            <td>> 47%</td>
                        </tr>
                        <tr>
                            <th scope="row">NIVEL_2</th>
                            <td>Activ.Gross</td>
                            <td>Gross/Act.Brutas</td>
                            <td>> 65%</td>
                        </tr>
                        <tr>
                            <th scope="row">NIVEL_1</th>
                            <td>Vende Simcard</td>
                            <td>Cualquier Activ.</td>
                            <td>> 0</td>
                        </tr>
                    </tbody>
                </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
