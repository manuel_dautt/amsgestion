@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Acuerdo de Confidencialidad</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-sm btn-primary" href="{{ route('home') }}"> Regresar</a>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>Mensaje de Términos y Condiciones INFOPOS:</strong></h6>

        <form class="form-inline" action="{{ route('gestion.guardar.confidencialidad') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>INFOPOS es una herramienta desarrollada para realizar seguimiento comercial a los canales de venta de TIGO. Si  tienes acceso a ella,  es para el adecuado funcionamiento/seguimiento del canalcon el que interactúas. Ten presente:</p>
                    <ol>
                        <li>El acceso a la herramienta está sólo permitido para los usuarios autorizados según el proceso definido</li>
                        <li>El usuario y contraseña entregado es personal e intransferible.</li>
                        <li>Está prohibido compartir el usuario y contraseña con otras personas, si consideras que deben ingresar, infórmales como solicitan acceso.</li>
                        <li>La información aquí descrita es de carácter confidencial, al ingresar estas aceptando cuidar la información de acuerdo a las políticas definidas</li>
                    </ol>
                    <p>¡¡Cuidar la información es compromiso de todos!!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5">
                    <select id="accept" name="accept" class="form-control">
                        <option value="S" checked>SI ACEPTO</option>
                        <option value="N">NO ACEPTO</option>
                    </select>

                </div>

                <div class="col-xs-3 col-sm-3 col-md-3">
                    <button type="submit" class="btn btn-md btn-primary">Enviar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
