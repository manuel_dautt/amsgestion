@extends('layouts.app')

@section('content')

<div class="card-body">
    <h6 class="card-title"><strong>INGRESO FOTO PARA PUNTOS DE VENTA</strong></h6>
    <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
        @csrf
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <h4>CARGUE FOTO</h4>
              <h6>{{$titulo}}</h6>
              <h6>{{$mensaje}}</h6>
              <input type="hidden" id="idpdv" name="idpdv" value="{{ $idpdv }}">
              <button type="submit" class="btn btn-md btn-primary">Volver a la Gestion del Punto Nuevamente</button>
          </div>
        </div>
      </form>
</div>

@endsection
