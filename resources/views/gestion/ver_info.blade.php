<div class="row text-center bg-tigo bg-light border border-white">
    <h5 class="center tituloPrincipal">INFO. Y ATENCION</h5>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>Regional</sup><br/>
        {{$rpoint['regional']}}<BR/>
        <sup>Dpto.</sup><br/>
        {{$rpoint['departamento']}}<BR/>
        <sup>Ciudad</sup><br/>
        {{$rpoint['ciudad']}}
    </div>
    <div class="col-6 border border-white">
        <sup>Circuito</sup><br/>
        {{$rpoint['circuito']}}<BR/>
        <sup>Dir.</sup><br/>
        {{$rpoint['direccion']}}<BR/>
        <sup>Barrio</sup><br/>
        {{$rpoint['barrio']}}
    </div>
</div>


<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>cc Propiet.</sup><br/>
        {{$rpoint['ced_propietario']}}
    </div>
    <div class="col-6 border border-white">
        <sup>Nom. Propiet.</sup><br/>
        {{$rpoint['nom_propietario']}}
    </div>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>Movil Contacto</sup><br/>
        {{$rpoint['tel_propietario']}}<br/>
		{{$rpoint['tel2_propietario']}}
    </div>
    <div class="col-6 border border-white">
        <sup>E-Mail.</sup><br/>
        {{$pv_correo}}
		<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Email_Modal">
			Crear/Editar
		</button>	
    </div>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>Dependientes</sup><br/>
        Listado de Dependientes
    </div>
    <div class="col-6 border border-white">
        <sup>Ver Dependientes</sup><br/>
        <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Dependientes_Modal">
			Ver Dependientes
		</button>
    </div>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>Cat. PV DMS</sup><br/>
        {{$rpoint['cat_dms']}}
    </div>
    <div class="col-6 border border-white">
        <sup>Cat. HOMOLOGADA</sup><br/>
        {{$rpoint['categoria_homologada']}}
    </div>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>Estado DMS.</sup><br/>
        {{$rpoint['estado_dms']}}<br>
        <sup>Fech. Creacion</sup><br/>
        {{$rpoint['fecha_creacion']}}
    </div>
    <div class="col-6 border border-white">
        <sup>Cod. POS</sup><br/>
        {{$rpoint['pos_padre']}}<br>
        <sup>Cod. SUB</sup><br/>
        {{$rpoint['pos_code']}}
    </div>
</div>
<div class="row bg-light border border-white">
    <sup>Frecuencia y dias de Visita</sup>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        Frecuencia de Visita: <strong>{{$frec_visita}}</strong>
    </div>
    <div class="col-6 border border-white">
        Dias: <strong>{{$dias_visita}}</strong>
    </div>
</div>
<div class="row bg-light border border-white">
    <sup>Servicios Activos</sup>
</div>
<div class="row bg-light border border-white">
    <div class="col-3 border border-white">
        SIM:{{$rpoint['serv_simcard']}}
    </div>
    <div class="col-3 border border-white">
        MBOX:{{$rpoint['serv_mbox']}}
    </div>
    <div class="col-3 border border-white">
        EPIN:{{$rpoint['serv_epin']}}
    </div>
    <div class="col-3 border border-white">
        GEST:{{$rpoint['serv_gestor']}}
    </div>
</div>
<div class="row bg-light border border-white">
    <div class="col-6 border border-white">
        <sup>Clasif. por Tipo PV</sup><br/>
        {{$rpoint['ultima_tipologia']}}
    </div>
    <div class="col-6 border border-white">
        <sup>Tipo de Punto</sup><br/>
        {{$rpoint['tipo_punto']}}
    </div>
</div>
<div class="row bg-light border border-white">
    <h6 class="center tituloPrincipal">ESTRUCTURA CIAL.</h6>
    <p><small>Presenta la estructura comercial para el punto de venta, iniciando con la persona que activa, si esta no existe, es decir en el punto de venta no se ha asociado un activador, se asume que el punto no realiza activaciones por lo tanto, la estructura comercial no aplica</small></p>
</div>
@if($status['ec'])
<div class="row bg-light border border-white">
    @foreach ($ec as $item)
    <div class="col border text-wrap border-white nombre">
        {{$item['cargo']}}-
        {{$item['nom_asesor']}}
    </div>
    @endforeach
</div>
<div class="row bg-light border border-white">
    <div class="col-12 border text-wrap border-white nombre">
        EJECUTIVO-
        {{$item['nom_ejecutivo']}}
    </div>
    <div class="col-12 border text-wrap border-white nombre">
        SUPERVISOR-
        {{$item['nom_supervisor']}}
    </div>
    <div class="col-12 border text-wrap border-white nombre">
        ENTRENADOR-
        {{$item['nom_entrenador']}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE ESTR.CIAL.
</div>
@endif

<!-- Modal1 -->
<div class="modal fade" id="Email_Modal" tabindex="-1" role="dialog" aria-labelledby="Email_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel2">Correo Electronico</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
      <form class="form-inline" action="{{ route('gestion.correo.pv') }}" method="POST">
        @csrf
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <h4>CORREO ELECTRONICO</h4>
			<input type="email" name="correo" value="{{$pv_correo}}">
			<input type="hidden" name="anterior" value="{{$pv_correo}}">
            <input type="hidden" name="idpdv" value="{{$rpoint['idpdv']}}">
			<button type="submit" class="btn btn-sm btn-primary">Crear/Cargar</button>
          </div>
        </div>
      </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- FinModal1 -->

<!-- Modal2 -->
<div class="modal fade" id="Dependientes_Modal" tabindex="-1" role="dialog" aria-labelledby="Dependientes_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel2">Lista Dependientes</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
			<H2>En Construccion</H2>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- FinModal2 -->