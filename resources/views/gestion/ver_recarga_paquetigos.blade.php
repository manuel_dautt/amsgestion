<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">P A Q U E T E S</h5>
</div>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>
<div class="row tituloKPI text-center">
    PAQUETES FOCO
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['rec_pq_foco']) ) ? number_format($recargas['rec_pq_foco'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['rec_pq_foco']) ) ? number_format($recargas['rec_pq_foco'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['rec_pq_foco']) ) ? number_format($recargas['rec_pq_foco'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['rec_pq_foco']) ) ? number_format($recargas['rec_pq_foco'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif

<div class="row tituloKPI text-center">
    PAQUETE DE $6K
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['rec_pq_6k']) ) ? number_format($recargas['rec_pq_6k'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['rec_pq_6k']) ) ? number_format($recargas['rec_pq_6k'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['rec_pq_6k']) ) ? number_format($recargas['rec_pq_6k'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['rec_pq_6k']) ) ? number_format($recargas['rec_pq_6k'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif

<div class="row tituloKPI text-center">
    PAQUETES TODO CONTIGO.
</div>

@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['rec_pq_todo']) ) ? number_format($recargas['rec_pq_todo'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['rec_pq_todo']) ) ? number_format($recargas['rec_pq_todo'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['rec_pq_todo']) ) ? number_format($recargas['rec_pq_todo'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['rec_pq_todo']) ) ? number_format($recargas['rec_pq_todo'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif

<div class="row tituloKPI text-center">
    VENTA > $200K PAQ. FOCO
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['pdv_rec_200k_pq_foco']) ) ? ($recargas['pdv_rec_200k_pq_foco'][$mes_1] > 0) ? 'SI' : 'NO' : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['pdv_rec_200k_pq_foco']) ) ? ($recargas['pdv_rec_200k_pq_foco'][$mes_2] > 0) ? 'SI' : 'NO' : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['pdv_rec_200k_pq_foco']) ) ? ($recargas['pdv_rec_200k_pq_foco'][$mes_3] > 0) ? 'SI' : 'NO' : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['pdv_rec_200k_pq_foco']) ) ? ($recargas['pdv_rec_200k_pq_foco'][$mes_4] > 0) ? 'SI' : 'NO' : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif
