@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Aceptacion de Terminos</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-sm btn-primary" href="{{ route('gestion.buscar.punto') }}"> Regresar</a>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>Gracias!!!</strong></h6>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>Ud ha aceptado los terminos y condiciones de confidencialidad para el uso de INFOPOS, este mensaje no volvera a aparecer cuando entre a esta opcion</p>
                    <p>¡¡Cuidar la información es compromiso de todos!!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <a class="btn btn-sm btn-primary" href="{{ route('gestion.buscar.punto') }}"> Regresar</a>
                </div>
            </div>

    </div>
</div>
@endsection
