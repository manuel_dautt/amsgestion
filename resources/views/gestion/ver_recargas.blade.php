<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">RECARGAS Y PAQUETES</h5>
</div>
<div class="row contenedorTitulo text-center">
    <div class="col-3 tituloMes text-center">
        {{$mes_1}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_2}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_3}}
    </div>
    <div class="col-3 tituloMes text-center">
        {{$mes_4}}
    </div>
</div>
<div class="row tituloKPI text-center">
    RECARGA PURA
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['recarga_pura']) ) ? number_format($recargas['recarga_pura'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['recarga_pura']) ) ? number_format($recargas['recarga_pura'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['recarga_pura']) ) ? number_format($recargas['recarga_pura'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['recarga_pura']) ) ? number_format($recargas['recarga_pura'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif

<div class="row tituloKPI text-center">
    PAQUETIGOS
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['paquetigo']) ) ? number_format($recargas['paquetigo'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['paquetigo']) ) ? number_format($recargas['paquetigo'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['paquetigo']) ) ? number_format($recargas['paquetigo'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['paquetigo']) ) ? number_format($recargas['paquetigo'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif
<div class="row tituloKPI text-center">
    PRECARGADOS
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['precargados']) ) ? number_format($recargas['precargados'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['precargados']) ) ? number_format($recargas['precargados'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['precargados']) ) ? number_format($recargas['precargados'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['precargados']) ) ? number_format($recargas['precargados'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif

<div class="row tituloKPI text-center">
    BOLSAS
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['bolsas']) ) ? number_format($recargas['bolsas'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['bolsas']) ) ? number_format($recargas['bolsas'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['bolsas']) ) ? number_format($recargas['bolsas'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['bolsas']) ) ? number_format($recargas['bolsas'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif

<div class="row tituloKPI text-center">
    RECARGA TOTAL
</div>
@if($status['recargas'])
<div class="row text-center">
    <div class="col-3 datos">
        {{( array_key_exists($mes_1,$recargas['total']) ) ? number_format($recargas['total'][$mes_1]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_2,$recargas['total']) ) ? number_format($recargas['total'][$mes_2]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_3,$recargas['total']) ) ? number_format($recargas['total'][$mes_3]) : 0}}
    </div>
    <div class="col-3 datos">
        {{( array_key_exists($mes_4,$recargas['total']) ) ? number_format($recargas['total'][$mes_4]) : 0}}
    </div>
</div>
@else
<div class="col-4 border border-white">
    SIN INFORMACION EN FUENTE RECARGAS
</div>
@endif
