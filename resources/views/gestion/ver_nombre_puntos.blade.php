@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h6>PUNTOS RESULTANTES</h6>
        </div>
        <div class="pull-right">
            <a class="btn btn-sm btn-primary" href="{{ route('gestion.buscar.punto') }}"> Regresar</a>
        </div>
    </div>
</div>
<p class="lead"><small>Coincidencias encontradas con{{ $mensaje_resultado }}seleccione el que considere es el punto requerido.</small></p>
<ul class="list-group">
    @foreach ($rpoint as $pv)
        <li class="list-group-item list-group-item-action">
          <div class="d-flex w-100 justify-content-between">
          <h6 class="mb-1">[{{$pv['idpdv']}}]-{{$pv['nombre_punto']}}</h6>
            <small><strong>{{$pv['circuito']}}</strong></small>
          </div>
          <p class="mb-1">[{{$pv['direccion']}}]-[{{$pv['ciudad']}}]</p>
          <div class="d-flex w-100 justify-content-between">
                <small>{{$pv['nom_propietario']}}</small>
                <small>{{$pv['ced_propietario']}}</small>
          </div>
          <div class="d-flex w-100 justify-content-between">
                @if($pv['estado_dms'] == 'VENDE')
                <small class='text-success'>
                @elseif($pv['estado_dms'] == 'VENDE OTRO DEALER')
                <small class='text-warning'>
                @else
                <small class='text-danger'>
                @endif
                    {{$pv['estado_dms']}}
                </small>
                <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
                    @csrf
                    <input type="hidden" id="idpdv" name="idpdv" value="{{ $pv['idpdv'] }}">
                    <button type="submit" class="btn btn-outline-primary btn-sm">Ver...</button>
                </form>
          </div>
        </li>
    @endforeach
    </ul>

@endsection
