@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Correo Electronico Cliente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-sm btn-primary" href="{{ route('gestion.buscar.punto') }}"> Regresar</a>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>Gracias!!!</strong></h6>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p>Ud Ha Creado/Actualizado el correo Electronico del punto {{ $idpdv }}</p>
					<p>Correo Anterior: {{ $anterior }}</p>
					<p>Correo Actualizado: {{ $correo }}</p>
                </div>
            </div>
            <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
				@csrf
				<div class="row">
				  <div class="col-xs-12 col-sm-12 col-md-12">
					  <input type="hidden" id="idpdv" name="idpdv" value="{{ $idpdv }}">
					  <button type="submit" class="btn btn-md btn-primary">Volver a la Gestion del Punto Nuevamente</button>
				  </div>
				</div>
			  </form>

    </div>
</div>
@endsection