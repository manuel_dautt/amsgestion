@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Administradores Regionales</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('admins.ver.buscar.empleado') }}">
        <i class="fas fa-plus-circle"></i> Nuevo Administrador
      </a>
      <a class="btn btn-sm btn-info" href="{{ route('empleados.index') }}">
        <i class="far fa-user-circle"></i> Ver Empleados
      </a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif
@if ($message = Session::get('error'))
  @include('layouts.alert_error')
@endif

<table class="table table-bordered">
  <tr>
    <th>Empleado</th>
    <th>Cedula</th>
    <th>Regional Asociada</th>
    <th>Canal Asociado</th>
    <th>Admin/CoAdmin</th>
    <th>Acciones</th>
  </tr>
  @foreach ($administradores as $admin)
  <tr>
    <td>{{ $admin->user->name }}</td>
    <td>{{ $admin->employee->document }}</td>
    <td>{{ $admin->regional->regional_name }}</td>
    <td>{{ $admin->channel->channel_name }}</td>
    <td>{{ $admin->level }}</td>
    <td>

      <form action="{{ route('admins.destroy',$admin->id) }}" method="POST">

        @csrf
        @method('DELETE')
        <a class="btn btn-sm btn-info" href="{{ route('admins.show',$admin->id) }}">
          <i class="fas fa-search"></i></a>
        <!--
                    <a class="btn btn-sm btn-primary" href="{ route('roles.edit',$admin->id) }"><i class="fas fa-edit"></i></a>
                    -->
        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
      </form>
    </td>
  </tr>
  @endforeach
</table>

{!! $administradores->links() !!}

@endsection
