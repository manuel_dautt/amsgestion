@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Asignar Administrador</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('admins.index') }}"> Regresar</a>
    </div>
  </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hubo algunos problemas con su Informacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<hr>
<form action="{{ route('admins.store') }}" method="POST">
  @csrf
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Nombre:</strong>
        {{ $administrador['nom_empleado'] }}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Cargo:</strong>
        {{ $administrador['nom_cargo'] }}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Regional:</strong>
        {{ $administrador['nom_regional'] }}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Canal:</strong>
        {{ $administrador['nom_canal'] }}
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Asignar a Regional:</strong>
        <select id="regional_asignada" name="regional_asignada" class="form-control">
          @foreach($regionales as $regional)
            <option value="{{ $regional['id'] }}" {{ ($regional['id'] == $administrador['id_regional']) ? 'selected':'' }}>
              {{ $regional['regional_name'] }}
            </option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Asignar a Canal:</strong>
        <select id="canal_asignado" name="canal_asignado" class="form-control">
          @foreach($canales as $canal)
            <option value="{{ $canal['id'] }}" {{ ($canal['id'] == $administrador['id_canal']) ? 'selected':'' }}>
              {{ $canal['channel_name'] }}
            </option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Admin / CoAdmin:</strong>
        <select id="nivel_asignado" name="nivel_asignado" class="form-control">
          <option value="X" selected>- Seleccione Por Favor -</option>
          <option value="ADMIN">Administrador Regional</option>
          <option value="COADMIN">CoAdministrador Regional</option>
        </select>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Crear</button>
    </div>

    <input type="hidden" name="empleado_asignado" value="{{ $administrador['id_empleado'] }}">
    <input type="hidden" name="usuario_asignado" value="{{ $administrador['id_usuario'] }}">
    <input type="hidden" name="cedula_empleado" value="{{ $administrador['cedula_empleado'] }}"><br />

  </div>
</form>

@endsection
