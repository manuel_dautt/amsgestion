@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Buscar Empleado</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('admins.index') }}"> Regresar</a>
    </div>
  </div>
</div>


<form action="{{ route('admins.buscar.empleado') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Ingrese Cedula Empleado:</strong>
        <input type="text" id="cedula_empleado" name="cedula_empleado" value="{{ old('cedula_empleado') }}" class="form-control @error('cedula_empleado') is-invalid @enderror" placeholder="Ingrese cedula">
        @error('cedula_empleado')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Buscar</button>
    </div>
  </div>

</form>

@endsection
