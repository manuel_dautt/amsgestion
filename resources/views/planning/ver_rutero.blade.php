@extends('layouts.app')

@section('content')

<div class="row">
    
	<span>
    <form class="form-inline" action="{{ route('gestion.puntos.ruta.dia') }}" method="POST">
        @csrf
        <input type="hidden" id="dia_hoy" name="dia_hoy" value="{{$n_dia}}">
        <input type="hidden" id="dia_semana" name="dia_semana" value="{{$n_dia}}">
        <button type="submit" class="btn btn-md btn-primary">Ver Ruta Para Hoy <strong>{{ strtoupper($dia_de_hoy)}}</strong></button>
    </form>
	<span>
    <div class="table-responsive">
    <table class="table table-hover table-sm">
	<caption>Puntos a Visitar x Dia</caption>
	<h6 class="row center"><strong>RUTERO - PLANEACION VISITAS</strong></h6>
        <thead>
			
            <tr>
                <th scope="col">CIRC.</th>
                <th scope="col">LUN</th>
                <th scope="col">MAR</th>
                <th scope="col">MIE</th>
                <th scope="col">JUE</th>
                <th scope="col">VIE</th>
                <th scope="col">SAB</th>
                <th scope="col">DOM</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rutas as $ruta)
            <tr>
                <td>{{$ruta->circuito}}</td>
                <td class="text-center">{{$ruta->lun == 1? $ruta->pv:''}}</td>
                <td class="text-center">{{$ruta->mar == 1? $ruta->pv:''}}</td>
                <td class="text-center">{{$ruta->mie == 1? $ruta->pv:''}}</td>
                <td class="text-center">{{$ruta->jue == 1? $ruta->pv:''}}</td>
                <td class="text-center">{{$ruta->vie == 1? $ruta->pv:''}}</td>
                <td class="text-center">{{$ruta->sab == 1? $ruta->pv:''}}</td>
                <td class="text-center">{{$ruta->dom == 1? $ruta->pv:''}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>

@endsection
