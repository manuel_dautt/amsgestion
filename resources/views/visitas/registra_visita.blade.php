@extends('layouts.app')

@section('content')

<div class="card-body">
    <h6 class="card-title"><strong>Inicia Visita Punto</strong></h6>
    <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
        @csrf
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <input type="hidden" id="idpdv" name="idpdv" value="{{ $ver_visita['idpdv'] }}">
                <table>
                    <tr><td>Fecha y Hora Visita:</td><td>{{ $ver_visita['fecha_ini'] }}</td></tr>
                    <tr><td>Tipo de Visita:</td><td>{{ $nom_tipo_visita['name_type_visit'] }}</td></tr>
                    <tr><td>Nombre Punto:</td><td>{{ $nom_pv }}</td></tr>
                    <tr><td>id Punto:</td><td>{{ $ver_visita['idpdv'] }}</td></tr>
                    <tr><td>Punto Planeado:</td><td>{{ $ver_visita['planeada'] }}</td></tr>
                    <tr><td colspan=2>Coordenadas:</td></tr>
                    <tr><td>Lat.:</td><td>{{ $ver_visita['latitud'] }}</td></tr>
                    <tr><td>Long.:</td><td>{{ $ver_visita['longitud'] }} </td></tr>
                    <tr><td colspan=2><button type="submit" class="btn btn-md btn-primary">Volver</button></td></tr>
                </table>

          </div>
        </div>



      </form>
</div>

@endsection
