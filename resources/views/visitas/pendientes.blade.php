@extends('layouts.app')

@section('content')

<div class="card-body">
    <h6 class="card-title"><strong>Finalizar Visita Punto</strong></h6>
    <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
        @csrf
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <h4>NO PUEDE FINALIZAR VISITA</h4>
              <h6>Tiene Actividades Pendientes</h6>
              <p class="text-white">Encuesta: {{ $encuesta}}</p>
			  <p class="text-white">Creada: {{ $fecha}}</p>
			  <p class="text-white">Pregunta Actual: {{ $pregunta}}</p>
			  
              <input type="hidden" id="idpdv" name="idpdv" value="{{ $idpdv }}">
              <button type="submit" class="btn btn-md btn-primary">Volver a la Gestion del Punto Nuevamente</button>
          </div>
        </div>



      </form>
	  <sub>{{ $traza }}</sub>
</div>

@endsection
