<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>


<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h4>Visitas Punto de Venta</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>
            @if(!$validaVisita['hay_visita_pendiente'])
            INICIAR VISITA - {{ $msg_ruta_hoy }}
            @else
            CERRAR VISITA
            @endif
        </strong></h6>
        @if(!$validaVisita['hay_visita_pendiente'])
        <form class="form-inline" action="{{ route('visits.register') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-xs-8 col-sm-8 col-md-8">

                    <select id="tipo_visita" name="tipo_visita" class="form-control @error('tipo_visita') is-invalid @enderror">
                        <option value="X" selected>Seleccione Tipo Visita...</option>
                        @foreach($tipos_visita as $tipo)
                            <option value="{{ $tipo['id'] }}">{{ $tipo['name_type_visit'] }}</option>
                        @endforeach
                    </select>
                    @error('tipo_visita')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3">
                    <input type="hidden" id="idpdv" name="idpdv" value="{{ $idpdv }}">
                    <input type="hidden" id="planeada" name="planeada" value="{{ $ruta_hoy }}">
                    <input type="hidden" id="nom_pv" name="nom_pv" value="{{ $rpoint['nombre_punto'] }}">
                    <input type="hidden" id="lat" name="lat" value="{{ $rpoint['lat'] }}">
                    <input type="hidden" id="lng" name="lng" value="{{ $rpoint['lng'] }}">
                    <button type="submit" class="btn btn-md btn-primary">Iniciar</button>
                </div>
            </div>
        </form>
        @else
        <form class="form-inline" action="{{ route('visits.close') }}" method="POST">
            @csrf
            <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="hidden" id="idvisita" name="idvisita" value="{{ $validaVisita['id_visita_pendiente'] }}">
                <table>
                    <tr><td>Fecha y Hora Visita:</td><td>{{ $validaVisita['fecha_visita_pendiente'] }}</td></tr>
                    <tr><td>Tipo de Visita:</td><td>{{ $validaVisita['tipo_visita_pendiente'] }}</td></tr>
                    <!--tr><td>Tiempo de la Visita:</td><td>{ $validaVisita['tiempo_visita_pendiente'] }</td></tr-->
					<hr/>
                    <tr><td colspan=2><button type="submit" class="btn btn-md btn-danger">Cerrar Visita</button></td></tr>
                </table>
            </div>
        </form>
        @endif
		@if($tiene_encuestas_activas)
		<small>
		<div class="table-responsive">
		<table class="table table-sm table-striped table-hover">
		<caption>
			[Cmp.] -> Completa S/N
			[Acomp.] -> Doc. Acompañamiento
			[Act.] -> Pregunta Actual
			[Ant.] -> Pregunta Anterior
		</caption>
			<small>
			<thead>
				<tr>
				  <th scope="col">Encuesta</th>
				  <th scope="col">Cmp.</th>
				  <th scope="col">Acomp.</th>
				  <th scope="col">Act.</th>
				  <th scope="col">Ant.</th>
				</tr>
			</thead>
			</small>
			<small>
			<tbody>
				@foreach($encuestas_activas_visita as $enc_vis)
				<tr class="text-{{ ($enc_vis['estado'] == 'A')? 'light' : 'danger' }}">
					<td class='text-lowercase'>{{ $enc_vis['encuesta']['nombre_encuesta'] }}{{ ($enc_vis['estado'] == 'I')? ' (cancel.)' : '' }}</td>
					<td class="text-{{ ($enc_vis['completada'] == 'S')? 'success' : 'danger' }}">{{ $enc_vis['completada'] }}</td>
					<td>{{ ($enc_vis['documento_digital']!=0) ? $enc_vis['documento_digital'] : ''}}</td>
					<td>{{ $enc_vis['pregunta_actual'] }}</td>
					<td>{{ $enc_vis['pregunta_anterior'] }}</td>
				</tr>
				@endforeach
			</tbody>
			</small>
		</table>
		</div>
		</small>
		@endif
    </div>
	<div id="mapid" style="height: 600px; height: 300px"></div>
	<div>
		ULTIMAS 5 VISITAS
		@forelse ($visitas_al_punto as $vap)
		<small>
			<li class="list-group-item list-group-item-action">
				<div class='row'>
					<div class='col'>{{ $vap['user']['name'] }}</div>
				</div>
				<div class='row'>
					<div class='col'>Ini.:{{ $vap['fecha_ini'] }}</div><div class='col'>Fin: {{ $vap['fecha_fin'] }}</div>
				</div>
				<div class='row'>
					<div class='col'>Activa:{{ $vap['activa'] }}</div><div class='col'>Planeada: {{ $vap['planeada'] }}</div>
				</div>
				Duracion de la visita : {{ \Carbon\Carbon::parse($vap['fecha_fin'])->diffInMinutes($vap['fecha_ini']) }} Minuto(s).
			</li>
		</small>
		@empty
			<p>No tiene Visitas</p>
		@endforelse
		
	</div>
</div> <!-- FIN CARD buscar x idpdv -->
<br/>




	<script type="text/javascript">
		var mymap = L.map('mapid').setView([{{$rpoint['lat']}}, {{$rpoint['lng']}}], 18);
		
		L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			maxZoom: 17,
			id: 'mapbox/streets-v11',
			tileSize: 512,
			zoomOffset: -1,
			accessToken: 'pk.eyJ1IjoibWRhdXR0ODkwIiwiYSI6ImNrZmdkdG9yaDA3OW8ycnAzcHg3dmNtNngifQ.bFjosQmsMYl7aH73vkxEMQ'
		}).addTo(mymap);
		
		var marker = L.marker([{{$rpoint['lat']}}, {{$rpoint['lng']}}]).addTo(mymap);
		
		var circle = L.circle([{{$rpoint['lat']}}, {{$rpoint['lng']}}], {
			color: '#000',
			fillColor: '#00377B',
			fillOpacity: 0.5,
			radius: 100
		}).addTo(mymap);
		
	</script>