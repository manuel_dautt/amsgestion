@extends('layouts.app')

@section('content')

<small>
<div class="table-responsive-sm">
    <table class="table table-hover table-bordered table-sm">
        <thead>
            <tr>
                <th>Nombre Archivo</th>
                <th>Tamaño</th>
                <th>Modificado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($archivos as $archivo)
                <tr>
                    <td>
                        {{ $archivo['nombre_corto'] }}
                        <span class="badge badge-info">{{ $archivo['humanos'] }}</span>
                    </td>
                    <td>{{ number_format($archivo['tamano']/1000) }} Kb</td>
                    <td>{{ $archivo['modificado'] }}</td>
                    <td>
                        <a class="btn btn-success" href="{{url( $archivo['nombre_largo'] )}}" role="button">Descargar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</small>

@endsection
