@extends('layouts.app')

@section('content')

<h5>INDICADORES TIENDAS</h5>
<h5>NIVEL DE SERVICIO</h5>
<h6><small>ALIADO: {{ $aliado != 'TODOS' ? $aliado:'*' }} REGIONAL: {{ $regional != 'TODAS' ? $regional:'*' }}</small></h6>
<small>
<div class="row">
PAIS
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">VALIDOS</th>
			<th scope="col">PUNTUALES</th>
			<th scope="col">ABANDONADOS</th>
			<th scope="col">NS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <a class="btn btn-primary btn-sm" href="{{ url('/tiendas/ns/ver') }}" role="button">TODOS</a></th>
		</tr>
	</thead>
	<tbody>
    @foreach($ns_pais as $pais)
	<tr>
		<td>{{ $pais->periodo }}</td>
		<td>{{ $pais->validos }}</td>
		<td>{{ $pais->puntuales }}</td>
		<td>{{ $pais->abandonados }}</td>
		<td>{{ $pais->NS }}</td>
		<td>{{ $pais->calificacion }}</td>
		<td>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/ns/ver/EDATEL') }}" role="button">EDATEL</a>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/ns/ver/TIGO') }}" role="button">TIGO</a>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/ns/ver/UNE') }}" role="button">UNE</a>
		</td>
	</tr>
    @endforeach
	</tbody>
	</table
  </div>
</div>


<div class="row">
REGIONALES
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">REGIONAL</th>
			<th scope="col">VALIDOS</th>
			<th scope="col">PUNTUALES</th>
			<th scope="col">ABANDONADOS</th>
			<th scope="col">NS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <a class="btn btn-primary btn-sm" href="{{ url('/tiendas/ns/ver/'.$aliado) }}" role="button">TODAS</a></th>
		</tr>
	</thead>
	<tbody>
    @foreach($ns_regiones as $regional)
	<tr>
		<td>{{ $regional->periodo }}</td>
		<td>{{ $regional->regional }}</td>
		<td>{{ $regional->validos }}</td>
		<td>{{ $regional->puntuales }}</td>
		<td>{{ $regional->abandonados }}</td>
		<td>{{ $regional->NS }}</td>
		<td>{{ $regional->calificacion }}</td>
		<td>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/ns/ver/'.$aliado.'/'.$regional->regional) }}" role="button">{{$regional->regional}}</a>
		</td>
	</tr>
    @endforeach
	</tbody>
	</table
  </div>
</div>

<div class="row">
TIENDAS
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">REGIONAL</th>
			<th scope="col">TIENDA</th>
			<th scope="col">VALIDOS</th>
			<th scope="col">PUNTUALES</th>
			<th scope="col">ABANDONADOS</th>
			<th scope="col">NS</th>
			<th scope="col">CALIF.</th>
		</tr>
	</thead>
	<tbody>
    @foreach($ns_tiendas as $tienda)
	<tr>
		<td>{{ $tienda->periodo }}</td>
		<td>{{ $tienda->regional }}</td>
		<td>{{ $tienda->tienda }}</td>
		<td>{{ $tienda->validos }}</td>
		<td>{{ $tienda->puntuales }}</td>
		<td>{{ $tienda->abandonados }}</td>
		<td>{{ $tienda->NS }}</td>
		<td>{{ $tienda->calificacion }}</td>
	</tr>
    @endforeach
	</tbody>
	</table
  </div>
</div>
</small>
@endsection
