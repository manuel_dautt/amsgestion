@extends('layouts.app')

@section('content')

<h5>INDICADORES TIENDAS</h5>
<h5>NET PROMOTER SCORE</h5>
<h6>
	<small>
	CANAL: {{ $v_canal != 'TODOS' ? $v_canal:'*' }} NEGOCIO: {{ $v_negocio != 'TODOS' ? $v_negocio:'*' }} REGIONAL: {{ $v_regional != 'TODAS' ? $v_regional:'*' }} TIENDA: {{ $v_tienda != 'TODAS' ? $v_tienda:'*' }}
	</small>
</h6>
<h6>
	<small>
	ENC:ENCUESTAS PRM:PROMOTORES NTR:NEUTROS DTR:DETRACTORES  
	</small>
</h6>
<small>

<div class="row">
PAIS
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">ENC</th>
			<th scope="col">PRM</th>
			<th scope="col">NTR</th>
			<th scope="col">DTR</th>
			<th scope="col">NPS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <a class="btn btn-primary btn-sm" href="{{ url('/tiendas/nps/ver') }}" role="button">TODOS</a></th>
		</tr>
	</thead>
	<tbody>
    @foreach($nps_pais as $pais)
	<tr>
		<td>{{ $pais->periodo }}</td>
		<td>{{ $pais->encuestas }}</td>
		<td>{{ $pais->promotores }}</td>
		<td>{{ $pais->neutros }}</td>
		<td>{{ $pais->detractores }}</td>
		<td>{{ $pais->nps }}</td>
		<td>
			@if($pais->nps >= 33.9)
				120%
			@elseif($pais->nps >= 31.9 AND $pais->nps < 33.9)
				100%
			@elseif($pais->nps >= 29.9 AND $pais->nps < 31.9)
				80%
			@elseif($pais->nps < 29.9)
				0%
			@else
				0%
			@endif
		</td>
		<td>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/nps/ver/Tigo') }}" role="button">Tigo</a>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/nps/ver/Tigo Business') }}" role="button">Tigo Business</a>
		</td>
	</tr>
    @endforeach
	</tbody>
	</table>
  </div>
</div>

<div class="row">
CANALES
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">NEGOCIO</th>
			<th scope="col">ENC</th>
			<th scope="col">PRM</th>
			<th scope="col">NTR</th>
			<th scope="col">DTR</th>
			<th scope="col">NPS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <a class="btn btn-primary btn-sm" href="{{ url('/tiendas/nps/ver/'.$v_canal) }}" role="button">TODOS</a></th>
		</tr>
	</thead>
	<tbody>
    @foreach($nps_canal as $canal)
	<tr>
		<td>{{ $canal->periodo }}</td>
		<td>{{ $canal->canal }}</td>
		<td>{{ $canal->encuestas }}</td>
		<td>{{ $canal->promotores }}</td>
		<td>{{ $canal->neutros }}</td>
		<td>{{ $canal->detractores }}</td>
		<td>{{ $canal->nps }}</td>
		<td>
			@if($canal->nps >= 33.9)
				120%
			@elseif($canal->nps >= 31.9 AND $canal->nps < 33.9)
				100%
			@elseif($canal->nps >= 29.9 AND $canal->nps < 31.9)
				80%
			@elseif($canal->nps < 29.9)
				0%
			@else
				0%
			@endif
		</td>
		<td>
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/nps/ver/'.$v_canal.'/Movil') }}" role="button">Movil</a-->
			<a class="btn btn-primary btn-sm" href="{{ url('/tiendas/nps/ver/'.$v_canal.'/Fijo') }}" role="button">Fijo</a -->
		</td>
	</tr>
    @endforeach
	</tbody>
	</table>
  </div>
</div>



<div class="row">
NEGOCIOS
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">NEGOCIO</th>
			<th scope="col">ENC</th>
			<th scope="col">PRM</th>
			<th scope="col">NTR</th>
			<th scope="col">DTR</th>
			<th scope="col">NPS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio) }" role="button">TODOS</a--></th>
		</tr>
	</thead>
	<tbody>
    @foreach($nps_negocio as $negocio)
	<tr>
		<td>{{ $negocio->periodo }}</td>
		<td>{{ $negocio->negocio }}</td>
		<td>{{ $negocio->encuestas }}</td>
		<td>{{ $negocio->promotores }}</td>
		<td>{{ $negocio->neutros }}</td>
		<td>{{ $negocio->detractores }}</td>
		<td>{{ $negocio->nps }}</td>
		<td>
			@if($negocio->nps >= 33.9)
				120%
			@elseif($negocio->nps >= 31.9 AND $negocio->nps < 33.9)
				100%
			@elseif($negocio->nps >= 29.9 AND $negocio->nps < 31.9)
				80%
			@elseif($negocio->nps < 29.9)
				0%
			@else
				0%
			@endif
		</td>
		<td>
			<!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.canal.'/Movil') }" role="button">Movil</a-->
			<!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.canal.'/Fijo') }" role="button">Fijo</a -->
		</td>
	</tr>
    @endforeach
	</tbody>
	</table>
  </div>
</div>


<div class="row">
REGIONALES
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">REGIONAL</th>
			<th scope="col">ENC</th>
			<th scope="col">PRM</th>
			<th scope="col">NTR</th>
			<th scope="col">DTR</th>
			<th scope="col">NPS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio) }" role="button">TODAS</a--></th>
		</tr>
	</thead>
	<tbody>
    @foreach($nps_regional as $regional)
	<tr>
		<td>{{ $regional->periodo }}</td>
		<td>{{ $regional->regional }}</td>
		<td>{{ $regional->encuestas }}</td>
		<td>{{ $regional->promotores }}</td>
		<td>{{ $regional->neutros }}</td>
		<td>{{ $regional->detractores }}</td>
		<td>{{ $regional->nps }}</td>
		<td>
			@if($regional->nps >= 33.9)
				120%
			@elseif($regional->nps >= 31.9 AND $regional->nps < 33.9)
				100%
			@elseif($regional->nps >= 29.9 AND $regional->nps < 31.9)
				80%
			@elseif($regional->nps < 29.9)
				0%
			@else
				0%
			@endif
		</td>
		<td>
			<!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio.'/'.$regional->regional) }" role="button">{$regional->regional}</a-->
		</td>
	</tr>
    @endforeach
	</tbody>
	</table>
  </div>
</div>

<div class="row">
TIENDAS
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">REGIONAL</th>
			<th scope="col">TIENDA</th>
			<th scope="col">ENC</th>
			<th scope="col">PRM</th>
			<th scope="col">NTR</th>
			<th scope="col">DTR</th>
			<th scope="col">NPS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio) }" role="button">TODAS</a--></th>
		</tr>
	</thead>
	<tbody>
    @foreach($nps_tienda as $tienda)
	<tr>
		<td>{{ $tienda->periodo }}</td>
		<td>{{ $tienda->regional }}</td>
		<td>{{ $tienda->tienda }}</td>
		<td>{{ $tienda->encuestas }}</td>
		<td>{{ $tienda->promotores }}</td>
		<td>{{ $tienda->neutros }}</td>
		<td>{{ $tienda->detractores }}</td>
		<td>{{ $tienda->nps }}</td>
		<td>
			@if($tienda->nps >= 33.9)
				120%
			@elseif($tienda->nps >= 31.9 AND $tienda->nps < 33.9)
				100%
			@elseif($tienda->nps >= 29.9 AND $tienda->nps < 31.9)
				80%
			@elseif($tienda->nps < 29.9)
				0%
			@else
				0%
			@endif
		</td>
		<td>
			<!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio.'/'.$regional->regional) }" role="button">{$regional->regional}</a-->
		</td>
	</tr>
    @endforeach
	</tbody>
	</table>
  </div>
</div>

<div class="row">
ASESORES
  <div class="table-responsive">
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">PERIODO</th>
			<th scope="col">REGIONAL</th>
			<th scope="col">TIENDA</th>
			<th scope="col">ASESOR</th>
			<th scope="col">ENC</th>
			<th scope="col">PRM</th>
			<th scope="col">NTR</th>
			<th scope="col">DTR</th>
			<th scope="col">NPS</th>
			<th scope="col">CALIF.</th>
			<th scope="col">ACCIONES <!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio) }" role="button">TODAS</a--></th>
		</tr>
	</thead>
	<tbody>
    @foreach($nps_asesor as $asesor)
	<tr>
		<td>{{ $asesor->periodo }}</td>
		<td>{{ $asesor->regional }}</td>
		<td>{{ $asesor->tienda }}</td>
		<td>{{ $asesor->asesor }}</td>
		<td>{{ $asesor->encuestas }}</td>
		<td>{{ $asesor->promotores }}</td>
		<td>{{ $asesor->neutros }}</td>
		<td>{{ $asesor->detractores }}</td>
		<td>{{ $asesor->nps }}</td>
		<td>
			@if($asesor->nps >= 33.9)
				120%
			@elseif($asesor->nps >= 31.9 AND $asesor->nps < 33.9)
				100%
			@elseif($asesor->nps >= 29.9 AND $asesor->nps < 31.9)
				80%
			@elseif($asesor->nps < 29.9)
				0%
			@else
				0%
			@endif
		</td>
		<td>
			<!--a class="btn btn-primary btn-sm" href="{ url('/tiendas/nps/ver/'.$canal.'/'.$negocio.'/'.$regional->regional) }" role="button">{$regional->regional}</a-->
		</td>
	</tr>
    @endforeach
	</tbody>
	</table>
  </div>
</div>


</small>
@endsection