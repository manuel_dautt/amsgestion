@extends('layouts.app')

@section('content')
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

   <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
   

   <script src="{{ url('geojson/base_cves.js') }}"></script>

    <h4><small>MOC :: CVE {{ $datos_cve['cve_nombre'] }}</small></h4>
    <small>Puntos de Venta</small>
     <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_todos" checked onclick="chequeo(this)" value="all">
        <span style="background-color: #FFF"><small> [ Todos ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_dealer" onclick="chequeo(this)" value="dealers">
        <span style="background-color: #00FF00"><small> [ Dealers ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_tcerca" onclick="chequeo(this)" value="tcerca">
        <span style="background-color: #FF8000"><small> [ TdaCerca ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_retail" onclick="chequeo(this)" value="retails">
        <span style="background-color: #FFFF00"><small> [ Retail ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_tiendas" onclick="chequeo(this)" value="tiendas">
        <span style="background-color: #0000FF; color: #FFFFFF"><small> [ Tiendas ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_bts" onclick="chequeo(this)" value="bts">
        <span style="background-color: #000000; color: #FFFFFF"><small> [ Antenas ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="ver_puntos" id="puntos_bts700" onclick="chequeo(this)" value="bts700">
        <span style="background-color: #454545; color: #FFFFFF"><small> [ BTS700 ] </small></span>
    </div>
    <div class="form-check form-check-inline">
        <a href="{{ route('mapas.exportar.puntos',['cve' => $datos_cve['cve_nombre']]) }}">
            <small>[ Exportar ]</small>
        </a>
    </div>
    <div class="form-check form-check-inline">
        <a href="{{ route('mapas.ver.opciones.moc') }}">
            <small>[ Regresar ]</small>
        </a>
    </div>
    <input type="hidden" id="flag_option" value="all">


    <div class="row">
        <div class="col-md-12">
            <div id="map" style="height: 500px"></div>
        </div>
    </div>



    <script type="text/javascript">

    var pv_json = JSON.parse("{{ json_encode($v_geojson) }}".replace(/(&quot\;)/g,"\""));
    //console.log(pv_json);



    var SW = L.latLng( {{ $datos_cve['SW_lat'] }}, {{ $datos_cve['SW_lon'] }} );
	var NE = L.latLng( {{ $datos_cve['NE_lat'] }}, {{ $datos_cve['NE_lon'] }} );
	var limites = L.latLngBounds(SW, NE);

    //Cargando la capa que contendra nuestro mapa
    //let mimapa = L.map('map').setView([4.570868, -74.2973328],5);
    let mimapa = L.map('map',{maxZoom: 18, minZoom: 9, maxBounds: limites}).setView([{{ $datos_cve['C_lat'] }}, {{ $datos_cve['C_lon'] }}],11);
    //4.570868, -74.2973328
    //4.8282597468669755,-74.22363281250001

    //en esta pagina puedo encontrar mas layers de mapas
    //https://leaflet-extras.github.io/leaflet-providers/preview/
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        zoomControl: false
    }).addTo(mimapa);

    var capas_mimapa = L.layerGroup().addTo(mimapa);

    //TRABAJAR CON LA CAPA GeoJson DE CVEs
    var capa_cve = L.geoJSON(base_cves,{style: estilo_cve});

    //Agregamos esta capa a Mi Mapa
    capas_mimapa.addLayer(capa_cve);



    var MarkerOptions = {
				    radius: 5,
				    fillColor: "#ff7800",
				    color: "#000",
				    weight: 1,
				    opacity: 1,
				    fillOpacity: 0.8
					};


    // CARGAMOS UNOS EVENTOS PARA CONTROLAR LOS RADIO BUTTONS
    // ESTOS HACEN UN RELOAD DEL GEOJSON DE CAPAS SEGUN LA VARIACION
    // DE RECARGAS, GROSS O MARKETSHARE
    function chequeo(componente){
        el = document.getElementById('flag_option');
        el.value = componente.value;
        tipo_punto = AsignaTipoPunto(componente.value);
        capa_cve = L.geoJSON(base_cves,{style: estilo_cve});

        capa_pv_cve = L.geoJSON(pv_json,
                {
                    pointToLayer: function (feature, latlng) {
                        return L.circleMarker(latlng, MarkerOptions);
                    },
                    style: estilo_pv,
                    onEachFeature: popup_pv,
                    filter: function(feature, layer)
                                    {
                                        if(tipo_punto == 'ALL'){
                                            return true;
                                        }

                                        if(tipo_punto == 'Antenas'){
                                            return (feature.properties.canal == 'Antenas'?true:false);
                                        }

                                        if(tipo_punto == 'Antenas700'){
                                            return (feature.properties.categoria == 'BTS700'?true:false);
                                        }

                                        if(tipo_punto == 'Dealers'){
                                            return (feature.properties.canal == 'Dealers'?true:false);
                                        }

                                        if(tipo_punto == 'Retail'){
                                            return (feature.properties.canal == 'Retail'?true:false);
                                        }

                                        if(tipo_punto == 'TiendasPropias'){
                                            return (feature.properties.canal == 'TiendasPropias'?true:false);
                                        }

                                        if(tipo_punto == 'TiendasCerca'){
                                            return (feature.properties.canal == 'TiendasCerca'?true:false);
                                        }

                                        return false;
                                    }
                }
        );

        capas_mimapa.clearLayers();
        capas_mimapa.addLayer(capa_cve);
        capas_mimapa.addLayer(capa_pv_cve);

    }

    function AsignaTipoPunto(tipo){
        switch (tipo){
            case 'all': tipo_punto = "ALL"; break;
            case 'dealers': tipo_punto = "Dealers"; break;
            case 'tcerca': tipo_punto = "TiendasCerca"; break;
            case 'retails': tipo_punto = "Retail"; break;
            case 'tiendas': tipo_punto = "TiendasPropias"; break;
            case 'bts': tipo_punto = "Antenas"; break;
            case 'bts700': tipo_punto = "Antenas700"; break;
            default: tipo_punto =  "ERROR"; break;
        }
        return tipo_punto;
    }

    function popup_pv (feature, layer) {
        let contenido_popup = '';
        contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.nombre+"</div>";
        contenido_popup = contenido_popup + "<div style=text-align:center>CVE:"+feature.properties.cve+"</div>";
        contenido_popup = contenido_popup + "<table>";
        contenido_popup = contenido_popup + "<tr><td>Canal: "+feature.properties.canal+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Tipo: "+feature.properties.tipo+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Categoria: "+feature.properties.categoria+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Segmentacion: "+feature.properties.segmentacion+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Visibilidad: "+feature.properties.visibilidad+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Transando: "+feature.properties.transando+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Aliado: "+feature.properties.distribuidor+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Sucursal: "+feature.properties.sucursal+"</td></tr>";
        contenido_popup = contenido_popup + "<tr><td>Codigo: "+feature.properties.codigo+"</td></tr>";
        contenido_popup = contenido_popup + "</table>";
        if(feature.properties.canal == 'Dealers'){
            contenido_popup = contenido_popup + "<form action={{ route('gestion.traer.punto') }} method='POST'>";
            contenido_popup = contenido_popup + '@csrf';
            contenido_popup = contenido_popup + "<input type='hidden' id='idpdv' name='idpdv' value='"+feature.properties.codigo+"'>";
            contenido_popup = contenido_popup + "<button type='submit' class='btn btn-xs btn-primary'>Ver Informacion Cial.</button>";
            contenido_popup = contenido_popup + "</form>";
        }
        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
    }

    function colorPuntos(canal, categ, tipo) {
        let cColor = '';
        switch (canal){
            case 'BTS': categ == 'BTS700'? cColor = "#454545" : cColor = "#000000"; break;
            case 'Antenas': categ == 'BTS700'? cColor = "#454545" : cColor = "#000000"; break;
            case 'DEALER': cColor = "#00FF00"; break;
            case 'Dealers': cColor = "#00FF00"; break;
            case 'TIENDAS': cColor = "#0000FF"; break;
            case 'TiendasPropias': cColor = "#0000FF"; break;
            case 'RETAIL': cColor = "#FFFF00"; break;
            case 'Retail': cColor = "#FFFF00"; break;
            case 'TCERCA': cColor = "#FF8000"; break;
            case 'TiendasCerca': cColor = "#FF8000"; break;
            default: cColor =  "#9b59b6"; break;
        }
        return cColor;
    };

    function estilo_pv (feature) {
        //console.log(feature.properties.tipo);
        return{
            radius: 5,
            fillColor: colorPuntos(feature.properties.canal, feature.properties.categoria, feature.properties.tipo),
            color: "#9b59b6",
            weight: 1,
            opacity : 1,
            fillOpacity : 0.8
        };
    }

     //TRABAJAR CON LA CAPA GeoJson DE LOS PUNTOS DE VENTA DEL CVEs
     var capa_pv_cve = L.geoJSON(pv_json,{
                                        pointToLayer: function (feature, latlng) {
                                            return L.circleMarker(latlng, MarkerOptions);
                                        },
                                        style: estilo_pv,
                                        onEachFeature: popup_pv
     });

     capas_mimapa.addLayer(capa_pv_cve);
/*

    var menucito = L.popup();

    mimapa.on('click', onMapClick);

    function onMapClick(e){
        var ubicacion = '<p>' + e.latlng.lat.toString() + "," +  e.latlng.lng.toString() + '</p>';
        menucito
            .setLatLng(e.latlng)
            .setContent('<H3>Has Hecho Click</H3><p>Las Coordenadas donde lo hiciste son</p>' + ubicacion + '<hr>')
            .openOn(mimapa);
    }
*/
    // CARGAMOS LOS ESTILOS DE LINEAS DEL CVE
    // DEPENDIENDO DEL CRITERIO DE SELECCION
    // POR ONDE SE QUIERA MIRAR
    function estilo_cve(feature) {
		let mColor = '';
        let mWeight = 2;
        let mOpacity = 1;
        let mDashArray = '';
        let criterio = '{{ $datos_cve['cve_orden'] }}';
        let name_capa = '{{ $datos_cve['cve_nombre'] }}';

        switch (criterio){
            case 'recarga': dColor = define_color(parseFloat(feature.properties.resultados.v_recargas)); break;
            case 'gross': dColor = define_color(parseFloat(feature.properties.resultados.v_gross_prepago)); break;
            case 'mkshare': dColor = define_color(parseFloat(feature.properties.resultados.v_mksh_datos)); break;
            default: dColor = "#0000ff"; break;
        }

        if(feature.properties.name == name_capa){
            mWeight = 10;
            mOpacity = 0.5;
            mDashArray = '3';
        }

		return {color: dColor, weight: mWeight, opacity: mOpacity, dashArray: mDashArray};
	}

    //SEGUN EL CRITERIO DE SELECCION DEFINIMOS LAS VARIABLES Y LOS COLORES
    function define_color(dato){

        if (dato < 0.0) {
            mColor = "#E5BE01"
        }else if (dato > 0.0){
            mColor = "#0000ff";
        }else{
            mColor = "#0000ff";
        }

        return mColor;
    }


    </script>
@endsection
