@extends('layouts.app')

@section('content')
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
  

<link type="text/css" rel="stylesheet" href="{{ asset('css/MarkerCluster.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('css/MarkerCluster.Default.css') }}" />
<script type="text/javascript" src="{{ asset('js/leaflet.markercluster-src.js') }}"></script>

<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h6>MAPA CENSO</h6>
        </div>
	</div>
</div>
<div class="row">	
	<div class="col-lg-12">	
		<div class="pull-right">
			
			<small>MOSTRAR CAPAS</small>
			<div class="form-check form-check-inline @if($capa_cves) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_cves" id="c_cves" value="cves" {{ $capa_cves?'checked' : '' }} onclick = "capas(this)">
				<span><small> [ CVEs ] </small></span>
				&nbsp;&nbsp;
			</div>
			<div class="form-check form-check-inline @if($capa_antenas) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_antenas" id="c_antenas" value="antenas" {{ $capa_antenas?'checked' : '' }} onclick = "capas(this)">
				<span><small><small> [ ANTENAS ] </small></span>
				&nbsp;&nbsp;
			</div>
			<div class="form-check form-check-inline @if($capa_cciales) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_cciales" id="c_cciales" value="cciales" {{ $capa_cciales?'checked' : '' }} onclick="capas(this)">
				<span><small><small> [ CENTROS COMERCIALES ] </small></span>
				&nbsp;&nbsp;
			</div>
			<div class="form-check form-check-inline @if($capa_puntos) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_puntos" id="c_puntos" value="puntos" {{ $capa_puntos?'checked' : '' }} onclick="capas(this)">
				<span><small><small> [ PUNTOS DE VENTA ] </small></span>
				&nbsp;&nbsp;
			</div>
			
			<div class="form-check form-check-inline">	
				<form class="form-inline" action="{{ route('mapas.actualizar.censo.propios') }}" method="POST">
					@csrf
					<input type="hidden" name="cves" id="cves" value="{{ $capa_cves }}">
					<input type="hidden" name="antenas" id="antenas" value="{{ $capa_antenas }}">
					<input type="hidden" name="cciales" id="cciales" value="{{ $capa_cciales }}">
					<input type="hidden" name="puntos" id="puntos" value="{{ $capa_puntos }}">
					
					<button type="submit" class="btn btn-sm btn-primary">Actualizar Mapa</button>
					<a class="btn btn-sm btn-primary" href="{{ route('home') }}"> Regresar</a>
				</form>
			</div>
		
		</div>
		
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<div id="map" style="height: 500px"></div>
	</div>
</div> 

<script src="{{ url('geojson/base_cves.js') }}"></script>
<script type="text/javascript">

	
	function capas(componente){
		//console.log(componente.checked);
		//console.log(componente.value);
		//console.log(componente.name);
		let el = document.getElementById(componente.value);
		let el2 = document.getElementById(componente.name);
		el.value = componente.checked ? 1 : 0;
		componente.checked ? el2.classList.add("bg-success") : el2.classList.remove("bg-success");
		//console.log(el);
		//console.log(el2);
	}
	
	
	
	var bts_geojson = JSON.parse("{{ json_encode($bts_geojson) }}".replace(/(&quot\;)/g,"\""));
    //console.log(bts_geojson);
	
	var cc_geojson = JSON.parse("{{ json_encode($cc_geojson) }}".replace(/(&quot\;)/g,"\""));
    //console.log(cc_geojson);
	//console.log({{ json_encode($pv_geojson) }});
	var pv_geojson = JSON.parse("{{ json_encode($pv_geojson) }}".replace(/(&quot\;)/g,"\""));
    //console.log(pv_geojson);
	
	//console.log(base_cves);

	// **********************************************************
	// ****   CAPA DE CVEs
	// **********************************************************
	capa_cve = L.geoJSON(base_cves, {onEachFeature: popup_cves, style: estilo_cve}); //,{style: estilo_cve});
	function popup_cves (feature, layer) {
        let contenido_popup = '';
        contenido_popup = contenido_popup + "<div style=text-align:center>CVE: "+feature.properties.Name+"</div>";        
        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
    };
	
	var estilo_cve = {
		color: "#090909",
		weight: 1,
		opacity: 0.65
	};

//  *******************  FIN CAPA CVEs  ****************************

	// **********************************************************
	// ****   CAPA DE ANTENAS
	// **********************************************************
	var MarkerAntenasOptions = {
				    radius: 3,
				    fillColor: "#FFF",
				    color: "#000",
				    weight: 1,
				    opacity: 3,
				    fillOpacity: 0.8
					};
	
	var capa_antenas = L.geoJSON(bts_geojson,{
                                        pointToLayer: function (feature, latlng) {
                                            return L.circleMarker(latlng, MarkerAntenasOptions);
                                        },
                                        style: estilo_antenas,
                                        onEachFeature: popup_antenas
     });
	 
	 function estilo_antenas (feature) {
        //console.log(feature.properties.tipo);
        return{
            radius: 8,
            fillColor: '#FFF',
            color: "#000",
            weight: 3,
            opacity : 1,
            fillOpacity : 0.8
        };
    }
	
	function popup_antenas (feature, layer) {
        let contenido_popup = '';
        contenido_popup = contenido_popup + "<div style=text-align:center>ANTENA: "+feature.properties.antena+"</div>";        
        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
		layer.on('mouseover', function() { layer.openPopup(); });
        layer.on('mouseout', function() { layer.closePopup(); });
    }

// *****************  FIN CAPA DE ANTENAS  *********************************	
	

	// **********************************************************
	// ****   CAPA DE CENTROS COMERCIALES
	// **********************************************************
	var MarkerCcialesOptions = {
				    radius: 8,
				    fillColor: "#000",
				    color: "#FFF",
				    weight: 3,
				    opacity: 1,
				    fillOpacity: 0.8
					};
	
	var capa_cciales = L.geoJSON(cc_geojson,{
                                        pointToLayer: function (feature, latlng) {
                                            return L.circleMarker(latlng, MarkerCcialesOptions);
                                        },
                                        style: estilo_cciales,
                                        onEachFeature: popup_cciales
     });
	 
	 function estilo_cciales (feature) {
        //console.log(feature.properties.tipo);
        return{
            radius: 8,
            fillColor: '#000',
            color: "#FFF",
            weight: 3,
            opacity : 1,
            fillOpacity : 0.8
        };
    }
	
	function popup_cciales (feature, layer) {
        let contenido_popup = '';
        contenido_popup = contenido_popup + "<div style=text-align:center>CENTRO COMERCIAL</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.nombre+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.departamento+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.ciudad+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.direccion+"</div>";
        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
		layer.on('mouseover', function() { layer.openPopup(); });
        layer.on('mouseout', function() { layer.closePopup(); });
    }

// *****************  FIN CAPA DE ANTENAS  *********************************	


	// **********************************************************
	// ****   CAPA DE PUNTOS DE VENTA
	// **********************************************************
	var MarkerPuntosOptions = {
				    radius: 8,
				    fillColor: "#1b5e20",
				    color: "#1b5e20",
				    weight: 1,
				    opacity: 1,
				    fillOpacity: 0.8
					};
	
	var capa_puntos = L.geoJSON(pv_geojson,{
                                        pointToLayer: function (feature, latlng) {
                                            return L.circleMarker(latlng, MarkerPuntosOptions);
                                        },
                                        style: estilo_puntos,
                                        onEachFeature: popup_puntos
     });
	 
	 function estilo_puntos (feature) {
        //console.log(feature.properties.tipo);
        return{
            radius: 8,
            fillColor: colorPuntos(feature.properties.operador),
            color: "#1b5e20",
            weight: 4,
            opacity : 1,
            fillOpacity : 0.8
        };
    }
	
	function popup_puntos (feature, layer) {
        let contenido_popup = '';
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.nombre+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.ubicacion+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>Puestos: "+feature.properties.puestos+" Asesores: "+feature.properties.asesores+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.regional+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.departamento+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.ciudad+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.direccion+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>CC:"+feature.properties.centro_comercial+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>Cadena:"+feature.properties.cadena+"</div>";
        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
		layer.on('mouseover', function() { layer.openPopup(); });
        layer.on('mouseout', function() { layer.closePopup(); });
    }

// *****************  FIN CAPA DE PUNTOS  *********************************	

    function colorPuntos(operador) {
        let cColor = '';
        switch (operador){
            case 'TIGO': cColor = "#00377D"; break;
            case 'CLARO': cColor = "#DA291C"; break;
            case 'MOVISTAR': cColor = "#5BC500"; break;
            case 'AVANTEL/ WOM': cColor = "#EB028B"; break;
            case 'DIRECTV': cColor = "#03A9F4"; break;
            case 'VIRGIN': cColor = "#8400A2"; break;
            case 'ETB': cColor = "#F3921A"; break;
            default: cColor =  "#DAD607"; break;
        }
        return cColor;
    };

	
	var mimapa = L.map('map').setView([4.570868, -74.2973328],5); //.fitBounds([ [4.0712, -74.227],[4.0774, -74.125] ]); //fitBounds( limites );
	//'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
	//'https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png'
	L.tileLayer('https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
				zoomControl: true
    }).addTo(mimapa);
	
	var capas_mimapa = L.layerGroup().addTo(mimapa);
	
	var markers = L.markerClusterGroup({ polygonOptions: {color: 'orange'}, maxClusterRadius: 40, disableClusteringAtZoom: 18 });	
	
	@if($capa_antenas == 1)
		markers.addLayer(capa_antenas);
	@endif
	
	@if($capa_cciales == 1)
		markers.addLayer(capa_cciales);
	@endif
	
	@if($capa_puntos == 1)
		markers.addLayer(capa_puntos);
	@endif
	
	@if($capa_cves == 1)
		capas_mimapa.addLayer(capa_cve);
	@endif
	
	capas_mimapa.addLayer(markers);
/*
	capas_mimapa.addLayer(capa_antenas);
	capas_mimapa.addLayer(capa_cciales);
	capas_mimapa.addLayer(capa_puntos);
*/
</script>
@endsection