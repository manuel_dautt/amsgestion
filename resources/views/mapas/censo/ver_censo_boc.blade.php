@extends('layouts.app')

@section('content')
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
  

<link type="text/css" rel="stylesheet" href="{{ asset('css/MarkerCluster.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('css/MarkerCluster.Default.css') }}" />
<script type="text/javascript" src="{{ asset('js/leaflet.markercluster-src.js') }}"></script>

<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h6>MAPA CENSO BOC :: Reporte Censo:{{ $mensaje['fecha'] }}</h6>
			<p><small>Notas:{{ $mensaje['texto'] }}</small></p>
        </div>
	</div>
</div>
<div class="row">	
	<div class="col-lg-12">	
		<div class="pull-right">
			
			<small>MOSTRAR CAPAS</small>
			<div class="form-check form-check-inline @if($capa_cves) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_cves" id="c_cves" value="cves" {{ $capa_cves?'checked' : '' }} onclick = "capas(this)" disabled>
				<span><small> [ CVEs ] </small></span>
				&nbsp;&nbsp;
			</div>
			<div class="form-check form-check-inline @if(!$capa_circuitos) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_circuitos" id="c_circuitos" value="circuitos" {{ !$capa_circuitos?'checked' : '' }} onclick = "capas(this)" disabled>
				<span><small><small> [ CIRCUITOS ] </small></span>
				&nbsp;&nbsp;
			</div>
			<div class="form-check form-check-inline @if($capa_puntos) bg-success @endif">
				&nbsp;&nbsp;
				<input class="form-check-input" type="checkbox" name="c_puntos" id="c_puntos" value="puntos" {{ $capa_puntos?'checked' : '' }} onclick="capas(this)" disabled>
				<span><small><small> [ PUNTOS DE VENTA ] </small></span>
				&nbsp;&nbsp;
			</div>
			
			<div class="form-check form-check-inline">	
				<form class="form-inline" action="{{ route('mapas.actualizar.censo.boc') }}" method="POST">
					@csrf
					<input type="hidden" name="cves" id="cves" value="{{ $capa_cves }}">
					<input type="hidden" name="antenas" id="circuitos" value="{{ $capa_circuitos }}">
					<input type="hidden" name="puntos" id="puntos" value="{{ $capa_puntos }}">
					
					<button type="submit" class="btn btn-sm btn-primary">Actualizar Mapa</button>
					<a class="btn btn-sm btn-primary" href="{{ route('home') }}"> Regresar</a>
				</form>
			</div>
		
		</div>
		
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<div id="map" style="height: 500px"></div>
	</div>
</div> 

<script src="{{ url('geojson/base_cves.js') }}"></script>
<script src="{{ url('geojson/circuitosDMS.js') }}"></script>
<script type="text/javascript">

	
	function capas(componente){
		//console.log(componente.checked);
		//console.log(componente.value);
		//console.log(componente.name);
		let el = document.getElementById(componente.value);
		let el2 = document.getElementById(componente.name);
		el.value = componente.checked ? 1 : 0;
		componente.checked ? el2.classList.add("bg-success") : el2.classList.remove("bg-success");
		//console.log(el);
		//console.log(el2);
	}
	
	
	var pv_geojson = JSON.parse("{{ json_encode($pv_geojson) }}".replace(/(&quot\;)/g,"\""));
    console.log(pv_geojson);
	
	console.log(base_cves);
	console.log(base_circuitos);

	// **********************************************************
	// ****   CAPA DE CVEs
	// **********************************************************
	capa_cve = L.geoJSON(base_cves, {onEachFeature: popup_cves, style: estilo_cve}); //,{style: estilo_cve});
	function popup_cves (feature, layer) {
        let contenido_popup = '';
        contenido_popup = contenido_popup + "<div style=text-align:center>CVE: "+feature.properties.Name+"</div>";        
        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
    };
	
	var estilo_cve = {
		color: "#090909",
		weight: 1,
		opacity: 0.65
	};

//  *******************  FIN CAPA CVEs  ****************************

	// **********************************************************
	// ****   CAPA DE CIRCUITOS
	// **********************************************************
	capa_circuitos = L.geoJSON(base_circuitos, {onEachFeature: popup_circuitos, style: estilo_circuitos}); //,{style: estilo_cve});
	function popup_circuitos (feature, layer) {
        let contenido_popup_c = '';
        contenido_popup_c = contenido_popup_c + "<div style=text-align:center>Circuito: "+feature.properties.CODE+"</div>";        
        layer.bindPopup(contenido_popup_c, {minWidth: 150, maxWidth: 200});
    };
	
	var estilo_circuitos = {
		color: "#f7f709",
		weight: 1,
		opacity: 0.65
	};

//  *******************  FIN CAPA CIRCUITOS  ****************************



	// **********************************************************
	// ****   CAPA DE PUNTOS DE VENTA
	// **********************************************************
	var MarkerPuntosOptions = {
				    radius: 8,
				    fillColor: "#1b5e20",
				    color: "#1b5e20",
				    weight: 1,
				    opacity: 1,
				    fillOpacity: 0.8
					};
	
	var capa_puntos = L.geoJSON(pv_geojson,{
                                        pointToLayer: function (feature, latlng) {
                                            return L.circleMarker(latlng, MarkerPuntosOptions);
                                        },
                                        style: estilo_puntos,
                                        onEachFeature: popup_puntos
     });
	 
	 function estilo_puntos (feature) {
        //console.log(feature.properties.tipo);
        return{
            radius: 8,
            fillColor: colorPuntos(feature.properties.regional),
            color: "#1b5e20",
            weight: 4,
            opacity : 1,
            fillOpacity : 0.8
        };
    }
	
	function popup_puntos (feature, layer) {
        let contenido_popup = '';
		contenido_popup = contenido_popup + "<div style=text-align:center>ID ENC: "+feature.properties.id_encuesta+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>PERIOD.: "+feature.properties.periodo+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>EMPLEADO: "+feature.properties.desarrollador+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>CIUDAD: "+feature.properties.ciudad+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>REGIONAL: "+feature.properties.regional+"</div>";
	//	contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.latitud+"</div>";
	//	contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.longitud+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>CERRADO: "+feature.properties.pv_cerrado+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>PERMITE ENC: "+feature.properties.permite_encuesta+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>ES PV NUEVO: "+feature.properties.pv_nuevo+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>PV EXISTE: "+feature.properties.pv_existe+"</div>";
	//	contenido_popup = contenido_popup + "<div style=text-align:center>"+feature.properties.negocio+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>IDPDV: "+feature.properties.idpdv+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>CIRC: "+feature.properties.circuito+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>VENDE TELECOM: "+feature.properties.vende_telecom+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>VENDE TIGO: "+feature.properties.vende_tigo+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>VENDE RECARGA: "+feature.properties.vende_recarga+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>RECARGA TIGO: "+feature.properties.vende_recarga_tigo+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>SIM TIGO: "+feature.properties.simcard_tigo+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>LE INTERESA SIM: "+feature.properties.interesado_simcard+"</div>";
		contenido_popup = contenido_popup + "<div style=text-align:center>LE INTERESA RECARGA: "+feature.properties.interesado_recarga+"</div>";

        layer.bindPopup(contenido_popup, {minWidth: 150, maxWidth: 200});
		layer.on('mouseover', function() { layer.openPopup(); });
        layer.on('mouseout', function() { layer.closePopup(); });
    }

// *****************  FIN CAPA DE PUNTOS  *********************************	

    function colorPuntos(regional) {
        let cColor = '';
        switch (regional){
            case 'BOGOTA': cColor = "#00377D"; break;
            case 'COSTA': cColor = "#DA291C"; break;
            case 'NOROCCIDENTE': cColor = "#5BC500"; break;
            case 'OCCIDENTE': cColor = "#EB028B"; break;
            case 'ORIENTE': cColor = "#03A9F4"; break;
            default: cColor =  "#DAD607"; break;
        }
        return cColor;
    };

	
	var mimapa = L.map('map').setView([4.570868, -74.2973328],5); //.fitBounds([ [4.0712, -74.227],[4.0774, -74.125] ]); //fitBounds( limites );
	//'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
	//'https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png'
	L.tileLayer('https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
				zoomControl: true
    }).addTo(mimapa);
	
	var capas_mimapa = L.layerGroup().addTo(mimapa);
	
	var markers = L.markerClusterGroup({ polygonOptions: {color: 'orange'}, maxClusterRadius: 40, disableClusteringAtZoom: 18 });	
	
	
	@if($capa_puntos == 1)
		markers.addLayer(capa_puntos);
	@endif
	
	@if($capa_cves == 1)
		capas_mimapa.addLayer(capa_cve);
	@endif

	@if($capa_circuitos == 0)
		capas_mimapa.addLayer(capa_circuitos);
	@endif
	
	capas_mimapa.addLayer(markers);
/*
	capas_mimapa.addLayer(capa_antenas);
	capas_mimapa.addLayer(capa_cciales);
	capas_mimapa.addLayer(capa_puntos);
*/
</script>
@endsection