@extends('layouts.app')

@section('content')
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>

<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h6>MAPA DE RUTA</h6>
        </div>
        <div class="pull-right">
			<form class="form-inline" action="{{ route('gestion.puntos.ruta.dia') }}" method="POST">
				@csrf
				<a class="btn btn-sm btn-primary" href="{{ route('gestion.buscar.punto') }}"> Regresar</a>
				<a class="btn btn-sm btn-primary" href="{{ route('planeacion.ver.planeador') }}">Ver Planeador</a>
				<input type="hidden" id="dia_hoy" name="dia_hoy" value="{{$dia_semana}}">
				<input type="hidden" id="dia_semana" name="dia_semana" value="{{$dia_semana}}">
				<button type="submit" class="btn btn-sm btn-primary">Listado Puntos</button>
			</form>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<div id="map" style="height: 500px"></div>
	</div>
</div> 

<script type="text/javascript">
	
	var SW = L.latLng( {{ $lat_min }}, {{ $lng_max }} );
	var NE = L.latLng( {{ $lat_max }}, {{ $lng_min }} );
	var limites = L.latLngBounds(SW, NE);

	var pv_geojson = JSON.parse("{{ json_encode($pv_geojson) }}".replace(/(&quot\;)/g,"\""));
	
	var estilo_marcadores = {
		radius: 8,
		fillColor: "#ff7800",
		color: "#000",
		weight: 2,
		opacity: 1,
		fillOpacity: 0.8
	};
	
	function onEachFeature(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.nombre) {
		mensaje = "<small>";
		mensaje = mensaje + "<h6>"+feature.properties.circuito+"</h6>";
		mensaje = mensaje + "<h6>"+feature.properties.idpdv+" - "+feature.properties.nombre+"</h6>";
		mensaje = mensaje + "<p>"+feature.properties.direccion+"</p>";
		mensaje = mensaje + "<p>ESTADO: "+feature.properties.estado_dms+"</p>";
		mensaje = mensaje + "<p>PROP: "+feature.properties.propietario+"</p>";
		mensaje = mensaje + "<p>CAT_DMS: "+feature.properties.categoria_dms+"</p>";
		mensaje = mensaje + "<p>CAT_TARDE: "+feature.properties.categoria_trade+"</p>";
		mensaje = mensaje + "<p>SEGM: "+feature.properties.segmentacion+"</p>";
		mensaje = mensaje + "<p>CVE: "+feature.properties.cve+"</p>";
		
		mensaje = mensaje + "<form action={{ route('gestion.traer.punto') }} method='POST'>";
        mensaje = mensaje + '@csrf';
        mensaje = mensaje + "<input type='hidden' id='idpdv' name='idpdv' value='"+feature.properties.idpdv+"'>";
        mensaje = mensaje + "<button type='submit' class='btn btn-xs btn-primary'>Ver Informacion Cial.</button>";
        mensaje = mensaje + "</form>";
		mensaje = mensaje + "</small>";
		
        layer.bindPopup(mensaje);
    }
}
	
	//var mimapa = L.map('map').setView([{{ $lat_prom }}, {{ $lng_prom }}], 15);
	var mimapa = L.map('map').fitBounds( limites ); //fitBounds([ [40.712, -74.227],[40.774, -74.125] ]);
	
	
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
				zoomControl: true
    }).addTo(mimapa);
	
	L.geoJSON(pv_geojson,{
		pointToLayer: function (feature, latlng) {
			return L.circleMarker(latlng, estilo_marcadores);
		},
		onEachFeature: onEachFeature
	}).addTo(mimapa);

</script>
@endsection