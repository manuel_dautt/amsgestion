@extends('layouts.app')

@section('content')
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

   <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>

   <script src="{{ url('geojson/base_cves.js') }}"></script>

   <style type="text/css">
   .n_cve {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255,255,255,0.8);
        box-shadow: 0 0 15px rgba(0,0,0,0.2);
        border-radius: 5px;
    }
    .n_cve h4 {
        margin: 0 0 5px;
        color: #777;
    }
    </style>

    <div class="row">
        <div class="col-md-8">
            <div>
                <h4><small>Mapa Oportunidades Comerciales2 :: MOC</small></h4>
                <small>Variacion CVE :</small>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ver_cve" id="orden_cve1" value="recarga" onclick="chequeo(this)" checked>
                    <small>Recarga</small>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ver_cve" id="orden_cve2" value="gross" onclick="chequeo(this)">
                    <small>Gross</small>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="ver_cve" id="orden_cve3" value="mkshare" onclick="chequeo(this)">
                    <small>Marketshare</small>
                </div>

                <input type="hidden" id="flag_option" value="recarga">
                <div id="map" style="height: 500px"></div>
            </div>

        </div>

        <div class="col-md-4" id="contenido">
            <div class="table-responsive" id="InfoCVE">
                <table class="table table-sm">
                    <thead>
                        <tr><th>CVE:</th></tr>
                    </thead>
                    <tbody>
                        <tr><td colspan=4>Pareto Usuarios: </td></tr>
                        <tr><td colspan=4>Fecha: </td></tr>
                        <tr><td>Region: </td><td>Dpto: </td><td>MkSh: </td><td>%var: </td></tr>
                        <tr><td>Usr Tot.: </td><td>Usr Pre.: </td><td>Usr Pos.: </td><td>Usr Base.: </td></tr>
                        <tr><td>Activ.: </td><td>Gross BTS.: </td><td>PortPre.: </td><td>PortPos.: </td></tr>
                        <tr><td>Recargas.: </td><td>Usr. Rec.: </td><td>Trans Rec.: </td><td>Tick. Prom.: </td></tr>
                        <tr><td>Usr Tot.: </td><td>Usr Pre.: </td><td>Usr Pos.: </td><td>Usr Base.: </td></tr>
                        <tr><td>PDA.: </td><td>%var.: </td><td>PDV.: </td><td>%var.: </td></tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>



    <script type="text/javascript">


    //Cargando la capa que contendra nuestro mapa
    let mimapa = L.map('map').setView([4.570868, -74.2973328],5);
    //let mimapa = L.map('map',{maxZoom: 20, minZoom: 1, maxBounds: limites}).setView([4.570868, -74.2973328],1);


    //en esta pagina puedo encontrar mas layers de mapas
    //https://leaflet-extras.github.io/leaflet-providers/preview/
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        zoomControl: false
    }).addTo(mimapa);

    var capas_mimapa = L.layerGroup().addTo(mimapa);

    var menucito = L.popup();

    var info = L.control();

    info.onAdd = function (mimapa) {
        this._div = L.DomUtil.create('div', 'n_cve'); // create a div with a class "n_cve"
        this.update();
        return this._div;
    };

    // method that we will use to update the control based on feature properties passed
    info.update = function (props) {
        CriterioFalso = '<h5>Info CVE</h5>'+'<p>Pase el mouse sobre CVE</p>';
        this._div.innerHTML =
                    (props ? //CriterioVerdadero
                            '<b>' + props.Name + '</b>'+ '<br />Pareto en Usr.: '+ props.resultados.es_pareto +
                            '<br />Usuarios: '+ props.resultados.usuarios +
                            '<br />Usr. Prepago: '+ props.resultados.usr_pre +
                            '<br />Usr. Pospago: '+ props.resultados.usr_pos +
                            '<br />Usr. Base Core: '+ props.resultados.usr_base_core +
                            '<hr />GrossPre: '+ props.resultados.gross_prepago + ' [' + props.resultados.v_gross_prepago +']'+
                            '<br />Recargas: ' + props.resultados.recargas + ' [' + props.resultados.v_recargas +']'+
                            '<br />MkShare : ' + props.resultados.mksh_tigo_act + ' [' + props.resultados.v_mksh_tigo +']'+'<hr>'
                        : CriterioFalso);
    };

    info.addTo(mimapa);

    mimapa.on('click', onMapClick);

    //TRABAJAR CON LA CAPA GeoJson DE CVEs
    var capa_cve = L.geoJSON(base_cves,{style: estilo_cve, onEachFeature: ventana_modal });

    //Agregamos esta capa a Mi Mapa
    capas_mimapa.addLayer(capa_cve);


    // CARGAMOS UNOS EVENTOS PARA CONTROLAR LOS RADIO BUTTONS
    // ESTOS HACEN UN RELOAD DEL GEOJSON DE CAPAS SEGUN LA VARIACION
    // DE RECARGAS, GROSS O MARKETSHARE
    function chequeo(componente){
        el = document.getElementById('flag_option');
        el.value = componente.value;
        capa_cve = L.geoJSON(base_cves, {style: estilo_cve, onEachFeature: ventana_modal});
        capas_mimapa.clearLayers();
        capas_mimapa.addLayer(capa_cve);
    }

    // CARGAMOS LOS ESTILOS DE LINEAS DEL CVE
    // DEPENDIENDO DEL CRITERIO DE SELECCION
    // POR ONDE SE QUIERA MIRAR
    function estilo_cve(feature) {
		let mColor = '';
        let el = document.getElementById('flag_option');
        criterio = el.value;

        switch (criterio){
            case 'recarga': dColor = define_color(parseFloat(feature.properties.resultados.v_recargas)); break;
            case 'gross': dColor = define_color(parseFloat(feature.properties.resultados.v_gross_prepago)); break;
            case 'mkshare': dColor = define_color(parseFloat(feature.properties.resultados.v_mksh_tigo)); break;
            default: dColor = "#0000ff"; break;
        }

		return {color: dColor, weight: 2, opacity: 1,};
	}

    //SEGUN EL CRITERIO DE SELECCION DEFINIMOS LAS VARIABLES Y LOS COLORES
    function define_color(dato){

        if (dato < 0.0) {
            mColor = "#E5BE01"
        }else if (dato > 0.0){
            mColor = "#0000ff";
        }else{
            mColor = "#0000ff";
        }

        return mColor;
    }

    function CapaAfuera(e){
        capa_cve.resetStyle(e.target);
        info.update();
    }

    function ventana_modal(feature, layer) {
		if (feature.properties && feature.properties.Name)
		{
            //console.log('layer');

            marco_NE = [layer._bounds._northEast.lat,layer._bounds._northEast.lng];
            marco_SW = [layer._bounds._southWest.lat,layer._bounds._southWest.lng];
            marco_TOTAL = {SW: marco_SW, NE: marco_NE} ;

            popup_capa = "";
            popup_capa = popup_capa + "<h5>"+feature.properties.Name+"</h5>";

            popup_capa = popup_capa + "<hr>";

            popup_capa = popup_capa + '<form id="cve_form" action="{{ route('mapas.ver.cve') }}" method="POST" >';
            popup_capa = popup_capa +    '@csrf';
            popup_capa = popup_capa + "<input type='hidden' name='cve_nombre' value='"+feature.properties.Name+"' >";
            popup_capa = popup_capa + "<input type='hidden' name='cve_orden' value='"+document.getElementById('flag_option').value+"' >";
            popup_capa = popup_capa + "<input type='hidden' name='SW_lat' value='"+marco_TOTAL['SW'][0]+"' >";
            popup_capa = popup_capa + "<input type='hidden' name='SW_lon' value='"+marco_TOTAL['SW'][1]+"' >";
            popup_capa = popup_capa + "<input type='hidden' name='NE_lat' value='"+marco_TOTAL['NE'][0]+"' >";
            popup_capa = popup_capa + "<input type='hidden' name='NE_lon' value='"+marco_TOTAL['NE'][1]+"' >";
            popup_capa = popup_capa + "<button type='submit' class='btn btn-sm btn-primary'>Ver CVE</button>";
            popup_capa = popup_capa + '</form>';

            //layer.bindPopup(popup_capa);
			layer.on({
                mouseover: function(e){
                    var layer = e.target;

                    layer.setStyle({
                        weight: 5,
                        color: '#666',
                        dashArray: '',
                        fillOpacity: 0.7
                    });

                    layer.bindPopup(feature.properties.Name);

                    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                        layer.bringToFront();
                    }

                    info.update(layer.feature.properties);
                },
                mouseout: CapaAfuera,
				click: function(){
                    marco_NE = [layer._bounds._northEast.lat,layer._bounds._northEast.lng];
                    marco_SW = [layer._bounds._southWest.lat,layer._bounds._southWest.lng];
                    marco_TOTAL = {SW: marco_SW, NE: marco_NE} ;
                    contenido_InfoCVE = cargar_contenido(feature.properties, marco_TOTAL);
                    var contenedor = document.getElementById('InfoCVE');
					contenedor.outerHTML = '<div class="table-responsive" id="InfoCVE">' + contenido_InfoCVE + '</div>';
				}
			});
        }
    }

    //Cargamos el div de contenido y resultados del CVE
    function cargar_contenido(propiedad, marco){

        //imagenes
        i_mksh_tigo = parseFloat(propiedad.resultados.v_mksh_tigo) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_mksh_claro = parseFloat(propiedad.resultados.v_mksh_claro) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_mksh_movistar = parseFloat(propiedad.resultados.v_mksh_movistar) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_activ_prepago=parseFloat(propiedad.resultados.v_activ_prepago) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_gross_prepago=parseFloat(propiedad.resultados.v_gross_prepago) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_recargas=parseFloat(propiedad.resultados.v_recargas) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_usr_recargando=parseFloat(propiedad.resultados.v_usr_recargando) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_transacciones_rec=parseFloat(propiedad.resultados.v_transacciones_rec) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_ticket_rec_prom=parseFloat(propiedad.resultados.v_ticket_rec_prom) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_cant_pdas=parseFloat(propiedad.resultados.v_cant_pdas) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_cant_pdvs=parseFloat(propiedad.resultados.v_cant_pdvs) >= 0 ? "class='bg-primary text-center'":"class='bg-warning text-center'";
        i_text_center = ' text-center';
        i_class_text_center = ' class="text-center"';

        var popupContent = '';
        popupContent = popupContent + '<form id="cve_form" action="{{ route('mapas.ver.cve') }}" method="POST" >';
        popupContent = popupContent +    '@csrf';
        popupContent = popupContent + "<input type='hidden' name='cve_nombre' value='"+propiedad.Name+"' >";
        popupContent = popupContent + "<input type='hidden' name='cve_orden' value='"+document.getElementById('flag_option').value+"' >";
        popupContent = popupContent + "<input type='hidden' name='SW_lat' value='"+marco['SW'][0]+"' >";
        popupContent = popupContent + "<input type='hidden' name='SW_lon' value='"+marco['SW'][1]+"' >";
        popupContent = popupContent + "<input type='hidden' name='NE_lat' value='"+marco['NE'][0]+"' >";
        popupContent = popupContent + "<input type='hidden' name='NE_lon' value='"+marco['NE'][1]+"' >";
        popupContent = popupContent + "<button type='submit' class='btn btn-sm btn-primary'>Ver CVE</button>";
        popupContent = popupContent + '</form>';
        popupContent = popupContent + '<small>';
        popupContent = popupContent + "<table class='table table-sm'><thead  class='bg-primary'><tr><th colspan=4>" + propiedad.Name + "</th></tr></thead><tbody>";
        popupContent = popupContent + "<tr><td colspan=2>Pareto Usuarios: "+propiedad.resultados.es_pareto+"</td><td colspan=2>Fecha: "+propiedad.resultados.fecha+"</tr>";
        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td>MS_MesAnt: </td><td"+i_class_text_center+">Var: </td><td>Region: </td><td>Dpto: </td></tr>";
        popupContent = popupContent + "<tr><td "+i_class_text_center+">"+propiedad.resultados.mksh_tigo_ant+"</td><td "+i_mksh_tigo+">"+propiedad.resultados.v_mksh_tigo+"</td><td>"+propiedad.resultados.regional+"</td><td>"+propiedad.resultados.dpto+"</td></tr>";
        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td>MkShare</td><td"+i_class_text_center+">Tigo</td><td"+i_class_text_center+">Claro</td><td"+i_class_text_center+">Movistar</td></tr>";
        popupContent = popupContent + "<tr><td>MesAct</td><td "+i_class_text_center+">"+propiedad.resultados.mksh_tigo_act+"</td><td "+i_class_text_center+">"+propiedad.resultados.mksh_claro+"</td><td "+i_class_text_center+">"+propiedad.resultados.mksh_movistar+"</td></tr>";
        popupContent = popupContent + "<tr><td>Var</td><td "+i_mksh_tigo+">"+propiedad.resultados.v_mksh_tigo+"</td><td "+i_mksh_claro+">"+propiedad.resultados.v_mksh_claro+"</td><td  "+i_mksh_movistar+">"+propiedad.resultados.v_mksh_movistar+"</td></tr>";

//        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td>Metas</td><td"+i_class_text_center+">Dealer</td><td"+i_class_text_center+">Retail</td><td"+i_class_text_center+">Tiendas</td></tr>";
//        popupContent = popupContent + "<tr><td>Pre</td><td "+i_class_text_center+">"+propiedad.resultados.meta_pre_dealer+"</td><td "+i_class_text_center+">"+propiedad.resultados.meta_pre_retail+"</td><td "+i_class_text_center+">"+propiedad.resultados.meta_pre_tiendas+"</td></tr>";
//        popupContent = popupContent + "<tr><td>Pos</td><td "+i_class_text_center+">"+propiedad.resultados.meta_pos_dealer+"</td><td "+i_class_text_center+">"+propiedad.resultados.meta_pos_retail+"</td><td  "+i_class_text_center+">"+propiedad.resultados.meta_pos_tiendas+"</td></tr>";

        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td "+i_class_text_center+">Usr Tot.: </td><td "+i_class_text_center+">Usr Pre.: </td><td "+i_class_text_center+">Usr Pos.: </td><td "+i_class_text_center+">Usr Base.: </td></tr>";
        popupContent = popupContent + "<tr><td "+i_class_text_center+">"+propiedad.resultados.usuarios+"</td><td "+i_class_text_center+">"+propiedad.resultados.usr_pre+"</td><td "+i_class_text_center+">"+propiedad.resultados.usr_pos+"</td><td "+i_class_text_center+">"+propiedad.resultados.usr_base_core+"</td></tr>";
        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td "+i_class_text_center+">Activ.: </td><td "+i_class_text_center+">Gross BTS.: </td><td "+i_class_text_center+">PortPre.: </td><td "+i_class_text_center+">PortPos.: </td></tr>";
        popupContent = popupContent + "<tr><td "+i_class_text_center+">"+propiedad.resultados.activ_prepago+"</td><td "+i_class_text_center+">"+propiedad.resultados.gross_prepago+"</td><td "+i_class_text_center+">"+propiedad.resultados.port_out_pre+"</td><td "+i_class_text_center+">"+propiedad.resultados.port_out_pos+"</td></tr>";
        popupContent = popupContent + "<tr><td "+i_activ_prepago+">"+propiedad.resultados.v_activ_prepago+"</td><td "+i_gross_prepago+">"+propiedad.resultados.v_gross_prepago+"</td><td "+i_class_text_center+">-</td><td "+i_class_text_center+">-</td></tr>";
        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td "+i_class_text_center+">Recargas.: </td><td "+i_class_text_center+">Usr. Rec.: </td><td "+i_class_text_center+">Transac.: </td><td "+i_class_text_center+">Tick. Prom.: </td></tr>";
        popupContent = popupContent + "<tr><td "+i_class_text_center+">"+propiedad.resultados.recargas+"</td><td "+i_class_text_center+">"+propiedad.resultados.usr_recargando+"</td><td "+i_class_text_center+">"+propiedad.resultados.transacciones_rec+"</td><td "+i_class_text_center+">"+propiedad.resultados.ticket_rec_prom+"</td></tr>";
        popupContent = popupContent + "<tr><td "+i_recargas+">"+propiedad.resultados.v_recargas+"</td><td "+i_usr_recargando+">"+propiedad.resultados.v_usr_recargando+"</td><td "+i_transacciones_rec+">"+propiedad.resultados.v_transacciones_rec+"</td><td "+i_ticket_rec_prom+">"+propiedad.resultados.v_ticket_rec_prom+"</td></tr>";
        popupContent = popupContent + "<tr class='bg-light font-weight-bold'><td "+i_class_text_center+">PDA.: </td><td "+i_class_text_center+">%var.: </td><td "+i_class_text_center+">PDV.: </td><td "+i_class_text_center+">%var.: </td></tr>";
        popupContent = popupContent + "<tr><td "+i_class_text_center+">"+propiedad.resultados.cant_pdas+"</td><td "+i_cant_pdas+">"+propiedad.resultados.v_cant_pdas+"</td><td "+i_class_text_center+">"+propiedad.resultados.cant_pdvs+"</td><td "+i_cant_pdvs+">"+propiedad.resultados.v_cant_pdvs+"</td></tr>";
        popupContent = popupContent + "</tbody></table>";
        popupContent = popupContent + '</small>';

        return popupContent;

    }

        //preparamos un componente que permite subir varias capas


    function onMapClick(e){
        var ubicacion = '<p>' + e.latlng.lat.toString() + "," +  e.latlng.lng.toString() + '</p>';
        var elemento = document.getElementById('contenido');
     //   menucito
     //       .setLatLng(e.latlng)
     //       .setContent('<H3>Has Hecho Click</H3><p>Las Coordenadas donde lo hiciste son</p>' + ubicacion + '<hr>')
     //       .openOn(mimapa);
    }

    </script>
@endsection
