@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Territorios</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('territorios.create') }}"><i class="fas fa-plus-circle"></i> Nuevo Territorio</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

@if ($message = Session::get('error'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <th>Pais</th>
        <th>Regional</th>
        <th>Territorio</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($territorios as $territorio)
      <tr>
        <td>{{ $territorio->regional->country->country_name }}</td>
        <td>{{ $territorio->regional->regional_name }}</td>
        <td>{{ $territorio->territory_name }}</td>
        <td>

          <form action="{{ route('territorios.destroy',$territorio->id) }}" method="POST">

            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Territorio" href="{{ route('territorios.show',$territorio->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Territorio" href="{{ route('territorios.edit',$territorio->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Territorio" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{!! $territorios->links() !!}

@endsection
