@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Ver Territorio</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('territorios.index') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Pais:</strong>
      {{ $territorio->regional->country->country_name }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Regional:</strong>
      {{ $territorio->regional->regional_name }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Nombre:</strong>
      {{ $territorio->territory_name }}
    </div>
  </div>
</div>

@endsection
