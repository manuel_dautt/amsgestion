@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Editar Territorio</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('territorios.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('territorios.update',$territorio->id) }}" method="POST">
  @csrf
  @method('PUT')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

      <div class="form-group">
        <strong>Pais:</strong>
        <select id="country_id" name="country_id" class="form-control" readonly>
          <option value="{{ $territorio->regional->country_id }}" selected>{{ $territorio->regional->country->country_name }}</option>
        </select>
      </div>
      <div class="form-group">
        <strong>Regional:</strong>
        <select id="regional_id" name="regional_id" class="form-control" readonly>
          <option value="{{ $territorio->regional_id }}" selected>{{ $territorio->regional->regional_name }}</option>
        </select>
      </div>
      <div class="form-group">
        <strong>Nombre Territorio:</strong>
        <input type="text" name="territory_name" value="{{ $territorio->territory_name }}" class="form-control" placeholder="Nombre Territorio">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
  </div>

</form>

@endsection
