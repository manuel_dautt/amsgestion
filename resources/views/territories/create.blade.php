@extends('layouts.app')

@section('content')
@include('scripts.crud_territorios.pais_regiones_2')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Crear Nuevo Territorio</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('territorios.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hubo algunos problemas con su entrada.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('territorios.store') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Seleccione Pais:</strong>

        @if(count($paises) > 1)
        <select id="country" name="country" class="form-control @error('country') is-invalid @enderror">
          @foreach($paises as $pais)
            <option value="{{ $pais['id'] }}">{{ $pais['country_name'] }}</option>
          @endforeach
        </select>
        @else
        <select id="country" name="country" class="form-control" readonly>
          @foreach($paises as $pais)
            <option value="{{ $pais['id'] }}" selected>{{ $pais['country_name'] }}</option>
          @endforeach
        </select>
        @endif

      </div>


      <div class="form-group">
        <strong>Seleccione Regional:</strong>

        <select id="regional" name="regional" class="form-control @error('regional') is-invalid @enderror">
          @foreach($regiones as $region)
            <option value="{{ $region['id'] }}">{{ $region['regional_name'] }}</option>
          @endforeach
        </select>

      </div>

      <div class="form-group">
        <strong>Nombre Nuevo Territorio:</strong>
        <input type="text" name="territory_name" class="form-control" placeholder="Ingrese Nombre">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Crear</button>
    </div>
  </div>

</form>

@endsection
