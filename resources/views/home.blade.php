@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card logueo">
                <div class="card-header logueo"></div>

                <div class="card-body">
                    @if ($message = Session::get('status'))
                        @include('layouts.alert_status')
                    @endif
                    @if ($message = Session::get('success'))
                        @include('layouts.alert_success')
                    @endif
                    @if ($message = Session::get('error'))
                        @include('layouts.alert_error')
                    @endif
                    <h4>Tablon de Notas</h4>
                    Hola {{ $nombre }}, Bienvenido!!!
					<img src="/img/AMS.jpg" class="img-fluid" alt="AMS TIGO">
					<img src="/img/AMS.jpeg" class="img-fluid" alt="AMS TIGO">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

