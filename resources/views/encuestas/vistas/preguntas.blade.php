@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>ENCUESTAS</h2>
	  <small><sup>Infopos Survey Builder <span class="badge badge-success">v.1.5.0</span></sup></small>

	  <div class="pull-right">
      @if($encuesta['tipo_encuesta_id'] == 1)
		<form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
            @csrf
            <input type="hidden" name="idpdv" value="{{$idpdv}}" />
            <button type="submit" class="btn btn-sm btn-primary">Seguir Gestionando Punto de Venta</button>
        </form>
	  @elseif($encuesta['tipo_encuesta_id'] == 2)
		<a class="btn btn-sm btn-primary" href="{{ route('home') }}">REGRESAR</a>
	  @else

	  @endif
	  </div>

    </div>
  </div>
</div>
<br>
<div class="row">
    @if ($message = Session::get('success'))
        @include('layouts.alert_success')
    @endif

    @if ($message = Session::get('error'))
        @include('layouts.alert_error')
    @endif
</div>

<div class="card">

    <div class="card-body">
		<h6 class="card-title">Encuesta: <strong>{{ $encuesta['nombre_encuesta']}}</strong></h6>
		<hr/>
        @if($datosEncuesta['preguntaAnterior'] > 0)
        <div class="pull-right">
            <form class="form-inline" action="{{ route('encuestas.pregunta.anterior') }}" method="POST">
                @csrf
                <input type="hidden" name="idpdv" value="{{$idpdv}}" />
                <input type="hidden" id="idEncuestaVisita" name="idEncuestaVisita" value="{{  $datosEncuesta['idEncuestaVisita'] }}">
                <input type="hidden" id="idEncuesta" name="idEncuesta" value="{{  $datosEncuesta['idEncuesta'] }}">
				<input type="hidden" id="version" name="version" value="{{  $datosEncuesta['version'] }}">
                <input type="hidden" id="encuestaCompletada" name="encuestaCompletada" value="{{  $datosEncuesta['encuestaCompletada'] }}">
                <input type="hidden" id="preguntaActual" name="preguntaActual" value="{{  $datosEncuesta['preguntaActual'] }}">
                <input type="hidden" id="preguntaAnterior" name="preguntaAnterior" value="{{  $datosEncuesta['preguntaAnterior'] }}">
				<input type="hidden" id="documentoDigital" name="documentoDigital" value="{{  $datosEncuesta['documentoDigital'] }}">

                <button type="submit" class="btn btn-sm btn-danger"><- Regresar a la Pregunta Anterior [{{ $datosEncuesta['preguntaAnterior'] }}]</button>
            </form>
        </div>
        @endif
    <hr/>
        <form class="form-inline" action="{{ route('encuestas.validar.guardar') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">

                <p class="h6 text-muted text-capitalize">[ {{ $preguntas['grupo']['grupo'] }} ]</p>
                <h6>{{ $preguntas['orden_pregunta'] }} - {{ $preguntas['texto_pregunta'] }}</h6>
                <small>{{ $preguntas['texto_ayuda'] }}</small>
				<br>
				<br>
                @include($opcion_html,['m_opciones' => $opciones])
				<hr>
              </div>



              <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="hidden" id="idPregunta" name="idPregunta" value="{{  $preguntas['id'] }}">
                <input type="hidden" id="requeridaPregunta" name="requeridaPregunta" value="{{  $preguntas['requerida'] }}">
                <input type="hidden" id="tipoPregunta" name="tipoPregunta" value="{{  $preguntas['tipo_pregunta_id'] }}">
                <input type="hidden" id="maxPregunta" name="maxPregunta" value="{{  $preguntas['tipo']['max_longitud'] }}">
                <input type="hidden" id="idEncuestaVisita" name="idEncuestaVisita" value="{{  $datosEncuesta['idEncuestaVisita'] }}">
                <input type="hidden" id="idEncuesta" name="idEncuesta" value="{{  $datosEncuesta['idEncuesta'] }}">
				<input type="hidden" id="version" name="version" value="{{  $datosEncuesta['version'] }}">
                <input type="hidden" id="encuestaCompletada" name="encuestaCompletada" value="{{  $datosEncuesta['encuestaCompletada'] }}">
                <input type="hidden" id="preguntaActual" name="preguntaActual" value="{{  $datosEncuesta['preguntaActual'] }}">
                <input type="hidden" id="preguntaAnterior" name="preguntaAnterior" value="{{  $datosEncuesta['preguntaAnterior'] }}">
				<input type="hidden" id="documentoDigital" name="documentoDigital" value="{{  $datosEncuesta['documentoDigital'] }}">

                <button type="submit" class="btn btn-sm btn-primary">
                    @if($preguntas['tipo_pregunta_id'] == 7)
                        Finalizar!!!
                    @else
                        Guardar y Siguiente Pregunta ->
                    @endif
                </button>


              </div>
            </div>
          </form>
    </div>
  </div>
</div>


@endsection
