<select id="pregunta_encuesta" name="pregunta_encuesta" class="form-control" {{$preguntas['requerida']=='S'?'required':''}}>
    <option value=0 selected data-peso=0>Elija Respuesta</option>
    @foreach($opciones as $opcion)
        <option value={{ $opcion['id'] }}
                @if($datosEncuesta['atras'] == 'T' AND $datosEncuesta['id_opc_resp'] == $opcion['id']) selected @endif
                data-peso={{ $opcion['peso'] }}>
            {{ $opcion['texto_opcion'] }}</option>
    @endforeach
</select>

<input
    type="hidden"
    id="idOpcion"
    name="idOpcion"
    value="{{ ($datosEncuesta['atras'] == 'T')?$datosEncuesta['id_opc_resp']:0 }}"
>

<input
    type="hidden"
    id="calificacion"
    name="calificacion"
    value="{{ ($datosEncuesta['atras'] == 'T')?$datosEncuesta['calif_resp']:0 }}"
>

<script>

    $( "#pregunta_encuesta" ).on('change', function() {
        $('#idOpcion').val(this.value);
        var peso = $('option:selected', this).data("peso");
        $('#calificacion').val(peso);
    });

</script>
