<!--
{( array_key_exists(1,$imagen_pv) ) ? $imagen_pv[1]['nombre'] : 'Sin Imagen Cargada'}
-->
<input
    type="file"
    accept="image/jpeg,image/png,image/jpg"
    name="imagen"
    class="form-control"
    placeholder="Imagen..."
>

<input
    type="hidden"
    name="pregunta_encuesta"
    data-peso={{ $opciones[0]['peso'] }}
    class="form-control" {{$preguntas['requerida']=='S'?'required':''}}
    value = "TRKCOMP_{{ $datosEncuesta['idEncuestaVisita'] }}"
>

<input type="hidden" id="idOpcion" name="idOpcion" value="{{ $opciones[0]['id'] }}">
<input type="hidden" id="calificacion" name="calificacion" value="{{ $opciones[0]['peso'] }}">
