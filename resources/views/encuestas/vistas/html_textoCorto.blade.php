<input
    type="text"
    name="pregunta_encuesta"
    data-peso={{ $opciones[0]['peso'] }}
    onkeypress="return check(event)"
    class="form-control" {{$preguntas['requerida']=='S'?'required':''}}
    @if($datosEncuesta['atras'] == 'T') value = "{{ $datosEncuesta['txt_opc_resp'] }}" @endif
>


<input type="hidden" id="idOpcion" name="idOpcion" value="{{ $opciones[0]['id'] }}">
<input type="hidden" id="calificacion" name="calificacion" value="{{ $opciones[0]['peso'] }}">

<script>
    function check(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z0-9 _-]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
</script>

