<select id="pregunta_encuesta" name="pregunta_encuesta[]" multiple class="form-control" {{$preguntas['requerida']=='S'?'':''}}>
    @foreach($opciones as $opcion)
        <option value={{ $opcion['id'] }}
                @if($datosEncuesta['atras'] == 'T' AND strpos($datosEncuesta['txt_opc_resp'],$opcion['texto_opcion']) !== false) selected @endif
                data-peso={{ $opcion['peso'] }}>
            {{ $opcion['texto_opcion'] }}
        </option>
    @endforeach
</select>
<br>
<p><small>Puede seleccionar mas de una opcion, si esta en un PC presione <span class="badge badge-dark">tecla CTRL</span></small></p>

@if($datosEncuesta['atras'] == 'T')
    @php
        $opcionAjustada = '';
        foreach($opciones as $itemOpc){
            if($datosEncuesta['atras'] == 'T'){
                if(strpos($datosEncuesta['txt_opc_resp'],$itemOpc['texto_opcion']) !== false){
                    $opcionAjustada .= $itemOpc['id'].'|';
                }
            }
        }
        echo "<input type='hidden' id='idOpcion' name='idOpcion' value='".substr($opcionAjustada,0,strlen($opcionAjustada)-1)."'>";
    @endphp
    <input type="hidden" id="calificacion" name="calificacion" value="{{ $datosEncuesta['calif_resp'] }}">
@else
    <input type="hidden" id="idOpcion" name="idOpcion" value="">
    <input type="hidden" id="calificacion" name="calificacion" value="0">
@endif



<script>

    $('#pregunta_encuesta').change(function(e) {

		var valores = $(e.target).val();
		var pesos = [];

		$(e.target).children(':selected').each((idx, el)=>{
			pesos.push(el.dataset.peso);
		});

        id_opciones = '';

        valores.forEach(valor => {
            id_opciones += valor;
            id_opciones += '|';

        });

        id_opciones = id_opciones.substring(0,id_opciones.length-1);

        $('#idOpcion').val(id_opciones);

		suma_pesos = 0;
		pesos.forEach(peso => {
            suma_pesos += parseFloat(peso);
        });

		$('#calificacion').val(suma_pesos);
    });

</script>

