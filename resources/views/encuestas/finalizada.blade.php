@extends('layouts.app')

@section('content')

<div class="card-body">

    <h6 class="card-title"><strong>Encuesta Exitosa</strong></h6>
    <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
        @csrf
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <h4>ENCUESTA FINALIZADA</h4>
              <h6>Su encuesta finalizo correctamente</h6>
			  @if( in_array($f_encuesta, [3,6,7,8,9,10]) )
				<input type="hidden" id="idpdv" name="idpdv" value="{{ $idpdv }}">
			    <button type="submit" class="btn btn-md btn-primary">Volver a la Gestion del Punto Nuevamente</button>
			  @else
				<a class="btn btn-sm btn-primary" href="{{ route('home') }}">FINALIZAR</a>
			  @endif
          </div>
        </div>



      </form>
</div>

@endsection
