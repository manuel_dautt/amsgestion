@extends('layouts.app')

@section('content')

<div class="card-body">

    <h6 class="card-title"><strong>Tiene Activo un Tracking de la Competencia ID->{{ $tiene_trackings['id'] }}</strong></h6>
	<div class="row">
	  <div class="col-xs-12 col-sm-12 col-md-12">
		  <h4>{{ $ver_enc['nombre_encuesta'] }}-v{{ $ver_enc['version'] }}</h4>
		  <h6>Aun esta activa y pendiente a un cierre</h6>
		  <hr>
		  Fecha Inicio Encuesta: {{ $tiene_trackings['created_at'] }}<br>

		  <a class="btn btn-sm btn-success" href="{{ route('encuestas.oficiales',['id_encuesta' => $ver_enc['id'], 'version' => $ver_enc['version'], 'idpdv' => 0, 'doc_acomp' => $tiene_trackings['id'] ]) }}">
			CONTINUAR
		  </a>

		  <a class="btn btn-sm btn-primary" href="{{ route('home') }}">SALIR</a>
	  </div>
	</div>
</div>

@endsection
