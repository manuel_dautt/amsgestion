@extends('layouts.app')

@section('content')

  <div class="card-body">
      <div class="col-lg-12">
          <div class="pull-left">
              <h6>CHECK LIST SALIDA A RUTA</h6>
          </div>
          <div class="pull-right">
		  EJECUTA: {{ $usuario['name'] }}<br>
		  FECHA: {{ date('Y-m-d') }}<br>
          </div>
		  <a class="btn btn-sm btn-primary" href="{{ route('home') }}">REGRESAR</a>
      </div>
  </div>
	<hr>
  <div class="card-body">
	<div class="col-lg-12">
          <div class="pull-left">
              <h6>EMPLEADO A QUIEN LE REALIZA EL CHEQUEO</h6>
          </div>
          <div class="pull-right">
                <input id='cc_empleado' name='cc_empleado' type="number" placeholder='Ingrese Cedula' class="form-control input-sm">
				<input id='busca_cc_empl' name='busca_cc_empl' type="button" class="btn btn-sm btn-primary" value="BUSCAR">
          </div>
		  <div class="pull-right">
			<div id='nom_empl' name='nom_empl'></div>
			<div id='cargo_empl' name='cargo_empl'></div>
			<div id='role_empl' name='role_empl'></div>
		  </div>
		  <form class="form-inline" action="{{ route('encuestas.crear.check.salida') }}" method="POST">
			@csrf
			  <input type="hidden" id='usr_auth' name='usr_auth' value="{{ $usuario['id'] }}">
			  <input type="hidden" id='usr_watch' name='usr_watch' value=''>
			  <input type="hidden" id='id_encuesta' name='id_encuesta' value=4>
			  <input type="hidden" id='version' name='version' value=1>
			  <input type="hidden" id='tipo_visita' name='tipo_visita' value=2>
			  <input type="hidden" id='idpdv' name='idpdv' value=0>
			  <div id='boton_envio' name='boton_envio'></div>
			</form>
    </div>
	
  </div>


<script type="text/javascript">
$(document).ready(function(){
		
	$('#busca_cc_empl').click(function(){
		cedula = $('#cc_empleado').val();
		if(cedula.length > 0){
			$.get('/ajax/empleado/'+cedula, function(data) {
				if(data.estado){
					$('#usr_watch').val(data.datos.id_usuario);
					$('#nom_empl').html('EMPLEADO: '+data.datos.nombre+' '+data.datos.apellido);
					$('#cargo_empl').html('CARGO: '+data.datos.cargo);
					$('#role_empl').html('ROLE: '+data.datos.role);
					
					$('#idpdv').val(cedula);
					
					$('#boton_envio').html('<button type="submit" class="btn btn-sm btn-primary">Chequear</button>');
					
				}else{
					swal("Empleado No Encontrado!", "La Cedula Ingresada NO petenece a un empleado registrado, por favor reintente!!", "error");
				}
            });
		}else{
			swal("Documento Invalido!", "Por favor ingrese una cedula correcta", "error");
		}
	});
});
</script>
@endsection
