@extends('layouts.app')

@section('content')

  <div class="card-body">
      <div class="col-lg-12">
          <div class="pull-left">
              <h6>TRACKING COMPETENCIA</h6>
          </div>
          <div class="pull-right">
		  EJECUTA: {{ $usuario['name'] }}<br>
		  FECHA: {{ date('Y-m-d') }}<br>
          </div>
		  <a class="btn btn-sm btn-primary" href="{{ route('home') }}">REGRESAR</a>
      </div>
  </div>
	<hr>
  <div class="card-body">
	<div class="col-lg-12">
          <div class="pull-left">
              <h6>MUNICIPIO DONDE SE REGISTRA ACTIVIDAD</h6>
          </div>
          <div class="pull-right">
                <input id='busca_municipio' name='busca_municipio' type="text" placeholder='Ingrese municipio' class="form-control input-sm">
				<input id='btn_busca_municipio' name='btn_busca_municipio' type="button" class="btn btn-sm btn-primary" value="BUSCAR">
          </div>
		  <div class="pull-right">
			<div id='n_regional' name='n_regional'></div>
			<div id='n_departamento' name='n_departamento'></div>
			<div id='n_ciudad' name='n_ciudad'></div>
          </div>
          <div name="tbl_geo" id="tbl_geo"></div>
		  <form class="form-inline" action="{{ route('encuestas.crear.tracking.competencia') }}" method="POST">
			@csrf
			  <input type="hidden" id='usr_auth' name='usr_auth' value="{{ $usuario['id'] }}">
			  <input type="hidden" id='id_encuesta' name='id_encuesta' value={{ $ver_enc['id']}}>
			  <input type="hidden" id='version' name='version' value={{ $ver_enc['version']}}>
			  <input type="hidden" id='tipo_visita' name='tipo_visita' value={{ $ver_enc['tipo_encuesta_id']}}>
              <input type="hidden" id='idpdv' name='idpdv' value=0>
              <input type="hidden" id='regional' name='regional' value=''0''>
              <input type="hidden" id='departamento' name='departamento' value=''0''>
              <input type="hidden" id='ciudad' name='ciudad' value=''>
			  <div id='boton_envio' name='boton_envio'></div>
			</form>
    </div>

  </div>


<script type="text/javascript">
$(document).ready(function(){

	$('#tbl_geo').on('click',function(e){

        if(e.target.dataset.regional){
            $('#regional').val(e.target.dataset.regional);
            $('#departamento').val(e.target.dataset.dpto);
            $('#ciudad').val(e.target.dataset.ciudad);

            $('#n_regional').html('REGIONAL: '+e.target.dataset.regional);
            $('#n_departamento').html('DPTO: '+e.target.dataset.dpto);
            $('#n_ciudad').html('CIUDAD: '+e.target.dataset.ciudad);

            $('#tbl_geo').empty();
            $('#busca_municipio').val('');

            $('#boton_envio').html('<button type="submit" class="btn btn-sm btn-primary">Chequear</button>');
        }

    });



    $('#btn_busca_municipio').click(function(){
		municipio = $('#busca_municipio').val();

		if(municipio.length > 0){
			$.get('/ajax/tracking/'+municipio, function(data) {
				if(data.estado){

                    $('#tbl_geo').empty();
                    tablaDatos = $("<small><table id='tbl_datos' name='tbl_datos'></table></small>");
                    tablaDatos.attr({id:"idtabla"});

                    tablaEncab = "<tr><td>ACCION</td><td>RESULTADOS</td></tr>";
                    tablaDatos.append(tablaEncab);

                    $.each(data.datos, function(inx, elem){
                        reg = elem.regional;
                        dpt = elem.departamento;
                        ciu = elem.ciudad;

                        tablaFila = "<td>"+reg+" | "+dpt+" | "+ciu+"</td></tr>";
                        tablaFila = "<tr><td id='td_"+inx+"' data-regional='"+reg+"' data-dpto='"+dpt+"' data-ciudad='"+ciu+"' class='btn btn-sm btn-primary'>[ + ]</td>"+tablaFila;
                        tablaDatos.append(tablaFila);
                    });
                    $('#tbl_geo').append(tablaDatos);


				}else{
					swal("Municipio No Encontrado!", "El Municipio Ingresado NO coincide con alguno registrado, por favor reintente!!", "error");
				}
            });
		}else{
			swal("Informacion MAl Ingresada!", "Por favor ingrese informacion correcta", "error");
		}
	});



});
</script>
@endsection
