@extends('layouts.app')

@section('content')

<div class="card-body">
	
    <h6 class="card-title"><strong>Tiene Activo Check_o_Acompañamiento {{ $tiene_checks['id'] }}</strong></h6>
	<div class="row">
	  <div class="col-xs-12 col-sm-12 col-md-12">
		  <h4>{{ $ver_enc['nombre_encuesta'] }}-v{{ $ver_enc['version'] }}</h4>
		  <h6>Aun esta activa y pendiente a un cierre</h6>
		  <hr>
		  Empleado Encuestado: {{ $empleado['name'].' '.$empleado['last_name'] }}<br>
		  Documento del Encuestado: {{ $empleado['document'] }}<br>
		  Fecha Inicio Encuesta: {{ $tiene_checks['created_at'] }}<br>
		  @if(in_array($ver_enc['id'], [4]))
		  <a class="btn btn-sm btn-success" href="{{ route('encuestas.oficiales',['id_encuesta' => $ver_enc['id'], 'version' => $ver_enc['version'], 'idpdv' => $empleado['document'],  'doc_acomp' => $tiene_checks['id'] ]) }}">
			CONTINUAR
		  </a>
		  @endif
    	  <a class="btn btn-sm btn-danger" href="{{ route('encuestas.finalizar.coachings',['id' => $tiene_checks['id'] ]) }}">
			@if(in_array($ver_enc['id'], [4]))
				DESCARTAR
			@elseif(in_array($ver_enc['id'], [6,7]))
				FINALIZAR ACOMPAÑAMIENTO
			@endif
		  </a>
		  <a class="btn btn-sm btn-primary" href="{{ route('home') }}">SALIR</a>
	  </div>
	</div>
</div>

@endsection