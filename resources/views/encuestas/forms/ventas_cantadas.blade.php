@extends('layouts.app')

@section('content')

<form>
    <h2>VENTAS CANTADAS</h2>
    <hr/>
    <div class="form-group">
        <h5>SIMCARDS</h5>
        <label for="simcard_01">Inventario Inicial de Simcards</label>
        <input type="number" class="form-control" id="simcard_01" aria-describedby="Hlpsimcard1" placeholder="Saldo inicial">
        <small id="Hlpsimcard1" class="form-text text-muted">Ingrese Inventario Inicial de Simcards en elpunto de venta, si no existe ingrese Cero</small>

        <label for="simcard_02">Colocacion/Venta Simcard</label>
        <input type="number" class="form-control" id="simcard_02" aria-describedby="Hlpsimcard2" placeholder="Ingrese Colocacion">
        <small id="Hlpsimcard2" class="form-text text-muted">Ingrese lo que coloca o vende en el punto de venta en esta visita, si no realiza colocacion ingrese Cero</small>

        <label for="simcard_03">Activaciones Realizadas</label>
        <input type="number" class="form-control" id="simcard_03" aria-describedby="Hlpsimcard3" placeholder="Ingrese Activaciones">
        <small id="Hlpsimcard3" class="form-text text-muted">Ingrese la cantidad de activaciones realizadas por el punto de venta desde la ultima visita, si no realiza activacion ingrese Cero</small>
    </div>
    <hr/>
    <div class="form-group">
        <h5>RECARGAS</h5>
        <label for="recarga_01">Inventario Inicial de recargas</label>
        <input type="number" class="form-control" id="recarga_01" aria-describedby="Hlprecarga1" placeholder="Saldo inicial">
        <small id="Hlprecarga1" class="form-text text-muted">Ingrese Inventario Inicial de recargas en elpunto de venta, si no existe ingrese Cero</small>

        <label for="recarga_02">Colocacion/Venta recarga</label>
        <input type="number" class="form-control" id="recarga_02" aria-describedby="Hlprecarga2" placeholder="Ingrese Colocacion">
        <small id="Hlprecarga2" class="form-text text-muted">Ingrese lo que coloca o vende en el punto de venta en esta visita, si no realiza colocacion ingrese Cero</small>

        <label for="recarga_03">Recargas Realizadas</label>
        <input type="number" class="form-control" id="recarga_03" aria-describedby="Hlprecarga3" placeholder="Ingrese Recargas">
        <small id="Hlprecarga3" class="form-text text-muted">Ingrese monto vendido en recargas desde la ultima visita, si no realiza activacion ingrese Cero</small>
    </div>
    <hr/>
    <div class="form-group">
        <h5>TELEFONOS</h5>
        <label for="telefonos_01">Inventario Inicial de telefonos</label>
        <input type="number" class="form-control" id="telefonos_01" aria-describedby="Hlptelefonos1" placeholder="Saldo inicial">
        <small id="Hlptelefonos1" class="form-text text-muted">Ingrese Inventario Inicial de telefonos en el punto de venta, si no existe ingrese Cero</small>

        <label for="telefonos_02">Colocacion/Venta telefonos</label>
        <input type="number" class="form-control" id="telefonos_02" aria-describedby="Hlptelefonos2" placeholder="Ingrese Colocacion">
        <small id="Hlptelefonos2" class="form-text text-muted">Ingrese lo que coloca o vende en el punto de venta en esta visita, si no realiza colocacion ingrese Cero</small>

        <label for="telefonos_03">Ventas Realizadas</label>
        <input type="number" class="form-control" id="telefonos_03" aria-describedby="Hlptelefonos3" placeholder="Ingrese Ventas">
        <small id="Hlptelefonos3" class="form-text text-muted">Ingrese la cantidad de ventas realizadas por el punto desde la ultima visita, si no realiza ventas ingrese Cero</small>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection
