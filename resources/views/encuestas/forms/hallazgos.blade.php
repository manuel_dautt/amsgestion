@extends('layouts.app')

@section('content')

<form>
    <h2>HALLAZGOS</h2>
    <hr/>
    <h4>inventario</h4>
    <div class="form-group">
        <h5>ESTADO POP</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="visibilidad1" id="visibilidad1_1" value="OK">
            <label class="form-check-label text-dark" for="visibilidad1_1">
              Buen Estado
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="visibilidad1" id="visibilidad1_2" value="MAL">
            <label class="form-check-label text-dark" for="visibilidad1_2">
              Mal Estado
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="visibilidad1" id="visibilidad1_3" value="N/A">
            <label class="form-check-label text-dark" for="visibilidad1_3">
              No Aplica
            </label>
          </div>
    </div>
    <div class="form-group">
        <h5>VIGENCIA POP</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="visibilidad2" id="visibilidad2_1" value="OK">
            <label class="form-check-label text-dark" for="visibilidad2_1">
              Vigente
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="visibilidad2" id="visibilidad2_2" value="MAL">
            <label class="form-check-label text-dark" for="visibilidad2_2">
              No Vigencia
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="visibilidad2" id="visibilidad2_3" value="N/A">
            <label class="form-check-label text-dark" for="visibilidad2_3">
              No Aplica
            </label>
          </div>
    </div>
    <hr/>

    <h4>INVENTARIO</h4>
    <div class="form-group">
        <h5>TIENE RECARGA</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="inventario1" id="inventario1_1" value="OK">
            <label class="form-check-label text-dark" for="inventario1_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="inventario1" id="inventario1_2" value="MAL">
            <label class="form-check-label text-dark" for="inventario1_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="inventario1" id="inventario1_3" value="N/A">
            <label class="form-check-label text-dark" for="inventario1_3">
              No Aplica
            </label>
          </div>
    </div>
    <div class="form-group">
        <h5>TIENE SIMCARD</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="inventario2" id="inventario2_1" value="OK">
            <label class="form-check-label text-dark" for="inventario2_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="inventario2" id="inventario1_2" value="MAL">
            <label class="form-check-label text-dark" for="inventario2_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="inventario2" id="inventario2_3" value="N/A">
            <label class="form-check-label text-dark" for="inventario2_3">
              No Aplica
            </label>
          </div>
    </div>
    <hr/>

    <h4>ENTRENAMIENTO</h4>
    <div class="form-group">
        <h5>CONOCE LA OFERTA PREPAGO</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="entrenamiento1" id="entrenamiento1_1" value="OK">
            <label class="form-check-label text-dark" for="entrenamiento1_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="entrenamiento1" id="entrenamiento1_2" value="MAL">
            <label class="form-check-label text-dark" for="entrenamiento1_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="entrenamiento1" id="entrenamiento1_3" value="N/A">
            <label class="form-check-label text-dark" for="entrenamiento1_3">
              No Aplica
            </label>
          </div>
    </div>
    <div class="form-group">
        <h5>CONOCE LA OFERTA POSPAGO</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="entrenamiento2" id="entrenamiento2_1" value="OK">
            <label class="form-check-label text-dark" for="entrenamiento2_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="entrenamiento2" id="entrenamiento1_2" value="MAL">
            <label class="form-check-label text-dark" for="entrenamiento2_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="entrenamiento2" id="entrenamiento2_3" value="N/A">
            <label class="form-check-label text-dark" for="entrenamiento2_3">
              No Aplica
            </label>
          </div>
    </div>
    <hr/>

    <h4>COMISIONES</h4>
    <div class="form-group">
        <h5>RECIBE PAGO DE COMISIONES</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="comisiones1" id="comisiones1_1" value="OK">
            <label class="form-check-label text-dark" for="comisiones1_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="comisiones1" id="comisiones1_2" value="MAL">
            <label class="form-check-label text-dark" for="comisiones1_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="comisiones1" id="comisiones1_3" value="N/A">
            <label class="form-check-label text-dark" for="comisiones1_3">
              No Aplica
            </label>
          </div>
    </div>
    <div class="form-group">
        <h5>ENTIENDE EL PAGO DE SU COMISION</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="comisiones2" id="comisiones2_1" value="OK">
            <label class="form-check-label text-dark" for="comisiones2_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="comisiones2" id="comisiones1_2" value="MAL">
            <label class="form-check-label text-dark" for="comisiones2_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="comisiones2" id="comisiones2_3" value="N/A">
            <label class="form-check-label text-dark" for="comisiones2_3">
              No Aplica
            </label>
          </div>
    </div>
    <hr/>

    <h4>BASICOS</h4>
    <div class="form-group">
        <h5>CONOCE AL VENDEDOR</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="basicos1" id="basicos1_1" value="OK">
            <label class="form-check-label text-dark" for="basicos1_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="basicos1" id="basicos1_2" value="MAL">
            <label class="form-check-label text-dark" for="basicos1_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="basicos1" id="basicos1_3" value="N/A">
            <label class="form-check-label text-dark" for="basicos1_3">
              No Aplica
            </label>
          </div>
    </div>
    <div class="form-group">
        <h5>VENDEDOR CUMPLE FRECUENCIA VISITA</h5>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="basicos2" id="basicos2_1" value="OK">
            <label class="form-check-label text-dark" for="basicos2_1">
              SI
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="basicos2" id="basicos1_2" value="MAL">
            <label class="form-check-label text-dark" for="basicos2_2">
              NO
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="basicos2" id="basicos2_3" value="N/A">
            <label class="form-check-label text-dark" for="basicos2_3">
              No Aplica
            </label>
          </div>
    </div>
    <hr/>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection
