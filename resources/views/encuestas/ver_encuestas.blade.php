<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h4>Encuestas de Visitas</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <h6 class="card-title"><strong>
            @if(!$validaVisita['hay_visita_pendiente'])
            INICIE VISITA
            @else
            ENCUESTAS DISPONIBLES
            @endif
        </strong></h6>
        @if(!$validaVisita['hay_visita_pendiente'])
        NO Se ha iniciado una visita para este punto de venta
        @else
        <p>Puede elegir entre las siguientes encuestas: </p>
        <div class="col-md-8 offset-md-1 col-sm-8 offset-sm-1">
			@forelse($encuestasDisponibles as $encDisp)
				@if(in_array($encDisp['id'], [4,6,7]))
					<p><a class="btn btn-sm btn-primary text-light text-uppercase" href="{{ route('encuestas.oficiales',['id_encuesta' => $encDisp['id'], 'version' => $encDisp['version'], 'idpdv' => $idpdv,  'doc_acomp' => $tiene_chequeos['id'] ]) }}">{{ $encDisp['nombre_encuesta'] }} PARA {{ $encDisp['canal']['channel_name'] }}</a></p>
				@else
					<p><a class="btn btn-sm btn-primary text-light text-uppercase" href="{{ route('encuestas.oficiales',['id_encuesta' => $encDisp['id'], 'version' => $encDisp['version'], 'idpdv' => $idpdv ]) }}">{{ $encDisp['nombre_encuesta'] }} PARA {{ $encDisp['canal']['channel_name'] }}</a></p>
				@endif
			@empty
				Sin Encuestas Disponibles
			@endforelse
        </div>
        <div class="col-md-4 offset-md-1 col-sm-4 offset-sm-1">

            <!--
            <div class="btn-group text-dark" role="group">
                <button id="btnEncuestas" type="button" class="text_dark btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Encuestas Disponibles
                </button>
                <div class="dropdown-menu text-dark" aria-labelledby="btnEncuestas">
                    <a class="dropdown-item text-dark" href="{ route('encuestas.oficiales',['id_encuesta' => 1 ]) }">Cierre de Ventas</a>
                    <hr/>
                    <a class="dropdown-item" href="{route('encuestas.formularios',['visita' => $validaVisita['id_visita_pendiente'], 'form' => 1])}">Plan de Accion</a>
                    <a class="dropdown-item" href="{route('encuestas.formularios',['visita' => $validaVisita['id_visita_pendiente'], 'form' => 2])}">Ventas Cantadas</a>
                    <a class="dropdown-item" href="{route('encuestas.formularios',['visita' => $validaVisita['id_visita_pendiente'], 'form' => 3])}">Hallazgos (FD11)</a>
                    <a class="dropdown-item" href="{route('encuestas.formularios',['visita' => $validaVisita['id_visita_pendiente'], 'form' => 4])}">AMS</a>
                </div>
            </div>
			
            <p><a class="btn btn-sm btn-primary text-light text-uppercase" href="{{ route('encuestas.oficiales',['id_encuesta' => 1, 'version' => 1, 'idpdv' => $idpdv ]) }}">ENCUESTA CIERRE DE VENTAS</a></p>			
			<p><a class="btn btn-sm btn-primary text-light text-uppercase" href="{{ route('encuestas.oficiales',['id_encuesta' => 5, 'version' => 1, 'idpdv' => $idpdv ]) }}">ENCUESTA RETAIL HARD-DISCOUNTER</a></p>
            <hr/>

            -->
        </div>
        @endif
    </div>
</div> <!-- FIN CARD buscar x idpdv -->
<br/>
