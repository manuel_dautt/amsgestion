@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Crear Nuevo Cargo</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('cargos.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hubo algunos problemas con su entrada.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('cargos.store') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Nombre Nuevo Cargo:</strong>
        <input type="text" name="position_name" class="form-control" placeholder="Ingrese Nombre">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Descripcion Nuevo Cargo:</strong>
        <input type="text" name="description" class="form-control" placeholder="Ingrese Descripion">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>NNivel del Cargo:</strong>
        <input type="text" name="gm_level" class="form-control" placeholder="Ingrese Nivel">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Crear</button>
    </div>
  </div>

</form>

@endsection
