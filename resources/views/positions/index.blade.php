@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Cargos</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('cargos.create') }}"><i class="fas fa-plus-circle"></i> Nuevo Cargo</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Nivel</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($cargos as $cargo)
      <tr>
        <td>{{ $cargo->position_name }}</td>
        <td>{{ $cargo->description }}</td>
        <td>{{ $cargo->gm_level }}</td>
        <td>

          <form action="{{ route('cargos.destroy',$cargo->id) }}" method="POST">

            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Cargo" href="{{ route('cargos.show',$cargo->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar cargo" href="{{ route('cargos.edit',$cargo->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Cargo" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>

          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{!! $cargos->links() !!}

@endsection
