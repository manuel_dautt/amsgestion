@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Roles</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('roles.create') }}"><i class="fas fa-plus-circle"></i> Nuevo Role</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
@include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Nombre</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($roles as $role)
      <tr>
        <!--<td>{{ $role->id }}</td>-->
        <td>{{ $role->name }}</td>
        <td>

          <form action="{{ route('roles.destroy',$role->id) }}" method="POST">

            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Role" href="{{ route('roles.show',$role->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Role" href="{{ route('roles.edit',$role->id) }}"><i class="fas fa-edit"></i></a>
            <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
            <a class="btn btn-sm btn-outline-dark" data-toggle="tooltip" data-placement="top" title="Borrar Role" href="{{ route('roles.permisos',$role->id) }}">Permisos <span
                class="badge badge-pill badge-info">{{ $role->permissions_count }}</span></a>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{!! $roles->links() !!}

@endsection
