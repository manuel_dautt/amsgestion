@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Role -> Permisos</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('roles.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Nombre Role:</strong>
      {{ $role->name }}
    </div>
  </div>
</div>

<form id="form_permisos" name="form_permisos" action="{{ route('roles.asignar.permiso') }}" method="POST">
  @csrf


  <input type="hidden" id="id_role" name="id_role" value="{{ $role->id }}">


  <div class="row">
    <div class="list-group list-group-flush">
      @foreach ($permisos as $permiso)
      <div class="list-group-item list-group-item-secundary bg-color">
        <input type="checkbox" name="permisox[]" id="permisox[]" value="{{ $permiso->id }}" {{ in_array($permiso->id, $rolePermissions) ? 'checked' : ''}}>
        <label class="text-dark" for="permisox[]">{{ $permiso->name }}</label>
      </div>
      @endforeach
    </div>
  </div>
  <button type="submit" id='btn_asigna' name='btn_asigna' class="btn btn-md btn-success">ASIGNAR PERMISOS >>></button>
</form>

@endsection
