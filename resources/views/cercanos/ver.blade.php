<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Google Geocoder</title>
<style>
body,
body * {
    margin:0;
    font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
}

.buscador {
    text-align:center;
    padding:30px 0px;
}

.buscador #direccion {
    margin:10px auto;
    width:100%;
    padding:7px;
    max-width:250px;
}

.buscador #buscar {
    margin:0 auto;
    max-width:250px;
    padding:7px;
    color:#fefefe;
    background:#1329bd;
    border:2px solid #0a4e9e;
    cursor:pointer;
}


.buscador #regresar {
    margin:0 auto;
    max-width:250px;
    padding:7px;
    color:#fefefe;
    background:#FF0000;
    border:2px solid #0a4e9e;
    cursor:pointer;
}
</style>
</head>
<body>

<div class="buscador">
    <h2>Ingrese una dirección</h2>
    <input type="text" id="direccion">
    <div id="buscar">Buscar</div>
    <a href="{{ url('/') }}"><div id="regresar">Regresar Infopos</div></a>
</div>

<div id="mapa-geocoder" class="mapa" style="width:100%;display:block;position:absolute;background:#fbfbfb;"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $(window).on("load resize", function() {
        var alturaBuscador = $(".buscador").outerHeight(true),
            alturaVentana = $(window).height(),
            alturaMapa = alturaVentana - alturaBuscador;


            $("#mapa-geocoder").css("height", alturaMapa+"px");
        var longitud;
        var latitud;

		var kml = 'http://infopostigo.com/kml/cve.kml';

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
              function(positions,latitud, longitud){
                  latitud = positions.coords.latitude;
                  longitud = positions.coords.longitude;

                  var map = new google.maps.Map(document.getElementById('mapa-geocoder'), {
                    center: new google.maps.LatLng(positions.coords.latitude, positions.coords.longitude),
                    maxZoom:21,
                    minZoom:2,
                    zoom: 15,
                    mapTypeControl: false,
                    mapTypeId: 'mi-estilo'    //google.maps.MapTypeId.ROADMAP
                  });

				  var kmlLayer = new google.maps.KmlLayer(kml, {
						suppressInfoWindows: true,
						preserveViewport: false,
						map: map
					});

                    /*
                    Estas son las opciones sacadas del Wizard de Google
                    */
                    var featureOpts = [
                        {
                        "featureType": "administrative",
                        "elementType": "labels",
                        "stylers": [
                            { "visibility": "on" }
                        ]
                        },{
                        "featureType": "water",
                        "stylers": [
                            { "color": "#A2D4E7" }
                        ]
                        },{
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            { "visibility": "on" },
                            { "color": "#F34600" }
                        ]
                        },{
                        featureType: 'poi',
                        elementType: 'labels',
                        stylers: [{"visibility": "off"}]
                        },{
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            { "visibility": "on" },
                            { "color": "#E5E5E5" }
                        ]
                        },{
                        "featureType": "road",
                        "stylers": [
                            { "visibility": "on" }
                        ]
                        }
                    ];
                    /*
                    Con ellas creamos un StyleMapType, con el nombre que hemos utilizado a la hora de crear el mapa en el
                    parámetro mapTypeId
                    */
                    var styledMapOptions = {
                    name: 'Mapa Tigo Cerca de ti'
                    };
                    var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
                    /*
                    Definimos el estilo del mapa como el nuestro
                    */
                    map.mapTypes.set("mi-estilo", customMapType);

                    var contentString = '<div><h3>Su Ubicacion Actual</h3><h5>Coordenadas:lat:'+latitud+' lng:'+longitud+'</h5><p>'+latitud+','+longitud+'</p></div>';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });


                  var marker = new google.maps.Marker({
                      map: map,
                      icon: 'https://www.google.com/mapfiles/arrow.png',
                      position: new google.maps.LatLng(positions.coords.latitude,positions.coords.longitude)
                  });

                  google.maps.event.addListener(map, 'click', function(){
                    infoWindow.close();
                    });

                  marker.addListener('click', function() {
                      infowindow.open(map, marker);
                  });

                  var Circulo500 = new google.maps.Circle({
                        strokeColor: '#FFDD63',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FFDD63',
                        fillOpacity: 0.35,
                        map: map,
                        center: new google.maps.LatLng(positions.coords.latitude,positions.coords.longitude),
                        radius: 500
                    });

                  $.get('/ajax/cercanos/puntos/lat/'+latitud+'/lng/'+longitud,
                    function(data) {

                        $.each(data.datos, function(index, r_array) {
                            var contentString = '<div><h3>'+r_array['nombre_punto']+'</h3><h5>Coordenadas:lat:'+r_array['lat']+' lng:'+r_array['lng']+'</h5><p>'+r_array['direccion']+'</p><p>'+r_array['tel_propietario']+'</p></div>';
                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });
							
							var kmlLayer = new google.maps.KmlLayer('http://infopostigo.com/kml/cve.kml', {
								suppressInfoWindows: true,
								preserveViewport: false,
								map: map
							});

                            var marker = new google.maps.Marker({
                                map: map,
                                icon: 'https://maps.google.com/mapfiles/kml/paddle/blu-circle-lv.png',
                                position: new google.maps.LatLng(r_array['lat'],r_array['lng'])
                            });

                            marker.addListener('click', function() {
                                infowindow.open(map, marker);
                            });

                        });
                    }
                );


              },
              function(errors){
                switch(errors.code){
              				case errors.PERMISSION_DENIED:
              					mensajeError = "Ocurrio un Error: Permiso denegado por el usuario."
              					break;
              				case errors.POSITION_UNAVAILABLE:
              					mensajeError = "Ocurrio un Error: Posici\363n no disponible."+" "+error.message;
              					break;
              				case errors.TIMEOUT:
              					mensajeError = "Ocurrio un Error: Desconexi\363n por tiempo."
              					break;
              				case errors.UNKNOWN_ERROR:
              					mensajeError = "Ocurrio un Error: Error desconocido."+" "+error.message;
              					break;
              		}
                  alert(mensajeError);
                  console.log(errors);
              },{maximumAge:60000, timeout: 4000});

        }else {
          alert('Actualiza el navegador web para usar el API de localización');
        }


    });


    function localizar(elemento,direccion) {
        var geocoder = new google.maps.Geocoder();

        var map = new google.maps.Map(document.getElementById('mapa-geocoder'), {
            maxZoom:21,
            minZoom:3,
            zoom: 15,
            mapTypeControl: false,
            mapTypeId: 'mi-estilo'    //google.maps.MapTypeId.ROADMAP
        });

        /*
        Estas son las opciones sacadas del Wizard de Google
        */
        var featureOpts = [
            {
            "featureType": "administrative",
            "elementType": "labels",
            "stylers": [
                { "visibility": "on" }
            ]
            },{
            "featureType": "water",
            "stylers": [
                { "color": "#A2D4E7" }
            ]
            },{
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                { "visibility": "on" },
                { "color": "#F34600" }
            ]
            },{
            featureType: 'poi',
            elementType: 'labels',
            stylers: [{"visibility": "off"}]
            },{
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "color": "#E5E5E5" }
            ]
            },{
            "featureType": "road",
            "stylers": [
                { "visibility": "on" }
            ]
            }
        ];
        /*
        Con ellas creamos un StyleMapType, con el nombre que hemos utilizado a la hora de crear el mapa en el
        parámetro mapTypeId
        */
        var styledMapOptions = {
        name: 'Mapa Tigo Cerca de ti'
        };
        var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
        /*
        Definimos el estilo del mapa como el nuestro
        */
        map.mapTypes.set("mi-estilo", customMapType);

        geocoder.geocode({'address': direccion}, function(results, status) {
            if (status === 'OK') {
                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();

                var contentString = '<div><h3>Nueva Ubicacion:'+direccion+'</h3><h5>Coordenadas:lat:'+resultados_lat+' lng:'+resultados_long+'</h5><p>'+resultados_lat+','+resultados_long+'</p></div>';
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    icon: 'https://www.google.com/mapfiles/arrow.png',
                    position: results[0].geometry.location
                });

                google.maps.event.addListener(map, 'click', function(){
                    infoWindow.close();
                });

                marker.addListener('click', function() {
                      infowindow.open(map, marker);
                  });


                  var Circulo500 = new google.maps.Circle({
                        strokeColor: '#FFDD63',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FFDD63',
                        fillOpacity: 0.35,
                        map: map,
                        center: results[0].geometry.location,
                        radius: 500
                    });

                $.get('/ajax/cercanos/puntos/lat/'+resultados_lat+'/lng/'+resultados_long,
                    function(data) {

                        $.each(data.datos, function(index, r_array) {
                            var contentString = '<div><h3>'+r_array['nombre_punto']+'</h3><h5>Coordenadas:lat:'+r_array['lat']+' lng:'+r_array['lng']+'</h5><p>'+r_array['direccion']+'</p><p>'+r_array['tel_propietario']+'</p></div>';
                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });

                            var marker = new google.maps.Marker({
                                map: map,
                                icon: 'https://maps.google.com/mapfiles/kml/paddle/blu-circle-lv.png',
                                position: new google.maps.LatLng(r_array['lat'],r_array['lng'])
                            });

                            google.maps.event.addListener(map, 'click', function(){
                                closeInfoWindow();
                            });

                            marker.addListener('click', function() {
                                infowindow.open(map, marker);
                            });
                        });
                    }
                );


            } else {
                var mensajeError = "";
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                alert(mensajeError);
            }
        });

    }

    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGSsfc1_xHWuqlWVi0PrVAQ27m3Csusf8", function() {
        $("#buscar").click(function() {
            var direccion = $("#direccion").val();
            if (direccion !== "") {
                localizar("mapa-geocoder", direccion);
            }
        });
    });

});
</script>
</body>
</html>
