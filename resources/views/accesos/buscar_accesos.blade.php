@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Accesos a Infopos</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('home') }}"> Regresar</a>
    </div>
  </div>
</div>


<form action="{{ route('accesos.ver.accesos') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Ingrese Cedula Empleado:</strong>
        <input type="text" id="cedula_empleado" name="cedula_empleado" value="{{ old('cedula_empleado') }}" class="form-control @error('cedula_empleado') is-invalid @enderror" placeholder="Ingrese cedula">
        @error('cedula_empleado')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
      </div>
      <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Buscar</button>
    </div>
  </div>

</form>

@endsection
