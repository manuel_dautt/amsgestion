@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Editar Regional</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('regionales.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('regionales.update',$regional->id) }}" method="POST">
  @csrf
  @method('PUT')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

      <div class="form-group">
        <strong>Pais:</strong>
        <select id="country_id" name="country_id" class="form-control" readonly="readonly">
          @foreach($paises as $pais)
            <option value="{{ $pais['id'] }}" selected>{{ $pais['country_name'] }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <strong>Nombre Regional:</strong>
        <input type="text" name="regional_name" value="{{ $regional->regional_name }}" class="form-control" placeholder="Nombre Regional">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
  </div>

</form>

@endsection
