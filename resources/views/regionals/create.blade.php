@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Crear Nueva Regional</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('regionales.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hubo algunos problemas con su entrada.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('regionales.store') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Seleccione Pais:</strong>

        @if(count($paises) > 1)
        <select id="country_id" name="country_id" class="form-control @error('country') is-invalid @enderror">
          @foreach($paises as $pais)
            <option value="{{ $pais['id'] }}">{{ $pais['country_name'] }}</option>
          @endforeach
        </select>
        @else
        <select id="country_id" name="country_id" class="form-control" readonly="readonly">
          @foreach($paises as $pais)
            <option value="{{ $pais['id'] }}" selected>{{ $pais['country_name'] }}</option>
          @endforeach
        </select>
        @endif

      </div>
      <div class="form-group">
        <strong>Nombre Nueva Regional:</strong>
        <input type="text" name="regional_name" class="form-control" placeholder="Ingrese Nombre">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Crear</button>
    </div>
  </div>

</form>

@endsection
