@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Regionales</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('regionales.create') }}"><i class="fas fa-plus-circle"></i> Nuevs Regional</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

@if ($message = Session::get('error'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <th>Pais</th>
        <th>Regional</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($regionales as $regional)
      <tr>
        <td>{{ $regional->country->country_name }}</td>
        <td>{{ $regional->regional_name }}</td>
        <td>

          <form action="{{ route('regionales.destroy',$regional->id) }}" method="POST">

            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Regional" href="{{ route('regionales.show',$regional->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Regional" href="{{ route('regionales.edit',$regional->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Regional" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{!! $regionales->links() !!}

@endsection
