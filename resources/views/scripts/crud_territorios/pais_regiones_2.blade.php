<script type="text/javascript">
$(document).ready(function(){

//***********************************************
// TRAER LAS REGIONALES DE UN PAIS SELECCIONADO
//***********************************************
$('#country').change(function(){
        pais = $(this).val();

       $.get('/ajax/pais/'+pais, 

       		function(data) {
  					var regional = $('#regional');
  					regional.empty();
          			
     				$.each(data.regiones, function(index, r_array) {
  			            regional.append("<option value='"+ r_array.id +"'>" + r_array.regional_name + "</option>");
  			        });
  				});
  });

});
</script>


  