<script>
    //***********************************************
    // TRAER CIRCUITOS DMS CON AUTOCOMPLETADO
    //***********************************************
    $(document).ready(function() {
       $( "#circuito" ).autocomplete({

           source: function(request, response) {
               $.ajax({
               url: "{{url('/ajax/circuito/autocompletar')}}",
               data: {
                       term : request.term
                },
               dataType: "json",
               success: function(data){
                  var resp = $.map(data,function(obj){
                       console.log(obj.circuito);
                       return obj.circuito;
                  });

                  response(resp);
               }
           });
       },
       minLength: 3
    });
   });

   </script>
