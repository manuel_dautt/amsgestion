<script type="text/javascript">
    $(document).ready(function(){

    //***********************************************
    // ASIGNAR Y DESASIGNAR PERMISOS A LOS ROLES
    //***********************************************

        $('#btn_asigna').on('click',function(){

            valor = $('#select_asigna').val();

            if(!valor){
                return false;
            }

            $('#tipo_mov').val('ASIGNA');

            $('#form_permisos').submit();
        });

        $('#btn_desasigna').on('click',function(){

            valor = $('#select_desasigna').val();

            if(!valor){
                return false;
            }

            $('#tipo_mov').val('DESASIGNA');

            $('#form_permisos').submit();
        });

    });
</script>
