<script type="text/javascript">
$(document).ready(function(){

//********************************************************************
// VERIFICAR MANUALMENTE EL CORREO LUEGO DE LA CREACION DEL USUARIO
//********************************************************************
    $('#chequear').click(function(){

        var d = new Date();
        
        var month = d.getMonth()+1;
        var day = d.getDate();

        var fecha = d.getFullYear() + '-' + 
                    (month<10 ? '0' : '') + month + '-' +
                    (day<10 ? '0' : '') + day +  ' ' +
                    d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        
        $('#email_verified').val(fecha);
        $('#is_email_verified').val(fecha);
        swal("Buen Trabajo!", "Se realizo la validacion del correo "+ fecha, "success")
    });
    $('#no_chequear').click(function(){
        $('#email_verified').val('');
        $('#is_email_verified').val('');
        swal("Buen Trabajo!", "Se elimino el check de validacion del correo", "success")
    });
});
</script>
