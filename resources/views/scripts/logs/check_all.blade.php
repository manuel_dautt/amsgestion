<script type="text/javascript">


    //***********************************************
    // Check Habilitar o Deshabilitar Todas de Cedula
    //***********************************************

    function habilitar(value)
    {
        if(value==true)
        {
            // habilitamos
            $( "#una" ).prop( "checked", false );
            $('#cedula').val('Todas');
            $( "#cedula" ).hide();
        }else if(value==false){
            // deshabilitamos
            $( "#una" ).prop( "checked", true );
            $('#cedula').val('');
            $( "#cedula" ).show();
        }

    }

    //***********************************************
    // Check Permitir una Cedula
    //***********************************************

    function habilitar_una(value)
    {
        if(value==true)
        {
            // habilitamos
            $( "#todos" ).prop( "checked", false );
            $('#cedula').val('');
            $( "#cedula" ).show();
        }else if(value==false){
            // deshabilitamos
            $( "#todos" ).prop( "checked", true );
            $('#cedula').val('Todas');
            $( "#cedula" ).hide();
        }

    }

</script>
