<script type="text/javascript">
$(document).ready(function(){

    v_regional = $('#regional').val();

    if(v_regional > 0){
      $('#regional').trigger('change');
    }

    //***********************************************
    // TRAER LAS REGIONALES DE UN PAIS SELECCIONADO
    //***********************************************
    $('#country').change(function(){
        pais = $(this).val();

        $.get('/ajax/pais/'+pais,

            function(data) {
                    var regional = $('#regional');
                    regional.empty();

                    $.each(data.regiones, function(index, r_array) {
                        regional.append("<option value='"+ r_array.id +"'>" + r_array.regional_name + "</option>");
                    });
                });
    });


    //**************************************************
    // TRAER LOS TERRITORIOS DE UNA REGION SELECCIONADA
    //**************************************************
    function loadTerritory(){

        region = $('#regional').val();

        $.get('/ajax/region/'+region, function(data) {

                var old = $('#territory').data('old') != '' ? $('#territory').data('old') : '';
                var territory = $('#territory');
                territory.empty();

                $.each(data.territorios, function(index, r_array) {
                    territory.append("<option value='"+ r_array.id +"' " +  (old == r_array.id ? 'selected' : '')+ ">" + r_array.territory_name + "</option>");
                });
            }
        );
    }

    loadTerritory();

    $('#regional').on('change',loadTerritory);

    $('#bday').datepicker({
        header: true,
        modal: true,
        footer: true,
        format: 'yyyy-mm-dd',
        weekStartDay: 1,
        uiLibrary: 'bootstrap4'
    });


});
</script>


