$.get('/ajax/cercanos/puntos/lat/{lat}/lng/{lng},
       	function(data) {
  			var regional = $('#regional');
  			regional.empty();

     		$.each(data.regiones, function(index, r_array) {
  		        regional.append("<option value='"+ r_array.id +"'>" + r_array.regional_name + "</option>");
  		    });
        }
     );
