@extends('layouts.app')

@section('content')
@include('scripts.auth.pais_regiones')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card logueo">
                <div class="card-header logueo"></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <h4 class="form-group row">
                            <p><i class="fas fa-address-card"></i> FORMULARIO DE REGISTRO</p>
                        </h4>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">NOMBRE</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">APELLIDO</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="document" class="col-md-4 col-form-label text-md-right">DOCUMENTO</label>

                            <div class="col-md-6">
                                <input id="document" type="number" class="form-control @error('document') is-invalid @enderror" name="document" value="{{ old('document') }}" required autocomplete="document" autofocus>

                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-MAIL</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cell" class="col-md-4 col-form-label text-md-right">CELULAR</label>

                            <div class="col-md-6">
                                <input id="cell" type="number" class="form-control @error('cell') is-invalid @enderror" name="cell" value="{{ old('cell') }}" required autocomplete="cell">

                                @error('cell')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">PASSWORD</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">CONFIRMA PASSWORD</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <!--
                        <div class="form-group row">
                            <label for="bday" class="col-md-4 col-form-label text-md-right">CUMPLEAÑOS</label>

                            <div class="col-md-6">
                                <input id="bday" type="text" class="form-control @error('bday') is-invalid @enderror" name="bday" value="{{ old('bday') }}">

                                @error('bday')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    -->
                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">PAIS</label>

                            <div class="col-md-6">
	                            	@if(count($paises) > 1)
                                        <select id="country" name="country" class="form-control @error('country') is-invalid @enderror">
                                            @foreach($paises as $pais)
                                                <option value="{{ $pais['id'] }}">{{ $pais['country_name'] }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select id="country" name="country" class="form-control" disabled="dishabled">
                                            @foreach($paises as $pais)
                                                <option value="{{ $pais['id'] }}" selected>{{ $pais['country_name'] }}</option>
                                            @endforeach
                                        </select>
                                    @endif

                                @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="regional" class="col-md-4 col-form-label text-md-right">REGIONAL</label>

                            <div class="col-md-6">

                            	<select id="regional" name="regional" class="form-control @error('regional') is-invalid @enderror">
                            		@foreach($regiones as $region)
                            		<option  value="{{ $region['id'] }}" {{ old('regional')==$region['id'] ? 'selected' : ''  }}>{{ $region['regional_name'] }}</option>
                            		@endforeach
                            	</select>

                                @error('regional')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="territory" class="col-md-4 col-form-label text-md-right">TERRITORIO</label>

                            <div class="col-md-6">

                            <select id="territory" data-old="{{ old('territory') }}" name="territory" class="form-control @error('territory') is-invalid @enderror">
                            		@foreach($territorios as $territorio)
                            		<option value="{{ $territorio['id'] }}" {{ old('territory')==$territorio['id'] ? 'selected' : ''  }}>{{ $territorio['territory_name'] }}</option>
                            		@endforeach
                            	</select>

                                @error('territory')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="channel" class="col-md-4 col-form-label text-md-right">CANAL</label>

                            <div class="col-md-6">

                            	<select id="channel" name="channel" class="form-control @error('channel') is-invalid @enderror">
                            		@foreach($canales as $canal)
                            		<option value="{{ $canal['id'] }}" {{ old('channel')==$canal['id'] ? 'selected' : ''  }}>{{ $canal['channel_name'] }}</option>
                            		@endforeach
                            	</select>

                                @error('channel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="position" class="col-md-4 col-form-label text-md-right">CARGO</label>

                            <div class="col-md-6">

                                <select id="position" name="position" class="form-control @error('position') is-invalid @enderror">
                                    @foreach($cargos as $cargo)
                                    <option value="{{ $cargo['id'] }}" {{ old('position')==$cargo['id'] ? 'selected' : ''  }}>{{ $cargo['position_name'] }}</option>
                                    @endforeach
                                </select>

                                @error('position')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
