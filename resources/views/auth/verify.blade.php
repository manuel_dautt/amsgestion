@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header logueo"></div>

                <div class="card-body">

                    <h4 class="form-group row">
                        <p><i class="fas fa-envelope-open-text"></i> CONFIRME SU EMAIL </p>
                    </h4>
                    <h5 class="form-group row">
                        <p>Verifique su direccion de Correo Eletronico!!! </p>
                    </h5>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nuevo vinculo de verificacion ha sido enviado a su direccion de correo electronico.') }}
                        </div>
                    @endif

                    {{ __('Antes de continuar, revise su correo electrónico para obtener un enlace de verificación.') }}
                    {{ __('Si no recibiste el correo electrónico') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('haga clic aquí para solicitar otro') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
