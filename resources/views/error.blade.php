@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card logueo">
                <div class="card-header logueo"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-success" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                    <h4>ERROR EN APLICACION</h4>
                    {{ $error }}
                    <p>Por favor Notifique a algun administrador de Infopos</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
