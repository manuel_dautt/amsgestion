@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Empleados</h2>
    </div>
    <div>
      <form class="form-inline" action="{{ route('empleados.filtros') }}" method="POST">
        @csrf
        <select class="form-control mb-2 mr-sm-2" id="filtro_empleados" name="filtro_empleados">
          <option value='' selected>-Seleccione Filtro-</option>
          <option value='cedula'>Buscar Cedula</option>
          <option value='nombre'>Buscar Nombre</option>
          <option value='apellido'>Buscar Apellido</option>
          <!--
                        <option value='Checks'>Ya Validados</option>
                        <option value='NoChecks'>Sin Validar</option>
                    -->
        </select>

        <label class="sr-only" for="text_filtro">Filtro</label>
        <div class="input-group mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-search"></i></div>
          </div>
          <input type="text" class="form-control" name='texto_filtro' id="texto_filtro" placeholder="Texto Filtro">
        </div>

        <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
      </form>
      <a class="btn btn-sm btn-success" href="#"><i class="fas fa-plus-circle"></i></a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

@if ($message = Session::get('error'))
  @include('layouts.alert_error')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <th>Documento</th>
        <th>Nombre (Activo)</th>
        <th>Regional / Canal</th>
        <th>Cargo/Roles <span data-toggle="tooltip" data-placement="top" title="Valida si ya se verifico el email">(email check)</span></th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($empleados as $empleado)
      <tr>
        <td>{{ $empleado->document }}</td>
        <td>{{ $empleado->name .' '.$empleado->last_name }}
           (@if($empleado->status == 'A')
                <i class="fas fa-check text-success"></i>
            @else
                <i class="fas fa-ban text-danger"></i>
            @endif)
        </td>
        <td>{{ $empleado->territory->regional->regional_name }} / <br />
          <span class="text-lowercase badge badge-pill badge badge-light">
            @foreach ($empleado->channels as $channel)
              @if ($loop->first)
                [
              @endif
              {{ $channel->channel_name }}
              @if ($loop->last)
                ]
                @continue
              @endif
              ,
            @endforeach
          </span>
        </td>
        <td>{{ $empleado->position->position_name }}/
          (
          @if($empleado->user->email_verified_at)
            <span class="text-success" data-toggle="tooltip" data-placement="top" title="Email ya verificado">
              <i class="fas fa-envelope-open-text"></i>
            </span>
          @else
            <span class="text-danger" data-toggle="tooltip" data-placement="top" title="No ha verificado Email">
              <i class="fas fa-envelope"></i>
            </span>
          @endif
          )<br />
          <span class="text-lowercase badge badge-pill badge badge-light">{{ $empleado->user->getRoleNames() }}</span>
        </td>
        <td>

          <form action="{{ route('empleados.destroy',$empleado->id) }}" method="POST">

            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Empleado" href="{{ route('empleados.show',$empleado->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Empleado" href="{{ route('empleados.edit',$empleado->id) }}"><i class="fas fa-edit"></i></a>

            <!--button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button-->
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
{!! $empleados->links() !!}


@endsection
