@extends('layouts.app')

@section('content')
@include('scripts.crud_empleados.verificar_correo')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Editar Empleado</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('empleados.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('empleados.update',$empleado->id) }}" method="POST">
  @csrf
  @method('PUT')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

      <div class="form-group">
        <strong>Documento:</strong>
        <input type="number" name="document" value="{{ $empleado->document }}" class="form-control" placeholder="documento">
      </div>

      <div class="form-group">
        <strong>Nombre:</strong>
        <input type="text" name="name" value="{{ $empleado->name .' '.$empleado->last_name }}" class="form-control" placeholder="Nombre Canal" readonly>
      </div>

      <div class="form-group">
        <strong>Regional:</strong>
        <input type="text" name="regional_name" value="{{ $empleado->territory->regional->regional_name }}" class="form-control" placeholder="Nombre Canal" readonly>
      </div>

      <div class="form-group">
        <strong>Correo:</strong>
        <input type="text" name="email" value="{{ $empleado->user->email }}" class="form-control" placeholder="Nombre Canal" readonly>
      </div>

      <div class="form-group">
        <strong>Celular:</strong>
        <input type="number" name="cell" value="{{ $empleado->phone }}" class="form-control" placeholder="Numero Movil">
      </div>

      <div class="form-group">
        <strong>Chequeado:</strong>
        <input type="text" id="email_verified" name="email_verified" value="{{ $empleado->user->email_verified_at }}" class="form-control" placeholder="Aun NO Chequeado" readonly>
        <button type="button" name='chequear' id='chequear' class="btn btn-success">Chequeado</button>
        <button type="button" name='no_chequear' id='no_chequear' class="btn btn-danger">Quitar Check</button>
        <input type="hidden" id="is_email_verified" name="is_email_verified" value="{{ $empleado->user->email_verified_at }}">
      </div>

      <div class="form-group">
        <strong>Cargo:</strong>

        <select id="position_id" name="position_id" class="form-control @error('regional') is-invalid @enderror">
          @foreach($cargos as $cargo)
            <option value="{{ $cargo['id'] }}" {{ $cargo['id'] == $empleado->position_id ? 'selected':'' }}>{{ $cargo['position_name'] }}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <strong>Role Principal:</strong>

        <select id="role_id" name="role_id" class="form-control @error('regional') is-invalid @enderror">
          @foreach($roles as $role)
            <option value="{{ $role['id'] }}" {{ $role['id'] == $role_asig[0]['id'] ? 'selected':'' }}>{{ $role['name'] }}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <strong>Role Principal Actual:</strong>
        <input type="text" name="role_empleado" value="{{ $role_asig[0]['name'] }}" class="form-control" placeholder="Sin Rol Asignado" readonly>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
  </div>

</form>

@endsection
