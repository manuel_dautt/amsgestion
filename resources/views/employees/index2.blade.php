@extends('layouts.app')

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Empleados</h2>
            </div>
            <div>
                <form class="form-inline" action="{{ route('empleados.filtros') }}" method="POST">
                    @csrf
                    <select class="form-control mb-2 mr-sm-2" id="filtro_empleados" name="filtro_empleados">
                        <option value='' selected>-Seleccione Filtro-</option>
                        <option value='cedula'>Buscar Cedula</option>
                        <option value='nombre'>Buscar Nombre</option>
                        <option value='apellido'>Buscar Apellido</option>
                    <!--
                        <option value='Checks'>Ya Validados</option>
                        <option value='NoChecks'>Sin Validar</option>
                    -->
                    </select>

                    <label class="sr-only" for="text_filtro">Filtro</label>
                    <div class="input-group mb-2 mr-sm-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-search"></i></div>
                        </div>
                        <input type="text" class="form-control" name='texto_filtro' id="texto_filtro" placeholder="Texto Filtro">
                    </div>

                    <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
                </form>
                <a class="btn btn-sm btn-success" href="#"><i class="fas fa-plus-circle"></i></a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        @include('layouts.alert_success')
    @endif
   <small>
    <table class="table table-bordered">
        <tr>
            <th>Documento</th>
            <th>Nombre (status)</th>
            <th>Regional</th>
            <th>Cargo</th>
            <th>CHK</th>
            <th>Acciones</th>
        </tr>
        @foreach ($usuarios as $usuario)
        <tr>
            <td>{{ $usuario->employee->document }}</td>
            <td>{{ $usuario->employee->name  }} ({{$usuario->employee->status}})</td>
            <td>{{ $usuario->employee->territory->regional->regional_name }}</td>
            <td>{{ $usuario->employee->position->position_name }}</td>
            <td>
                @if($usuarios->email_verified_at)
                    <p class="text-success"><i class="fas fa-check-circle fa-2x"></i></p>
                @else
                    <p class="text-danger"><i class="fas fa-user-edit fa-2x"></i></p>
                @endif
            </td>
            <td>

                <form action="{{ route('empleados.destroy',$usuario->employee->id) }}" method="POST">

                    @csrf
                    @method('DELETE')
                    <a class="btn btn-sm btn-info" href="{{ route('empleados.show',$usuario->employee->id) }}">
                    <i class="fas fa-search"></i></a>
                    <a class="btn btn-sm btn-primary" href="{{ route('empleados.edit',$usuario->employee->id) }}"><i class="fas fa-edit"></i></a>

                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    </small>
    {!! $usuarios->links() !!}

@endsection
