@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Ver Empleado</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('empleados.index') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Documento:</strong>
      {{ $empleado->document }}
    </div>
    <div class="form-group">
      <strong>Nombre Completo:</strong>
      {{ $empleado->name .' '. $empleado->last_name }}
    </div>
    <div class="form-group">
      <strong>Correo:</strong>
      {{ $empleado->user->email }}
    </div>
    <div class="form-group">
      <strong>Telefono:</strong>
      {{ $empleado->phone }}
    </div>
    <div class="form-group">
      <strong>Chequeado:</strong>
      {{ is_null($empleado->user->email_verified_at) ? 'No Chequeado':'Chequeado' }}
    </div>
    <div class="form-group">
      <strong>Cargo:</strong>
      {{ $empleado->position->position_name }}
    </div>
    <div class="form-group">
      <strong>Role Principal:</strong>
      {{ $role['name'] }}
    </div>
    <div class="form-group">
      <strong>Regional:</strong>
      {{ $empleado->territory->regional->regional_name }}
    </div>
    <div class="form-group">
      <strong>Territorio:</strong>
      {{ $empleado->territory->territory_name }}
    </div>

        <form action="{{ route('empleados.destroy',$empleado->id) }}" method="POST">
            @csrf
            @method('DELETE')

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Inactivar Empleado" class="btn btn-sm btn-danger">
                <i class="fas fa-trash-alt"></i>
                Inactivar Empleado
            </button>
        </form>

  </div>
</div>

@endsection
