@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Trade Tipos de Piezas</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('tipos.create') }}"><i class="fas fa-plus-circle"></i> Nuevo Tipo de Pieza</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Nombre Tipo Pieza</th>
        <th>Clase Pieza</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tipos as $tipo)
      <tr>
        <!--<td>{{ $tipo->id }}</td>-->
        <td>{{ $tipo->nombre_pieza }}</td>
        <td>{{ $tipo->clase->nombre_clase }}</td>
        <td>

          <form action="{{ route('tipos.destroy',$tipo->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Tipo Pieza" href="{{ route('tipos.show',$tipo->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Tipo Pieza" href="{{ route('tipos.edit',$tipo->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Tipo Pieza" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

{!! $tipos->links() !!}

@endsection
