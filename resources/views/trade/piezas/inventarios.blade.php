@extends('layouts.app')

@section('content')

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

@if ($message = Session::get('error'))
  @include('layouts.alert_error')
@endif

<h3>INVENTARIO PIEZAS IMPLEMENTACION</h3>
<h5>{{ $datos['nombre_implementacion'] }}</h5>
<div class="row">
	<div class="card flex-row flex-wrap">
		<div class="card-header border-1">
			<img src="https://infopostigo.com/storage/{{ $pieza['imagen'] }}" alt="ImagenTrade" height="64" width="64">
		</div>
		<div class="card-block px-2 border-1">
			<div class="col">
				<small>
				{{ $pieza['nombre'] }}<br/>
				{{ $pieza['contenido'] }}<br/>
				<b>{{ number_format($pieza['inventario']) }}</b> Pieza(s)
				</small>
			</div>
			<div class="col">
				<form action="{{ route('trade.implementacion.seleccionada') }}" method="POST">
				  @csrf
					<input type="hidden" name="implementacion_id" value="{{ $datos['implementacion_id'] }}" />
					<div class="col-xs-12 col-sm-12 col-md-12">
					  <button type="submit" class="btn btn-primary">Regresar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<hr>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('trade.inventario.ajustar') }}" method="POST">
    @csrf
    <small>
    <h5>AJUSTAR CANTIDADES</h5>
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <select name="regional" class="form-control">
                <option value="" selected>SELECCIONE REGIONAL</option>
                @foreach($regionales as $region)
                    <option value = {{ $region['id'] }}>{{ $region['regional_name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
            <input type="number" name="cantidad" value="" class="form-control" placeholder="CANTIDAD" />
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <button type="submit" class="btn btn-primary">Ajustar Inventario Pieza</button>

            <input type="hidden" name="impl_pieza" value="{{ $datos['id_implementacion_pieza'] }}" />
            <input type="hidden" name="usuario" value="{{ $datos['usuario'] }}" />
        </div>
    </div>
    </small>
</form>
<hr>
<h5>INVENTARIO X REGIONAL</h5>
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3"><b>REGIONAL</b></div>
    <div class="col-xs-3 col-sm-3 col-md-3"><b>INVENTARIO</b></div>
    <div class="col-xs-5 col-sm-5 col-md-5"><b>USUARIO</b></div>
</div>
@foreach($inv_regionales as $inventario)
<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3">{{ $inventario['nombre_regional'] }}</div>
    <div class="col-xs-3 col-sm-3 col-md-3">{{ number_format($inventario['cantidad']) }}</div>
    <div class="col-xs-5 col-sm-5 col-md-5">{{ $inventario['usuario'] }}</div>
</div>
@endforeach

@endsection
