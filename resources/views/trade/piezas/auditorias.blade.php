@extends('layouts.app')

@section('content')

  <div class="row">
      <div class="col-lg-12">
          <div class="pull-right">
            <form class="form-inline" action="{{ route('trade.colocacion.seleccionada') }}" method="POST">
                @csrf
                <input type="hidden" name="idpdv" value="{{$idpdv}}" />
				<input type="hidden" name="implementacion_id" value="{{$implem}}" />
                <button type="submit" class="btn btn-sm btn-primary">Seguir Gestionando Punto de Venta</button>
                <a class="btn btn-sm btn-danger" href="{{ route('gestion.buscar.punto') }}">Buscar Nuevo Punto de venta</a>
            </form>
		  </div>

      </div>
  </div>
<hr>
<h5>AUDITORIAS IMPLEMENTACION IDPDV: {{$idpdv}} </h5>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">

    @foreach($auditorias as $auditoria)
    <small>
        <div class="card mb-1 bg-light">
            <div class="row no-gutters">
              <div class="col-md-3">
                <div><small class="text-muted">{{ $auditoria->resultado->nombre }}</small></div>
                @if($auditoria->imagen == null)
                    <img src="{{ Storage::disk('auditoria')->url('imagen.png') }}" class="img-fluid" width="256" alt="...">
                @else
              <img src="{{ Storage::disk('auditoria')->url($auditoria->imagen) }}" class="img-fluid" width="256" alt="{{$auditoria->imagen}}">
                @endif
              </div>
              <div class="col-md-4">
                <div>
                  <h6>{{ $auditoria->usuario->name }}</h6>
                  <h6>{{ $auditoria->created_at }}</h6>
                  <small class="text-muted">OBS.:{{$auditoria->resultado->observaciones}}</small>
                </div>
              </div>
              <div class="col-md-5">
                <div>
                  <h6>HALLAZGOS:</h6>
                  @foreach($auditoria->hallazgos as $hallado)
                  <small>{{$hallado->hallazgo->nombre}}</small><br>
                  @endforeach
                </div>
              </div>
            </div>
        </div>
    </small>
    <hr>
    @endforeach


  </div>
  </div>

@endsection
