@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Ver Pieza</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('piezas.index') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Id:</strong>
      {{ $pieza->id }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Tipo Pieza:</strong>
      {{ $pieza->tipo->nombre_pieza }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Contenido Comunica:</strong>
      {{ $pieza->contenido->contenido }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Objetivo Colocacion:</strong>
      {{ $pieza->objetivo->objetivo }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Imagen:</strong>
      @if($pieza->imagen)
      <img src="https://infopostigo.com/storage/{{ $pieza->imagen }}" alt="Trade" height="64" width="64">
      @else
      No Imagen
      @endif
    </div>
  </div>
</div>

@endsection
