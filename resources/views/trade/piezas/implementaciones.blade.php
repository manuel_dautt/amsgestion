@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Piezas x Implementacion</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('piezas.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('trade.implementacion.seleccionada') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Implementaciones Activas:</strong>
          <select id="implementacion_id" name="implementacion_id" class="form-control">
            <option value="">Seleccione una</option>
            @foreach($implementaciones as $implementacion)
                <option value="{{ $implementacion['id'] }}">{{ $implementacion['descripcion'] }}
                </option>
            @endforeach
          </select>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Seleccionar</button>
    </div>
  </div>

</form>

@endsection
