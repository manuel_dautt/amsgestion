@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Trade Piezas</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('piezas.create') }}"><i class="fas fa-plus-circle"></i> Nueva Pieza</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

@if ($message = Session::get('error'))
  @include('layouts.alert_error')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Tipo Pieza</th>
        <th>Contenido</th>
        <th>Objetivo</th>
        <th>Imagen</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($piezas as $pieza)
      <tr>
        <!--<td>{{ $pieza->id }}</td>-->
        <td>{{ $pieza->tipo->nombre_pieza }}</td>
        <td>{{ $pieza->contenido->contenido }}</td>
        <td>{{ $pieza->objetivo->objetivo }}</td>
        @if($pieza->imagen)
        <td><img src="https://infopostigo.com/storage/{{ $pieza->imagen }}" alt="Trade" height="64" width="64"></td>
        @else
        <td>No Imagen</td>
        @endif
        <td>

          <form action="{{ route('piezas.destroy',$pieza->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Pieza" href="{{ route('piezas.show',$pieza->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Pieza" href="{{ route('piezas.edit',$pieza->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Pieza" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

{!! $piezas->links() !!}

@endsection
