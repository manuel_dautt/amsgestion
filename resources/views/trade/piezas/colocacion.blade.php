@extends('layouts.app')

@section('content')

  <div class="row">
      <div class="col-lg-12">
          <div class="pull-left">
              <h6>{{$idpdv}}-{{ $nombre_punto }}-{{ $tipo_negocio }}-{{ $segmento }}</h6>
          </div>
          <div class="pull-right">
            <form class="form-inline" action="{{ route('gestion.traer.punto') }}" method="POST">
                @csrf
                <input type="hidden" name="idpdv" value="{{$idpdv}}" />
                <button type="submit" class="btn btn-sm btn-primary">Seguir Gestionando Punto de Venta</button>
                <a class="btn btn-sm btn-danger" href="{{ route('gestion.buscar.punto') }}">Buscar Nuevo Punto de venta</a>
				@if($implementacion['clase_pieza'] == 'CAMPAÑA')
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ManualCampanaModal">
					Manual Visibilidad Campaña
				</button>
				@endif
				@if($implementacion['clase_pieza'] == 'DURABLE')
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ManualDurableModal">
					Manual Visibilidad Durable
				</button>
				@endif
            </form>
          </div>

      </div>
  </div>
<hr>
<h5>IMPLEMENTACION MATERIAL</h5>
@if($implementacion['clase_pieza'] == 'DURABLE')
<p>PIEZAS MAXIMAS RECOMENDADAS A IMPLEMENTAR: <b class='text-danger'>{{ $max_piezas }}</b></p>
@endif
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    {{ $implementacion['descripcion'] }} [{{ $implementacion['fecha_ini'] }}] - [{{ $implementacion['fecha_fin'] }}]
  </div>
  @canany(['AUDITAR_IMPLEMENTACION'])
  <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#resultados_auditoria">
	VALIDACION IMPLEMENTACION
  </button>
  @endcanany
  
  @canany(['AUDITAR_IMPLEMENTACION'])
  <a href="{{ route('trade.implementacion.ver.auditorias',['impl' => $implementacion['id'], 'idpdv' => $idpdv]) }}" class="btn btn-sm btn-primary">
    Ver [{{ count($auditorias) }}] Auditoria(s) de Validacion
  </a>
  @endcanany


</div>

<div class="row">
  <div class="col-xs-6 col-sm-6 col-md-6">
    <h4> Piezas Disponibles</h4>
        @foreach($piezas_colocacion as $coloca)
          @if( ( $coloca->objetivo == 'MIXTO' AND 
		         ($tipo_negocio == 'MIXTO' OR 
				  $tipo_negocio == 'PDV' OR 
				  $tipo_negocio == 'PDA' )) OR 
			   ($implementacion['clase_pieza'] == 'DURABLE') OR
               ($coloca->objetivo == 'PDA' AND ($tipo_negocio == 'MIXTO' OR $tipo_negocio == 'PDA')) OR
               ($coloca->objetivo == 'PDV' AND ($tipo_negocio == 'MIXTO' OR $tipo_negocio == 'PDV'))
            )
                <div class="card flex-row flex-wrap">
                  <div class="card-header border-1">
                    <img src="https://infopostigo.com/storage/{{ $coloca->imagen }}" alt="ImagenTrade" height="64" width="64">
                    <a href="{{ route('trade.piezas.colocar',['idpdv' => $idpdv, 'impl' => $implementacion['id'], 'pieza' => $coloca->id]) }}" class="btn btn-sm btn-primary">
                      Colocar
                    </a>
                  </div>
                  <div class="card-block px-2 border-1">
                    <small>
                      {{ $coloca->pieza }}<br/>
                      {{ $coloca->contenido }}<br/>
                      {{ $coloca->objetivo }}
                    </small>
                  </div>
                </div>
          @endif
        @endforeach
  </div>
  <div class="col-xs-6 col-sm-6 col-md-6">
    <h4> Piezas Implementadas</h4>
    @foreach($piezas_implementadas as $asoc)
    <div class="card flex-row flex-wrap">
      <div class="card-header border-1">
        <img src="https://infopostigo.com/storage/{{ $asoc->imagen }}" alt="ImagenTrade" height="64" width="64">
        <a href="{{ route('trade.piezas.quitar',['idpdv' => $idpdv, 'impl' => $implementacion['id'], 'pieza' => $asoc->id]) }}" class="btn btn-sm btn-danger">Quitar</a>
        <a href="{{ route('trade.piezas.colocar',['idpdv' => $idpdv, 'impl' => $implementacion['id'], 'pieza' => $asoc->id]) }}" class="btn btn-sm btn-success">Recambio</a>
      </div>
      <div class="card-block px-2 border-1">
        <small>
        {{ $asoc->pieza }}<br/>
        {{ $asoc->contenido }}<br/>
        {{ $asoc->objetivo }}<br/>
        <b>{{ $asoc->cantidad }}</b> Pieza(s) Implm.
        </small>
      </div>
    </div>
    @endforeach
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ManualCampanaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Manual Visibilidad Campaña</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		@if($tipo_negocio == 'PDA')
			<p>Para PDA</p>
			<img src="{{ asset('manuales/Manual_Dealer_PDA.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@elseif($tipo_negocio == 'MIXTO')
			<p>Para MIXTO</p>
			<img src="{{ asset('manuales/Manual_Dealer_MIXTO.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@elseif($tipo_negocio == 'PDV')
			<p>Para PDV</p>
			<img src="{{ asset('manuales/Manual_Dealer_PDV.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@else
			<p>No Hay Manual para esa tipologia de negocio</p>
		@endif

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ManualDurableModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Manual Visibilidad Durable</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<p>{{ $categoriaH }}</p>
		@if($categoriaH == 'AGENTES COMERCIALES')
			<p>Agentes Comerciales, Subdistribuidores de Comunicaciones</p>
			<img src="{{ asset('manuales/1.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@elseif($categoriaH == 'APOSTADORES' OR $categoriaH == 'DROGUERIAS')
			<p>Droguerias, Apostadores</p>
			<img src="{{ asset('manuales/2.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@elseif($categoriaH == 'PAPELERIAS' OR $categoriaH == 'CABINAS TELEFONICAS' )
			<p>Papelerias, Cafe Internet, Cabinas Telefonicas</p>
			<img src="{{ asset('manuales/3.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@elseif($categoriaH == 'KIOSKOS + CAJONEROS COLOMBIA' OR $categoriaH == 'MINUTEROS' )
			<p>Kioscos, Cajoneros, Minuteros</p>
			<img src="{{ asset('manuales/4.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@elseif($categoriaH == 'SUPERMERCADOS' OR $categoriaH == 'TRADICIONALES' )
			<p>Tiendas de Barrio, Superetes, Minimercados, Supermercados</p>
			<img src="{{ asset('manuales/5.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@else
			<p>Categorias No determinadas</p>
			<img src="{{ asset('manuales/0.png') }}" alt="ImagenTrade" height="100%" width="100%">
		@endif

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="resultados_auditoria" tabindex="-1" role="dialog" aria-labelledby="resultados_auditoria_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel">AUDITORIA IMPLEMENTACION</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
		<h5>{{ $implementacion['descripcion'] }}</h5>

		<form action="{{ route('trade.implementacion.auditada') }}" method="POST" enctype="multipart/form-data">
		@csrf
		<h6>Resultados de la Validacion</h6>
        <select id="resultado" name="resultado" class="form-control">
			@foreach($resultados as $resultado)
				<option value="{{ $resultado['id'] }}" @if($resultado['id'] == 1) selected @endif>{{ $resultado['nombre'] }}</option>
			@endforeach
        </select>
        <br>
        <h6>Hallazgos Encontrados</h6>
        <select id="hallazgos" name="hallazgos[]" class="form-control" multiple>
			@foreach($hallazgos as $hallazgo)
				<option value="{{ $hallazgo['id'] }}" @if($hallazgo['id'] == 1) selected @endif>{{ $hallazgo['nombre'] }}</option>
			@endforeach
        </select>
        <br>
        <h6>Imagen</h6>
        <input type="file" accept="image/jpeg,image/png,image/jpg" name="imagen" class="form-control" placeholder="Imagen..." required>
        <br>
        <h6>Observaciones</h6>
		<textarea name='observaciones' cols="50" maxlength=250>
        </textarea>

		<input type="hidden" name="idpdv" value="{{ $idpdv }}" />
		<input type="hidden" name="implementacion" value="{{ $implementacion['id'] }}" />

		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-sm btn-primary">Registrar Auditoria</button>
		</div>

		</form>


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- FinModal -->

@endsection
