@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Asociacion Piezas - Implementaciones</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('trade.selecciona.implementacion') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    {{ $implementacion['descripcion'] }} [{{ $implementacion['fecha_ini'] }}] - [{{ $implementacion['fecha_fin'] }}]
  </div>
</div>

<div class="row">
  <div class="col-xs-6 col-sm-6 col-md-6">
    <h4> Piezas Disponibles</h4>
        @foreach($no_asociados as $no_asoc)
        <div class="card flex-row flex-wrap">
          <div class="card-header border-1">
            <img src="https://infopostigo.com/storage/{{ $no_asoc->imagen }}" alt="ImagenTrade" height="64" width="64">
            <a href="{{ route('trade.piezas.asociado',['impl' => $implementacion['id'], 'pieza' => $no_asoc->id]) }}" class="btn btn-sm btn-primary">Asociar</a>
          </div>
          <div class="card-block px-2 border-1">
            <small>
            {{ $no_asoc->pieza }}<br/>
            {{ $no_asoc->contenido }}<br/>
            {{ $no_asoc->objetivo }}
            </small>
          </div>
        </div>
        @endforeach
  </div>
  <div class="col-xs-6 col-sm-6 col-md-6">
    <h4> Piezas Asignadas</h4>
    @foreach($asociados as $asoc)
    <div class="card flex-row flex-wrap">
      <div class="card-header border-1">
        <img src="https://infopostigo.com/storage/{{ $asoc->imagen }}" alt="ImagenTrade" height="64" width="64">
        <a href="{{ route('trade.piezas.desasociado',['impl' => $implementacion['id'], 'pieza' => $asoc->id]) }}" class="btn btn-sm btn-primary">Quitar</a><br/>
        <small>Inv.: {{ number_format($asoc->inventario) }} pieza(s)</small>
      </div>
      <div class="card-block px-2 border-1">
        <small>
        {{ $asoc->pieza }}<br/>
        {{ $asoc->contenido }}<br/>
        {{ $asoc->objetivo }}<br/>
        <a href="{{ route('trade.inventario.ver',['impl_pieza' => $asoc->impl_pieza_id]) }}" class="btn btn-sm btn-success">Inventario</a>
        </small>
      </div>
    </div>
    @endforeach
  </div>
</div>

@endsection
