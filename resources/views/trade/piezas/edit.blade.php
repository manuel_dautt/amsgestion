@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Editar Tipo Piezas</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('tipos.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('tipos.update',$tipo->id) }}" method="POST">
  @csrf
  @method('PUT')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Nombre Tipos Pieza:</strong>
        <input type="text" name="nombre_pieza" value="{{ $tipo->nombre_pieza }}" class="form-control" placeholder="Nombre Objetivo">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Clase de Pieza:</strong>
          <select id="clase_pieza_id" name="clase_pieza_id" class="form-control">
            <option value="">Seleccione una</option>
            @foreach($clases as $clase)
                <option value="{{ $clase['id'] }}" {{ $clase['id'] == $tipo->clase_pieza_id ? 'selected':'' }} >
                    {{ $clase['nombre_clase'] }}
                </option>
            @endforeach
          </select>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
  </div>

</form>

@endsection
