@extends('layouts.app')

@section('content')
@include('scripts.trade_objetivos.trade_objetivos')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Crear Nueva Pieza</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('piezas.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hubo algunos problemas con su entrada.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

@if ($message = Session::get('error'))
  @include('layouts.alert_error')
@endif

<form action="{{ route('piezas.store') }}" method="POST" enctype="multipart/form-data">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Tipo Pieza:</strong>
          <select id="tipo_pieza_id" name="tipo_pieza_id" class="form-control">
            <option value="">Seleccione...</option>
            @foreach($tipos as $tipo)
                <option value="{{ $tipo['id'] }}">{{ $tipo['nombre_pieza'] }}</option>
            @endforeach
          </select>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Contenido:</strong>
          <select id="contenido_id" name="contenido_id" class="form-control">
            <option value="">Seleccione...</option>
            @foreach($contenidos as $contenido)
                <option value="{{ $contenido['id'] }}">{{ $contenido['contenido'] }}</option>
            @endforeach
          </select>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Fuente Objetivo Colocacion:</strong>
          <select id="fuente" name="fuente" class="form-control">
            <option value="X" selected>Seleccione Fuente...</option>
            <option value="TIPOLOGIA">POR TIPOLOGIA</option>
            <option value="SEGMENTACION_GROSS">POR SEGMENTACION GROSS</option>
            <option value="CATEGORIA_HOMOLOGADA">POR CATEGORIA HOMOLOGADA</option>
            <option value="REGIONAL">POR REGIONAL</option>
          </select>
          <strong>Objetivo Colocacion:</strong>
          <select id="objetivo_id" name="objetivo_id[]" class="form-control" multiple>
            <option value="">Objetivos...</option>
            <!--
            (arroba)foreach($objetivos as $objetivo)
                <option value="{ $objetivo['id'] }">{ $objetivo['objetivo'] }</option>
            (arroba)endforeach
            -->
          </select>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Imagen:</strong>
          <input type="file" accept="image/jpeg,image/png,image/jpg" name="imagen" class="form-control" placeholder="Imagen...">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Crear</button>
    </div>
  </div>

</form>

@endsection
