@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Seguimiento Implementaciones Corte: {{ date('Y-m-d', strtotime('-1 day')) }}</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('trade.dash.ver') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<hr>
<h3>NACIONAL</h3>
<div class="col-xl-6 col-md-6 mb-6">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1"><h4 style="color: #F6B300;">{{ $inv_nacional[0]['implementacion'] }}</h4></div>
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
								<small>
								% Ejecutado {{ $inv_nacional[0]['porcentaje'] }}%<br>
								Inventario: {{ number_format($inv_nacional[0]['inventario']) }} pieza(s)<br>
                                Implementado: {{ number_format($inv_nacional[0]['colocado']) }} pieza(s)
								</small>
                            </div>
                        </div>
					</div>
					<div class="row no-gutters align-items-center">
                        <div class="col">
                            <div class="progress progress-sm mr-2">
                                <div class="progress-bar
											@if ($inv_nacional[0]['porcentaje'] <= 50)
													bg-danger
											@elseif ($inv_nacional[0]['porcentaje'] <= 80)
													bg-warning
											@else
													bg-success
											@endif"
									 role="progressbar" 
									 style="width: {{ $inv_nacional[0]['porcentaje'] }}%" 
									 aria-valuenow="{{ $inv_nacional[0]['porcentaje'] }}" 
									 aria-valuemin="0" 
									 aria-valuemax="100">
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
						<a href="{{ route('trade.dash.ver') }}">
							<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
						</a>
                </div>
            </div>
        </div>
    </div>
</div>

<h3>REGIONALES</h3>
<div class='row'>
@foreach($inv_regional as $inventario)
<div class="col-xl-4 col-md-4 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1"><h4 style="color: #F6B300;">{{ $inventario['regional'] }}</h4></div>
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
								<small>
								% Ejecutado {{ $inventario['porcentaje'] }}%<br>
								Inventario: {{ number_format($inventario['inventario']) }} pieza(s)<br>
                                Implementado: {{ number_format($inventario['colocado']) }} pieza(s)
								</small>
                            </div>
                        </div>
					</div>
					<div class="row no-gutters align-items-center">
                        <div class="col">
                            <div class="progress progress-sm mr-2">
                                <div class="progress-bar
											@if ($inventario['porcentaje'] <= 50)
													bg-danger
											@elseif ($inventario['porcentaje'] <= 80)
													bg-warning
											@else
													bg-success
											@endif"
									 role="progressbar" 
									 style="width: {{ $inventario['porcentaje'] }}%" 
									 aria-valuenow="{{ $inventario['porcentaje'] }}" 
									 aria-valuemin="0" 
									 aria-valuemax="100">
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
					<a href="{{ route('trade.dash.ver.regional',[ 'implementacion' => $inventario['id_implementacion'], 'regional' => $inventario['id_regional'] ]) }}">
						<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
					</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
</div>

<h3>PIEZAS NACIONAL</h3>
<div class='row'>
@foreach($inv_piezas as $pieza)
<div class="col-xl-4 col-md-4 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1"><span style="color: #F6B300;">{{ $pieza['pieza'] }}</span></div>
                    <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
								<small>
								% Ejecutado {{ $pieza['porcentaje'] }}%<br>
								Inventario: {{ number_format($pieza['inventario']) }} pieza(s)<br>
                                Implementado: {{ number_format($pieza['colocado']) }} pieza(s)
								</small>
                            </div>
                        </div>
					</div>
					<div class="row no-gutters align-items-center">
                        <div class="col">
                            <div class="progress progress-sm mr-2">
                                <div class="progress-bar
											@if ($pieza['porcentaje'] <= 50)
													bg-danger
											@elseif ($pieza['porcentaje'] <= 80)
													bg-warning
											@else
													bg-success
											@endif"
									 role="progressbar" 
									 style="width: {{ $pieza['porcentaje'] }}%" 
									 aria-valuenow="{{ $pieza['porcentaje'] }}" 
									 aria-valuemin="0" 
									 aria-valuemax="100">
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
						<img src="https://infopostigo.com/storage/{{ $pieza['imagen'] }}" alt="ImagenTrade" height="64" width="64">
						
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
</div>
@endsection
