@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Ver Hallazgo</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('hallazgos.index') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Clase Hallazgo:</strong>
      {{ $hallazgo->clase->nombre_clase }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Nombre Hallazgo:</strong>
      {{ $hallazgo->nombre }}
    </div>
  </div>
</div>

@endsection
