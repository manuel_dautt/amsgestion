@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Editar Hallazgos</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('hallazgos.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Parece que tenemos algun problema con su Actualizacion.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('hallazgos.update',$hallazgo->id) }}" method="POST">
  @csrf
  @method('PUT')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">

      <div class="form-group">
        <strong>Clase Hallazgo:</strong>
        <select id="clase_hallazgo_id" name="clase_hallazgo_id" class="form-control" >
          @foreach($clasesH as $claseh)
            <option value="{{ $claseh['id'] }}" @if($claseh['id'] == $hallazgo->trade_clase_hallazgo_id) selected @endif >
                {{ $claseh['nombre_clase'] }}
            </option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <strong>Nombre Hallazgo:</strong>
        <input type="text" name="nombre" value="{{ $hallazgo->nombre }}" class="form-control" placeholder="Nombre Hallazgo">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
  </div>

</form>

@endsection
