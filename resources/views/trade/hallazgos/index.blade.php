@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Hallazgos</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('hallazgos.create') }}"><i class="fas fa-plus-circle"></i> Nuevos Hallazgos</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

@if ($message = Session::get('error'))
  <div class="alert alert-danger">
    <p>{{ $message }}</p>
  </div>
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <th>Clase</th>
        <th>Hallazgo</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($hallazgos as $hallazgo)
      <tr>
        <td>{{ $hallazgo->clase->nombre_clase }}</td>
        <td>{{ $hallazgo->nombre }}</td>
        <td>

          <form action="{{ route('hallazgos.destroy',$hallazgo->id) }}" method="POST">

            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Hallazgo" href="{{ route('hallazgos.show',$hallazgo->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Hallazgo" href="{{ route('hallazgos.edit',$hallazgo->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Hallazgo" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{!! $hallazgos->links() !!}

@endsection
