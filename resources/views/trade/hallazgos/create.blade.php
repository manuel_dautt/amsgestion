@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Crear Nuevo Hallazgo</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('hallazgos.index') }}"> Regresar</a>
    </div>
  </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
  <strong>Whoops!</strong> Hubo algunos problemas con su entrada.<br><br>
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<form action="{{ route('hallazgos.store') }}" method="POST">
  @csrf

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
        <strong>Seleccione Clase Hallazgo:</strong>

        <select id="clase_hallazgo_id" name="clase_hallazgo_id" class="form-control @error('clase_hallazgo_id') is-invalid @enderror">
            <option value="X" selected>Elija una Clase de Hallazgo</option>
            @foreach($clasesH as $claseh)
            <option value="{{ $claseh['id'] }}">{{ $claseh['nombre_clase'] }}</option>
          @endforeach
        </select>

      </div>
      <div class="form-group">
        <strong>Nombre Hallazgo:</strong>
        <input type="text" name="nombre" class="form-control" placeholder="Ingrese Nombre">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-sm btn-primary">Crear</button>
    </div>
  </div>

</form>

@endsection
