@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Trade Objetivos Colocacion</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('objetivos.create') }}"><i class="fas fa-plus-circle"></i> Nueva Objetivo</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Objetivo Colocacion</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($objetivos as $objetivo)
      <tr>
        <!--<td>{{ $objetivo->id }}</td>-->
        <td>{{ $objetivo->objetivo }}</td>
        <td>

          <form action="{{ route('objetivos.destroy',$objetivo->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Objetivo" href="{{ route('objetivos.show',$objetivo->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Objetivo" href="{{ route('objetivos.edit',$objetivo->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Objetivo" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

{!! $objetivos->links() !!}

@endsection
