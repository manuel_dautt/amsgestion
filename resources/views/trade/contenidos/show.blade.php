@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Ver Contenido/Comunicacion</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('contenidos.index') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Id:</strong>
      {{ $contenido->id }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Contenido/Comunicacion:</strong>
      {{ $contenido->contenido }}
    </div>
  </div>
</div>

@endsection
