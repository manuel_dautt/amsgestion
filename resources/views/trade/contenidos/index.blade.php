@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Trade Contenidos/Comunicacion</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('contenidos.create') }}"><i class="fas fa-plus-circle"></i> Nueva Contenido</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Contenido/Comunicacion</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($contenidos as $contenido)
      <tr>
        <!--<td>{{ $contenido->id }}</td>-->
        <td>{{ $contenido->contenido }}</td>
        <td>

          <form action="{{ route('contenidos.destroy',$contenido->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Contenido" href="{{ route('contenidos.show',$contenido->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Contenido" href="{{ route('contenidos.edit',$contenido->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Contenido" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

{!! $contenidos->links() !!}

@endsection
