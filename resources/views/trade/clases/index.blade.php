@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Trade Clases de Piezas</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('clases.create') }}"><i class="fas fa-plus-circle"></i> Nueva Clase de Pieza</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Nombre Clase</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clases as $clase)
      <tr>
        <!--<td>{{ $clase->id }}</td>-->
        <td>{{ $clase->nombre_clase }}</td>
        <td>

          <form action="{{ route('clases.destroy',$clase->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Clase" href="{{ route('clases.show',$clase->id) }}">
              <i class="fas fa-search"></i></a>
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Clase" href="{{ route('clases.edit',$clase->id) }}"><i class="fas fa-edit"></i></a>

            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Clase" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

{!! $clases->links() !!}

@endsection
