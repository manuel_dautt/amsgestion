<div class="row bg-tigo text-center">
    <h5 class="center tituloPrincipal">IMPLEMENTACION VISIBILIDAD</h5>
</div>
<form action="{{ route('trade.colocacion.seleccionada') }}" method="POST">
  @csrf

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Implementaciones Activas:</strong>
          <select id="implementacion_id" name="implementacion_id" class="form-control">
            <option value="">Seleccione una</option>
            @foreach($implementaciones as $implementacion)
                <option value="{{ $implementacion['id'] }}">{{ $implementacion['descripcion'] }}
                </option>
            @endforeach
          </select>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <input type="hidden" name="idpdv" value="{{$rpoint['idpdv']}}" />
      <input type="hidden" name="nombre_punto" value="{{ $rpoint['nombre_punto'] }}" />
      <input type="hidden" name="tipo_negocio" value="{{ $tipo_negocio }}" />
      <input type="hidden" name="max_piezas" value="{{ $max_piezas }}" />
      <button type="submit" class="btn btn-primary">Implementar ...</button>
    </div>

<!-- trade.colocacion.material -->
</form>
