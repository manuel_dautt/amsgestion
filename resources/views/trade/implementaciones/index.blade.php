@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Trade Implementaciones</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-success" href="{{ route('implementaciones.create') }}"><i class="fas fa-plus-circle"></i> Nueva Implementacion</a>
    </div>
  </div>
</div>

@if ($message = Session::get('success'))
  @include('layouts.alert_success')
@endif

<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <!--<th>Id</th>-->
        <th>Implementacion</th>
		<th>Estado</th>
        <th>Fecha Inicial</th>
        <th>Fecha Final</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($implementaciones as $implementacion)
      <tr>
        <!--<td>{{ $implementacion->id }}</td>-->
        <td>{{ $implementacion->descripcion }}</td>
		<td>{{ ($implementacion->status == 'A' ? 'ACTIVO' : ($implementacion->status == 'I' ? 'INACTIVO' : 'ERROR')) }}</td>
        <td>{{ $implementacion->fecha_ini}}</td>
        <td>{{ $implementacion->fecha_fin }}</td>
        <td>

          <form action="{{ route('implementaciones.destroy',$implementacion->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Ver Implementacion" href="{{ route('implementaciones.show',$implementacion->id) }}">
              <i class="fas fa-search"></i></a>
			@if($implementacion->status == 'A')  
            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Implementacion" href="{{ route('implementaciones.edit',$implementacion->id) }}"><i class="fas fa-edit"></i></a>			
            <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar Implementacion" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
			@endif
		  </form>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

{!! $implementaciones->links() !!}

@endsection
