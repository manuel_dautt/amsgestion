@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2> Ver Implementacion</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('implementaciones.index') }}"> Regresar</a>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Id:</strong>
      {{ $implementacion->id }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Descripcon Implementacion:</strong>
      {{ $implementacion->descripcion }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Fecha Inicial:</strong>
      {{ $implementacion->fecha_ini }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Fecha Final:</strong>
      {{ $implementacion->fecha_fin }}
    </div>
  </div>
  
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
      <strong>Estado:</strong>
		@if($implementacion->status == 'A')
			ACTIVA
		@elseif($implementacion->status == 'I')
			INACTIVA
			<a class="btn btn-sm btn-info" title="Reactivar" href="{{ route('trade.implementacion.reactivar',$implementacion->id) }}">REACTIVAR</a>
		@else
			ERR-CALL ADMIN
		@endif
    </div>
	
  </div>
</div>

@endsection

