<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Bienvenid@</title>
</head>
<body>
    <h3>HOLA!!</h3>
    <p>Un nuevo usuario se ha registrado en la plataforma INFOPOS.</p>
    <p>En las proximas 48 horas deberas habilitarlo para que pueda navegar por las opciones de la herramienta.</p>
    <p>Sus datos registrados son los siguientes:</p>
    <ul>
        <li>Nombre: {{ $datosMail['nombre'] }}</li>
        <li>Apellido: {{ $datosMail['apellido'] }}</li>
        <li>Documento: {{ $datosMail['documento'] }}</li>
        <li>Correo: {{ $datosMail['correo']  }}</li>
        <li>Regional: {{ $datosMail['regional'] }}</li>
        <li>Territorio: {{ $datosMail['territorio'] }}</li>
        <li>Canal: {{ $datosMail['canal'] }}</li>
        <li>Cargo: {{ $datosMail['cargo'] }}</li>
    </ul>

<p>Infopos Admin</p>
</body>
</html>
