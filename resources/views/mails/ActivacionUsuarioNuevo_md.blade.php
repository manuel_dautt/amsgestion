@component('mail::message')
# Hola!

Un nuevo usuario se ha registrado en la plataforma INFOPOS.
En las proximas 48 horas deberas habilitarlo para que pueda navegar por las opciones de la herramienta.
Sus datos registrados son los siguientes:


- Nombre: {{ $datosMail['nombre'] }}
- Apellido: {{ $datosMail['apellido'] }}
- Documento: {{ $datosMail['documento'] }}
- Correo: {{ $datosMail['correo']  }}
- Regional: {{ $datosMail['regional'] }}
- Territorio: {{ $datosMail['territorio'] }}
- Canal: {{ $datosMail['canal'] }}
- Cargo: {{ $datosMail['cargo'] }}



Gracias,

{{ config('app.name') }}
@endcomponent
