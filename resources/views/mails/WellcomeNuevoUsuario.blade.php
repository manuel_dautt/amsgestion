<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo Usuario Creado</title>
</head>
<body>
    <p>Hola! Se ha reportado un nuevo usuario en Infopos y requiere su atencion.</p>
    <p>Estos son los datos del usuario que ha realizado el registro:</p>
    <ul>
        <li>Nombre: {{ $nombre }}</li>
        <li>Apellido: {{ $apellido }}</li>
        <li>Documento: {{ $documento }}</li>
        <li>Correo: {{ $correo  }}</li>
        <li>Regional: {{ $regional }}</li>
        <li>Territorio: {{ $territorio }}</li>
        <li>Canal: {{ $canal }}</li>
        <li>Cargo: {{ $cargo }}</li>
    </ul>


</body>
</html>
