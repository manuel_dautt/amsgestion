@component('mail::message')
# {{ $datosMail['nombre'] . ' ' . $datosMail['apellido'] }} Bienvenid@

Bienvenido a INFOPOS! has sido registrado como nuevo usuario de la plataforma.
En las proximas 48 horas seras habilitado para poder navegar por las opciones de la herramienta.
Tus datos registrados son los siguientes:

- Nombre: {{ $datosMail['nombre'] }}
- Apellido: {{ $datosMail['apellido'] }}
- Documento: {{ $datosMail['documento'] }}
- Correo: {{ $datosMail['correo']  }}
- Regional: {{ $datosMail['regional'] }}
- Territorio: {{ $datosMail['territorio'] }}
- Canal: {{ $datosMail['canal'] }}
- Cargo: {{ $datosMail['cargo'] }}

Gracias,

{{ config('app.name') }}
@endcomponent
