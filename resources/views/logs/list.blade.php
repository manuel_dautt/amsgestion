@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Accesos Hace {{$rango}} dia(s) hasta Ayer</h2>
		<h6>{{ $dateIni }} - {{ $dateFin }}</h6>
      </div>
      <div class="pull-right">
        <a class="btn btn-sm btn-primary" href="{{ route('logs.index') }}"> Regresar</a>
        <a class="btn btn-sm btn-success" href="{{ route('log.exportar.accesos',
                                                          ['rango' => $rango, 'cedula' => $cedula]) }}">
                                                          <i class="fas fa-file-excel"></i>
                                                           Listar Accesos</a>
        <a class="btn btn-sm btn-success" href="{{ route('log.exportar.accesos.resumen',
                                                          ['rango' => $rango, 'cedula' => $cedula]) }}">
                                                          <i class="fas fa-file-excel"></i>
                                                          Resumen Accesos</a>
      </div>
    </div>
  </div>

  <div class="row">

      @if ($message = Session::get('success'))
          @include('layouts.alert_success')
      @endif

      @if ($message = Session::get('error'))
          @include('layouts.alert_error')
      @endif
  </div>
<small>
<div class="table-responsive-sm">
  <table class="table table-hover table-bordered table-sm">
    <thead>
      <tr>
        <th>C.C.</th>
        <th>Nombre Empleado</th>
        <th>Cargo</th>
        <th>Regional</th>
        <th>Fecha</th>
        <th>I.P.</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($accesos as $acceso)
      <tr>
        <td>{{ $acceso->user->employee->document }}</td>
        <td>{{ $acceso->user->name }}</td>
        <td>{{ $acceso->user->employee->position->position_name }}</td>
        <td>{{ $acceso->user->employee->territory->regional->regional_name }}</td>
        <td>{{ $acceso->created_at }}</td>
        <td>{{ $acceso->user_ip }}</td>
      </tr>
      @empty
      <tr>
        <td colspan="6">Sin Datos de Accesos</td>
      </tr>
      @endforelse
    </tbody>
  </table>
</div>
</small>

@endsection
