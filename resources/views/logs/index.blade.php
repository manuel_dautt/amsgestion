@extends('layouts.app')

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Ver Log de Accesos</h2>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="{{ route('home') }}"> Regresar</a>
    </div>
  </div>
</div>

<div class="row">

    @if ($message = Session::get('success'))
        @include('layouts.alert_success')
    @endif

    @if ($message = Session::get('error'))
        @include('layouts.alert_error')
    @endif
</div>

<div class="card">

    <div class="card-body">
        <h6 class="card-title"><strong>BUSCAR USUARIO(S)</strong></h6>
        <form class="form-inline" action="{{ route('log.traer.cedula') }}" method="POST">
            @csrf

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="checkbox" id="una" name="una" class="form-check-input" onchange="habilitar_una(this.checked);" checked>
                    Un Usuario
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8">
                    <input type="number" id="cedula" name="cedula" value="{{ old('cedula') }}" class="form-control @error('cedula') is-invalid @enderror" placeholder="Ingrese Cedula">
                    @error('cedula')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="checkbox" id="todos" name="todos" class="form-check-input" onchange="habilitar(this.checked);">
                    Todos los Usuarios
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <br>
                <strong>Generar Reporte de ...</strong>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="radio" id="rango" name="rango" class="form-check-input" value="1" checked>
                Dia Anterior
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="radio" id="rango" name="rango" class="form-check-input" value="7">
                Ultimos 7 dias
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <input type="radio" id="rango" name="rango" class="form-check-input" value="30">
                Ultimos 30 dias
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <br>
                <button type="submit" class="btn btn-md btn-primary">Generar</button>
            </div>
          </form>
    </div>
  </div> <!-- FIN CARD buscar x idpdv -->
<br/>
@include('scripts.logs.check_all')

@endsection
