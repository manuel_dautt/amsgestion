<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Infopos Version 2') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/2e8332d9c9.js" crossorigin="anonymous"></script>
    <!-- https://gijgo.com/datepicker/example/bootstrap-4 -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js" integrity="sha256-JirYRqbf+qzfqVtEE4GETyHlAbiCpC005yBTa4rj6xg=" crossorigin="anonymous"></script>

    <!--script src="{ asset('js/app.js') }" defer></script-->
    <!--script src="{ asset('js/main.js') }" defer></script-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--link href="{{-- asset('css/app.css') --}}" rel="stylesheet"-->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <!-- https://gijgo.com/datepicker/example/bootstrap-4 -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" integrity="sha256-zuyRv+YsWwh1XR5tsrZ7VCfGqUmmPmqBjIvJgQWoSDo=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- { config('app.name', 'Laravel') } -->
                    <img class="img-fluid" src="{{ asset('img/Logo.png') }}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                            <!-- MENU PARA GESTION PUNTOS DE VENTA Y CALLE -->
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Gestion y Ruta<span class="caret"></span>
                                </a>
                                @canany(['VER_DEALERS'])
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <!--can('VER_PUNTO_VENTA_DEALERS')-->
                                    <a class="dropdown-item" href="{{ route('gestion.buscar.punto') }}">
                                        Gestion Punto Venta
                                    </a>
                                    <!--endcan-->
                                    <a class="dropdown-item" href="{{ route('encuestas.iniciar.tracking') }}">
                                        Tracking Competencia
                                    </a>
									<a class="dropdown-item" href="{{ route('encuestas.ver.check.salida') }}">
                                        Check List Salida Ruta
                                    </a>

									<a class="dropdown-item" href="{{ route('encuestas.ver.coachings') }}">
                                        Acompañamientos (Coaching)
                                    </a>
                                    <!--can('VER_PLANEACION')-->
                                    <a class="dropdown-item" href="{{ route('planeacion.ver.planeador') }}">
                                        Ver Planeador / Rutero
                                    </a>
                                    <!--endcan-->

                                    @canany(['VER_MAPA2'])
                                    <a class="dropdown-item" href="{{ route('mapas.gestores.validar') }}">
                                        Ajustar Mapa Gestores
                                    </a>
                                    @endcanany

                                </div>
                                @endcanany
                            </li>


                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    File Manager <span class="caret"></span>
                                </a>
                                @canany(['VER_FILE_MANAGER','VER_REPORTES_TRADE'])
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('archivos.verArchivos') }}">
                                        Ver Archivos
                                    </a>
                                </div>
                                @endcanany
                            </li>

							<li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Trade <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @canany(['VER_TRADE'])
                                    <a class="dropdown-item" href="{{ route('clases.index') }}">
                                        Clases de Piezas
                                    </a>
                                    <a class="dropdown-item" href="{{ route('contenidos.index') }}">
                                        Contenidos/Comunicacion
                                    </a>
                                    <a class="dropdown-item" href="{{ route('objetivos.index') }}">
                                        Objetivos de Colocacion
                                    </a>
                                    <a class="dropdown-item" href="{{ route('tipos.index') }}">
                                        Tipos de Piezas
                                    </a>
                                    <a class="dropdown-item" href="{{ route('piezas.index') }}">
                                        Crear Piezas Visibilidad
                                    </a>
                                    <hr/>
                                    <a class="dropdown-item" href="{{ route('implementaciones.index') }}">
                                        Implementaciones
                                    </a>
                                    <a class="dropdown-item" href="{{ route('trade.selecciona.implementacion') }}">
                                        Piezas Por Implementacion
                                    </a>
									<hr/>
                                    <a class="dropdown-item" href="{{ route('clasehallazgo.index') }}">
                                        Clases Hallazgo
                                    </a>
                                    <a class="dropdown-item" href="{{ route('hallazgos.index') }}">
                                        Hallazgos
                                    </a>
                                    @endcanany
                                    <hr/>
                                    @canany(['VER_REPORTES_TRADE'])
                                        <a class="dropdown-item" href="{{ route('trade.exportar.reporte.basico') }}">
                                        Reporte General de Implementacion
                                    </a>
									<a class="dropdown-item" href="{{ route('trade.dash.ver') }}">
                                        Resultados Implementaciones
                                    </a>
                                    @endcanany
                                </div>

                            </li>

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Mapas <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									@canany(['VER_MAPA_MOC'])
									<a class="dropdown-item" href="{{ route('mapas.ver.opciones.moc') }}">
                                        Mapas MOC
                                    </a>
									<a class="dropdown-item" href="{{ route('mapas.ver.censo.propios',[ 'c_cves' => 0,'c_antenas' => 0,'c_cciales' => 1,'c_puntos' => 1 ]) }}">
                                        Mapas CENSO
                                    </a>
									<a class="dropdown-item" href="{{ route('mapas.ver.censo.boc',[ 'c_cves' => 0, 'c_circuitos' => 0, 'c_puntos' => 1 ]) }}">
                                        Mapas CENSO BOC
                                    </a>
									<a class="dropdown-item" href="{{ route('home') }}">
                                        Mapas Go Comercial
                                    </a>
									@endcanany
                                    <a class="dropdown-item" href="{{ route('cercanos.ver') }}">
                                        Cerca a ti
                                    </a>
                                </div>
                            </li>


                            <!-- MENU pARA CHAMPIONS TOOLS -->

							@canany(['VER_DASH_HOME','VER_DASH_MOVIL'])
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Canales Propios <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('tiendas.ver.ns') }}">
                                        Service Level
                                    </a>

									<a class="dropdown-item" href="{{ route('tiendas.ver.nps') }}">
                                        Net Promoter score
                                    </a>
									@can('VER_DASH_HOME')
                                    <a class="dropdown-item" href="{{ route('bi.ctools.home') }}">
                                        Dashboard Home
                                    </a>
                                    @endcan
                                    @can('VER_DASH_MOVIL')
                                    <a class="dropdown-item" href="{{ route('bi.ctools.movil') }}">
                                        Dashboard Movil
                                    </a>
                                    @endcan
                                </div>
                            </li>
                           @endcanany
						   @canany('VER_RETAIL')
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Retail <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('retail.ver.todo') }}">
                                        Ver Hard Discounter
                                    </a>
									<a class="dropdown-item" href="{{ route('gestion.buscar.punto') }}">
                                        Buscar Puntos de Venta
                                    </a>
                                </div>
                            </li>
							@endcanany
                            <!-- MENU ADMINISTRACION MAESTROS-->
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Administrar <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @canany(['CREAR_REGIONALES','VER_REGIONALES','LISTAR_REGIONALES','EDITAR_REGIONALES','BORRAR_REGIONALES'])
                                        <a class="dropdown-item" href="{{ route('paises.index') }}">Paises</a>
                                    @endcanany
                                    @canany(['CREAR_REGIONALES','VER_REGIONALES','LISTAR_REGIONALES','EDITAR_REGIONALES'.'BORRAR_REGIONALES'])
                                        <a class="dropdown-item" href="{{ route('regionales.index') }}">Regionales</a>
                                    @endcanany
                                    @canany(['CREAR_TERRITORIOS','VER_TERRITORIOS','LISTAR_TERRITORIOS','EDITAR_TERRITORIOS','BORRAR_TERRITORIOS'])
                                        <a class="dropdown-item" href="{{ route('territorios.index') }}">Territorios</a>
                                    @endcanany
                                    @canany(['CREAR_CANALES','VER_CANALES','LISTAR_CANALES','EDITAR_CANALES','BORRAR_CANALES'])
                                        <a class="dropdown-item" href="{{ route('canales.index') }}">Canales</a>
                                    @endcanany
                                    @canany(['CREAR_EMPLEADOS','VER_EMPLEADOS','LISTAR_EMPLEADOS','EDITAR_EMPLEADOS','BORRAR_EMPLEADOS'])
                                        <a class="dropdown-item" href="{{ route('empleados.index') }}">Empleados</a>
                                    @endcanany
                                    @canany(['CREAR_CARGOS','VER_CARGOS','LISTAR_CARGOS','EDITAR_CARGOS','BORRAR_CARGOS'])
                                        <a class="dropdown-item" href="{{ route('cargos.index') }}">Cargos</a>
                                    @endcanany
                                    @canany(['CREAR_ROLES','VER_ROLES','LISTAR_ROLES','EDITAR_ROLES','BORRAR_ROLES'])
                                        <a class="dropdown-item" href="{{ route('roles.index') }}">Roles</a>
                                    @endcanany
                                    @canany(['CREAR_PERMISOS','VER_PERMISOS','LISTAR_PERMISOS','EDITAR_PERMISOS','BORRAR_PERMISOS'])
                                        <a class="dropdown-item" href="{{ route('permisos.index') }}">Permisos</a>
                                    @endcanany
                                    @canany(['CREAR_ADMINS','VER_ADMINS','LISTAR_ADMINS','EDITAR_ADMINS','BORRAR_ADMINS'])
                                        <a class="dropdown-item" href="{{ route('admins.index') }}">Admin. App.</a>
                                    @endcanany
                                    @canany(['VER_ACCESOS','LISTAR_ACCESOS'])
                                        <a class="dropdown-item" href="{{ route('logs.index') }}">Accesos App.</a>
                                    @endcanany

                                </div>
                            </li>
							@if(Auth::user()->id == 19)
							<li class="nav-item dropdown">
								<a id="navbarDropdown" class="nav-link dropdown-toggle text-danger" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									Admin Logs <span class="caret"></span>
								</a>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item text-danger" target="_blank" href="{{ url('/admin/log-reader') }}" rel="noopener noreferrer">
										Ver Logs
									</a>
								</div>
							</li>
							@endif
							<li class="nav-item dropdown">
								<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									Apps de Apoyo <span class="caret"></span>
								</a>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" target="_blank" href="https://amstigo.com.co" rel="noopener noreferrer">
										AMS
									</a>
									<a class="dropdown-item" target="_blank" href="https://tt.amstigo.com.co" rel="noopener noreferrer">
										Tigo Trainner
									</a>
									<a class="dropdown-item" target="_blank" href="https://mtt.amstigo.com.co" rel="noopener noreferrer">
										MTT
									</a>
									<a class="dropdown-item" target="_blank" href="https://tt.amstigo.com.co/dealers" rel="noopener noreferrer">
										Portal Dealers
									</a>
									<a class="dropdown-item" target="_blank" href="https://campustigo.com.co/" rel="noopener noreferrer">
										Campus Tigo
									</a>
									<a class="dropdown-item" target="_blank" href="https://tigo.com.co" rel="noopener noreferrer">
										Tigo Colombia
									</a>
								</div>
							</li>

                        @endauth
                    </ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Ingresar') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Registrate') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown text-lowercase" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas fa-user-check"></i> <small></small> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<p class="dropdown-item">{{ Auth::user()->name }}</p>
                                    <p class="dropdown-item">{{ Auth::user()->employee->position->position_name }}</p>
                                    <p class="dropdown-item">{{ Auth::user()->employee->territory->regional->regional_name }}</p>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('SALIR') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                @yield('content')
            </div>
        </main>
    </div>

    @canany(['GRABAR_PANTALLA'])
    <div class="start-screen-recording">
        <div>
            <div class="rec-dot"></div>
            <span>Grabador de Pantalla deshabilitado</span>
        </div>
    </div>
    <!--script src="https://api.apowersoft.com/screen-recorder?lang=es" defer></script-->
    @endcanany

    <script type="text/javascript">
        $(document).ready(function(){

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>
