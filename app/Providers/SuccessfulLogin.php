<?php

namespace App\Providers;

use App\Models\UserAccess;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class SuccessfulLogin
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Carbon
     */
    private $carbon;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, Carbon $carbon)
    {
        //
        $this->request = $request;
        $this->carbon = $carbon;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event) : void
    {
        //
        $user_id = $event->user->id;
        $login_ip = $this->request->ip();

        $accesos = new UserAccess;

        $accesos->user_id = $user_id;
        $accesos->user_ip = $login_ip;

        $accesos->save();


    }
}
