<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionNuevoUsuario extends Mailable
{
    use Queueable, SerializesModels;


    public $datosMail = array();

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datosMail)
    {
        //
        $this->datosMail = $datosMail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.NotificacionUsuarioNuevo_md')
                    ->subject('BIENVENID@ A INFOPOS')
                    ->with(['datosMail' => $this->datosMail ]);
    }
}
