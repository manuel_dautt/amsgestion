<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $tipo_pregunta_id
 * @property int $grupo_pregunta_id
 * @property int $encuesta_id
 * @property int $version_encuesta
 * @property int $orden_pregunta
 * @property string $texto_pregunta
 * @property string $texto_ayuda
 * @property float $peso
 * @property string $requerida
 * @property string $genera_respuesta
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_tipo_pregunta
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_base
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_grupo_preguntas
 *
 * @package App\Models
 */
class EncuestaPreguntas extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_preguntas';

	protected $fillable = [
        'tipo_pregunta_id',
        'grupo_pregunta_id',
        'encuesta_id',
        'version_encuesta',
        'orden_pregunta',
        'texto_pregunta',
        'texto_ayuda',
        'peso',
        'requerida',
        'genera_respuesta'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/
	public function tipo()
	{
        return $this->belongsTo(\App\Models\EncuestaTipoPregunta::class, 'tipo_pregunta_id');
    }

    public function encuesta()
	{
        return $this->belongsTo(\App\Models\EncuestaBase::class, 'encuesta_id');
    }

    public function opciones()
	{
        return $this->hasMany(\App\Models\EncuestaOpciones::class, 'pregunta_id');
    }

    public function grupo()
	{
        return $this->belongsTo(\App\Models\EncuestaGrupoPreguntas::class, 'grupo_pregunta_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
