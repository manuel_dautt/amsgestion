<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $tipo_pieza_id
 * @property int $contenido_id
 * @property int $objetivo_id
 * @property string $imagen
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradePieza extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_piezas';

	protected $fillable = [
        'tipo_pieza_id',
        'contenido_id',
        'objetivo_id',
        'imagen'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function tipo()
	{
		return $this->belongsTo(\App\Models\TradeTipo::class, 'tipo_pieza_id', 'id');
    }

    public function contenido()
	{
		return $this->belongsTo(\App\Models\TradeContenido::class);
    }

    public function objetivo()
	{
		return $this->belongsTo(\App\Models\TradeObjetivo::class);
    }


    public function pieza_objetivo()
	{
		return $this->hasMany(\App\Models\TradePiezaObjetivos::class);
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


    /**
     * Recupera El Nombre de una Clase de
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNombre($query, $id)
    {
        return $query->whereId($id)->select('imagen')->first();
    }


//FIN
}
