<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $user_id
 * @property int $implementacion_id
 * @property int $resultados_auditoria_id
 * @property string $idpdv
 * @property string $imagen
 * @property string $observaciones
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeAuditoria extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_auditoria';

	protected $fillable = [
        'user_id',
        'resultados_auditoria_id',
		'implementacion_id',
        'idpdv',
        'imagen',
		'observaciones'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function usuario()
	{
		return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function resultado()
	{
		return $this->belongsTo(\App\Models\TradeResultado::class, 'resultados_auditoria_id');
    }

	public function implementacion()
	{
		return $this->belongsTo(\App\Models\TradeImplementacion::class, 'implementacion_id');
    }

    public function hallazgos()
	{
		return $this->hasMany(\App\Models\TradeAuditoriaHallazgos::class, 'trade_auditoria_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}
