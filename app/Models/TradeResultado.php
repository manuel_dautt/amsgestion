<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $nombre
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeResultado extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_resultados_auditoria';

	protected $fillable = [
		'nombre',
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/


    public function auditoria()
	{
		return $this->hasMany(\App\Models\TradeAuditoria::class, 'resultados_auditoria_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}