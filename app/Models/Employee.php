<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Employee
 *
 * @property int $id
 * @property int $user_id
 * @property int $position_id
 * @property int $territory_id
 * @property string $document
 * @property string $phone
 * @property string $name
 * @property string $last_name
 * @property \Carbon\Carbon $birthday
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Position $position
 * @property \App\Models\Territory $territory
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $routes
 * @property \Illuminate\Database\Eloquent\Collection $employee_subsidiaries
 * @property \Illuminate\Database\Eloquent\Collection $employee_territories
 * @property \Illuminate\Database\Eloquent\Collection $heads
 *
 * @package App\Models
 */
class Employee extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'employees';

	protected $casts = [
		'user_id' => 'int',
		'position_id' => 'int',
		'territory_id' => 'int'
	];

	protected $dates = [
		'birthday'
	];

	protected $fillable = [
		'user_id',
		'position_id',
		'territory_id',
		'document',
		'phone',
		'name',
		'last_name',
		'birthday',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function position()
	{
		return $this->belongsTo(\App\Models\Position::class);
	}

	public function territory()
	{
		return $this->belongsTo(\App\Models\Territory::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}


	public function userActivo()
	{
		return $this->belongsTo(\App\User::class)
					->whereNotNull('email_verified_at');
	}


	public function admins()
	{
		return $this->belongsToMany(\App\Models\Admins::class, 'Admins')
					->withPivot('id')
					->withTimestamps();
	}

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Channel::class, 'employee_channels')
					->withPivot('id')
					->withTimestamps();
	}

	public function routes()
	{
		return $this->belongsToMany(\App\Models\Route::class, 'employee_routes')
					->withPivot('id', 'status')
					->withTimestamps();
	}

	public function employee_subsidiaries()
	{
		return $this->hasMany(\App\Models\EmployeeSubsidiary::class);
	}

	public function employee_territories()
	{
		return $this->hasMany(\App\Models\EmployeeTerritory::class);
	}

	public function heads()
	{
		return $this->hasMany(\App\Models\Head::class, 'head_id');
	}
	
	public function empleado_roles()
	{
		return $this->hasMany(\App\Models\empleado_role::class, 'id_empleado');
	}


/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

    /**
     * Recupera Todos los registros Con Ststud = A
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatusA()
    {
        return $this->where('status','=','A');
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }

    /**
     * Recupera Un registro Con una Cedula determinada
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCedula($query, $cedula)
    {
        return $query->where('document',$cedula);
    }

	/**
     * Recupera Un registro Que coincidan con un nombre
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeComoNombre($query, $nombre)
    {
        return $query->where('name', 'like', '%'.$nombre.'%');
    }

	/**
     * Recupera Un registro Que coincidan con un Apellido
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeComoApellido($query, $apellido)
    {
        return $query->where('last_name', 'like', '%'.$apellido.'%');
    }

    /**
     * Trae la info basica del empleado Actual (Autenticado) (Territorio, regional, cargo)
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRegional()

    {
            $id_usr = Auth::user()->id;
            $emp = $this->with('territory.regional')->where('user_id',$id_usr)->first();
            $empleado['id_regional'] = $emp->territory->regional->id;
            $empleado['nom_regional'] = $emp->territory->regional->regional_name;
            return $empleado;
    }


    /**
     * Lista los empleados de Una regional
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeListaRegional($nombre_regional)
    {

        return $this->with(['territory','territory.regional','position','user'])
            ->whereHas('territory.regional', function (Builder $q) use($nombre_regional)
            {
                $q->where('regional_name',$nombre_regional);
            })->get();

    }



    /**
     * Recupera Registros ya validados, es decir que tengan un Role
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTraerEmpleados($query, $regional)
    {
        return $this->with(['territory','territory.regional','position','user','channels'])
                    ->whereHas('territory.regional', function (Builder $q) use($regional)
                        {
                            $q->where('regional_name','LIKE',"%$regional%");
                        })
                    ->where('user_id','<>',Auth::user()->id);
    }



    /**
     * Recupera Registros ya validados, es decir que tengan un Role
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFiltros($query,$regional, $filtro, $texto)
    {
        return $query->where(function($query) use($filtro, $texto){
                        if($filtro == 'cedula'){
                            $query->Cedula($texto);
                        }elseif($filtro == 'nombre'){
                            $query->ComoNombre($texto);
                        }elseif($filtro == 'apellido'){
                            $query->ComoApellido($texto);
                        }
                    });

    }

    /**
     * Trae la info basica del empleado Actual (Autenticado) (Nombre, Apellido, Cedula, Celular)
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDatosEmpleado()

    {
            $id_usr = Auth::user()->id;
            $emp = $this->where('user_id',$id_usr)->first();
            $empleado['cedula'] = $emp->document;
            $empleado['nombre'] = $emp->name;
            $empleado['apellido'] = $emp->last_name;
            $empleado['nombre_completo'] = $emp->name.' '.$emp->last_name;
            $empleado['celular'] = $emp->phone;
            return $empleado;
    }


//FIN
}
