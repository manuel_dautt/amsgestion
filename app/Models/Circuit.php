<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Circuit
 * 
 * @property int $id
 * @property int $zone_id
 * @property string $circuit_name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Zone $zone
 * @property \Illuminate\Database\Eloquent\Collection $circuit_pos
 * @property \Illuminate\Database\Eloquent\Collection $routes
 *
 * @package App\Models
 */
class Circuit extends Model
{
	
/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'circuits';

	protected $casts = [
		'zone_id' => 'int'
	];

	protected $fillable = [
		'zone_id',
		'circuit_name',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function zone()
	{
		return $this->belongsTo(\App\Models\Zone::class);
	}

	public function circuit_pos()
	{
		return $this->hasMany(\App\Models\CircuitPo::class);
	}

	public function routes()
	{
		return $this->belongsToMany(\App\Models\Route::class, 'route_circuits')
					->withPivot('id', 'status')
					->withTimestamps();
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

