<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Position
 * 
 * @property int $id
 * @property string $position_name
 * @property int $gm_level
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $employees
 *
 * @package App\Models
 */
class Position extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'positions';

	protected $casts = [
		'gm_level' => 'int'
	];

	protected $fillable = [
		'position_name',
        'description',
		'gm_level',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function employees()
	{
		return $this->hasMany(\App\Models\Employee::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }



    /**
     * Recupera El Nombre de un CArgo 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNombre($query, $id)
    {
        return $query->whereId($id)->select('position_name')->first();
    }

//FIN
}

