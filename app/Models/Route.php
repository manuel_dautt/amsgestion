<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Route
 * 
 * @property int $id
 * @property int $subsidiary_id
 * @property string $route_name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Subsidiary $subsidiary
 * @property \Illuminate\Database\Eloquent\Collection $employees
 * @property \Illuminate\Database\Eloquent\Collection $circuits
 *
 * @package App\Models
 */
class Route extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'routes';

	protected $casts = [
		'subsidiary_id' => 'int'
	];

	protected $fillable = [
		'subsidiary_id',
		'route_name',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function subsidiary()
	{
		return $this->belongsTo(\App\Models\Subsidiary::class);
	}

	public function employees()
	{
		return $this->belongsToMany(\App\Models\Employee::class, 'employee_routes')
					->withPivot('id', 'status')
					->withTimestamps();
	}

	public function circuits()
	{
		return $this->belongsToMany(\App\Models\Circuit::class, 'route_circuits')
					->withPivot('id', 'status')
					->withTimestamps();
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

