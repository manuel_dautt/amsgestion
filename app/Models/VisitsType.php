<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Territory
 *
 * @property bigint $id
 * @property string $name_type_visit
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Visits $visit
 * @property \Illuminate\Database\Eloquent\Collection $visit
 *
 * @package App\Models
 */
class VisitsType extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'visit_type';

	protected $fillable = [
        'name_type_visit'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

    public function visitas()
    {
        return $this->hasMany(\App\Models\Visits::class, 'visit_type_id');
    }



/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }


	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

