<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoposVisitas extends Model
{

    protected $table = 'infopos_visitas_pv';

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

public function usuario()
{
    return $this->belongsTo(\App\User::class);
}

public function tipo_visita()
{
    return $this->hasOne(\App\Models\InfoposTipoVisita::class);
}


//FIN
}
