<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Territory
 * 
 * @property int $id
 * @property int $regional_id
 * @property string $territory_name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Regional $regional
 * @property \Illuminate\Database\Eloquent\Collection $employee_territories
 * @property \Illuminate\Database\Eloquent\Collection $employees
 * @property \Illuminate\Database\Eloquent\Collection $subsidiaries
 *
 * @package App\Models
 */
class Territory extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'territories';

	protected $casts = [
		'regional_id' => 'int'
	];

	protected $fillable = [
		'regional_id',
		'territory_name',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function regional()
	{
		return $this->belongsTo(\App\Models\Regional::class);
	}

	public function employee_territories()
	{
		return $this->hasMany(\App\Models\EmployeeTerritory::class);
	}

	public function employees()
	{
		return $this->hasMany(\App\Models\Employee::class);
	}

	public function subsidiaries()
	{
		return $this->hasMany(\App\Models\Subsidiary::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }

    /**
     * Trae Los Territorios asociadas a una Regional 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeUnaRegional($query, $id)
    {
        return $query->where('regional_id',$id)->whereStatus('A')->get();
    }


    /**
     * Trae el nombre de un territorio 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNombre($query, $id)
    {
        return $query->whereId($id)->select('territory_name')->first();
    }


//FIN
}

