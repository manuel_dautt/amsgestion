<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $trade_auditoria_id
 * @property int $trade_hallazgo_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeAuditoriaHallazgos extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_auditoria_hallazgos';

	protected $fillable = [
        'trade_auditoria_id',
		'trade_hallazgo_id'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function auditoria()
	{
		return $this->belongsTo(\App\Models\TradeAuditoria::class, 'trade_auditoria_id');
    }

    public function hallazgo()
	{
		return $this->belongsTo(\App\Models\TradeHallazgos::class, 'trade_hallazgo_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}
