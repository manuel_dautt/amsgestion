<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Territory
 *
 * @property bigint $id
 * @property bigint $user_id
 * @property bigint $visit_type_id
 * @property string $idpdv
 * @property string $activa
 * @property string $planeada
 * @property \Carbon\Carbon $fecha_ini
 * @property \Carbon\Carbon $fecha_fin
 * @property decimal $longitud
 * @property decimal $latitud
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\User $user
 * @property \App\Models\VsitsType $visits_type
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $visits_type
 *
 * @package App\Models
 */
class Visits extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'visits';

	protected $fillable = [
        'user_id',
        'visit_type_id',
        'idpdv',
        'planeada',
        'fecha_ini',
        'longitud',
        'latitud'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

	public function tipo_visita()
	{
		return $this->belongTo(\App\Models\VisitsType::class, 'visit_type_id');
	}



/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }


	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

