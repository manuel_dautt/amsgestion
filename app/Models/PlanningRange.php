<?php

/**
 * Created by Manuel Dautt.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Circuit
 *
 * @property int $id
 * @property string $range_name
 * @property int $range_days
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Planning $planning
 *
 * @package App\Models
 */
class PlanningRange extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'planning_ranges';

	protected $casts = [
	];

	protected $fillable = [
		'range_name',
		'range_days'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function planning()
	{
		return $this->hasMany(\App\Models\Planning::class);
	}


/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}

