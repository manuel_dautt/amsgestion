<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Head
 *
 * @property int $id
 * @property int $numero
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class FotosPuntos extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'fotos_puntos';

	protected $fillable = [
		'idpdv',
		'numero',
		'url'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}

