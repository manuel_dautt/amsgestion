<?php

/**
 * Created by MANUEL DAUTT.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $user_id
 * @property int $employee_id.
 * @property int $channel_id
 * @property int $regional_id
 * @property int $level

 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $employees
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $regionals
 *
 * @package App\Models
 */
class Admins extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'local_admins';

	protected $fillable = [
		'user_id','employee_id','channel_id','regional_id','level'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

public function channel()
{
  return $this->belongsTo(\App\Models\Channel::class);
}

public function regional()
{
  return $this->belongsTo(\App\Models\Regional::class);
}

public function employee()
{
  return $this->belongsTo(\App\Models\Employee::class);
}

public function user()
{
  return $this->belongsTo(\App\User::class);
}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Valida si el Usuario es una Admin/CoAdmin
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeValidar()
    {
        $id_usr = Auth::user()->id;
        $admin = $this->where('user_id',$id_usr)->first();
        return $admin?true:false;
    }
	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }

    /**
     * Recupera La regional asignada al admin/coadmin
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRegion()
    {
        $id_usr = Auth::user()->id;
        $emp = $this->with('regional')->where('user_id',$id_usr)->first();

        $regional['id_regional'] = $emp->regional_id;
        $regional['nom_regional'] = $emp->regional->regional_name;
        return $regional;
    }


	/**
     * Recupera El Canal asignada al admin/coadmin
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCanal()
    {
			$id_usr = Auth::user()->id;
			$emp = $this->with('channel')->where('user_id',$id_usr)->first();
			$canal['id_canal'] = $emp->channel->id;
			$canal['nom_canal'] = $emp->channel->channel_name;
			return $canal;
    }


	/**
     * Trae los Admins/CoAdmins Validos excepto el user actual
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTraerAdmins($query, $regional)
    {
        return $query->with(['user','employee','channel','regional'])
        ->whereHas('regional', function (Builder $q) use($regional)
                  {
                      $q->where('regional_name','LIKE',"%$regional%");
                  })
        ->where('user_id','<>',Auth::user()->id);
    }



//FIN
}
