<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Regional
 *
 * @property int $id
 * @property int $country_id
 * @property string $regional_name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Country $country
 * @property \Illuminate\Database\Eloquent\Collection $territories
 *
 * @package App\Models
 */
class Regional extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'regionals';

	protected $casts = [
		'country_id' => 'int'
	];

	protected $fillable = [
		'country_id',
		'regional_name',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function territories()
	{
		return $this->hasMany(\App\Models\Territory::class);
	}

	public function admins()
	{
		return $this->belongsToMany(\App\Models\Admins::class, 'local_admins')
					->withPivot('id')
					->withTimestamps();
	}	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


	/**
     * Trae Las Regiones asociadas a un Pais
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeUnPais($query, $id)
    {
        return $query->where('country_id',$id)->whereStatus('A')->get();
    }


    /**
     * Recupera El Nombre de una REGIONAL
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNombre($query, $id)
    {
        return $query->whereId($id)->select('regional_name')->first();
    }

//FIN
}
