<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $nombre
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeClaseHallazgo extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_clase_hallazgos';

	protected $fillable = [
        'nombre_clase'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function hallazgos()
	{
		return $this->hasMany(\App\Models\TradeHallazgos::class, 'trade_clase_hallazgo_id');
    }


/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}
