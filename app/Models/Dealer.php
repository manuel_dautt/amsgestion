<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dealer
 * 
 * @property int $id
 * @property string $dealer_name
 * @property string $city_name
 * @property string $address
 * @property string $phone_number
 * @property string $cell_number
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $channels
 * @property \Illuminate\Database\Eloquent\Collection $subsidiaries
 *
 * @package App\Models
 */
class Dealer extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'dealers';

	protected $fillable = [
		'dealer_name',
		'city_name',
		'address',
		'phone_number',
		'cell_number',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function channels()
	{
		return $this->belongsToMany(\App\Models\Channel::class, 'channel_dealers')
					->withPivot('id', 'status')
					->withTimestamps();
	}

	public function subsidiaries()
	{
		return $this->hasMany(\App\Models\Subsidiary::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

