<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $encuesta_id
 * @property int $pregunta_id
 * @property int $orden_pregunta
 * @property string $texto_pcion
 * @property string $siguiente_pregunta
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_pregunta
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_base
 *
 * @package App\Models
 */
class EncuestaOpciones extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_opciones_respuestas';

	protected $fillable = [
		'pregunta_id', 'encuesta_id', 'texto_opcion', 'siguiente_pregunta'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/
	public function pregunta()
	{
        return $this->belongsTo(\App\Models\EncuestaTipoPregunta::class, 'tipo_pregunta_id');
    }

    public function encuesta()
	{
        return $this->belongsTo(\App\Models\EncuestaBase::class, 'encuesta_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
