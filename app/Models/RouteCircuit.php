<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RouteCircuit
 * 
 * @property int $id
 * @property int $route_id
 * @property int $circuit_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Circuit $circuit
 * @property \App\Models\Route $route
 *
 * @package App\Models
 */
class RouteCircuit extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'route_circuits';

	protected $casts = [
		'route_id' => 'int',
		'circuit_id' => 'int'
	];

	protected $fillable = [
		'route_id',
		'circuit_id',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function circuit()
	{
		return $this->belongsTo(\App\Models\Circuit::class);
	}

	public function route()
	{
		return $this->belongsTo(\App\Models\Route::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

