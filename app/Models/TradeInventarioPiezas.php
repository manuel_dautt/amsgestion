<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $regional_id
 * @property int $implementacion_id
 * @property int $user_id
 * @property int $cantidad
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeInventarioPiezas extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_inventario_piezas';

	protected $fillable = [
        'regional_id',
        'implementacion_id',
        'user_id',
        'cantidad'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function regional()
	{
		return $this->belongsTo(\App\Models\Regional::class, 'regional_id', 'id');
    }

    public function piezas_implementacion()
	{
		return $this->belongsTo(\App\Models\TradeImplementacion::class, 'implementacion_id','id');
    }

    public function usuario()
	{
		return $this->belongsTo(\App\User::class, 'user_id','id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     *
     *
     */



//FIN
}
