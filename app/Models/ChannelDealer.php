<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChannelDealer
 * 
 * @property int $id
 * @property int $channel_id
 * @property int $dealer_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Dealer $dealer
 *
 * @package App\Models
 */
class ChannelDealer extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'channel_dealers';

	protected $casts = [
		'channel_id' => 'int',
		'dealer_id' => 'int'
	];

	protected $fillable = [
		'channel_id',
		'dealer_id',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class);
	}

	public function dealer()
	{
		return $this->belongsTo(\App\Models\Dealer::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

