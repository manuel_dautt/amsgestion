<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $nombre_clase
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradePLanFidelizacion extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_plan_fidelizacion';

	protected $fillable = [];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function puntos()
	{
		return $this->hasMany(\App\Models\TradePvFidelizado::class, 'plan_id');
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */

//FIN
}
