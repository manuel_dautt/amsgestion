<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $trade_clase_hallazgo_id
 * @property string $nombre
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeHallazgos extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_hallazgos';

	protected $fillable = [
        'trade_clase_hallazgo_id',
        'nombre'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function clase()
	{
		return $this->belongsTo(\App\Models\TradeClaseHallazgo::class, 'trade_clase_hallazgo_id');
    }

    public function auditorias()
	{
		return $this->hasMany(\App\Models\TradeAuditoriaHallazgos::class, 'trade_hallazgo_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}
