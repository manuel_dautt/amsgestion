<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordReset
 * 
 * @property int $id
 * @property string $email
 * @property string $token
 * @property string $created_at
 *
 * @package App\Models
 */
class PasswordReset extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'password_resets';
	public $timestamps = false;

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'email',
		'token'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/



//FIN
}

