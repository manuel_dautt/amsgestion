<?php

/**
 * Created by Manuel Dautt.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Circuit
 *
 * @property int $id
 * @property string $codigo
 * @property string $nombre
 * @property string $tipo
 * @property string $categoria
 * @property string $cve
 * @property float $lat
 * @property float $lng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\InfoposPuntosCve $infopos_pv_cve
 *
 * @package App\Models
 */
class InfoposPuntosCve extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'infopos_pv_cve';

	protected $casts = [
	];

	protected $fillable = [
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/



/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}

