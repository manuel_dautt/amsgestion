<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoposTipoVisita extends Model
{

    protected $table = 'infopos_tipo_visita';


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

public function visita()
{
    return $this->belongsTo(\App\Models\InfoposVisitas::class);
}


//FIN
}
