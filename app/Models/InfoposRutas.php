<?php

/**
 * Created by Manuel Dautt.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Circuit
 *
 * @property int $id
 * @property int $id_circuito
 * @property string $circuito
 * @property int $id_ruta
 * @property string $nom_ruta
 * @property string $desc_ruta
 * @property string $range_name
 * @property string $estado_ruta
 * @property int $lun
 * @property int $mar
 * @property int $mie
 * @property int $jue
 * @property int $vie
 * @property int $sab
 * @property int $dom
 * @property string $empleado
 * @property string $cedula
 * @property string $dms_user
 * @property string $dms_user_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\InfoposRutas $infopos_rutas
 *
 * @package App\Models
 */
class InfoposRutas extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'infopos_rutas_dms';

	protected $casts = [
	];

	protected $fillable = [
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/



/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}

