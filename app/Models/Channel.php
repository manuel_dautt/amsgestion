<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $channel_name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $dealers
 * @property \Illuminate\Database\Eloquent\Collection $employees
 * @property \Illuminate\Database\Eloquent\Collection $points_of_sales
 *
 * @package App\Models
 */
class Channel extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'channels';

	protected $fillable = [
		'channel_name'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function dealers()
	{
		return $this->belongsToMany(\App\Models\Dealer::class, 'channel_dealers')
					->withPivot('id', 'status')
					->withTimestamps();
	}

	public function employees()
	{
		return $this->belongsToMany(\App\Models\Employee::class, 'employee_channels')
					->withPivot('id')
					->withTimestamps();
	}


	public function admins()
	{
		return $this->belongsToMany(\App\Models\Admins::class, 'Admins')
					->withPivot('id')
					->withTimestamps();
	}

	public function points_of_sales()
	{
		return $this->hasMany(\App\Models\PointsOfSale::class);
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


    /**
     * Recupera El Nombre de un Canal
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNombre($query, $id)
    {
        return $query->whereId($id)->select('channel_name')->first();
    }


//FIN
}
