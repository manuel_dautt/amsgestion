<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Zone
 * 
 * @property int $id
 * @property int $subsidiary_id
 * @property string $zone_name
 * @property string $zone_description
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Subsidiary $subsidiary
 * @property \Illuminate\Database\Eloquent\Collection $circuits
 * @property \Illuminate\Database\Eloquent\Collection $points_of_sales
 *
 * @package App\Models
 */
class Zone extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'zones';

	protected $casts = [
		'subsidiary_id' => 'int'
	];

	protected $fillable = [
		'subsidiary_id',
		'zone_name',
		'zone_description',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function subsidiary()
	{
		return $this->belongsTo(\App\Models\Subsidiary::class);
	}

	public function circuits()
	{
		return $this->hasMany(\App\Models\Circuit::class);
	}

	public function points_of_sales()
	{
		return $this->hasMany(\App\Models\PointsOfSale::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

