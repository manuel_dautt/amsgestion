<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dealer
 * 
 * @property int $id
 * @property int $periodo
 * @property string $canal
* @property string $regional
* @property string $tienda
 * @property int $validos
 * @property int $puntuales
 * @property int $atendidos
 * @property int $abandonados
 * @property int $ns
 * @property int $p_abandonos
 * @property string $calificacion
 * @property string $alarma
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 *
 * @package App\Models
 */
class TiendasNPS extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'tiendas_nps';

	protected $fillable = [	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/


//FIN
}

