<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $visita_id
 * @property int $encuesta_id
 * @property string $completada
 * @property int $pregunta_actual
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $visitas
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_base
 *
 * @package App\Models
 */
class EncuestaVisitas extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_visita';

	protected $fillable = [
		'visita_id', 'encuesta_id'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/
	public function visita()
	{
        return $this->belongsTo(\App\Models\Visits::class, 'visita_id');
    }

    public function encuesta()
	{
        return $this->belongsTo(\App\Models\EncuestaBase::class, 'encuesta_id');
    }

    public function respuestas()
	{
        return $this->hasMany(\App\Models\EncuestaRespuestas::class, 'encuesta_visita_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
