<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CircuitPos
 * 
 * @property int $id
 * @property int $pos_id
 * @property int $circuit_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Circuit $circuit
 * @property \App\Models\PointsOfSale $points_of_sale
 *
 * @package App\Models
 */
class CircuitPos extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'circuit_pos';

	protected $casts = [
		'pos_id' => 'int',
		'circuit_id' => 'int'
	];

	protected $fillable = [
		'pos_id',
		'circuit_id',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function circuit()
	{
		return $this->belongsTo(\App\Models\Circuit::class);
	}

	public function points_of_sale()
	{
		return $this->belongsTo(\App\Models\PointsOfSale::class, 'pos_id');
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

