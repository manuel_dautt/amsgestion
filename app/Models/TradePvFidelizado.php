<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $nombre_clase
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradePvFidelizado extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_pv_fidelizacion';

	protected $fillable = [];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function plan()
	{
		return $this->belongsTo(\App\Models\TradePlanFidelizacion::class, 'plan_id');
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

//FIN
}
