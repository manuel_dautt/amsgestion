<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $nombre_tipo
 * @property string $descripcion
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $dealers
 * @property \Illuminate\Database\Eloquent\Collection $employees
 * @property \Illuminate\Database\Eloquent\Collection $points_of_sales
 *
 * @package App\Models
 */
class EncuestaTipo extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_tipos';

	protected $fillable = [
		'nombre_tipo', 'descripcion'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/
	public function encuestas()
	{
		return $this->hasMany(\App\Models\EncuestaBase::class);
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
