<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{

    protected $table = 'users_access';

    protected $fillable = [
        'user_id', 'acceso','user_ip',
    ];



    public function user()
	{
		return $this->belongsTo(\App\User::class);
	}


//FIN
}
