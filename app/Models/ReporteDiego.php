<?php

/**
 * Created by Manuel Dautt.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**

 * @package App\Models
 */
class ReporteDiego extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'infopos_reporte_diego_700';

	protected $casts = [
	];

	protected $fillable = [

	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/



/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/

	public function scopeCves($departamento, $municipio){
		
		
		return $this->where('departamento',strtoupper($departamento))
						->where('municipio',$municipio)
						->select('cve','cve_go_cial')
						->distinct()
						->orderBy('municipio','ASC')
						->get()
						->toArray();
	}

//FIN
}

