<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $tipo_encuesta_id
 * @property int $canal_id
 * @property string $nombre_encuesta
 * @property string $descripcion
 * @property int $version
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_tipo
 * @property \Illuminate\Database\Eloquent\Collection $channels
 *
 * @package App\Models
 */
class EncuestaBase extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_base';

	protected $fillable = [
		'tipo_encuesta_id', 'canal_id', 'nombre_encuesta', 'descripcion', 'version'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/
	public function tipo()
	{
        return $this->belongsTo(\App\Models\EncuestaTipo::class, 'tipo_encuesta_id');
    }

    public function canal()
	{
        return $this->belongsTo(\App\Models\Channel::class, 'canal_id');
    }

    public function preguntas()
	{
        return $this->hasMany(\App\Models\EncuestaPreguntas::class, 'encuesta_id');
    }

    public function opciones()
	{
        return $this->hasMany(\App\Models\EncuestaOpciones::class, 'encuesta_id');
    }

    public function visitas()
	{
        return $this->hasMany(\App\Models\EncuestaVisitas::class, 'encuesta_id');
    }
	
	public function documentos()
	{
        return $this->hasMany(\App\Models\EncuestaDocumentos::class, 'encuesta_id');
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
