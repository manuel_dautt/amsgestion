<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TradePiezasImplementacion;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $channel_id
 * @property int $regional_id
 * @property string $descripcion
 * @property date $fecha_ini
 * @property date $fecha_fin
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradeImplementacion extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_implementaciones';

	protected $fillable = [
		'descripcion','fecha_ini','fecha_fin'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function canal()
	{
		return $this->belongsTo(\App\Models\Channel::class);
  }

  public function regional()
	{
		return $this->belongsTo(\App\Models\Regional::class);
	}

	public function piezas_implementacion()
	{
		return $this->hasMany(\App\Models\TradePiezasImplementacion::class);
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


    /**
     * Recupera El Nombre de una Clase de
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNombre($query, $id)
    {
        return $query->whereId($id)->select('descripcion')->first();
    }


//FIN
}
