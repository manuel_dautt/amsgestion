<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Subsidiary
 * 
 * @property int $id
 * @property int $dealer_id
 * @property int $territory_id
 * @property string $subsidiary_name
 * @property string $city_name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Dealer $dealer
 * @property \App\Models\Territory $territory
 * @property \Illuminate\Database\Eloquent\Collection $employee_subsidiaries
 * @property \Illuminate\Database\Eloquent\Collection $points_of_sales
 * @property \Illuminate\Database\Eloquent\Collection $routes
 * @property \Illuminate\Database\Eloquent\Collection $zones
 *
 * @package App\Models
 */
class Subsidiary extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'subsidiaries';

	protected $casts = [
		'dealer_id' => 'int',
		'territory_id' => 'int'
	];

	protected $fillable = [
		'dealer_id',
		'territory_id',
		'subsidiary_name',
		'city_name',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function dealer()
	{
		return $this->belongsTo(\App\Models\Dealer::class);
	}

	public function territory()
	{
		return $this->belongsTo(\App\Models\Territory::class);
	}

	public function employee_subsidiaries()
	{
		return $this->hasMany(\App\Models\EmployeeSubsidiary::class);
	}

	public function points_of_sales()
	{
		return $this->hasMany(\App\Models\PointsOfSale::class);
	}

	public function routes()
	{
		return $this->hasMany(\App\Models\Route::class);
	}

	public function zones()
	{
		return $this->hasMany(\App\Models\Zone::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

