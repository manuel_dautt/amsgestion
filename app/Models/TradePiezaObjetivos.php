<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $pieza_id
 * @property int $objetivo_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class TradePiezaObjetivos extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'trade_pieza_objetivos';

	protected $fillable = [
        'pieza_id',
        'objetivo_id'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/

	public function pieza()
	{
		return $this->belongsTo(\App\Models\TradePieza::class);
    }

    public function objetivo()
	{
		return $this->belongsTo(\App\Models\TradeObjetivo::class);
	}

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
	/**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }




//FIN
}
