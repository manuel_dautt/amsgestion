<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $encuesta_visita_id
 * @property int $pregunta_id
 * @property int $opcion_respuesta_id
 * @property string $opcion_respuesta_txt
 * @property string $texto_respuesta
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_preguntas
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_visita
 * @property \Illuminate\Database\Eloquent\Collection $encuesta_opciones_respuesta
 *
 * @package App\Models
 */
class EncuestaRespuestas extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_respuestas_visitas';

	protected $fillable = [
		'encuesta_visita_id', 'pregunta_id', 'opcion_respuesta_id', 'opcion_respuesta_txt', 'texto_respuesta'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/
	public function visita()
	{
        return $this->belongsTo(\App\Models\EncuestaVisitas::class, 'encuesta_visita_id');
    }

    public function pregunta()
	{
        return $this->belongsTo(\App\Models\EncuestaPreguntas::class, 'pregunta_id');
    }

    public function opciones()
	{
        return $this->belongsTo(\App\Models\EncuestaPreguntas::class, 'pregunta_id');
    }

    /********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
