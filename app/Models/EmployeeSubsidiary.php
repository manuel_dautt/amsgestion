<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EmployeeSubsidiary
 * 
 * @property int $id
 * @property int $employee_id
 * @property int $subsidiary_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Employee $employee
 * @property \App\Models\Subsidiary $subsidiary
 *
 * @package App\Models
 */
class EmployeeSubsidiary extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/
	
	protected $table = 'employee_subsidiaries';

	protected $casts = [
		'employee_id' => 'int',
		'subsidiary_id' => 'int'
	];

	protected $fillable = [
		'employee_id',
		'subsidiary_id',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function employee()
	{
		return $this->belongsTo(\App\Models\Employee::class);
	}

	public function subsidiary()
	{
		return $this->belongsTo(\App\Models\Subsidiary::class);
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

