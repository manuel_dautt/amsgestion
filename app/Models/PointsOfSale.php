<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PointsOfSale
 * 
 * @property int $id
 * @property int $subsidiary_id
 * @property int $channel_id
 * @property int $zone_id
 * @property string $pos_name
 * @property string $pos_city
 * @property string $pos_address
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Channel $channel
 * @property \App\Models\Subsidiary $subsidiary
 * @property \App\Models\Zone $zone
 * @property \Illuminate\Database\Eloquent\Collection $circuit_pos
 *
 * @package App\Models
 */
class PointsOfSale extends Model
{

/*****************************
  DECLARACION DE VARIABLES 
******************************/

	protected $table = 'points_of_sale';

	protected $casts = [
		'subsidiary_id' => 'int',
		'channel_id' => 'int',
		'zone_id' => 'int'
	];

	protected $fillable = [
		'subsidiary_id',
		'channel_id',
		'zone_id',
		'pos_name',
		'pos_city',
		'pos_address',
		'status'
	];

/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS 
**********************************************/

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class);
	}

	public function subsidiary()
	{
		return $this->belongsTo(\App\Models\Subsidiary::class);
	}

	public function zone()
	{
		return $this->belongsTo(\App\Models\Zone::class);
	}

	public function circuit_pos()
	{
		return $this->hasMany(\App\Models\CircuitPo::class, 'pos_id');
	}

	
/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO 
*********************************************/
	/**
     * Recupera Todos los registros 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

	/**
     * Recupera Todos los registros Activos 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereStatus('A')->get();
    }

	/**
     * Recupera Un registro Especifico 
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN
}

