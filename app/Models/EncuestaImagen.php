<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 12 Oct 2019 17:07:10 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property int $encuesta_visita_id
 * @property string $nombre_foto
 * @property string $url
 * @property string $estado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 *
 * @package App\Models
 */
class EncuestaImagen extends Model
{

/*****************************
  DECLARACION DE VARIABLES
******************************/

	protected $table = 'encuesta_imagenes';

	protected $fillable = [
		'encuesta_visita_id', 'nombre_foto','url'
	];


/*********************************************
  METODOS PARA RELACIONES CON OTROS MODELOS
**********************************************/


/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/


//FIN
}
