<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

use App\Notifications\UserResetPassword;
use App\Notifications\VerifyEmail;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $api_token
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $employees
 *
 * @package App\Models
 */

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;

   /*****************************
    DECLARACION DE VARIABLES
    ******************************/


    /**
     * The attributes that specific a table in the database.
     *
     * @var string
     */

    /*
     * protected $table = 'users';
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
       // 'email_verified_at' => 'datetime',
    ];

    /*********************************************
      METODOS PARA PERSONALIZAR LOS CORREOS DE
      RESET PASSWORD Y VERIFY EMAIL
     **********************************************/
    // Este metodo se encarga de enviar la notificacion
    // para el reseteo
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }


    /*********************************************
      METODOS PARA RELACIONES CON OTROS MODELOS
     **********************************************/



     public function employee()
     {
	       return $this->hasOne(\App\Models\Employee::class);
     }

     public function confidencialidad()
     {
	       return $this->hasOne(\App\Models\Confidencialidad::class);
     }

     public function admins()
   	{
   		return $this->belongsToMany(\App\Models\Admins::class, 'local_admins')
   					->withPivot('id')
   					->withTimestamps();
    }


    public function users_access()
    {
	    return $this->hasManny(\App\Models\UserAccess::class);
    }

    public function visitas()
    {
	    return $this->hasManny(\App\Models\InfoposVisitas::class);
    }
	
	public function trade()
    {
	    return $this->hasManny(\App\Models\TradeAuditorias::class);
    }

/********************************************
  METODOS DE RECUPERACION Y ALMACENAMIENTO
*********************************************/
    /**
     * Recupera Todos los registros
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTodos($query)
    {
        return $query->get();
    }

    /**
     * Recupera Todos los registros Activos
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivos($query)
    {
        return $query->whereNotNull('email_verified_at');
    }

	/**
     * Recupera Un registro Especifico
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUno($query, $id)
    {
        return $query->find($id)->get();
    }


//FIN


}
