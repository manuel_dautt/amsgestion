<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MapasExportPuntosCve implements FromArray, WithHeadings, ShouldAutoSize
{

    use Exportable;
    protected $puntosCve;


    public function __construct($puntosCve = null)
    {
        $this->puntosCve = $puntosCve;
    }

    public function headings(): array
    {
        return [
                'CODIGO_ID','NOMBRE','REGIONAL','DPTO','CIUDAD','COD_DANE','MUNIC_DANE','DIRECCION',
                'CANAL','TIPO','CATEGORIA','SEGMENTACION','VISIBILIDAD','TRANSANDO','POS_CODE','NOMBRE_CVE',
                'ALIADO','SUCURSAL',];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        //
        return $this->puntosCve;
    }
}
