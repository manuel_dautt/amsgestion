<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogAccesosExportDetalles implements FromArray, WithHeadings, ShouldAutoSize
{

    use Exportable;
    protected $totales;
    //protected $encabezados;

    public function __construct($totales = null)
    {
        $this->totales = $totales;
        //$this->accesos = $encabezados;
    }

    public function headings(): array
    {
        return ['DOCUMENTO','NOMBRE', 'CARGO','ROLE','REGIONAL','CANAL','INGRESO',];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        //
        return $this->totales;
    }
}
