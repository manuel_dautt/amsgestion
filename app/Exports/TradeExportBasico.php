<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TradeExportBasico implements FromArray, WithHeadings, ShouldAutoSize
{

    use Exportable;
    protected $tradeImplementacion;


    public function __construct($tradeImplementacion = null)
    {
        $this->tradeImplementacion = $tradeImplementacion;
    }

    public function headings(): array
    {
        return [
                'FECHA_IMPL.','IMPLEMENTADO_POR', 'DOCUMENTO','ROLE_ASIGNADO','IMPLEMENTACION','PIEZA','CLASE','COMUNICACION','OBJETIVO',
                'ID_PDV','NOMBRE_PUNTO','TIPOLOGIA','CATEGORIA_HOMOLOGADA','REGIONAL','DEPARTAMENTO',
                'CIUDAD', 'CIRCUITO', 'ESTADO_DMS', 'DISTRIBUIDOR','SUCURSAL','NOMBRE_CVE',];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        //
        return $this->tradeImplementacion;
    }
}
