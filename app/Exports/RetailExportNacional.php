<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RetailExportNacional implements FromArray, WithHeadings, ShouldAutoSize
{

    use Exportable;
    protected $puntos;


    public function __construct($puntos = null)
    {
        $this->puntos = $puntos;
    }

    public function headings(): array
    {
        return [
                'IDPDV','CADENA','NOMBRE_PUNTO','REGIONAL','DEPARTAMENTO','CIUDAD','DIRECCION','POS_PADRE',
                'POS_CODE','EJECUTIVO_RESPONSABLE',];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        //
        return $this->puntos;
    }
}
