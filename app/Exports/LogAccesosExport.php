<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LogAccesosExport implements FromArray, WithHeadings, ShouldAutoSize
{

    use Exportable;
    protected $accesos;
    //protected $encabezados;

    public function __construct($accesos = null)
    {
        $this->accesos = $accesos;
        //$this->accesos = $encabezados;
    }

    public function headings(): array
    {
        return ['DOCUMENTO','NOMBRE','CARGO','ROLE','REGIONAL','CANAL','FECHA ACCESO','I.P. ACCESO',];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        //
        return $this->accesos;
    }
}
