<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MapasExportPuntosCve;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\InfoposPuntosCve;



class MapaGestorController extends Controller
{

/**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      /*
      $this->middleware('permission:VER_ACCESOS', ['only' => ['index']]);
      $this->middleware('permission:LISTAR_ACCESOS', ['only' => ['traer_cedula']]);
      $this->middleware('permission:VER_PAISES', ['only' => ['show']]);
      $this->middleware('permission:CREAR_PAISES', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_PAISES', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_PAISES', ['only' => ['destroy']]);
      */
    }
	
	public function ActualizarCenso(Request $request){
		
		$capa_cves = $request->cves == 1 ? 1 : 0;
		$capa_antenas = $request->antenas == 1 ? 1 : 0;
		$capa_cciales = $request->cciales == 1 ? 1 : 0;
		$capa_puntos = $request->puntos == 1 ? 1 : 0;
		
		return redirect()->route('mapas.ver.censo.propios',
								[
									'c_cves' => $capa_cves,
									'c_antenas' => $capa_antenas,
									'c_cciales' => $capa_cciales,
									'c_puntos' => $capa_puntos
								]);

	}



	public function ActualizarCensoBOC(Request $request){
		
		$capa_cves = $request->cves == 1 ? 1 : 0;
		$capa_circuitos = $request->circuitos == 1 ? 1 : 0;
		$capa_puntos = $request->puntos == 1 ? 1 : 0;
		
		return redirect()->route('mapas.ver.censo.boc',
								[
									'c_cves' => $capa_cves,
									'c_circuitos' => $capa_circuitos,
									'c_puntos' => $capa_puntos
								]);

	}

	
	public function verCensoBOC($c_cves, $c_circuitos, $c_puntos){
		$capa_cves = $c_cves;
		$capa_circuitos = $c_circuitos;
		$capa_puntos = $c_puntos;
		
		$qry_mensaje = "SELECT fecha_actualizacion, mensaje FROM censo_puntos_boc_fecha;";
		$mensaje_actualizacion = DB::select($qry_mensaje);
		$mensaje['fecha'] = $mensaje_actualizacion[0]->fecha_actualizacion;
		$mensaje['texto'] = $mensaje_actualizacion[0]->mensaje;
		
		$qry_pv_censo = "SELECT id_encuesta,periodo,desarrollador,ciudad,regional,latitud,longitud,pv_cerrado,permite_encuesta,
						pv_nuevo,pv_existe,negocio,idpdv,circuito,vende_telecom,vende_tigo,vende_recarga,vende_recarga_tigo,
						simcard_tigo,interesado_simcard,interesado_recarga
						FROM censo_puntos_boc";
 
		$data_puntos = DB::select($qry_pv_censo);
		
		$pv_geojson = [];
		
        foreach($data_puntos as $pv_item){
            array_push($pv_geojson,
			    array(
                    'type' => 'Feature',
                    'geometry' =>
                        array(
                                'type' => 'Point',
                                'coordinates' => [ (float) $pv_item->longitud , (float) $pv_item->latitud ]
                        ),
			        'properties' =>
			            array (
				            'id_encuesta' => $pv_item->id_encuesta,
							'periodo' => $pv_item->periodo,
							'desarrollador' => $pv_item->desarrollador,
							'ciudad' => $pv_item->ciudad,
							'regional' => $pv_item->regional,
						//	'latitud' => $pv_item->latitud,
						//	'longitud' => $pv_item->longitud,
							'pv_cerrado' => $pv_item->pv_cerrado,
							'permite_encuesta' => $pv_item->permite_encuesta,
							'pv_nuevo' => $pv_item->pv_nuevo,
							'pv_existe' => $pv_item->pv_existe,
						//	'negocio' => htmlspecialchars($pv_item->negocio, ENT_SUBSTITUTE),
							'idpdv' => htmlspecialchars($pv_item->idpdv, ENT_SUBSTITUTE),
							'circuito' => htmlspecialchars($pv_item->circuito, ENT_SUBSTITUTE),
							'vende_telecom' => $pv_item->vende_telecom,
							'vende_tigo' => $pv_item->vende_tigo,
							'vende_recarga' => $pv_item->vende_recarga,
							'vende_recarga_tigo' => $pv_item->vende_recarga_tigo,
							'simcard_tigo' => $pv_item->simcard_tigo,
							'interesado_simcard' => $pv_item->interesado_simcard,
							'interesado_recarga' => $pv_item->interesado_recarga
			            )
                )

            );
        }
		
		$pv_geojson = ['type' => 'FeatureCollection','features' => $pv_geojson];
		//dd($pv_geojson);
		//dd($pv_geojson['features'][1]);
		return view('mapas.censo.ver_censo_boc',[
												'pv_geojson' => $pv_geojson,
												'capa_cves' => $capa_cves,
												'capa_circuitos' => $capa_circuitos,
												'capa_puntos' => $capa_puntos,
												'mensaje' => $mensaje,
											]);		
		
	}
	
	
	
	public function verCenso($c_cves, $c_antenas, $c_cciales, $c_puntos){
				
		//dd($c_cves, $c_antenas, $c_cciales, $c_puntos);
		
		$capa_cves = $c_cves;
		$capa_antenas = $c_antenas;
		$capa_cciales = $c_cciales;
		$capa_puntos = $c_puntos;
		
		$qry_pv_censo = "SELECT regional, departamento, ciudad, canal, ubicacion, centro_comercial, cadena, direccion, latitud, longitud, operador, puestos, asesores FROM censo_cp_puntos";
		$data_puntos = DB::select($qry_pv_censo);
		
		$pv_geojson = [];
		
        foreach($data_puntos as $pv_item){
            array_push($pv_geojson,
			    array(
                    'type' => 'Feature',
                    'geometry' =>
                        array(
                                'type' => 'Point',
                                'coordinates' => [ (float) $pv_item->longitud , (float) $pv_item->latitud ]
                        ),
			        'properties' =>
			            array (
				            'nombre' => $pv_item->canal.'_'.$pv_item->operador,
							'ubicacion' => $pv_item->ubicacion,
							'puestos' => $pv_item->puestos,
							'asesores' => $pv_item->asesores,
							'regional' => $pv_item->regional,
							'departamento' => $pv_item->departamento,
							'ciudad' => $pv_item->ciudad,
							'direccion' => 'Direccion', //htmlspecialchars($pv_item->direccion, ENT_SUBSTITUTE),
							'centro_comercial' => htmlspecialchars($pv_item->centro_comercial, ENT_SUBSTITUTE),
							'cadena' => $pv_item->cadena,
							'operador' => $pv_item->operador,
							'canal' => $pv_item->canal
			            )
                )

            );
        }
		
		$pv_geojson = ['type' => 'FeatureCollection','features' => $pv_geojson];
		//dd($pv_geojson['features'][1]);
		
		
		$qry_bts_censo = "SELECT antena, latitud, longitud FROM censo_cp_bts700";
		$data_bts700 = DB::select($qry_bts_censo);
		
		$bts_geojson = [];
		
        foreach($data_bts700 as $bts_item){
            array_push($bts_geojson,
			    array(
                    'type' => 'Feature',
                    'geometry' =>
                        array(
                                'type' => 'Point',
                                'coordinates' => [ (float) $bts_item->longitud , (float) $bts_item->latitud ]
                        ),
			        'properties' =>
			            array (
				            'antena' => $bts_item->antena
			            )
                )

            );
        }
		
		$bts_geojson = ['type' => 'FeatureCollection','features' => $bts_geojson];
		
		
		
		$qry_cc_censo = "SELECT regional, departamento, ciudad, direccion, nombre_cc, latitud, longitud FROM censo_cp_cciales";
		$data_cciales = DB::select($qry_cc_censo);
		
		$cc_geojson = [];
		
        foreach($data_cciales as $cc_item){
            array_push($cc_geojson,
			    array(
                    'type' => 'Feature',
                    'geometry' =>
                        array(
                                'type' => 'Point',
                                'coordinates' => [ (float) $cc_item->longitud , (float) $cc_item->latitud ]
                        ),
			        'properties' =>
			            array (
				            'nombre' => $cc_item->nombre_cc,
							'regional' => $cc_item->regional,
							'departamento' => $cc_item->departamento,
							'ciudad' => $cc_item->ciudad,
							'direccion' => $cc_item->direccion
			            )
                )

            );
        }
		
		$cc_geojson = ['type' => 'FeatureCollection','features' => $cc_geojson];
		//dd($data_bts700, json_encode($bts_geojson));
		
        return view('mapas.censo.ver_censo',[
												'bts_geojson' => $bts_geojson, 
												'cc_geojson' => $cc_geojson, 
												'pv_geojson' => $pv_geojson,
												'capa_cves' => $capa_cves,
												'capa_antenas' => $capa_antenas,
												'capa_cciales' => $capa_cciales,
												'capa_puntos' => $capa_puntos,
											]);
    }
	
    public function verOpciones(){
        return view('mapas.ver_opciones_moc');
    }

    public function ver_cve(Request $request){

        $datos_cve =
        [
        'SW_lat' => $request->SW_lat,
        'SW_lon' => $request->SW_lon,
        'NE_lat' => $request->NE_lat,
        'NE_lon' => $request->NE_lon,
        'cve_nombre' => $request->cve_nombre,
        'cve_orden' => $request->cve_orden,
        'C_lat' => ($request->SW_lat + $request->NE_lat)/2,
        'C_lon' => ($request->SW_lon + $request->NE_lon)/2,
        ];

        $pv_cve = InfoposPuntosCve::where('cve','=',$datos_cve['cve_nombre'])->get()->toArray();
        $a_geojson = [];

        foreach($pv_cve as $pv){
            array_push($a_geojson,
			    array(
                    'type' => 'Feature',
                    'geometry' =>
                        array(
                                'type' => 'Point',
                                'coordinates' => [ (float) $pv['lng'] , (float) $pv['lat'] ]
                        ),
			        'properties' =>
			            array (
				            'nombre' => $pv['nombre'],
                            'codigo' => $pv['codigo'],
                            'canal' => $pv['canal'],
				            'tipo' => $pv['tipo'],
                            'categoria' => $pv['categoria'],
                            'segmentacion' => $pv['segmentacion'],
                            'visibilidad' => $pv['visibilidad'],
                            'transando' => $pv['transando'],
                            'distribuidor' => $pv['distribuidor'],
                            'sucursal' => $pv['sucursal'],
				            'cve' => $pv['cve']
			            )
                )

            );
        }

        $v_geojson = ['type' => 'FeatureCollection','features' => $a_geojson];

        return view('mapas.ver_cve',['datos_cve' => $datos_cve, 'v_geojson' => $v_geojson]);
    }

    public function entrar(){
        return view('mapas.gestores.entrar');
    }

    /**
     * Exportar a Excel.
     *
     */
    public function exportar_puntos_cve($cve)
    {
        //

		    $consulta_puntos =
                  "SELECT
                        codigo AS CODIGO_ID,
                    	nombre AS NOMBRE,
                        regional AS REGIONAL,
                        departamento AS DPTO,
                    	ciudad AS CIUDAD,
                    	codigo_dane AS COD_DANE,
                    	municipio_dane AS MUNIC_DANE,
                        direccion AS DIRECCION,
                        canal AS CANAL,
                        tipo AS TIPO,
                        categoria AS CATEGORIA,
                        segmentacion AS SEGMENTACION,
                        visibilidad AS VISIBILIDAD,
                        transando AS TRANSANDO,
                        pos_code AS POS_CODE,
                        cve AS NOMBRE_CVE,
                        distribuidor AS ALIADO,
                        sucursal AS SUCURSAL
                    FROM
                    	infopos_pv_cve
                    WHERE
                    	cve = '$cve';";


		$puntos_cve = DB::select($consulta_puntos);

        return (new MapasExportPuntosCve($puntos_cve))
                ->download('puntos_x_cve.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }



    public function validar( Request $request ){
        $cedula = $request->input('cedula');
        $clave = $request->input('clave');
        $flag = false;
        $accesos =
        [
            ['nombre' => 'Manuel Dautt', 'cedula' => '72190955', 'clave' => 'S0712'],
            ['nombre' => 'Samuel Castellon', 'cedula' => '1002192366', 'clave' => '1880E'],
            ['nombre' => 'Luis ferreira', 'cedula' => '1085182992', 'clave' => '1A9C0'],
            ['nombre' => 'Tonys Barrera', 'cedula' => '72336502', 'clave' => 'B0A2E'],
            ['nombre' => 'Jhonatan Mendoza', 'cedula' => '1129495967', 'clave' => '1B947'],
            ['nombre' => 'MILADYS SOTO', 'cedula' => '1131069183', 'clave' => '1BA1F'],
            ['nombre' => 'NAYLUBIS CORTES', 'cedula' => '1047225618', 'clave' => '198AA'],
            ['nombre' => 'JELAINE MARCELES', 'cedula' => '1140876266', 'clave' => '1BE5A'],
            ['nombre' => 'Carlos Florez', 'cedula' => '1102810663', 'clave' => '1B047'],
            ['nombre' => 'ASHLY ARGUMEDO', 'cedula' => '1148436030', 'clave' => '1BD6E'],
            ['nombre' => 'SOLEY MONTERROZA', 'cedula' => '1070811056', 'clave' => '1A230'],
            ['nombre' => 'ROBERTO PANIAGUA', 'cedula' => '15620672', 'clave' => '26400'],
            ['nombre' => 'Alvaro Lacera', 'cedula' => '1082858884', 'clave' => '1A954'],
            ['nombre' => 'Dajan Arias', 'cedula' => '1102853346', 'clave' => '1AF0A'],
            ['nombre' => 'PAOLA NAVARRO', 'cedula' => '22865550', 'clave' => '37CC6'],
        ];

        foreach($accesos as $acceso){
            if ($acceso['cedula'] == $cedula){
                if($acceso['clave'] == $clave){
                    echo 'Correcto Todo<br>';
                    $flag = true;
                }else{
                    echo 'Error, Clave no Valida<br>';
                }
            }
        }

        if($flag){
            echo 'Acceso Garantizado<br>';
        }else{
            echo 'Acceso Negado<br>';
        }
        return $accesos;
    }



}
