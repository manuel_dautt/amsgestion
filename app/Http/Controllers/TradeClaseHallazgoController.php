<?php

namespace App\Http\Controllers;

Use App\Models\TradeClaseHallazgo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TradeClaseHallazgoController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_CLASE_HALLAZGO', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_CLASE_HALLAZGO', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_CLASE_HALLAZGO', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_CLASE_HALLAZGO', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_CLASE_HALLAZGO', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clasesH = TradeClaseHallazgo::latest()->paginate(5);

        return view('trade.clasehallazgo.index',compact('clasesH'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trade.clasehallazgo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre_clase' => ['required', 'max:50','unique:trade_clase_hallazgos']
        ]);


        TradeClaseHallazgo::create(['nombre_clase' => strtoupper($request->nombre_clase)]);


        return redirect()->route('clasehallazgo.index')
                        ->with('success','Clase Creada Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $claseh = TradeClaseHallazgo::find($id);
        return view('trade.clasehallazgo.show',compact('claseh'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $claseh = TradeClaseHallazgo::find($id);
        return view('trade.clasehallazgo.edit',compact('claseh'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nombre_clase' => ['required', 'max:50','unique:trade_clase_hallazgos,nombre_clase,'.$id]
        ]);

        $claseh = TradeClaseHallazgo::find($id);

        $claseh->update(['nombre_clase' => strtoupper($request->nombre_clase)]);

        return redirect()->route('clasehallazgo.index')
                        ->with('success','Clase Actualizada Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $claseh = TradeClaseHallazgo::find($id);
        $claseh->delete();

        return redirect()->route('clasehallazgo.index')
                        ->with('success','Clase Eliminada Exitosamente');
    }
}
