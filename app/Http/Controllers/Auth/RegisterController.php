<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\User;
Use App\Models\Employee;
Use App\Models\Position;
Use App\Models\Country;
Use App\Models\Regional;
Use App\Models\Territory;
Use App\Models\Channel;
Use App\Models\Admins;


use Illuminate\Support\Arr;
use Mail;
use App\Mail\NotificacionNuevoUsuario;
use App\Mail\ActivacionNuevoUsuario;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,
            [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'document' => ['required', 'digits_between:6,11', 'unique:employees,document'],
            'email' => ['required', 'email', 'email', 'max:255', 'unique:users'],
            'cell' => ['required', 'digits:10', 'unique:employees,phone'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
          /*  'bday' => ['required', 'date'], */
            'regional' => ['required', 'not_in:0','integer'],
            'territory' => ['required', 'not_in:0','integer'],
            'channel' => ['required', 'not_in:0','integer'],
            'position' => ['required', 'not_in:0','integer'],
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $nombre = strtoupper($data['name'].' '.$data['last_name']);
        $canal_id = $data['channel'];
        $territorio = Territory::find($data['territory']);

        //*************************** */
        //Creacion del Usuario::crete
        //*************************** */
        $usuario = User::create([
                'name' => $nombre,
                'email' => $data['email'],
				//Solicitan eliminar por tema de simplificacion
                //'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make($data['password']),
        ]);

        $id_usuario = $usuario->id;

        //**************************** */
        //Creacion del Empleado::crete
        //**************************** */
        $empleado = Employee::create([
                'user_id' => $id_usuario,
                'position_id' => $data['position'],
                'territory_id' => $data['territory'],
                'name' => strtoupper($data['name']),
                'phone' => $data['cell'],
                'last_name' => strtoupper($data['last_name']),
                'document' => $data['document'],
                'birthday' => date('Y-m-d'),
        ]);

        //ASOCIA AL EMPLEADO A UNO DE VARIOS CANALES
        //EN ESTE CASO POR SER EL PRIMERO ENTONCES
        //SE CONVIERTE EN EL  CANAL PRINCIPAL
        $empleado->channels()->attach($canal_id);

        //ASOCIAMOS EL ROL DE USUARIO SIN ACCESO
        $usuario->assignRole('INVITADO SIN ACCESOS');

        $n_regional = Regional::Nombre($data['regional']);
        $n_territorio = Territory::Nombre($empleado->territory_id);
        $n_canal = Channel::Nombre($canal_id);
        $n_cargo = Position::Nombre($empleado->position_id);

        //******************************************** */
        // BUSCAMOS A LOS ADMINISTRADORES REGIONALES
        // PARA NOTIFICARLES LA CREACION DEL EMPLEADO
        //******************************************** */
        // PRIMERO POR REGIONAL/CANAL LUEGO BUSCAMOS A ALGUIEN DE LA REGIONAL

        $admins = Admins::where('regional_id',$territorio->regional_id)
                         ->where('channel_id',$canal_id)
                         ->get();

        if(count($admins)<=0){
            // AQUI VALIDA PARA LA REGIONAL CUALQUIER CANAL
            $admins = Admins::where('regional_id',$territorio->regional_id)->get();
            if(count($admins)<=0){
                //EN CASO QUE LA REGIONAL NO TENGA A NINGUN ADMIN REGISTRADO
                //SE ENVIA A TODOS LOS ADMINISTRADORES PARA QUE AYUDEN
                $admins = Admins::where('level','ADMIN')->get();
            }
        }

        $datosMail = [];
        $datosMail['id_empleado'] = $empleado->id;
        $datosMail['nombre'] = $empleado->name;
        $datosMail['apellido'] = $empleado->last_name;
        $datosMail['documento'] = $empleado->document;
        $datosMail['correo'] = $usuario->email;
        $datosMail['nombre_completo'] = $usuario->name;
        $datosMail['regional'] = $n_regional->regional_name;
        $datosMail['territorio']  = $n_territorio->territory_name;;
        $datosMail['canal'] = $n_canal->channel_name;;
        $datosMail['cargo'] = $n_cargo->position_name;;

        $correo_cliente = $usuario->email;
        $nombre_cliente = $usuario->name;
        $asunto_correo = 'BIENVENID@ A INFOPOS';

        // NOTIFICAMOS AL NUEVO USUARIO LA CREACION DE SU CUENTA
		      Mail::to($correo_cliente, $nombre_cliente)->send(new NotificacionNuevoUsuario($datosMail));

        //NOTIFICAMOS A EL/LOS ADMINISTRADOR(ES) EL NUEVO USUARIO

        if(count($admins)){

            foreach($admins as $admin){
                $user_admin = User::find($admin->user_id);
                $correo_admin = $user_admin->email;
                $nombre_admin = $user_admin->name;

                Mail::to($correo_admin, $nombre_admin)->send(new ActivacionNuevoUsuario($datosMail));

            }

        }

        return $usuario;
    }

    /**
     * SOBRE ESCRIBIMOS LA FUNCION showRegistrationForm.
     * PARA AÑADIRLE LA LOGICA QUE NECESITAMOS Y PUEDA
     * CREAR TANTO USERS COMO EMPLOYEE
     *
     * @param  array  $data
     * @return \App\User
     */
    public function showRegistrationForm()
    {
        //Trigo lospaises activos
        $var_paises = Country::select('id','country_name')->Activos()->toArray();

        //si es mas de uno, dejo en blanco,
        //si solo es uno cargo loasociado a ese pais
        if(count($var_paises) > 1){
            $var_regiones = [];
        }else{
            $first_pais = head($var_paises);
            $var_regiones = Regional::select('id','regional_name')->DeUnPais($first_pais['id'])->toArray();
        }

        //Traer Territorios
        $var_territorios = [];

        //traigo los canales
        $var_canales = Channel::select('id','channel_name')->Activos()->toArray();

        $var_cargos = Position::select('id','position_name')->Activos()->toArray();

        //AJUSTO LOS ARRAYS QUE VAN A PASAR A LA VISTA
        $paises = count($var_paises) > 1 ?
                            Arr::prepend($var_paises, ['id' => 0, 'country_name' => '--SELECCIONE PAIS--']) :
                            $var_paises;
        $regiones = Arr::prepend($var_regiones, ['id' => 0, 'regional_name' => '--SELECCIONE REGION--']);
        $territorios = Arr::prepend($var_territorios, ['id' => 0, 'territory_name' => '--SELECCIONE TERRITORIO--']);
        $canales = Arr::prepend($var_canales, ['id' => 0, 'channel_name' => '--SELECCIONE CANAL--']);

        $cargos = Arr::prepend($var_cargos, ['id' => 0, 'position_name' => '--SELECCIONE CARGO--']);

        //RETORNAMOS LA VISTA Y LOS ARRAYS
        return view('auth.register2', compact('paises','regiones', 'territorios','canales','cargos'));
    }
}
