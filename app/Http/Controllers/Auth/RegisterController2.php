<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
Use App\Models\Employee;
Use App\Models\Position;
Use App\Models\Country;
Use App\Models\Regional;
Use App\Models\Territory;
Use App\Models\Channel;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Arr;

class RegisterController2 extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*
    protected function validator(array $data)
    {

        // OJO: INCLUYO VALIDACION PARA CREAR EN EL MISMO REGISTRO
        //      LA INFORMACION DEL EMPLEADO QUE SE VA A ASOCIAR AL USUARIO
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'document' => ['required', 'string', 'max:11'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'bday' => ['required', 'date'],
            'regional' => ['required', 'integer'],
            'terrytory' => ['required', 'integer'],
            'channel' => ['required', 'integer'],

        ]);
    }
    */

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $nombre = strtoupper($data['name'].' '.$data['last_name']);
        $canal_id = $data['channel'];

        //Employee::crete
        $usuario = User::create([
                'name' => $nombre,
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
        ]);

        $id_usuario = $usuario->id;

        $empleado = Employee::create([
                'user_id' => $id_usuario,
                'position_id' => $data['position'],
                'territory_id' => $data['territory'],
                'name' => strtoupper($data['name']),
                'last_name' => strtoupper($data['last_name']),
                'document' => $data['document'],
                'birthday' => $data['bday'],
        ]);

        $empleado->channels()->attach($canal_id);



        return true;
    }

    /**
     * SOBRE ESCRIBIMOS LA FUNCION showRegistrationForm.
     * PARA AÑADIRLE LA LOGICA QUE NECESITAMOS Y PUEDA
     * CREAR TANTO USERS COMO EMPLOYEE
     *
     * @param  array  $data
     * @return \App\User
     */
    public function showRegistrationForm()
    {
        //Trigo lospaises activos
        $var_paises = Country::select('id','country_name')->Activos()->toArray();

        //si es mas de uno, dejo en blanco,
        //si solo es uno cargo loasociado a ese pais
        if(count($var_paises) > 1){
            $var_regiones = [];
        }else{
            $first_pais = head($var_paises);
            $var_regiones = Regional::select('id','regional_name')->DeUnPais($first_pais['id'])->toArray();
        }

        //Traer Territorios
        $var_territorios = [];

        //traigo los canales
        $var_canales = Channel::select('id','channel_name')->Activos()->toArray();

        $var_cargos = Position::select('id','position_name')->Activos()->toArray();

        //AJUSTO LOS ARRAYS QUE VAN A PASAR A LA VISTA
        $paises = count($var_paises) > 1 ?
                            Arr::prepend($var_paises, ['id' => 0, 'country_name' => '--SELECCIONE PAIS--']) :
                            $var_paises;
        $regiones = Arr::prepend($var_regiones, ['id' => 0, 'regional_name' => '--SELECCIONE REGION--']);
        $territorios = Arr::prepend($var_territorios, ['id' => 0, 'territory_name' => '--SELECCIONE TERRITORIO--']);
        $canales = Arr::prepend($var_canales, ['id' => 0, 'channel_name' => '--SELECCIONE CANAL--']);

        $cargos = Arr::prepend($var_cargos, ['id' => 0, 'position_name' => '--SELECCIONE CARGO--']);

        //RETORNAMOS LA VISTA Y LOS ARRAYS
        return view('auth.register2', compact('paises','regiones', 'territorios','canales','cargos'));
    }


    /**
     * SOBRE ESCRIBIMOS LA FUNCION register.
     * PARA AÑADIRLE LA LOGICA QUE NECESITAMOS Y PUEDA
     * CREAR TANTO USERS COMO EMPLOYEE
     *
     * @param  array  $data
     * @return \App\User
     */
    public function register(Request $request)
    {

        Validator::make($request->all(),
            [
            'name' => ['required', 'alpha', 'max:255'],
            'last_name' => ['required', 'alpha', 'max:255'],
            'document' => ['required', 'digits_between:6,11', 'unique:employees,document'],
            'email' => ['required', 'email', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'bday' => ['required', 'date'],
            'regional' => ['required', 'not_in:0','integer'],
            'territory' => ['required', 'not_in:0','integer'],
            'channel' => ['required', 'not_in:0','integer'],
            'position' => ['required', 'not_in:0','integer'],
            ])->validate();


        $creacion = $this->create($request->all());

        //$this->guard()->login($user);
        dd(1);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}
