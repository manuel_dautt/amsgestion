<?php

namespace App\Http\Controllers;

Use App\Models\TradeImplementacion;
use App\Models\Channel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TradeImplementacionController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_IMPLEMENTACION', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_IMPLEMENTACION', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_IMPLEMENTACION', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_IMPLEMENTACION', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_IMPLEMENTACION', ['only' => ['destroy']]);
	  $this->middleware('permission:TRADE_REACTIVAR_IMPLEMENTACION', ['only' => ['reactivate']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $implementaciones = TradeImplementacion::with(['canal','regional'])->latest()->paginate(5);
        return view('trade.implementaciones.index',compact('implementaciones'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trade.implementaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'descripcion' => ['required', 'max:150','unique:trade_implementaciones'],
            'fecha_ini' => ['required','date','after_or_equal:today'],
            'fecha_fin' => ['required','date','after:fecha_ini']
        ],[
            'descripcion.required' => 'Descripcion es Requerida',
            'descripcion.max' => 'Descripcion debe contener hasta 150 caracteres',
            'descripcion.unique' => 'Descripcion ya existe en su registro de base de datos',
            'fecha_ini.required' => 'Fecha Inicial es Necesaria',
            'fecha_ini.date' => 'Fecha Inicial debe ser una fecha valida',
            'fecha_ini.after_or_equal' => 'Fecha inicial No debe ser menor a la actual',
            'fecha_fin.required' => 'Fecha Final es Necesaria',
            'fecha_fin.date' => 'Fecha Final debe ser una fecha valida',
            'fecha_fin.after' => 'Fecha Final no debe ser anterior a la Fecha Inicial',
        ]);


        TradeImplementacion::create(['descripcion' => strtoupper($request->descripcion),
                                     'fecha_ini' => $request->fecha_ini,
                                     'fecha_fin' => $request->fecha_fin]);


        return redirect()->route('implementaciones.index')
                        ->with('success','Implementacion Creada Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $implementacion = TradeImplementacion::find($id);
        return view('trade.implementaciones.show',compact('implementacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $implementacion = TradeImplementacion::find($id);
        return view('trade.implementaciones.edit',compact('implementacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'descripcion' => ['required', 'max:150','unique:trade_implementaciones,descripcion,'.$id],
            'fecha_ini' => ['required','date','after_or_equal:fecha_ini'],
            'fecha_fin' => ['required','date','after:fecha_ini'],
        ],[
            'descripcion.required' => 'Descripcion es Requerida',
            'descripcion.max' => 'Descripcion debe contener hasta 150 caracteres',
            'descripcion.unique' => 'Descripcion ya existe en su registro de base de datos',
            'fecha_ini.required' => 'Fecha Inicial es Necesaria',
            'fecha_ini.date' => 'Fecha Inicial debe ser una fecha valida',
            'fecha_ini.after_or_equal' => 'Fecha inicial No debe ser menor a la actual',
            'fecha_fin.required' => 'Fecha Final es Necesaria',
            'fecha_fin.date' => 'Fecha Final debe ser una fecha valida',
            'fecha_fin.after' => 'Fecha Final no debe ser anterior a la Fecha Inicial',
        ]);

        $implementacion = TradeImplementacion::find($id);

        $implementacion->update(['descripcion' => strtoupper($request->descripcion),
                                 'fecha_ini' => $request->fecha_ini,
                                 'fecha_fin' => $request->fecha_fin]);

        return redirect()->route('implementaciones.index')
                        ->with('success','Implementacion Actualizada Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $implementacion = TradeImplementacion::find($id);
        //$implementacion->delete();
		$implementacion->status = 'I';
        $implementacion->save();

        return redirect()->route('implementaciones.index')
                        ->with('success','Implementacion Eliminada Exitosamente');
    }
	
	public function reactivate($id)
	{
        //
        $implementacion = TradeImplementacion::find($id);
        //$implementacion->delete();
		$implementacion->status = 'A';
        $implementacion->save();

        return redirect()->route('implementaciones.index')
                        ->with('success','Implementacion Reactivada Exitosamente');
    }
}
