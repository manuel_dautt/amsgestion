<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use DB;

class RoleController extends Controller
{
    /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_ROLES', ['only' => ['index']]);
      $this->middleware('permission:VER_ROLES', ['only' => ['show']]);
      $this->middleware('permission:CREAR_ROLES', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_ROLES', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_ROLES', ['only' => ['destroy']]);
    }


/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = Role::withCount('permissions')->latest()->paginate(5);

        return view('roles.index',compact('roles'))
            ->with('i', (request()->input('page', 1) - 1) * 5);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'role_name' => ['required', 'max:255','unique:roles,name']
        ]);


        Role::create(['name' => strtoupper($request->role_name)]);

        return redirect()->route('roles.index')
                        ->with('success','Rol Creado Exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $role = Role::find($id);
        return view('roles.show',compact('role'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::find($id);
        return view('roles.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => ['required', 'max:100','unique:roles,name,'.$id]
        ]);

        $role = Role::find($id);

        $role->update(['name' => strtoupper($request->name)]);

        return redirect()->route('roles.index')
                        ->with('success','Role Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Role::find($id);
        $role->delete();

        return redirect()->route('roles.index')
                        ->with('success','Role Eliminado Exitosamente');
    }

    public function permisos($id){
        //
        $role = Role::find($id);
        $permisos = Permission::all();
        $rolePermissions = DB::table("role_has_permissions")
                             ->where("role_has_permissions.role_id",$id)
                             ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
                             ->all();

        return view('roles.permisos',compact('role','permisos','rolePermissions'));
    }

    public function AsociarPermiso(Request $request){

        $permisox = $request->has('permisox') ? $request->permisox : [];
        $id_role = $request->id_role;

        $role = Role::find($id_role);

        $role->syncPermissions($permisox);

        return redirect()->route('roles.index')
                         ->with('success','Role/Permiso Actualizado Exitosamente');

    }
}
