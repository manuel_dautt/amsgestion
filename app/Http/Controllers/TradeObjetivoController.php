<?php

namespace App\Http\Controllers;

Use App\Models\TradeObjetivo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TradeObjetivoController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_OBJETIVO', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_OBJETIVO', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_OBJETIVO', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_OBJETIVO', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_OBJETIVO', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objetivos = TradeObjetivo::latest()->paginate(5);

        return view('trade.objetivos.index',compact('objetivos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trade.objetivos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'objetivo' => ['required', 'max:20','unique:trade_objetivos']
        ]);


        TradeObjetivo::create(['objetivo' => strtoupper($request->objetivo)]);


        return redirect()->route('objetivos.index')
                        ->with('success','Objetivo Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetivo = TradeObjetivo::find($id);
        return view('trade.objetivos.show',compact('objetivo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $objetivo = TradeObjetivo::find($id);
        return view('trade.objetivos.edit',compact('objetivo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'objetivo' => ['required', 'max:20','unique:trade_objetivos,objetivo,'.$id]
        ]);

        $objetivo = TradeObjetivo::find($id);

        $objetivo->update(['objetivo' => strtoupper($request->objetivo)]);

        return redirect()->route('objetivos.index')
                        ->with('success','Objetivo Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $objetivo = TradeObjetivo::find($id);
        $objetivo->delete();

        return redirect()->route('objetivos.index')
                        ->with('success','Objetivo Eliminado Exitosamente');
    }
}
