<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Http\Request;

use App\Exports\LogAccesosExport;
use App\Exports\LogAccesosExportDetalles;
use Maatwebsite\Excel\Facades\Excel;


Use App\User;
Use App\Models\Head;
Use App\Models\Channel;
Use App\Models\Regional;
Use App\Models\Position;
Use App\Models\Employee;
Use App\Models\Territory;
Use App\Models\empleado_role;
use App\Models\UserAccess;

class LogController extends Controller
{
    /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {

      $this->middleware('permission:VER_ACCESOS', ['only' => ['index']]);
      $this->middleware('permission:LISTAR_ACCESOS', ['only' => ['traer_cedula']]);
      /*
      $this->middleware('permission:VER_PAISES', ['only' => ['show']]);
      $this->middleware('permission:CREAR_PAISES', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_PAISES', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_PAISES', ['only' => ['destroy']]);
      */
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('logs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Valida y trae el log de accesos de una cedula.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function traer_cedula(Request $request)
    {
        //No Generamos Validacion,
        //Tarea: Buscar validacion para la complejidad de la logica

        $cedula = '';

        if ($request->has('una')) {
            //
            if ($request->filled('cedula')) {
                //
                $cedula = $request->cedula;

            }else{
                return redirect()->route('logs.index')
                        ->with('error','Cedula no fue Ingresada');
            }

        }

        if ($request->has('todos')) {
            //
            $cedula = 'Todas';
        }

        $rango = $request->rango;

        $dateBase = Carbon::now();
        $dateIni = $dateBase->subDays($rango)->toDateString();
        $dateFin = Carbon::today()->toDateString();
        $regional = Employee::Regional();
        $nombre_regional = $regional['nom_regional'];
        $nombre_regional = $nombre_regional == 'TODOS/TODAS' ? '' : $nombre_regional;

        $accesos = UserAccess::with(['user.employee.territory',
                                'user.employee.territory.regional',
                                'user.employee.position',
                                'user',
                                'user.employee',
                                'user.employee.channels'])
                                ->whereHas('user.employee',function ($query) use($cedula){
                                    if($cedula != 'Todas'){
                                        $query->where('document',$cedula);
                                    }
                                })
                                ->whereBetween('created_at',[$dateIni,$dateFin])
                                ->whereHas('user.employee.territory.regional', function (Builder $q) use($nombre_regional)
                                {
                                    $q->where('regional_name','LIKE',"%$nombre_regional%");
                                })->get();


        return view('logs.list',compact('accesos','rango','cedula','dateIni','dateFin'));
//                ->with('i', (request()->input('page', 1) - 1) * 5);

    }


    /**
     * Exportar a Excel.
     *
     */
    public function exportar_accesos($rango, $cedula)
    {
        //
        if($cedula != 'Todas'){
            $criterio_cedula = "E.document = '$cedula' ";
        }else{
            $criterio_cedula = "E.document LIKE '%%' ";
        }
        $dateBase = Carbon::now();
        $dateIni = $dateBase->subDays($rango)->toDateString();
        $dateFin = Carbon::today()->toDateString();
        $regional = Employee::Regional();
        $nombre_regional = $regional['nom_regional'];
        $nombre_regional = $nombre_regional == 'TODOS/TODAS' ? '' : $nombre_regional;

        $consulta_detallado =
                  "SELECT
                          E.document as DOCUMENTO,
                          U.name as NOMBRE,
                          P.position_name as CARGO,
                          RL.name as ROLE,
                          R.regional_name as REGIONAL,
                          C.channel_name as CANAL,
                          A.created_at as FECHA,
                          A.user_ip as IP
                   FROM    users_access A
                          INNER JOIN users U ON (A.user_id=U.id)
                          INNER JOIN employees E ON (E.user_id=U.id)
                          INNER JOIN employee_channels EC ON E.id = EC.employee_id
                          INNER JOIN empleado_role ER ON ER.id_empleado = E.id
    	                    INNER JOIN roles RL ON ER.id_role = RL.id
	                        INNER JOIN channels C ON C.id = EC.channel_id
                          INNER JOIN territories T ON (E.territory_id =T.id)
                          INNER JOIN regionals R ON (T.regional_id=R.id)
                          INNER JOIN positions P ON (E.position_id=P.id)
                   WHERE
                      R.regional_name LIKE '%".$nombre_regional."%'
                      AND A.created_at BETWEEN '".$dateIni."' AND '".$dateFin."' AND ".$criterio_cedula;

        $accesos = DB::select($consulta_detallado);

        //$encabezados = ['DOCUMENTO','NOMBRE','CARGO','REGIONAL','FECHA ACCESO','I.P. ACCESO',];

        return (new LogAccesosExport($accesos))
                ->download('accesos.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }



    /**
     * Exportar a Excel.
     *
     */
    public function exportar_accesos_resumen($rango, $cedula)
    {
        //
        if($cedula != 'Todas'){
            $criterio_cedula = "E.document = '$cedula' ";
        }else{
            $criterio_cedula = "E.document LIKE '%%' ";
        }
        $dateBase = Carbon::now();
        $dateIni = $dateBase->subDays($rango)->toDateString();
        $dateFin = Carbon::today()->toDateString();
        $regional = Employee::Regional();
        $nombre_regional = $regional['nom_regional'];
        $nombre_regional = $nombre_regional == 'TODOS/TODAS' ? '' : $nombre_regional;

		    $consulta_accesos =
                  "SELECT DISTINCT
                    	E.document AS DOCUMENTO,
                    	U.name AS NOMBRE,
                      P.position_name AS CARGO,
                      RL.name AS ROLE,
                    	R.regional_name AS REGIONAL,
                    	C.channel_name AS CANAL,
                    	IF(ISNULL(A.user_id), 'NO', 'SI') AS INGRESO
                    FROM
                    	users U
                    	LEFT JOIN employees E ON E.user_id = U.id
                      LEFT JOIN employee_channels EC ON E.id = EC.employee_id
                      LEFT JOIN empleado_role ER ON ER.id_empleado = E.id
	                    LEFT JOIN roles RL ON ER.id_role = RL.id
                      LEFT JOIN channels C ON C.id = EC.channel_id
                    	LEFT JOIN territories T ON E.territory_id = T.id
                    	LEFT JOIN regionals R ON T.regional_id = R.id
                    	LEFT JOIN positions P ON E.position_id = P.id
                    	LEFT JOIN
                    			(	SELECT DISTINCT users_access.user_id
                    				FROM users_access
                    				WHERE users_access.created_at
                    							BETWEEN '".$dateIni."' AND '".$dateFin."'
                    			) A ON A.user_id = U.id
                    WHERE
                    	ER.id_role <> 9
                      AND R.regional_name LIKE '%".$nombre_regional."%' AND ".$criterio_cedula;
;

		     $totales = DB::select($consulta_accesos);

        //$encabezados = ['DOCUMENTO','NOMBRE','CARGO','REGIONAL','FECHA ACCESO','I.P. ACCESO',];

        return (new LogAccesosExportDetalles($totales))
                ->download('detalle_accesos.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }

}
