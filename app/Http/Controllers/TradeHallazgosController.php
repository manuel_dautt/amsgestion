<?php

namespace App\Http\Controllers;

Use App\Models\TradeClaseHallazgo;
Use App\Models\TradeHallazgos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TradeHallazgosController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_HALLAZGOS', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_HALLAZGOS', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_HALLAZGOS', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_HALLAZGOS', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_HALLAZGOS', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hallazgos = TradeHallazgos::with('clase')->where('status','A')->orderBy('nombre')->latest()->paginate(5);

        return view('trade.hallazgos.index',compact('hallazgos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clasesH = TradeClaseHallazgo::select('id','nombre_clase')->where('status','A')->get()->toArray();
        return view('trade.hallazgos.create',compact('clasesH'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'clase_hallazgo_id' => ['required', 'not_in:X','integer'],
            'nombre' => ['required', 'max:255'],
        ]);

        $clase_hallazgo_id = $request->clase_hallazgo_id;
        $nombre = strtoupper($request->nombre);

        $valida_hallazgo = TradeHallazgos::where('trade_clase_hallazgo_id', $clase_hallazgo_id)
                                    ->where('nombre', $nombre)
                                    ->get();

        if(count($valida_hallazgo) > 0){
            return redirect()->route('hallazgos.index')
                              ->with('error',"Hallazgo: $nombre, ya fue creado");
        }
        TradeHallazgos::create(['nombre' => $nombre, 'trade_clase_hallazgo_id' => $clase_hallazgo_id]);

        return redirect()->route('hallazgos.index')
                        ->with('success','Hallazgo Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
                //
        $hallazgo = TradeHallazgos::with('clase')->find($id);
        return view('trade.hallazgos.show',compact('hallazgo'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $hallazgo = TradeHallazgos::with('clase')->find($id);
        $clasesH = TradeClaseHallazgo::select('id','nombre_clase')->where('status','A')->get()->toArray();
        return view('trade.hallazgos.edit',compact('hallazgo', 'clasesH'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'clase_hallazgo_id' => ['required', 'integer'],
            'nombre' => ['required', 'max:255'],
        ]);

        $clase_hallazgo_id = $request->clase_hallazgo_id;
        $nombre = strtoupper($request->nombre);

        $hallazgo = TradeHallazgos::find($id);


        if($hallazgo->nombre == $nombre AND $hallazgo->clase->id == $clase_hallazgo_id){
            return redirect()->route('hallazgos.index')
                              ->with('success',"Hallazgo: $nombre, sin cambios...");
        }

        $hallazgo->update(['nombre' => $nombre, 'trade_clase_hallazgo_id' => $clase_hallazgo_id]);

        return redirect()->route('hallazgos.index')
                        ->with('success','Hallazgo Actualizado Exitosamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $hallazgo = TradeHallazgos::find($id);
        $hallazgo->status = 'I';
        $hallazgo->save();

        return redirect()->route('hallazgos.index')
                        ->with('success','Hallazgo Eliminado Exitosamente');
    }
}
