<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

Use App\User;
use App\Models\TiendasNS;
use App\Models\TiendasNPS;




class ReporteTiendasController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function ver_ns($aliado = 'TODOS', $regional = 'TODAS'){
		
		$aliado = strtoupper($aliado);
		$regional = strtoupper($regional);
		
		if($aliado != 'TODOS' OR $regional != 'TODAS'){
			
			$condicion = ' WHERE ';
			if($aliado != 'TODOS'){
				$condicion .= " ALIADO = '$aliado'";
			}
			if($regional != 'TODAS'){
				$condicion .= ($aliado != 'TODOS')? " AND ":"";
				$condicion .= " REGIONAL = '$regional'";
			}
		}else{
			$condicion = "";
		}
	
	
		$qry_pais = "
		SELECT 
			periodo, SUM(validos) validos, SUM(puntuales) puntuales, 
			SUM(atendidos) puntuales, SUM(abandonados) abandonados,
			ROUND((SUM(puntuales)/SUM(validos))*100,2) NS,
			CASE 
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 81
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 85 
				THEN '120%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 80
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 81
				THEN '100%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 85
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 87
				THEN '80%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 87
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 80 
				THEN '0%'
				ELSE '0%'
			END calificacion
		FROM 
			tiendas_ns
		$condicion
		GROUP BY
			periodo	
		;";
		
		
		$ns_pais = DB::select($qry_pais);
		
		$qry_regiones = "
		SELECT 
			periodo, regional, SUM(validos) validos, SUM(puntuales) puntuales, 
			SUM(atendidos) puntuales, SUM(abandonados) abandonados,
			ROUND((SUM(puntuales)/SUM(validos))*100,2) NS,
			CASE 
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 81
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 85 
				THEN '120%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 80
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 81
				THEN '100%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 85
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 87
				THEN '80%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 87
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 80 
				THEN '0%'
				ELSE '0%'
			END calificacion
		FROM 
			tiendas_ns
		$condicion
		GROUP BY
			periodo, regional
		;";
		
		$ns_regiones = DB::select($qry_regiones);

		$qry_tiendas = "
		SELECT 
			periodo, regional, tienda,
			SUM(validos) validos, SUM(puntuales) puntuales, 
			SUM(atendidos) puntuales, SUM(abandonados) abandonados,
			ROUND((SUM(puntuales)/SUM(validos))*100,2) NS,
			CASE 
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 81
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 85 
				THEN '120%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 80
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 81
				THEN '100%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 85
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 87
				THEN '80%'
				WHEN ROUND((SUM(puntuales)/SUM(validos))*100,2) >= 87
				AND  ROUND((SUM(puntuales)/SUM(validos))*100,2) < 80 
				THEN '0%'
				ELSE '0%'
			END calificacion		
		FROM 
			tiendas_ns
		$condicion
		GROUP BY
			periodo, regional, tienda
		;";
		$ns_tiendas = DB::select($qry_tiendas);
		
		return view('reportes.tiendas.ns', compact('ns_pais','ns_regiones','ns_tiendas','aliado','regional'));
		//dd($ns_pais, $ns_regiones, $ns_tiendas);

    }



    public function ver_nps($canal = 'TODOS', $negocio = 'TODOS', $regional = 'TODAS', $tienda = 'TODAS'){

		$v_canal = strtoupper($canal);
		$v_negocio = strtoupper($negocio);
		$v_unidad = 'TODAS';
		$v_regional = strtoupper($regional);
		$v_tienda = strtoupper($tienda);
		
		if($v_canal != 'TODOS' OR $v_negocio != 'TODOS' OR $v_regional != 'TODAS' OR $v_tienda != 'TODAS'){
			
			$condicion = ' WHERE ';
			if($v_canal != 'TODOS'){
				$condicion .= " CANAL = '$v_canal'";
			}
			if($v_negocio != 'TODOS'){
				$condicion .= ($v_canal != 'TODOS')? " AND ":"";
				$condicion .= " NEGOCIO = '$v_unidad'";
			}
			if($v_unidad != 'TODAS'){
				$condicion .= ($v_canal != 'TODOS' OR $v_negocio != 'TODOS')? " AND ":"";
				$condicion .= " UNIDAD = '$v_unidad'";
			}
			if($v_regional != 'TODAS'){
				$condicion .= ($v_canal != 'TODOS' OR $v_negocio != 'TODOS' OR $v_unidad != 'TODAS')? " AND ":"";
				$condicion .= " REGIONAL = '$v_regional'";
			}
			if($v_tienda != 'TODAS'){
				$condicion .= ($v_canal != 'TODOS' OR $v_negocio != 'TODOS' OR $v_unidad != 'TODAS' OR $v_regional != 'TODAS')? " AND ":"";
				$condicion .= " TIENDA = '$v_tienda'";
			}
		}else{
			$condicion = "";
		}
	
	
		$qry_pais =
		"SELECT 
			periodo, SUM(encuestas) encuestas, SUM(promotores) promotores, 
			SUM(neutros) neutros, SUM(detractores) detractores,
			ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) nps
		FROM tiendas_nps
		$condicion
		GROUP BY periodo;";
		
		$nps_pais = DB::select($qry_pais);
		/* $nps_pais = TiendasNPS::select(DB::raw("SELECT periodo, SUM(encuestas) encuestas, SUM(promotores) promotores, SUM(neutros) neutros, SUM(detractores) detractores"))
					->groupBy('periodo')
					->get();
		 */

		
		$qry_canal =
		"SELECT 
			periodo, canal, SUM(encuestas) encuestas, SUM(promotores) promotores, 
			SUM(neutros) neutros, SUM(detractores) detractores,
			CASE
				WHEN SUM(encuestas) = 0 THEN 0
				ELSE ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2)
                END	nps
		FROM tiendas_nps
		$condicion
		GROUP BY periodo, canal;";
		
		$nps_canal = DB::select($qry_canal);
		
		//dd($nps_canal);
		
		$qry_negocio =
		"SELECT 
			periodo, negocio, SUM(encuestas) encuestas, SUM(promotores) promotores, 
			SUM(neutros) neutros, SUM(detractores) detractores,
			ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) nps
		FROM tiendas_nps
		$condicion
		GROUP BY periodo, negocio;";
		
		$nps_negocio = DB::select($qry_negocio);

		$qry_regional =
		"SELECT 
			periodo, regional, SUM(encuestas) encuestas, SUM(promotores) promotores, 
			SUM(neutros) neutros, SUM(detractores) detractores,
			ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) nps
		FROM tiendas_nps
		$condicion
		GROUP BY periodo, regional;";
		
		$nps_regional = DB::select($qry_regional);
		
		
		$qry_tienda =
		"SELECT 
			periodo, regional, tienda,
			SUM(encuestas) encuestas, SUM(promotores) promotores, 
			SUM(neutros) neutros, SUM(detractores) detractores,
			ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) nps,
			CASE 
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) >= 33.9 THEN '120%'
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) >= 31.9
					AND  ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) < 33.9 THEN '100%'
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) >= 29.9
					AND  ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) < 31.9 THEN '80%'
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) < 29.9 THEN '0%'
				ELSE '0%'
			END calificacion
		FROM tiendas_nps
		$condicion
		GROUP BY periodo, regional, tienda;";
		
		$nps_tienda = DB::select($qry_tienda);



		$qry_asesor =
		"SELECT 
			periodo, regional, tienda, asesor,
			SUM(encuestas) encuestas, SUM(promotores) promotores, 
			SUM(neutros) neutros, SUM(detractores) detractores,
			ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) nps,
			CASE 
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) >= 33.9 THEN '120%'
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) >= 31.9
					AND  ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) < 33.9 THEN '100%'
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) >= 29.9
					AND  ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) < 31.9 THEN '80%'
				WHEN ROUND(((SUM(promotores)-SUM(detractores))/SUM(encuestas))*100,2) < 29.9 THEN '0%'
				ELSE '0%'
			END calificacion
		FROM tiendas_nps
		$condicion
		GROUP BY periodo, regional, tienda, asesor;";
		
		$nps_asesor = DB::select($qry_asesor);
		
		//dd($nps_pais, $nps_canal, $nps_negocio, $nps_regional, $nps_tienda, $nps_asesor);
		return view('reportes.tiendas.nps', compact('nps_pais', 'nps_canal', 'nps_negocio', 'nps_regional', 'nps_tienda', 'nps_asesor', 'v_canal', 'v_negocio', 'v_regional', 'v_tienda'));
		

    }



    public function encuestas_oficiales($id_encuesta, $idpdv){

        $id_usuario = Auth::id();

        $id_visita = Visits::where('user_id',$id_usuario)
                            ->where('idpdv',$idpdv)
                            ->where('activa','S')
                            ->get()
                            ->last();


        $validar_encuesta =EncuestaVisitas::with('visita')->where('visita_id',$id_visita->id)
                                                          ->where('completada','N')
                                                          ->first();

        //dd(empty($validar_encuesta), $validar_encuesta);
        if(!empty($validar_encuesta)){
            $datosEncuesta = [
                'idEncuestaVisita' => $validar_encuesta['id'],
                'idEncuesta' => $id_encuesta,
                'encuestaCompletada' =>$validar_encuesta['completada'],
                'preguntaActual' =>$validar_encuesta['pregunta_actual'],
            ];
        }else{
            // Trae el ID de Visita activo del usuario en el punto de venta
            // guarda el registro de visita y encuesta

            $id_encuesta_visita = new EncuestaVisitas();

            $id_encuesta_visita->visita_id = $id_visita->id;
            $id_encuesta_visita->encuesta_id = $id_encuesta;
            $id_encuesta_visita->completada = 'N';
            $id_encuesta_visita->pregunta_actual = 1;

            $id_encuesta_visita->save();

            $datosEncuesta = [
                                'idEncuestaVisita' => $id_encuesta_visita->id,
                                'idEncuesta' => $id_encuesta,
                                'encuestaCompletada' =>$id_encuesta_visita->completada,
                                'preguntaActual' =>$id_encuesta_visita->pregunta_actual,
                            ];

        }

        return redirect()->route('encuestas.preguntas.constructor',
                                    [
                                        'encuesta_visita' => $datosEncuesta['idEncuestaVisita'],
                                        'encuesta' => $datosEncuesta['idEncuesta'],
                                        'completada' => $datosEncuesta['encuestaCompletada'],
                                        'pregunta' => $datosEncuesta['preguntaActual']
                                    ]
                                );
    }

    public function encuestasPreguntasConstructor($encuesta_visita, $encuesta, $completada, $pregunta){

        $datosEncuesta['idEncuestaVisita'] = $encuesta_visita;
        $datosEncuesta['idEncuesta'] = $encuesta;
        $datosEncuesta['encuestaCompletada'] = $completada;
        $datosEncuesta['preguntaActual'] = $pregunta;

        $encuestaVisita = EncuestaVisitas::with('visita')->where('id',$encuesta_visita)
                                      ->first()
                                      ->toArray();


        $idpdv = $encuestaVisita['visita']['idpdv'];

        if($encuestaVisita['completada'] == 'S'){
            //ENCUESTA TERMINADA
            return view('encuestas.finalizada',compact('idpdv'));
        }


        $encuesta = EncuestaBase::where('id',$datosEncuesta['idEncuesta'])
                                  ->first()
                                  ->toArray();

        $preguntas = EncuestaPreguntas::with('tipo')->where('encuesta_id',$datosEncuesta['idEncuesta'])
                                                    ->where('orden_pregunta',$datosEncuesta['preguntaActual'])
                                                    ->first()
                                                    ->toArray();

        $opciones = EncuestaOpciones::where('pregunta_id', $preguntas['id'])
                                      ->get()
                                      ->toArray();

        //Validamos el tipo de pregunta para construir el HTML
        $opcion_html = $this->tipo_pregunta($preguntas['tipo_pregunta_id']);

        //dd($datosEncuesta, $encuesta, $preguntas, $opciones, $opcion_html, $encuestaVisita['visita']['idpdv']);

        return view('encuestas.vistas.preguntas',compact('opcion_html','preguntas','opciones','encuesta','datosEncuesta', 'idpdv'));
    }


    public function encuestas_guardar(Request $request){

        $txt_opcion_respuesta = '';
        $texto_respuesta = '';
        $pregunta_siguiente = 0;
        $fin_encuesta = 'N';
        $idOpcion = $request->idOpcion;

        $requerida = $request->requeridaPregunta == 'S'?true:false;

        //dd($requerida, $request);

        switch ($request->tipoPregunta) {
            case 1: //Texto Corto es <input type='text'>
                if($request->pregunta_encuesta == NULL AND $requerida){
                    return back()->withInput()->with('error','Debe Ingresar una respuesta');
                }
                if(strlen(trim($request->pregunta_encuesta)) > $request->maxPregunta){
                    return back()->withInput()->with('error','Longitud de la respuesta excede '.$request->maxPregunta.' caracteres');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper(trim($request->pregunta_encuesta));
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 2: //Texto largo es <textarea>
                if($request->pregunta_encuesta == NULL AND $requerida){
                    return back()->withInput()->with('error','Debe Ingresar una respuesta');
                }
                if(strlen(trim($request->pregunta_encuesta)) > $request->maxPregunta){
                    return back()->withInput()->with('error','Longitud de la respuesta excede '.$request->maxPregunta.' caracteres');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper(trim($request->pregunta_encuesta));
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 3: //Numero es <input type='number'>
                if($request->pregunta_encuesta == NULL AND $requerida){
                    return back()->withInput()->with('error','Debe Ingresar una respuesta');
                }
                if(strlen(trim($request->pregunta_encuesta)) > $request->maxPregunta){
                    return back()->withInput()->with('error','Longitud de la respuesta excede '.$request->maxPregunta.' caracteres');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper(trim($request->pregunta_encuesta));
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 4: //SeleccionUnica es <select>
                if($request->pregunta_encuesta == 0 AND $requerida){
                    return back()->withInput()->with('error','Debe seleccionar una opcion');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper($txt_opcion_respuesta);
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 5: //SeleccionMultiple es <select multiple>
                if(!$request->has('pregunta_encuesta') AND $requerida){
                    return back()->withInput()->with('error','Debe seleccionar una o varias opciones');
                }

                $cadaIdOpcion = explode('|',$request->idOpcion);
                $txt_opcion_respuesta = '';
                $texto_respuesta = '';

                foreach ($cadaIdOpcion as $unaOpcion) {
                    $tmpOpcionRespTxt = EncuestaOpciones::where('id',$unaOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();

                    $txt_opcion_respuesta .= $tmpOpcionRespTxt->texto_opcion.'|';
                    $texto_respuesta .= strtoupper($txt_opcion_respuesta).'|';
                    $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;

                }

                $txt_opcion_respuesta = substr($txt_opcion_respuesta, 0, -1);
                $texto_respuesta = substr($texto_respuesta, 0, -1);

                $idOpcion = null;

                break;

            case 6: //SeleccionSi_No es <select>
                if($request->pregunta_encuesta == 0 AND $requerida){
                    return back()->withInput()->with('error','Debe seleccionar una opcion');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper($txt_opcion_respuesta);
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 7: //Cierre <button>
                $fin_encuesta = 'S';
                $txt_opcion_respuesta = '_FIN_';
                $texto_respuesta = '_FIN_';
                break;
        }

        $id_encuesta_visita = $request->idEncuestaVisita;
        $id_pregunta = $request->idPregunta;
        $id_opcion_respuesta = $idOpcion;
        //$txt_opcion_respuesta
        //$texto_respuesta
        //$pregunta_siguiente

        //GUARDAR RESPUESTA VISITA
        $respuesta_visita = new EncuestaRespuestas;

        $respuesta_visita->encuesta_visita_id = $id_encuesta_visita;
        $respuesta_visita->pregunta_id = $id_pregunta;
        $respuesta_visita->opcion_respuesta_id = $id_opcion_respuesta;
        $respuesta_visita->opcion_respuesta_txt = $txt_opcion_respuesta;
        $respuesta_visita->texto_respuesta = $texto_respuesta;

        $respuesta_visita->save();

        if($fin_encuesta == 'S'){
            $encuesta_visita = EncuestaVisitas::with('visita')->where('id',$request->idEncuestaVisita)
                                                ->first();

            $idpdv = $encuesta_visita->visita->idpdv;
            $encuesta_visita->completada = 'S';
            $encuesta_visita->pregunta_actual = $pregunta_siguiente;
            $encuesta_visita->save();

            return view('encuestas.finalizada',compact('idpdv'));
        }else{
            $encuesta_visita = EncuestaVisitas::with('visita')->where('id',$request->idEncuestaVisita)
                                                ->first();
            $encuesta_visita->pregunta_actual = $pregunta_siguiente;
            $encuesta_visita->save();
        }


        return redirect()->route('encuestas.preguntas.constructor',
                                    [
                                        'encuesta_visita' => $request->idEncuestaVisita,
                                        'encuesta' => $request->idEncuesta,
                                        'completada' => $fin_encuesta,
                                        'pregunta' => $pregunta_siguiente
                                    ]
                                );

    }

    private function html_textoCorto(){
        return 'encuestas.vistas.html_textoCorto';
    }

    private function html_textoLargo(){
        return 'encuestas.vistas.html_textoLargo';
    }

    private function html_Numero(){
        return 'encuestas.vistas.html_Numero';
    }

    private function html_selectSimple(){
        return 'encuestas.vistas.html_selectSimple';
    }

    private function html_selectMultiple(){
        return 'encuestas.vistas.html_selectMultiple';
    }

    private function html_selectBinario(){
        return 'encuestas.vistas.html_selectBinario';
    }

    private function html_buttonClose(){
        return 'encuestas.vistas.html_buttonClose';
    }

    private function tipo_pregunta($tipo_pregunta){
        switch ($tipo_pregunta) {
            case 1: //Texto Corto es <input type='text'>
                $opcion = $this->html_textoCorto();
                break;
            case 2: //Texto largo es <textarea>
                $opcion = $this->html_textoLargo();
                break;
            case 3: //Numero es <input type='number'>
                $opcion = $this->html_Numero();
                break;
            case 4: //SeleccionUnica es <select>
                $opcion = $this->html_selectSimple();
                break;
            case 5: //SeleccionMultiple es <select multiple>
                $opcion = $this->html_selectMultiple();
                break;
            case 6: //SeleccionSi_No es <select>
                $opcion = $this->html_selectBinario();
                break;
            case 7: //Cierre <button>
                $opcion = $this->html_buttonClose();
                break;
        }

        return $opcion;

    }

}
