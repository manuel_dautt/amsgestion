<?php

namespace App\Http\Controllers;

Use App\Models\Territory;
Use App\Models\Regional;
Use App\Models\Country;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class TerritoryController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_TERRITORIOS', ['only' => ['index']]);
      $this->middleware('permission:VER_TERRITORIOS', ['only' => ['show']]);
      $this->middleware('permission:CREAR_TERRITORIOS', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_TERRITORIOS', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_TERRITORIOS', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $territorios = Territory::with('regional.country')
                                ->where('status','A')
								->orderBy('regional_id','ASC')
								->orderBy('territory_name','ASC')
								->paginate(5);

        return view('territories.index',compact('territorios'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //Traigo los paises activos
        $var_paises = Country::select('id','country_name')->Activos()->toArray();

        //si es mas de uno, dejo en blanco,
        //si solo es uno cargo loasociado a ese pais
        if(count($var_paises) > 1){
            $var_regiones = [];
        }else{
            $first_pais = head($var_paises);
            $var_regiones = Regional::select('id','regional_name')->DeUnPais($first_pais['id'])->toArray();
        }

        $paises = count($var_paises) > 1 ?
                    Arr::prepend($var_paises, ['id' => 0, 'country_name' => '--SELECCIONE PAIS--']) :
                    $var_paises;

        $regiones = Arr::prepend($var_regiones, ['id' => 0, 'regional_name' => '--SELECCIONE REGION--']);

        return view('territories.create',compact('paises', 'regiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'country' => ['required', 'not_in:0','integer'],
            'regional' => ['required', 'not_in:0','integer'],
            'territory_name' => ['required', 'max:255'],
        ]);

        $country_id = $request->country;
        $regional_id = $request->regional;
        $territory_name = strtoupper($request->territory_name);

        $valida_territorio = Territory::where('regional_id', $regional_id)
                                    ->where('territory_name', $territory_name)
                                    ->get();

        if(count($valida_territorio) > 0){
            return redirect()->route('territorios.index')
                              ->with('error',"Territorio: $territory_name, ya existe en el la Regional");
        }

        Territory::create(['territory_name' => $territory_name, 'regional_id' => $regional_id]);

        return redirect()->route('territorios.index')
                        ->with('success','Territorio Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $territorio = Territory::with('regional','regional.country')->find($id);
        return view('territories.show',compact('territorio'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $territorio = Territory::with('regional','regional.country')->find($id);
        return view('territories.edit',compact('territorio'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'territory_name' => ['required', 'max:255'],
        ]);

        $regional_id = $request->regional_id;
        $territory_name = strtoupper($request->territory_name);

        if(count(Territory::where('territory_name',$territory_name)->get()) > 0){
            return redirect()->route('territorios.index')
                              ->with('success',"Territorio: $territory_name, Ya Existe");
        }

        // CREO QUE PODEMOS EVALUAR SI QUITAMOS ESTA CONDICIONAL Y DEJAMOS
        // LA ANTERIOR QUE ES MAS OPTIMA
        $territorio = Territory::with('regional','regional.country')->find($id);


        if($territorio->territory_name == $territory_name){
            return redirect()->route('territorios.index')
                              ->with('success',"Territorio: $territory_name, sin cambios...");
        }

        $territorio->update(['territory_name' => $territory_name]);

        return redirect()->route('territorios.index')
                        ->with('success','Territorio Actualizado Exitosamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $territorio = Territory::find($id);
        $territorio->status = 'I';
        $territorio->save();

        return redirect()->route('territorios.index')
                        ->with('success','Territorio Eliminado Exitosamente');

    }
}
