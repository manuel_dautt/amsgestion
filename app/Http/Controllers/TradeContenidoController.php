<?php

namespace App\Http\Controllers;

Use App\Models\TradeContenido;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TradeContenidoController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_CONTENIDO', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_CONTENIDO', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_CONTENIDO', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_CONTENIDO', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_CONTENIDO', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contenidos = TradeContenido::latest()->paginate(5);

        return view('trade.contenidos.index',compact('contenidos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trade.contenidos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'contenido' => ['required', 'max:50','unique:trade_contenidos']
        ]);


        TradeContenido::create(['contenido' => strtoupper($request->contenido)]);


        return redirect()->route('contenidos.index')
                        ->with('success','Contenido Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $contenido = TradeContenido::find($id);
        return view('trade.contenidos.show',compact('contenido'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $contenido = TradeContenido::find($id);
        return view('trade.contenidos.edit',compact('contenido'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'contenido' => ['required', 'max:50','unique:trade_contenidos,contenido,'.$id]
        ]);

        $contenido = TradeContenido::find($id);

        $contenido->update(['contenido' => strtoupper($request->contenido)]);

        return redirect()->route('contenidos.index')
                        ->with('success','Contenido Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $contenido = TradeContenido::find($id);
        $contenido->delete();

        return redirect()->route('contenidos.index')
                        ->with('success','Contenido Eliminado Exitosamente');
    }
}
