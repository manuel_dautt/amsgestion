<?php

namespace App\Http\Controllers;

Use App\Models\Channel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ChannelController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_CANALES', ['only' => ['index']]);
      $this->middleware('permission:VER_CANALES', ['only' => ['show']]);
      $this->middleware('permission:CREAR_CANALES', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_CANALES', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_CANALES', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $canales = Channel::latest()->paginate(5);

        return view('channels.index',compact('canales'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('channels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'channel_name' => ['required', 'max:255','unique:channels']
        ]);


        Channel::create(['channel_name' => strtoupper($request->channel_name)]);


        return redirect()->route('canales.index')
                        ->with('success','Canal Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $canal = Channel::find($id);
        return view('channels.show',compact('canal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $canal = Channel::find($id);
        return view('channels.edit',compact('canal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'channel_name' => ['required', 'max:255','unique:channels,channel_name,'.$id]
        ]);

        $canal = Channel::find($id);

        $canal->update(['channel_name' => strtoupper($request->channel_name)]);

        return redirect()->route('canales.index')
                        ->with('success','Canal Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $canal = Channel::find($id);
        $canal->delete();

        return redirect()->route('canales.index')
                        ->with('success','Canal Eliminado Exitosamente');
    }
}
