<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Employee;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
		$id_usuario = Auth::id();
        $empleado = Employee::DatosEmpleado();
        $nombre = $empleado['nombre_completo'];

		return view('home',compact('nombre'));
    }
	
	public function en_construccion()
    {
        return view('en_construccion');
    }
}
