<?php

namespace App\Http\Controllers;

Use App\Models\Country;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_PAISES', ['only' => ['index']]);
      $this->middleware('permission:VER_PAISES', ['only' => ['show']]);
      $this->middleware('permission:CREAR_PAISES', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_PAISES', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_PAISES', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $paises = Country::latest()->paginate(5);

        return view('countries.index',compact('paises'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'country_name' => ['required', 'max:255','unique:countries']
        ]);


        Country::create(['country_name' => strtoupper($request->country_name)]);

        return redirect()->route('paises.index')
                        ->with('success','Pais Creado Exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pais = Country::find($id);
        return view('countries.show',compact('pais'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pais = Country::find($id);
        return view('countries.edit',compact('pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'country_name' => ['required', 'max:255','unique:countries,country_name,'.$id]
        ]);

        $pais = Country::find($id);

        $pais->update(['country_name' => strtoupper($request->country_name)]);

        return redirect()->route('paises.index')
                        ->with('success','Pais Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pais = Country::find($id);
        $pais->delete();

        return redirect()->route('paises.index')
                        ->with('success','Pais Eliminado Exitosamente');
    }
}
