<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Models\InfoposRutas;
use App\Models\Employee;
use App\Models\Infopos_Rpoint;

class PlaneadorController extends Controller
{
    //
    public function verPlaneador(){

        $empleado = Employee::DatosEmpleado();
        $cc_empleado = $empleado['cedula'];

        //$rutas = InfoposRutas::where('cedula',$cc_empleado)->get()->toArray();
        $consulta =
            "SELECT R.circuito, R.lun, R.mar, R.mie, R.jue, R.vie, R.sab, R.dom, COUNT(1) pv
             FROM infopos_rutas_dms R INNER JOIN infopos_rpoint P ON (R.circuito = P.circuito)
             WHERE P.estado_dms = 'VENDE' AND R.cedula = '$cc_empleado'
             GROUP BY R.circuito, R.lun, R.mar, R.mie, R.jue, R.vie, R.sab, R.dom";

        $rutas = DB::select($consulta);
        $flag_rutas = true;

        if(!count($rutas)){
            $flag_rutas = false;
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','Usuario No tiene rutero asignado en DMS, Solicitelo por favor!!!');
        }
        $array_dias_semana = ['dom','lun','mar','mie','jue','vie','sab'];

        $dia_de_hoy = $array_dias_semana[date('w')];
        $n_dia = date('w');

        return view('planning.ver_rutero', compact('rutas','flag_rutas','dia_de_hoy', 'n_dia'));

    }


}
