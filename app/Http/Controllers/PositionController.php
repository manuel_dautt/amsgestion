<?php

namespace App\Http\Controllers;

Use App\Models\Head;
Use App\Models\Position;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PositionController extends Controller
{
   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_CARGOS', ['only' => ['index']]);
      $this->middleware('permission:VER_CARGOS', ['only' => ['show']]);
      $this->middleware('permission:CREAR_CARGOS', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_CARGOS', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_CARGOS', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cargos = Position::latest()->paginate(5);

        return view('positions.index',compact('cargos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('positions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $request->validate([
            'position_name' => ['required', 'max:255','unique:positions'],
            'description' => ['string', 'max:255'],
            'gm_level' => ['required', 'numeric',],
        ]);


        Position::create(['position_name' => strtoupper($request->position_name),
                          'description' => strtoupper($request->description),
                          'gm_level' => $request->gm_level]);

        return redirect()->route('cargos.index')
                        ->with('success','Cargo Creado Exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cargo = Position::find($id);
        return view('positions.show',compact('cargo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cargo = Position::find($id);
        return view('positions.edit',compact('cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'position_name' => ['required', 'max:255','unique:positions,position_name,'.$id],
            'description' => ['string', 'max:255'],
            'gm_level' => ['required', 'numeric',],
        ]);

        $cargo = Position::find($id);

        $cargo->update(['position_name' => strtoupper($request->position_name),
                        'description' => strtoupper($request->description),
                        'gm_level' => $request->gm_level]);

        return redirect()->route('cargos.index')
                        ->with('success','Cargo Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cargo = Position::find($id);
        $cargo->delete();

        return redirect()->route('cargos.index')
                        ->with('success','Cargo Eliminado Exitosamente');
    }
}
