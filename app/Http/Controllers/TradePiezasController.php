<?php

namespace App\Http\Controllers;

Use App\Models\TradePieza;
use App\Models\TradeTipo;
use App\Models\TradeObjetivo;
use App\Models\TradeContenido;
use App\Models\TradeImplementacion;
use App\Models\TradeColocacionPiezas;
use App\Models\TradePiezasImplementacion;
use App\Models\TradeInventarioPiezas;
use App\Models\TradePiezaObjetivos;
use App\Models\TradeAuditoria;
use App\Models\TradeAuditoriaHallazgos;
use App\Models\TradeHallazgos;
use App\Models\TradeClaseHallazgos;
use App\Models\TradeResultado;
use App\Models\Infopos_Rpoint;
use App\Models\Regional;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TradeExportBasico;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Image;


class TradePiezasController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_PIEZAS', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_PIEZAS', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_PIEZAS', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_PIEZAS', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_PIEZAS', ['only' => ['destroy']]);

	  $this->middleware('permission:VER_DEALERS', ['only' => ['ColocacionSeleccionada',
																'seleccionaImplementacion',
																'implementacionSeleccionada',
																'colocarPiezas',
																'quitarPiezas',
																'asociarPiezas',
																'desasociarPiezas',
															   ]
													]);

	  $this->middleware('permission:AUDITAR_IMPLEMENTACION',  ['only' => ['implementacionAuditada']]);

	  $this->middleware('permission:VER_AUDITORIA_IMPLEMENTACION_TRADE', ['only' => ['verAuditoria']]);

	  $this->middleware('permission:VER_REPORTES_TRADE', ['only' => ['exportar_reporte_basico',
                                                                        'dashVer'
																	]
														]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $piezas = TradePieza::with(['tipo','contenido','objetivo'])->where('status','A')->latest()->paginate(5);
        return view('trade.piezas.index',compact('piezas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipos = TradeTipo::where('status','=','A')->get();
        $contenidos = TradeContenido::where('status','=','A')->get();
        $objetivos = TradeObjetivo::where('status','=','A')->get();

        return view('trade.piezas.create',compact('tipos','contenidos','objetivos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'tipo_pieza_id' => ['required'],
            'contenido_id' => ['required'],
            'fuente' => ['required','not_in:X'],
            'objetivo_id' => ['required'],
            'imagen' => ['required','max:10000'],
        ],[
            'tipo_pieza_id.required' => 'Debe seleccionar un tipo de pieza',
            'contenido_id.required' => 'Debe seleccionar un Contenido a Comunicar',
            'fuente.required' => 'Seleccione una Fuente por la cual seleccione un objetivo',
            'fuente.not_in' => 'Seleccione una Fuente por la cual seleccione un objetivo',
            'objetivo_id.required' => 'Seleccione un Objetivo de Colocacion',
            'imagen.required' => 'Por favor asocie una imgen a la pieza',
            'imagen.max' => 'La imagen debe ser de maximo 10Mb',
        ]);

//		dd($request->files);

        $extension = $request->file('imagen')->extension();
        if(!(strpos('jpg,jpeg,png', $extension) !== false)){
            return redirect()->route('piezas.create')
                        ->with('error','La imagen debe ser JPG, JPEG, PNG');
        }

        $nombre_pieza = 'pz_'.$request->tipo_pieza_id.'-'
                             .$request->contenido_id.'-'
                             .'obj1.'
                             .$extension;


		$exists = Storage::disk('public')->exists('trade/'.$nombre_pieza);

		if($exists == true){
            return redirect()->route('piezas.create')
                        ->with('error','Ese Tipo de Pieza ya Existe Con esa Comunicacion');
        }


        $dir_guardado = Storage::disk('public')
                        ->putFileAs(
                            'trade',
                            $request->file('imagen'),
                            $nombre_pieza
                        );


        $piezaGuardada = TradePieza::create([
            'tipo_pieza_id' => $request->tipo_pieza_id,
            'contenido_id' => $request->contenido_id,
            'objetivo_id' => 3,
            'imagen' => $dir_guardado
            ]);

        $id_pieza = $piezaGuardada->id;


        foreach($request->objetivo_id as $objetivo){
            $asociaObjetivos = TradePiezaObjetivos::create([
                'pieza_id' => $id_pieza,
                'objetivo_id' => $objetivo
                ]);
        }


        return redirect()->route('piezas.index')
                        ->with('success','Pieza Creada Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pieza = TradePieza::find($id);
        return view('trade.piezas.show',compact('pieza'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return redirect()->route('piezas.index')
                        ->with('error','La edicion de Piezas no esta Implementada aun, contacte al administrador');
        $pieza = TradePieza::find($id);
        return view('trade.piezas.edit',compact('pieza'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'tipo_pieza_id' => ['required'],
            'contenido_id' => ['required'],
            'objetivo_id' => ['required'],
            'imagen' => ['required','image','mimes:jpg,png,jpeg','max:3000'],
        ],[
            'tipo_pieza_id.required' => 'Debe seleccionar un tipo de pieza',
            'contenido_id.required' => 'Debe seleccionar un Contenido a Comunicar',
            'objetivo_id.required' => 'Seleccione un Objetivo de Colocacion',
            'imagen.required' => 'Por favor asocie una imgen a la pieza',
            'imagen.image' => 'Debe ser una imagen por favor',
            'imagen.mimes' => 'Imagen debe ser jpg,png,jpeg',
            'imagen.max' => 'Imagen debe ser de maximo 3Mb',
        ]);

        $pieza = TradePieza::find($id);

        $pieza->update(['tipo_pieza_id' => $request->tipo_pieza_id,
                        'contenido_id' => $request->contenido_id,
                        'objetivo_id' => $request->objetivo_id]);

        return redirect()->route('piezas.index')
                        ->with('success','Pieza Actualizada Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pieza = TradePieza::find($id);
        $pieza->status = 'I';
        $pieza->save();

        //$pieza->delete();

        return redirect()->route('piezas.index')
                        ->with('success','Pieza Eliminada Exitosamente');
    }


    /**
     * Muestra la pagina para seleccionar implementacion a trabajar.
     *
     * @return \Illuminate\Http\Response
     */
    public function seleccionaImplementacion()
    {
        //
        $implementaciones = TradeImplementacion::where('status','=','A')->get();

        return view('trade.piezas.implementaciones',compact('implementaciones'));
    }

    /**
     * Muestra la pagina para seleccionar implementacion a trabajar.
     *
     * @return \Illuminate\Http\Response
     */
    public function implementacionSeleccionada(Request $request)
    {
        //
        $id_impl = $request->implementacion_id;
        $implementacion = $this->trae_implementacion($id_impl);
        $no_asociados = $this->trae_no_asociados($id_impl);
        $asociados = $this->trae_asociados($id_impl);
        //dd($implementacion,$no_asociados);

		//$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		//$var_log_datos['controlador'] = 'EncuestasController@EncuestasOficiales()';
		//$var_log_datos['valores'] = ['punto_venta' => $idpdv, 'Encuesta' => $id_encuesta];
		//$var_log_mensaje = 'Usuario Ejecuta Encuesta en Punto de Venta';
		//Log::notice($var_log_mensaje,$var_log_datos);

        return view('trade.piezas.asociacion',compact('implementacion','no_asociados','asociados'));
    }

    private function trae_implementacion($id_implementacion)
    {
      $implementacion = TradeImplementacion::find($id_implementacion)->toArray();
      return $implementacion;
    }

    private function trae_no_asociados($id_implementacion)
    {
      $qry_piezas_no_asoc = "
      SELECT P.id id,  T.nombre_pieza pieza, C.contenido contenido, O.objetivo objetivo, P.imagen imagen
      FROM trade_piezas P
            INNER JOIN trade_tipo_piezas T ON (T.id = P.tipo_pieza_id)
            INNER JOIN trade_contenidos C ON (C.id = P.contenido_id)
            INNER JOIN trade_objetivos O ON (O.id = P.objetivo_id)
      WHERE
           P.status = 'A' AND P.id NOT IN (SELECT I.pieza_id FROM trade_implementacion_piezas I WHERE I.status = 'A' AND I.implementacion_id = $id_implementacion);";
      $no_asociados = DB::select($qry_piezas_no_asoc);
      return $no_asociados;
    }

    private function trae_asociados($id_implementacion)
    {
      //Este es el query anterior, si funciona el nuevo,elimina esto
      $qry_piezas_asoc_old = "
      SELECT P.id id,  T.nombre_pieza pieza, C.contenido contenido, O.objetivo objetivo, P.imagen imagen
      FROM trade_piezas P
            INNER JOIN trade_tipo_piezas T ON (T.id = P.tipo_pieza_id)
            INNER JOIN trade_contenidos C ON (C.id = P.contenido_id)
            INNER JOIN trade_objetivos O ON (O.id = P.objetivo_id)
      WHERE
           P.status = 'A' AND P.id IN (SELECT I.pieza_id FROM trade_implementacion_piezas I WHERE I.status = 'A' AND I.implementacion_id = $id_implementacion);";

      $qry_piezas_asoc = "
      SELECT IP.id impl_pieza_id, P.id,  T.nombre_pieza pieza, C.contenido contenido, O.objetivo objetivo, P.imagen imagen, SUM(IFNULL(INV.cantidad,0)) inventario
      FROM trade_piezas P
           INNER JOIN trade_tipo_piezas T ON (T.id = P.tipo_pieza_id)
           INNER JOIN trade_contenidos C ON (C.id = P.contenido_id)
           INNER JOIN trade_objetivos O ON (O.id = P.objetivo_id)
           INNER JOIN trade_implementacion_piezas IP ON (IP.pieza_id = P.id)
           INNER JOIN trade_implementaciones I ON (IP.implementacion_id = I.id)
           LEFT JOIN trade_inventario_piezas INV ON (INV.implementacion_id = IP.id)
      WHERE
        P.status = 'A' AND I.status = 'A' AND I.id = $id_implementacion
      GROUP BY IP.id, P.id, T.nombre_pieza, C.contenido, O.objetivo, P.imagen;";

      $asociados = DB::select($qry_piezas_asoc);
      return $asociados;
    }


    /**
     * Muestra la pagina para seleccionar implementacion a trabajar.
     *
     * @return \Illuminate\Http\Response
     */
    public function asociarPiezas($implem, $pieza)
    {
        //
        TradePiezasImplementacion::create(['pieza_id' => $pieza,'implementacion_id' => $implem]);

        $implementacion = $this->trae_implementacion($implem);
        $no_asociados = $this->trae_no_asociados($implem);
        $asociados = $this->trae_asociados($implem);
        //dd($implementacion,$no_asociados);
        return view('trade.piezas.asociacion',compact('implementacion','no_asociados','asociados'));
    }

    /**
     * Muestra la pagina para seleccionar implementacion a trabajar.
     *
     * @return \Illuminate\Http\Response
     */
    public function desasociarPiezas($implem, $pieza)
    {
        //
        $pieza_implem = TradePiezasImplementacion::where('pieza_id',$pieza)->where('implementacion_id',$implem)->delete();

        $implementacion = $this->trae_implementacion($implem);
        $no_asociados = $this->trae_no_asociados($implem);
        $asociados = $this->trae_asociados($implem);
        //dd($implementacion,$no_asociados);
        return view('trade.piezas.asociacion',compact('implementacion','no_asociados','asociados'));
    }


	public function trae_categoriaH($idpdv){

		$categoriaH = Infopos_Rpoint::where('idpdv',$idpdv)->select('categoria_homologada')->first()->toArray();
		return $categoriaH['categoria_homologada'];
	}



	public function trae_segmento($idpdv){

		$segm_punto = Infopos_Rpoint::where('idpdv',$idpdv)->select('ult_seg_gross')->first()->toArray();
		return $segm_punto['ult_seg_gross'];
	}

	public function trae_maxPiezas($segmento){

		switch ($segmento) {
          case 'A. RevenueCero':
            $max_piezas = 2;
            break;
          case 'B. Principiantes':
            $max_piezas = 3;
            break;
          case 'C. EnDesarrollo':
            $max_piezas = 4;
            break;
		  case 'D. Desarrollados':
            $max_piezas = 5;
            break;
		  case 'E. Productivos':
            $max_piezas = 7;
            break;
          default:
            $max_piezas = 2;
            break;
        }

		return $max_piezas;

	}

	public function traeNombrePunto($idpdv){

		$rpoint =  Infopos_Rpoint::where('idpdv',$idpdv)->first()->toArray();
        $nombre_punto = $rpoint['nombre_punto'];
		return $nombre_punto;
	}

	public function trae_auditorias($idpdv, $implem){

        $auditorias =  TradeAuditoria::with('resultado',
                                            'usuario',
                                            'implementacion',
                                            'hallazgos',
                                            'hallazgos.hallazgo'
                                            )
										->where('idpdv',$idpdv)
										->where('implementacion_id',$implem)
										->get();
		return $auditorias;
	}

	public function trae_resultados(){

		$resultados =  TradeResultado::select('id','nombre')->get()->toArray();
		return $resultados;
    }

    public function trae_hallazgos(){

		$hallazgos =  TradeHallazgos::select('id','nombre')->get()->toArray();
		return $hallazgos;
    }

	public function traeTipoNegocio($idpdv){

		$rpoint =  Infopos_Rpoint::where('idpdv',$idpdv)->first()->toArray();

       // $nombre_punto = $rpoint['nombre_punto'];

        switch ($rpoint['ultima_tipologia']) {
          case 'PDA':
            $tipo_negocio = 'PDA';
            break;
          case 'PDV':
            $tipo_negocio = 'PDV';
            break;
          case 'MIXTO':
            $tipo_negocio = 'MIXTO';
            break;
          default:
            $tipo_negocio = 'MIXTO';
            break;
        }

		return $tipo_negocio;

	}


    /**
     * Muestra la pagina para seleccionar implementacion a trabajar.
     *
     * @return \Illuminate\Http\Response
     */
    public function ColocacionSeleccionada(Request $request)
    {
        //
        $idpdv = $request->idpdv;
//        $nombre_punto = $request->nombre_punto;
//        $tipo_negocio = $request->tipo_negocio;
//        $max_piezas = $request->max_piezas;
        $id_impl = $request->implementacion_id;

		$tipo_negocio = $this->traeTipoNegocio($idpdv);
		$segmento = $this->trae_segmento($idpdv);
		$categoriaH = $this->trae_categoriaH($idpdv);
		$max_piezas = $this->trae_maxPiezas($segmento);
		$nombre_punto = $this->traeNombrePunto($idpdv);

        $resultados = $this->trae_resultados();
        $hallazgos = $this->trae_hallazgos();
		$auditorias = $this->trae_auditorias($idpdv, $id_impl);


        $implementacion = $this->trae_implementacion($id_impl);
        $piezas_colocacion = $this->trae_piezas_colocacion($id_impl, $idpdv);
        $piezas_implementadas = $this->trae_piezas_implementadas($id_impl, $idpdv);

        //dd($implementacion, $piezas_colocacion, $piezas_implementadas, $tipo_negocio, $Segm_punto, $max_piezas);

        return view('trade.piezas.colocacion',compact('implementacion', 'piezas_colocacion', 'piezas_implementadas', 'resultados', 'auditorias', 'hallazgos', 'max_piezas', 'segmento', 'categoriaH', 'tipo_negocio', 'idpdv', 'nombre_punto'));
    }


    private function trae_piezas_colocacion($id_implementacion, $idpdv)
    {
      $qry_piezas_colocacion = "
      SELECT P.id id,  T.nombre_pieza pieza, C.contenido contenido, O.objetivo objetivo, P.imagen imagen
      FROM trade_piezas P
            INNER JOIN trade_tipo_piezas T ON (T.id = P.tipo_pieza_id)
            INNER JOIN trade_contenidos C ON (C.id = P.contenido_id)
            INNER JOIN trade_objetivos O ON (O.id = P.objetivo_id)
            INNER JOIN trade_implementacion_piezas I ON (I.pieza_id = P.id)
      WHERE
           P.status = 'A' AND I.implementacion_id = $id_implementacion AND I.status = 'A'
           AND P.id NOT IN (SELECT J.pieza_id FROM trade_colocacion_piezas J WHERE J.implementacion_id = $id_implementacion AND J.idpdv = $idpdv);";
      $piezas = DB::select($qry_piezas_colocacion);
      return $piezas;
    }


    private function trae_piezas_implementadas($id_implementacion, $idpdv)
    {
      $qry_piezas_implementadas = "SELECT P.id id,  T.nombre_pieza pieza, C.contenido contenido, O.objetivo objetivo, P.imagen imagen, I.cantidad ";
      $qry_piezas_implementadas .= " FROM trade_piezas P
            INNER JOIN trade_tipo_piezas T ON (T.id = P.tipo_pieza_id)
            INNER JOIN trade_contenidos C ON (C.id = P.contenido_id)
            INNER JOIN trade_objetivos O ON (O.id = P.objetivo_id)
            INNER JOIN trade_colocacion_piezas I ON (I.pieza_id = P.id)
      WHERE ";
      $qry_piezas_implementadas .= "P.status = 'A' AND I.implementacion_id = ".$id_implementacion." AND I.idpdv = ".$idpdv." AND I.colocado = 'S';";
      $piezas = DB::select($qry_piezas_implementadas);
      return $piezas;
    }



    public function colocarPiezas($idpdv, $implem, $pieza)
    {
        //

        $existe_pieza = TradeColocacionPiezas::where('idpdv', $idpdv)
                                             ->where('implementacion_id', $implem)
                                             ->where('pieza_id', $pieza)
                                             ->first();


        if(is_null($existe_pieza)){
            TradeColocacionPiezas::create(
                [
                    'user_id' => Auth::id(),
                    'pieza_id' => $pieza,
                    'implementacion_id' => $implem,
                    'idpdv' => $idpdv,
                    'fecha_implementacion' => Carbon::now()
                ]);
        }else{
            $existe_pieza->cantidad += 1;
            $existe_pieza->save();
        }

        $tipo_negocio = $this->traeTipoNegocio($idpdv);
        $segmento = $this->trae_segmento($idpdv);
		$categoriaH = $this->trae_categoriaH($idpdv);
        $max_piezas = $this->trae_maxPiezas($segmento);
        $nombre_punto = $this->traeNombrePunto($idpdv);

		$resultados = $this->trae_resultados();
        $auditorias = $this->trae_auditorias($idpdv, $implem);
        $hallazgos = $this->trae_hallazgos();

        $implementacion = $this->trae_implementacion($implem);
        $piezas_colocacion = $this->trae_piezas_colocacion($implem, $idpdv);
        $piezas_implementadas = $this->trae_piezas_implementadas($implem, $idpdv);

		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'TradePiezasController@colocarPiezas()';
		$var_log_datos['valores'] = ['punto_venta' => $idpdv, 'pieza' => $pieza, 'implementacion' => $implem];
		$var_log_mensaje = 'Usuario Implementa Pieza en Punto de Venta';
		Log::notice($var_log_mensaje,$var_log_datos);
		
        //dd($implementacion, $piezas_colocacion, $piezas_implementadas, $tipo_negocio, $Segm_punto, $max_piezas);

        return view('trade.piezas.colocacion',compact('implementacion', 'piezas_colocacion', 'piezas_implementadas', 'resultados', 'auditorias', 'hallazgos', 'max_piezas', 'segmento', 'categoriaH', 'tipo_negocio', 'idpdv', 'nombre_punto'));
    }



    public function quitarPiezas($idpdv, $implem, $pieza)
    {
        //
        $pieza_implem = TradeColocacionPiezas::where('idpdv',$idpdv)
                                              ->where('pieza_id',$pieza)
                                              ->where('implementacion_id',$implem)
                                              ->delete();


        $tipo_negocio = $this->traeTipoNegocio($idpdv);
        $segmento = $this->trae_segmento($idpdv);
		$categoriaH = $this->trae_categoriaH($idpdv);
        $max_piezas = $this->trae_maxPiezas($segmento);
        $nombre_punto = $this->traeNombrePunto($idpdv);

		$resultados = $this->trae_resultados();
        $auditorias = $this->trae_auditorias($idpdv, $implem);
        $hallazgos = $this->trae_hallazgos();

        $implementacion = $this->trae_implementacion($implem);
        $piezas_colocacion = $this->trae_piezas_colocacion($implem, $idpdv);
        $piezas_implementadas = $this->trae_piezas_implementadas($implem, $idpdv);
		
		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'TradePiezasController@quitarPiezas()';
		$var_log_datos['valores'] = ['punto_venta' => $idpdv, 'pieza' => $pieza, 'implementacion' => $implem];
		$var_log_mensaje = 'Usuario Intenta Retirar Pieza Implementada';
        Log::notice($var_log_mensaje,$var_log_datos);


        return redirect()->route('home')
                        ->with('error','No Se Puede Quitar Piezas Ya Implementadas');
        //dd($implementacion, $piezas_colocacion, $piezas_implementadas, $tipo_negocio, $Segm_punto, $max_piezas);

        return view('trade.piezas.colocacion',compact('implementacion', 'piezas_colocacion', 'piezas_implementadas', 'resultados', 'auditorias', 'hallazgos', 'max_piezas', 'segmento', 'categoriaH', 'tipo_negocio', 'idpdv', 'nombre_punto'));
    }


    public function implementacionAuditada(Request $request)
    {
		$idpdv = $request->idpdv;
		$implem = $request->implementacion;
        $resultado = $request->resultado;
        $hallazgos = $request->hallazgos;
		$observaciones = '';
		if ($request->has('observaciones')) {
			$observaciones = $request->observaciones;
        }

        //
        $auditado = new TradeAuditoria;
        $auditado->user_id = Auth::id();
        $auditado->implementacion_id = $implem;
        $auditado->idpdv = $idpdv;
        $auditado->resultados_auditoria_id = $resultado;
        $auditado->observaciones = trim($observaciones);
        $auditado->save();


        $idAuditoria = $auditado->id;

        foreach ($hallazgos as $hallazgo) {
            $hallados = new TradeAuditoriaHallazgos;
            $hallados->trade_auditoria_id = $idAuditoria;
            $hallados->trade_hallazgo_id = $hallazgo;
            $hallados->save();
        }

        $extension = $request->file('imagen')->extension();

        if(!(strpos('jpg,jpeg,png', $extension) !== false)){
            return view('home');
        }


        $nombre_foto = 'aud_'.$idAuditoria.'_pv_'.$request->idpdv.'.'.$extension;

        $image_resize = Image::make($request->file('imagen')->getRealPath());

        $image_resize->resize(800, null, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
       });

       $image_resize->orientate();

       $image_resize->save(storage_path("app/public/auditoria/" . $nombre_foto),60);

       $path = Storage::disk('auditoria')->url($nombre_foto);

       $auditado = TradeAuditoria::find($idAuditoria);
       $auditado->imagen = $nombre_foto;
       $auditado->save();


        $tipo_negocio = $this->traeTipoNegocio($idpdv);
        $segmento = $this->trae_segmento($idpdv);
		$categoriaH = $this->trae_categoriaH($idpdv);
        $max_piezas = $this->trae_maxPiezas($segmento);
        $nombre_punto = $this->traeNombrePunto($idpdv);

		$resultados = $this->trae_resultados();
        $auditorias = $this->trae_auditorias($idpdv, $implem);
        $hallazgos = $this->trae_hallazgos();

        $implementacion = $this->trae_implementacion($implem);
        $piezas_colocacion = $this->trae_piezas_colocacion($implem, $idpdv);
        $piezas_implementadas = $this->trae_piezas_implementadas($implem, $idpdv);

        //dd($auditorias);

        //dd($implementacion, $piezas_colocacion, $piezas_implementadas, $tipo_negocio, $Segm_punto, $max_piezas);

        return view('trade.piezas.colocacion',compact('implementacion', 'piezas_colocacion', 'piezas_implementadas', 'resultados', 'auditorias', 'hallazgos', 'max_piezas', 'segmento', 'categoriaH', 'tipo_negocio', 'idpdv', 'nombre_punto'));
    }


	public function verAuditoria($implem, $idpdv){
        $auditorias = $this->trae_auditorias($idpdv, $implem);
		return view('trade.piezas.auditorias',compact('auditorias', 'idpdv', 'implem'));
    }


    /**
     * Ver Inventario de piezas.
     * Producidas para una implementacion
     */
    public function ajustarInventario(Request $request){
        $request->validate([
            'regional' => ['required'],
            'cantidad' => ['required','gt:0'],
        ],[
            'regional.required' => 'Debe seleccionar una Regional',
            'cantidad.required' => 'Debe Ingresar Cantidad de Piezas',
            'cantidad.gt' => 'Por lo menos debe ingresar 1 pieza',
        ]);


        $inventario = TradeInventarioPiezas::where('implementacion_id', $request->impl_pieza)
                                           ->where('regional_id', $request->regional)
                                           ->first();

        if($inventario){
            $inventario->cantidad = $request->cantidad;
            $inventario->user_id = $request->usuario;
            $inventario->save();
        }else{
            $nuevo_inventario = new TradeInventarioPiezas;
            $nuevo_inventario->implementacion_id = $request->impl_pieza;
            $nuevo_inventario->regional_id = $request->regional;
            $nuevo_inventario->cantidad = $request->cantidad;
            $nuevo_inventario->user_id = $request->usuario;
            $nuevo_inventario->save();
        }

		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'TradePiezas@ajustarInventario()';
		$var_log_datos['valores'] = ['regional' => $request->regional, 'cantidad' =>$request->cantidad,'impl_pieza_id' => $request->impl_pieza];
		$var_log_mensaje = 'Usuario Ajusta Inventario de Pieza en Implementacion de Campaña';
		Log::notice($var_log_mensaje,$var_log_datos);
        
		return redirect()->route('trade.inventario.ver',['impl_pieza' => $request->impl_pieza])->with('success','Inventario Pieza Ajustado Exitosamente');

    }

    /**
     * Ver Inventario de piezas.
     * Producidas para una implementacion
     */
    public function verInventario($impl_pieza){

        $trade_impl_pieza = TradePiezasImplementacion::with(['pieza','implementacion'])->find($impl_pieza);
        $datos_pieza = TradePieza::with(['tipo','contenido','objetivo'])->find($trade_impl_pieza->pieza_id);
        $cant_regiones = TradeInventarioPiezas::with(['regional','usuario'])->where('implementacion_id', $impl_pieza)->get();
        $inventario = TradeInventarioPiezas::where('implementacion_id', $impl_pieza)->sum('cantidad');
        $regionales = Regional::where('status','A')->select('id','regional_name')->orderBy('id','ASC')->get()->toArray();

        $datos =
        [
            'id_implementacion_pieza' => $impl_pieza,
            'nombre_implementacion' => $trade_impl_pieza->implementacion->descripcion,
			'implementacion_id' => $trade_impl_pieza->implementacion->id,
            'usuario' => Auth::user()->id,
        ];

        $pieza =
        [
            'id_pieza' => $datos_pieza->id,
            'nombre' => $datos_pieza->tipo->nombre_pieza,
            'contenido' => $datos_pieza->contenido->contenido,
            'imagen' => $datos_pieza->imagen,
            'inventario' => $inventario,
        ];

        $inv_regionales = [];

        foreach($cant_regiones as $region){
            $inv_regionales[] = [
                                'id_region' => $region->regional_id,
                                'nombre_regional' => $region->regional->regional_name,
                                'usuario' => $region->usuario->name,
                                'cantidad' => $region->cantidad
                            ];
        }

/*         dd(
            'DATOS', $datos,
            'PIEZA', $pieza,
            'REGIONALES', $regionales,
            'CANT/INV REGIONALES', $inv_regionales,
        ); */

		return view('trade.piezas.inventarios',compact('datos', 'pieza', 'regionales','inv_regionales'));
    }



    /**
     * Muestra la pagina para seleccionar implementacion a trabajar.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashVer()
    {
        //
        $qry_inv_nacional = 
			"SELECT id_implementacion, implementacion,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
             GROUP BY id_implementacion, implementacion;";
		
		$inv_nacional = DB::select($qry_inv_nacional);
		$inv_nacional = json_decode(json_encode($inv_nacional),true);

        return view('trade.dash.ver',compact('inv_nacional'));
    }

	/**
     * Ver Inventario de piezas.
     * Producidas para una implementacion
     */
    public function dashVerImplementacion($implementacion){

        $qry_inv_nacional = 
			"SELECT id_implementacion, implementacion,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
			 WHERE id_implementacion = $implementacion
             GROUP BY id_implementacion, implementacion;";
		
		$inv_nacional = DB::select($qry_inv_nacional);
		$inv_nacional = json_decode(json_encode($inv_nacional),true);
		
		$qry_inv_regional = 
			"SELECT id_implementacion, implementacion, id_regional, regional,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
			 WHERE id_implementacion = $implementacion AND id_regional IS NOT NULL
             GROUP BY id_implementacion, implementacion, id_regional, regional;";
		
		$inv_regional = DB::select($qry_inv_regional);
		$inv_regional = json_decode(json_encode($inv_regional),true);
		
		$qry_inv_piezas = 
			"SELECT id_implementacion, implementacion, pieza, imagen,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
             WHERE id_implementacion = $implementacion
             GROUP BY id_implementacion, implementacion, pieza, imagen;";
		
		$inv_piezas = DB::select($qry_inv_piezas);
		$inv_piezas = json_decode(json_encode($inv_piezas),true);
		
        
		//dd($implementacion, $inv_nacional, $inv_regional, $inv_piezas);
		
        return view('trade.dash.verNacional',
                    compact(
                            'implementacion',
                            'inv_nacional',
                            'inv_regional',
							'inv_piezas'
                        )
                    );
		
    }
	
	
	/**
     * Ver Inventario de piezas.
     * Producidas para una implementacion
     */
    public function dashVerRegional($implementacion, $regional){

        $qry_inv_nacional = 
			"SELECT id_implementacion, implementacion,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
			 WHERE id_implementacion = $implementacion
             GROUP BY id_implementacion, implementacion;";
		
		$inv_nacional = DB::select($qry_inv_nacional);
		$inv_nacional = json_decode(json_encode($inv_nacional),true);
		
		$qry_inv_regional = 
			"SELECT id_implementacion, implementacion, regional,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
			 WHERE id_implementacion = $implementacion AND id_regional = $regional
             GROUP BY id_implementacion, implementacion, regional;";
		
		$inv_regional = DB::select($qry_inv_regional);
		$inv_regional = json_decode(json_encode($inv_regional),true);
		
		$qry_inv_piezas = 
			"SELECT id_implementacion, implementacion, pieza, imagen,
             SUM(cnt_colocacion) colocado, SUM(cnt_inventario) inventario,
             ROUND( IF(SUM(cnt_inventario) > 0.0, (SUM(cnt_colocacion)/SUM(cnt_inventario))*100, 0.0) , 2) porcentaje
             FROM trade_resumen_implementacion
             WHERE id_implementacion = $implementacion AND id_regional = $regional
             GROUP BY id_implementacion, implementacion, pieza, imagen;";
		
		$inv_piezas = DB::select($qry_inv_piezas);
		$inv_piezas = json_decode(json_encode($inv_piezas),true);
		
        
		//dd($implementacion, $inv_nacional, $inv_regional, $inv_piezas);
		
        return view('trade.dash.verRegional',
                    compact(
                            'implementacion',
                            'inv_nacional',
                            'inv_regional',
							'inv_piezas'
                        )
                    );
		
    }	
	
	
	
    /**
     * Exportar a Excel.
     *
     */
    public function exportar_reporte_basico()
    {
        //
		// REDIRECCINAMOS A EL FILE MANAGER

		return redirect()->route('archivos.verArchivos');

		// ESTO NO LO VAMOS A EJECUTAR POR TEMAS DE RENDIMIENTO
			ini_set('max_execution_time', '300');
		    $consulta_implementaciones =
                  "SELECT
                        C.fecha_implementacion FECHA,
                        U.name QUIEN_IMPLEMENTA,
                        E.document DOCUMENTO,
                        R.name ROLE_ASIGNADO,
                        I.descripcion IMPLEMENTACION,
                        TP.nombre_pieza PIEZA,
                        CP.nombre_clase CLASE,
                        TC.contenido COMUNICACION,
                        O.objetivo OBJETIVO,
                        RP.idpdv ID_PDV,
                        RP.nombre_punto NOMBRE_PUNTO,
                        RP.ultima_tipologia TIPOLOGIA,
                        RP.categoria_homologada CATEGORIA_HOMOLOGADA,
                        RP.regional REGIONAL,
                        RP.departamento DEPARTAMENTO,
                        RP.ciudad CIUDAD,
                        RP.circuito CIRCUITO,
                        RP.estado_dms ESTADO_DMS,
                        RP.dealer DISTRIBUIDOR,
                        RP.sucursal SUCURSAL,
                        RP.nombre_cve NOMBRE_CVE
                  FROM
                      trade_colocacion_piezas C
                      INNER JOIN trade_implementaciones I ON (C.implementacion_id = I.id)
                      INNER JOIN infopos_rpoint RP ON (C.idpdv = RP.idpdv)
                      INNER JOIN trade_piezas P ON (C.pieza_id = P.id)
                      INNER JOIN trade_objetivos O ON (P.objetivo_id = O.id)
                      INNER JOIN trade_contenidos TC ON (P.contenido_id = TC.id)
                      INNER JOIN trade_tipo_piezas TP ON (P.tipo_pieza_id = TP.id)
                      INNER JOIN trade_clase_piezas CP ON (TP.clase_pieza_id = CP.id)
                      INNER JOIN users U ON (U.id = C.user_id)
                      INNER JOIN employees E ON (U.id = E.user_id)
                      INNER JOIN empleado_role ER ON(ER.id_empleado = E.id)
                      INNER JOIN roles R ON (ER.id_role = R.id)
                  ORDER BY RP.idpdv;";


		$puntos_implementados = DB::select($consulta_implementaciones);

        return (new TradeExportBasico($puntos_implementados))
                ->download('TradeImplementacionGeneral.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }



}
