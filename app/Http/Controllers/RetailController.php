<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\RetailExportNacional;

Use App\User;
use App\Models\Infopos_Rpoint;




class RetailController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function ver_todo(){
		
		$qry_regionales = "SELECT DISTINCT regional FROM pv_retail";
		$regionales = DB::select($qry_regionales);

		$qry_pv_hd = "SELECT P.idpdv, R.cadena, R.nombre_punto, R.regional, R.departamento, R.ciudad, R.direccion, R.pos_padre, R.cod_pos, R.ejecutivo FROM pv_retail R LEFT JOIN infopos_rpoint P on R.cod_pos = P.pos_code";
		$puntos = DB::select($qry_pv_hd);
		
		return view('retail.hd.ver_todo', compact('regionales','puntos'));
	}
	
	
	public function ver_regional($regional){
		
		$esta_regional = $regional;
		
		$qry_regionales = "SELECT DISTINCT regional FROM pv_retail";
		$regionales = DB::select($qry_regionales);
		
		$qry_dptos = "SELECT DISTINCT departamento FROM pv_retail WHERE regional = '$regional'";
		$departamentos = DB::select($qry_dptos);

		$qry_pv_hd = "SELECT P.idpdv, R.cadena, R.nombre_punto, R.regional, R.departamento, R.ciudad, R.direccion, R.pos_padre, R.cod_pos, R.ejecutivo FROM pv_retail R LEFT JOIN infopos_rpoint P on R.cod_pos = P.pos_code WHERE R.regional = '$regional'";
		$puntos = DB::select($qry_pv_hd);
		
		return view('retail.hd.ver_regional', compact('regionales','puntos','esta_regional', 'departamentos'));
	}
	
	public function ver_departamento($regional, $departamento){
		
		$esta_regional = $regional;
		$este_dpto = $departamento;
		
		$qry_regionales = "SELECT DISTINCT regional FROM pv_retail";
		$regionales = DB::select($qry_regionales);
		
		$qry_dptos = "SELECT DISTINCT departamento FROM pv_retail WHERE regional = '$regional'";
		$departamentos = DB::select($qry_dptos);

		$qry_pv_hd = "SELECT P.idpdv, R.cadena, R.nombre_punto, R.regional, R.departamento, R.ciudad, R.direccion, R.pos_padre, R.cod_pos, R.ejecutivo FROM pv_retail R LEFT JOIN infopos_rpoint P on R.cod_pos = P.pos_code WHERE R.regional = '$regional' AND R.departamento = '$departamento'";
		$puntos = DB::select($qry_pv_hd);
		
		return view('retail.hd.ver_departamento', compact('regionales','puntos','esta_regional', 'este_dpto', 'departamentos'));
	}
	
	
	    /**
     * Exportar a Excel.
     *
     */
    public function exportar_puntos_nacional()
    {
        //

		$qry_puntos = "SELECT P.idpdv, R.cadena, R.nombre_punto, R.regional, R.departamento, R.ciudad, R.direccion, R.pos_padre, R.cod_pos, R.ejecutivo FROM pv_retail R LEFT JOIN infopos_rpoint P on R.cod_pos = P.pos_code";
		$puntos = DB::select($qry_puntos);
        
		return (new RetailExportNacional($puntos))
                ->download('puntos_nacional.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }
	
	public function exportar_puntos_regional($regional)
    {
        //

		$qry_puntos = "SELECT P.idpdv, R.cadena, R.nombre_punto, R.regional, R.departamento, R.ciudad, R.direccion, R.pos_padre, R.cod_pos, R.ejecutivo FROM pv_retail R LEFT JOIN infopos_rpoint P on R.cod_pos = P.pos_code WHERE R.regional = '$regional'";
		$puntos = DB::select($qry_puntos);
        
		return (new RetailExportNacional($puntos))
                ->download('puntos_regional.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }
	
	public function exportar_puntos_departamento($regional, $departamento)
    {
        //

		$qry_puntos = "SELECT P.idpdv, R.cadena, R.nombre_punto, R.regional, R.departamento, R.ciudad, R.direccion, R.pos_padre, R.cod_pos, R.ejecutivo FROM pv_retail R LEFT JOIN infopos_rpoint P on R.cod_pos = P.pos_code WHERE R.regional = '$regional' AND R.departamento = '$departamento'";
		$puntos = DB::select($qry_puntos);
        
		return (new RetailExportNacional($puntos))
                ->download('puntos_departamento.xlsx', \Maatwebsite\Excel\Excel::XLSX);

    }

}
