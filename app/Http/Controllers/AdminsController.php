<?php

namespace App\Http\Controllers;

Use App\Models\Admins;
Use App\Models\Channel;
Use App\Models\Employee;
Use App\Models\EmployeeChannel;
Use App\Models\Regional;
Use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class AdminsController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_ADMINS', ['only' => ['index']]);
      $this->middleware('permission:VER_ADMINS',    ['only' => ['show','ver_buscar_empleado','buscar_empleado']]);
      $this->middleware('permission:CREAR_ADMINS',  ['only' => ['create','store','ver_buscar_empleado','buscar_empleado']]);
      $this->middleware('permission:EDITAR_ADMINS', ['only' => ['edit','update','ver_buscar_empleado','buscar_empleado']]);
      $this->middleware('permission:BORRAR_ADMINS', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(! Admins::Validar()){
            return redirect()->route('home')
                        ->with('error','Usuario no esta asignado como Admin/CoAdmin.');
        }


        $regional = Admins::Region();
        $nombre_regional = $regional['nom_regional'];
        $nombre_regional = $nombre_regional == 'TODOS/TODAS' ? '' : $nombre_regional;

        $administradores = Admins::TraerAdmins($nombre_regional)->paginate(5);

        /*
        $administradores = Admins::with(['user','employee','channel','regional'])
            ->whereHas('regional', function (Builder $q) use($nombre_regional)
                      {
                          $q->where('regional_name','LIKE',"%$nombre_regional%");
                      })
            ->whereHas('channel', function (Builder $q) use($nombre_canal)
                      {
                          $q->where('channel_name','LIKE',"%$nombre_canal%");
                      })
            ->where('user_id','<>',Auth::user()->id)
            ->paginate(5);
        */

/*
        $administradores = Admins::with('user','employee','channel','regional')
                                  ->orderBy('regional_id','ASC')
                                  ->latest()->paginate(5);
*/
        return view('admins.index',compact('administradores'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ver_buscar_empleado()
    {
        //
        return view('admins.buscar_empleado');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buscar_empleado(Request $request)
    {
        //
        $request->validate([
            'cedula_empleado' => ['required', 'integer','exists:employees,document'],
        ]);
        $empleado = Employee::with('user','position','channels','territory.regional')
                             ->where('document',$request->cedula_empleado)
                             ->where('user_id','<>',Auth::user()->id)
                             ->first();

        if(!$empleado){
            return redirect()->route('admins.index')
                             ->with('error','No puede ser ud. mismo.');
        }

        $validar_admin = Admins::where('employee_id',$empleado->id)->first();

        if($validar_admin){
            return redirect()->route('admins.index')
                             ->with('error','Ya el Empleado es Admin/CoAdmin.');
        }

        $administrador['id_empleado'] = $empleado->id;
        $administrador['nom_empleado'] = $empleado->user->name;
        $administrador['cedula_empleado'] = $empleado->document;
        $administrador['id_usuario'] = $empleado->user->id;
        $administrador['id_regional'] = $empleado->territory->regional->id;
        $administrador['nom_regional'] = $empleado->territory->regional->regional_name;
        $administrador['id_cargo'] = $empleado->position_id;
        $administrador['nom_cargo'] = $empleado->position->position_name;
        $administrador['id_canal'] = $empleado->channels->first()->id;
        $administrador['nom_canal'] = $empleado->channels->first()->channel_name;

        $canales = Channel::all()->toArray();
        $regionales = Regional::all()->toArray();

        return view('admins.create',compact('canales','regionales','administrador'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $canales = Channel::all()->toArray();
        $regionales = Regional::all()->toArray();

        return view('admins.create', compact('canales', 'regionales'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		
        /*
        $request->validate([
            'usuario_asignado' => ['required', 'integer','exists:users,id'],
            'empleado_asignado' => ['required', 'integer','exists:employees,id'],
            'nivel_asignado' => ['required', 'not_in:X','string','in:ADMIN,COADMIN'],
            'canal_asignado' => ['required', 'not_in:0','integer','exists:channels,id'],
            'regional_asignada' => ['required', 'not_in:0','integer','exists:regionals,id'],
        ]);
        */
        $validator = Validator::make($request->all(), [
          'usuario_asignado' => ['required', 'integer','exists:users,id'],
          'empleado_asignado' => ['required', 'integer','exists:employees,id'],
          'nivel_asignado' => ['required', 'not_in:X'],
          'canal_asignado' => ['required', 'not_in:0','integer','exists:channels,id'],
          'regional_asignada' => ['required', 'not_in:0','integer','exists:regionals,id'],
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->route('admins.ver.buscar.empleado')
                        ->withErrors($validator)
                        ->withInput();
        }

	
        Admins::create([
          'user_id' => strtoupper($request->usuario_asignado),
          'channel_id' => strtoupper($request->canal_asignado),
          'employee_id' => strtoupper($request->empleado_asignado),
          'regional_id' => strtoupper($request->regional_asignada),
          'level' => strtoupper($request->nivel_asignado),
        ]);



		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'AdminsController@store()';
		$var_log_datos['valores'] = ['user_id' => strtoupper($request->usuario_asignado),
          'channel_id' => strtoupper($request->canal_asignado),
          'employee_id' => strtoupper($request->empleado_asignado),
          'regional_id' => strtoupper($request->regional_asignada),
          'level' => strtoupper($request->nivel_asignado)];
		$var_log_mensaje = 'Se ha creado un nuevo Admin/CoAdmin';
		Log::notice($var_log_mensaje,$var_log_datos);

        return redirect()->route('admins.index')
                        ->with('success','Administrador Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $administrador = Admins::with('user','employee.position','channel','regional')->find($id);
        //dd($administrador);
        return view('admins.show',compact('administrador'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $administrador = Admins::find($id);
        $administrador->delete();
		
		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'AdminsController@destroy()';
		$var_log_datos['valores'] = ['id_admin' => $id];
		$var_log_mensaje = 'Se ha Eliminado un nuevo Admin/CoAdmin';
		Log::notice($var_log_mensaje,$var_log_datos);

        return redirect()->route('admins.index')
                        ->with('success','Administrador Eliminado Exitosamente');
    }
}
