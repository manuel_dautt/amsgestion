<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Models\Visits;
use App\Models\VisitsType;
use App\Models\EncuestaVisitas;


class VisitsController extends Controller
{
    //
    public function registrar_visita(Request $request){

        $request->validate([
            'tipo_visita'  => ['required','not_in:X'],
        ]);

        $usuario = Auth::id();
        $tipo_visita = $request->input('tipo_visita');
        $idpdv =  $request->input('idpdv');
        $planeada =  $request->input('planeada');
        $nom_pv = $request->input('nom_pv');
        $lat  = $request->input('lat');
        $lng  = $request->input('lng');
        $fechaIni = Carbon::now();
        $nom_tipo_visita = VisitsType::find($tipo_visita)->toArray();

        $visita = Visits::create([
            'user_id' => $usuario,
            'visit_type_id' => $tipo_visita,
            'idpdv' => $idpdv,
            'planeada' => $planeada,
            'fecha_ini' => $fechaIni,
            'longitud' => $lat,
            'latitud' => $lng,
            ]);

        $ver_visita = $visita->toArray();

        return view('visitas.registra_visita', compact('ver_visita','nom_tipo_visita','nom_pv'));

    }


    public function cerrar_visita(Request $request){
        $id_visita =  $request->input('idvisita');

        $validaEncuesta = EncuestaVisitas::with('visita', 'encuesta')
                                           ->where('visita_id',$id_visita)
                                           ->where('completada','N')
										   ->where('estado','A')
                                           ->first();
										   

        if(!empty($validaEncuesta)){
            $idpdv = $validaEncuesta->visita->idpdv;
			$encuesta = $validaEncuesta->encuesta->nombre_encuesta;
			$pregunta = $validaEncuesta->pregunta_actual;
			$fecha = $validaEncuesta->created_at;
			
			$traza = 'EnV'.$validaEncuesta->id.
				 'ViD'.$validaEncuesta->visita_id.
				 'EnI'.$validaEncuesta->encuesta_id.
				 'V0'.$validaEncuesta->version_encuesta.
				 'C-'.$validaEncuesta->completada.
				 'PAc'.$validaEncuesta->pregunta_actual.
				 'PAn'.$validaEncuesta->pregunta_anterior.
				 'dD'.$validaEncuesta->documento_digital.
				 'St-'.$validaEncuesta->status;
				 
            //return view('visitas.pendientes', compact('idpdv','encuesta','pregunta','fecha', 'traza'));
        }

        $visita = Visits::find($id_visita);
        $visita->activa = 'N';
        $visita->fecha_fin = Carbon::now();
        $visita->save();

        return redirect()->route('gestion.buscar.punto')
                             ->with('success','Visita Cerrada Con Exito!!!');

    }

}
