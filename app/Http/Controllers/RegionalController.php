<?php

namespace App\Http\Controllers;

Use App\Models\Country;
Use App\Models\Regional;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegionalController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_REGIONALES', ['only' => ['index']]);
      $this->middleware('permission:VER_REGIONALES', ['only' => ['show']]);
      $this->middleware('permission:CREAR_REGIONALES', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_REGIONALES', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_REGIONALES', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $regionales = Regional::with('country')->where('status','A')->orderBy('regional_name')->latest()->paginate(5);

        return view('regionals.index',compact('regionales'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $paises = Country::select('id','country_name')->Activos()->toArray();
        return view('regionals.create',compact('paises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'country_id' => ['required', 'not_in:0','integer'],
            'regional_name' => ['required', 'max:255'],
        ]);

        $country_id = $request->country_id;
        $regional_name = strtoupper($request->regional_name);

        $valida_regional = Regional::where('country_id', $country_id)
                                    ->where('regional_name', $regional_name)
                                    ->get();

        if(count($valida_regional) > 0){
            return redirect()->route('regionales.index')
                              ->with('error',"Regional: $regional_name, ya existe en el pais");
        }
        Regional::create(['regional_name' => $regional_name, 'country_id' => $country_id]);

        return redirect()->route('regionales.index')
                        ->with('success','Canal Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
                //
        $regional = Regional::with('country')->find($id);
        return view('regionals.show',compact('regional'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $regional = Regional::with('country')->find($id);
        $paises = Country::select('id','country_name')->Activos()->toArray();
        return view('regionals.edit',compact('regional', 'paises'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'regional_name' => ['required', 'max:255'],
        ]);

        $country_id = $request->country_id;
        $regional_name = strtoupper($request->regional_name);

        $regional = Regional::find($id);


        if($regional->regional_name == $regional_name){
            return redirect()->route('regionales.index')
                              ->with('success',"Regional: $regional_name, sin cambios...");
        }

        $regional->update(['regional_name' => $regional_name]);

        return redirect()->route('regionales.index')
                        ->with('success','Canal Actualizado Exitosamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $regional = Regional::find($id);
        $regional->status = 'I';
        $regional->save();

        return redirect()->route('regionales.index')
                        ->with('success','Regional Eliminado Exitosamente');
    }
}
