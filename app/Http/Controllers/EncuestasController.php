<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Storage;

Use App\User;
Use App\Models\Employee;
use App\Models\EncuestaBase;
use App\Models\EncuestaOpciones;
use App\Models\EncuestaPreguntas;
use App\Models\EncuestaDocumentos;
use App\Models\EncuestaDocumentoGeo;
use App\Models\EncuestaTipo;
use App\Models\EncuestaRespuestas;
use App\Models\EncuestaTipoPregunta;
use App\Models\EncuestaGrupoPreguntas;
use App\Models\EncuestaVisitas;
use App\Models\EncuestaImagen;
use App\Models\Visits;



class EncuestasController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function formularios_encuestas($visita, $formulario){

        switch ($formulario) {
            case 1:
                return view('encuestas.forms.plan_accion', compact('visita'));
                break;
            case 2:
                return view('encuestas.forms.ventas_cantadas', compact('visita'));
                break;
            case 3:
                return view('encuestas.forms.hallazgos', compact('visita'));
                break;
            case 4:
                return view('encuestas.forms.ams', compact('visita'));
                break;
            default:
            return view('encuestas.forms.no_form');
                break;
        }

    }

    public function encuestas_oficiales($id_encuesta, $version, $idpdv, $doc_acomp = 0){

        $id_usuario = Auth::id();

        $id_visita = Visits::where('user_id',$id_usuario)
                            ->where('idpdv',$idpdv)
                            ->where('activa','S')
                            ->get()
                            ->last();


		//Validamos si es la encuesta de cierre y la ejecutamos de una
		//Si hay alguna encuesta pendiente, no se ejecuta
		if($id_encuesta == 1){
			$validaEncuesta = EncuestaVisitas::with('visita', 'encuesta')
                                           ->where('visita_id',$id_visita->id)
                                           ->where('completada','N')
										   ->where('estado','A')
                                           ->first();


			if(!empty($validaEncuesta)){
				$c_idpdv = $validaEncuesta->visita->idpdv;
				$c_encuesta = $validaEncuesta->encuesta->nombre_encuesta;
				$c_pregunta = $validaEncuesta->pregunta_actual;
				$c_fecha = $validaEncuesta->created_at;

				$traza = 'EnV'.$validaEncuesta->id.
				 'ViD'.$validaEncuesta->visita_id.
				 'EnI'.$validaEncuesta->encuesta_id.
				 'V0'.$validaEncuesta->version_encuesta.
				 'C-'.$validaEncuesta->completada.
				 'PAc'.$validaEncuesta->pregunta_actual.
				 'PAn'.$validaEncuesta->pregunta_anterior.
				 'dD'.$validaEncuesta->documento_digital.
				 'St-'.$validaEncuesta->status;

				return view('visitas.pendientes', [
													'idpdv'=>$c_idpdv,
													'encuesta'=>$c_encuesta,
													'pregunta'=>$c_pregunta,
													'fecha'=>$c_fecha,
													'traza' => $traza
												]);
			}
		}

		//validamos si se intenta ejecutar un acompañamiento sin un documento
		if(in_array($id_encuesta, [6,7]) AND $doc_acomp == 0){
			$mensaje = 'Debe Iniciar un acompañamiento antes de realizar las encuestas en campo';
			return view('encuestas.error_encuesta', ['idpdv' => $idpdv, 'mensaje' => strtoupper($mensaje)]);
		}




        $validar_encuesta = EncuestaVisitas::with('visita')->where('visita_id',$id_visita->id)
                                                            ->where('encuesta_id',$id_encuesta)
															->where('version_encuesta', $version)
                                                            ->where('completada','N')
                                                            ->first();

        if(!empty($validar_encuesta)){
            $datosEncuesta = [
                'idEncuestaVisita' => $validar_encuesta['id'],
                'idEncuesta' => $id_encuesta,
				'version' => $version,
                'encuestaCompletada' =>$validar_encuesta['completada'],
                'preguntaActual' =>$validar_encuesta['pregunta_actual'],
                'preguntaAnterior' => $validar_encuesta['pregunta_anterior'],
				'documentoDigital' => $validar_encuesta['documento_digital'],
            ];
        }else{
            // Trae el ID de Visita activo del usuario en el punto de venta
            // guarda el registro de visita y encuesta

            $id_encuesta_visita = new EncuestaVisitas();

            $id_encuesta_visita->visita_id = $id_visita->id;
            $id_encuesta_visita->encuesta_id = $id_encuesta;
			$id_encuesta_visita->version_encuesta = $version;
            $id_encuesta_visita->completada = 'N';
            $id_encuesta_visita->pregunta_actual = 1;
            $id_encuesta_visita->pregunta_anterior = 0;
			$id_encuesta_visita->documento_digital = $doc_acomp;

            $id_encuesta_visita->save();

            $datosEncuesta = [
                                'idEncuestaVisita' => $id_encuesta_visita->id,
                                'idEncuesta' => $id_encuesta,
								'version' => $version,
                                'encuestaCompletada' =>$id_encuesta_visita->completada,
                                'preguntaActual' =>$id_encuesta_visita->pregunta_actual,
                                'preguntaAnterior' => $id_encuesta_visita->pregunta_anterior,
								'documentoDigital' => $id_encuesta_visita->documento_digital,
                            ];

        }

        $var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'EncuestasController@EncuestasOficiales()';
		$var_log_datos['valores'] = ['punto_venta' => $idpdv, 'Encuesta' => $id_encuesta];
		$var_log_mensaje = 'Usuario Ejecuta Encuesta en Punto de Venta';
		Log::notice($var_log_mensaje,$var_log_datos);

		return redirect()->route('encuestas.preguntas.constructor',
                                    [
                                        'encuesta_visita' => $datosEncuesta['idEncuestaVisita'],
                                        'encuesta' => $datosEncuesta['idEncuesta'],
										'version' => $datosEncuesta['version'],
                                        'completada' => $datosEncuesta['encuestaCompletada'],
                                        'pregunta' => $datosEncuesta['preguntaActual'],
                                        'anterior' => $datosEncuesta['preguntaAnterior'],
										'doc_acomp' => $datosEncuesta['documentoDigital'],
                                    ]
                                );
    }

    public function encuestasPreguntasConstructor($encuesta_visita,
                                                    $encuesta,
                                                    $version,
                                                    $completada,
                                                    $pregunta,
                                                    $anterior,
                                                    $doc_acomp = 0,
                                                    $atras = 'F',
                                                    $id_opc_resp = 0,
                                                    $txt_opc_resp = '_',
                                                    $txt_resp = '_',
                                                    $calif_resp = 0){

        $datosEncuesta['idEncuestaVisita'] = $encuesta_visita;
        $datosEncuesta['idEncuesta'] = $encuesta;
		$datosEncuesta['version'] = $version;
        $datosEncuesta['encuestaCompletada'] = $completada;
        $datosEncuesta['preguntaActual'] = $pregunta;
        $datosEncuesta['preguntaAnterior'] = $anterior;
		$datosEncuesta['documentoDigital'] = $doc_acomp;
        $datosEncuesta['atras'] = $atras;

        $datosEncuesta['id_opc_resp'] = $id_opc_resp;
        $datosEncuesta['txt_opc_resp'] = $txt_opc_resp;
        $datosEncuesta['txt_resp'] = $txt_resp;
        $datosEncuesta['calif_resp'] = $calif_resp;

        $encuestaVisita = EncuestaVisitas::with('visita')->where('id',$encuesta_visita)
                                      ->first()
                                      ->toArray();


        $idpdv = $encuestaVisita['visita']['idpdv'];

        if($encuestaVisita['completada'] == 'S'){
            //ENCUESTA TERMINADA
            return view('encuestas.finalizada',compact('idpdv'));
        }


        $encuesta = EncuestaBase::where('id',$datosEncuesta['idEncuesta'])
								  ->where('version', $datosEncuesta['version'])
                                  ->first()
                                  ->toArray();

        $preguntas = EncuestaPreguntas::with('tipo','grupo')->where('encuesta_id',$datosEncuesta['idEncuesta'])
                                                    ->where('version_encuesta',$datosEncuesta['version'])
													->where('orden_pregunta',$datosEncuesta['preguntaActual'])
                                                    ->first()
                                                    ->toArray();


        $opciones = EncuestaOpciones::where('pregunta_id', $preguntas['id'])
                                      ->get()
                                      ->toArray();


        //Validamos el tipo de pregunta para construir el HTML
        $opcion_html = $this->tipo_pregunta($preguntas['tipo_pregunta_id']);

        //dd($opcion_html, $preguntas, $preguntas, $opciones, $encuesta, $datosEncuesta, $idpdv); //

        return view('encuestas.vistas.preguntas',compact('opcion_html','preguntas','opciones','encuesta','datosEncuesta', 'idpdv'));
    }


    public function encuestas_guardar(Request $request){

        $txt_opcion_respuesta = '';
        $texto_respuesta = '';
        $idOpcion = $request->idOpcion;
        $calificacion = $request->calificacion;
        $pregunta_siguiente = 0;
        $pregunta_anterior = $request->preguntaActual;
		$doc_acomp = $request->documentoDigital;
        $fin_encuesta = 'N';

        $requerida = $request->requeridaPregunta == 'S'?true:false;

        switch ($request->tipoPregunta) {
            case 1: //Texto Corto es <input type='text'>
                if($request->pregunta_encuesta == NULL AND $requerida){
                    return back()->withInput()->with('error','Debe Ingresar una respuesta');
                }
                if(strlen($request->pregunta_encuesta) > $request->maxPregunta){
                    return back()->withInput()->with('error','Longitud de la respuesta excede '.$request->maxPregunta.' caracteres');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper(trim($request->pregunta_encuesta," \t\n\r"));
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 2: //Texto largo es <textarea>
                if($request->pregunta_encuesta == NULL AND $requerida){
                    return back()->withInput()->with('error','Debe Ingresar una respuesta');
                }
                if(strlen($request->pregunta_encuesta) > $request->maxPregunta){
                    return back()->withInput()->with('error','Longitud de la respuesta excede '.$request->maxPregunta.' caracteres');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = strtoupper($this->sanear_string($request->pregunta_encuesta));
                $texto_respuesta = strtoupper($this->sanear_string($request->pregunta_encuesta));
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 3: //Numero es <input type='number'>
                if($request->pregunta_encuesta == NULL AND $requerida){
                    return back()->withInput()->with('error','Debe Ingresar una respuesta');
                }
                if(strlen($request->pregunta_encuesta) > $request->maxPregunta){
                    return back()->withInput()->with('error','Longitud de la respuesta excede '.$request->maxPregunta.' caracteres');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();

                $txt_opcion_respuesta = strtoupper(preg_replace('([^A-Za-z0-9 ])', '', trim($request->pregunta_encuesta)));
                $texto_respuesta = strtoupper(preg_replace('([^A-Za-z0-9 ])', '', trim($request->pregunta_encuesta)));
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

                case 4: //SeleccionUnica es <select>
                if($request->pregunta_encuesta == 0){  // AND $requerida
                    return back()->withInput()->with('error','Debe seleccionar una opcion');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper($txt_opcion_respuesta);
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 5: //SeleccionMultiple es <select multiple>
                if(!$request->has('pregunta_encuesta') ){  //AND $requerida
                    return back()->withInput()->with('error','Debe seleccionar una o varias opciones');
                }

                $cadaIdOpcion = explode('|',$request->idOpcion);
                $txt_opcion_respuesta = '';
                $texto_respuesta = '';

                foreach ($cadaIdOpcion as $unaOpcion) {
                    $tmpOpcionRespTxt = EncuestaOpciones::where('id',$unaOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();

                    $txt_opcion_respuesta .= $tmpOpcionRespTxt->texto_opcion.'|';
                    $texto_respuesta = strtoupper($txt_opcion_respuesta).'|';
                    $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;

                }

                $txt_opcion_respuesta = substr($txt_opcion_respuesta, 0, -1);
                $texto_respuesta = substr($texto_respuesta, 0, -1);

                $idOpcion = null;

                break;

            case 6: //SeleccionSi_No es <select>
                if($request->pregunta_encuesta == 0 ){  //AND $requerida
                    return back()->withInput()->with('error','Debe seleccionar una opcion');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                      ->select('texto_opcion','siguiente_pregunta')
                                                      ->first();
                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper($txt_opcion_respuesta);
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 7: //Cierre <button>
                $fin_encuesta = 'S';
                $txt_opcion_respuesta = '_FIN_';
                $texto_respuesta = '_FIN_';
                break;

            case 8: //Seleccion Si_No_NA es <select>
                if($request->pregunta_encuesta == 0 ){  //AND $requerida
                    return back()->withInput()->with('error','Debe seleccionar una opcion');
                }

                $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                        ->select('texto_opcion','siguiente_pregunta')
                                                        ->first();

                $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                $texto_respuesta = strtoupper($txt_opcion_respuesta);
                $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                break;

            case 9: //Cargar Imagen es <input type='text'>
                    if($request->pregunta_encuesta == NULL AND $requerida){
                        return back()->withInput()->with('error','Debe Ingresar una respuesta');
                    }

                    $request->validate([
                        'imagen' => ['max:10000'],
                    ],[
                        'imagen.max' => 'La imagen debe ser de maximo 10Mb',
                    ]);

                    $foto_nombre = strtoupper($request->pregunta_encuesta);

                    if(isset($request->imagen)){

                        $extension = $request->file('imagen')->extension();
                        if(!(strpos('jpg,jpeg,png', $extension) !== false)){
                            return back()->withInput()->with('error','El tipo de imagen debe ser JPG/PNG/JPEG');
                        }

                        $nombre_foto = $foto_nombre.'.'.$extension;

                        $valida_fotos = EncuestaImagen::where('nombre_foto', 'LIKE', "$foto_nombre%")->first();
                        //dd($foto_nombre,$valida_fotos);
                        if($valida_fotos == null){
                            $fotos_encuesta = new EncuestaImagen;
                            $fotos_encuesta->encuesta_visita_id = $request->idEncuestaVisita;

                        }else{
                            $fotos_encuesta = $valida_fotos;
                            $fotos_encuesta->status = 'A';
                            Storage::disk('tracking')->delete($fotos_encuesta->nombre_foto);
                        }
                        $fotos_encuesta->nombre_foto = $nombre_foto;

                        $image_resize = Image::make($request->file('imagen')->getRealPath());

                        $image_resize->resize(800, null, function($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        $image_resize->orientate();

                        $image_resize->save(storage_path("app/public/tracking/" . $nombre_foto),60);

                        $path = Storage::disk('tracking')->url($nombre_foto);

                        $fotos_encuesta->url = $path;
                        $fotos_encuesta->save();

                    }else{
                        $valida_fotos = EncuestaImagen::where('nombre_foto', 'LIKE', "$foto_nombre%")->first();
                        if($valida_fotos != null){
                            $fotos_encuesta = $valida_fotos;
                            $fotos_encuesta->status = 'I';
                            $fotos_encuesta->url = '';
                            Storage::disk('tracking')->delete($fotos_encuesta->nombre_foto);
                            $fotos_encuesta->save();
                        }
                    }



                    $tmpOpcionRespTxt = EncuestaOpciones::where('id',$idOpcion)
                                                          ->select('texto_opcion','siguiente_pregunta')
                                                          ->first();

                    $txt_opcion_respuesta = $tmpOpcionRespTxt->texto_opcion;
                    $texto_respuesta = strtoupper(trim($request->pregunta_encuesta," \t\n\r"));
                    $pregunta_siguiente = $tmpOpcionRespTxt->siguiente_pregunta;
                    break;
        }

        $id_encuesta_visita = $request->idEncuestaVisita;
        $id_pregunta = $request->idPregunta;
        $id_opcion_respuesta = $idOpcion;

        //GUARDAR RESPUESTA VISITA
        $respuesta_visita = new EncuestaRespuestas;

        $respuesta_visita->encuesta_visita_id = $id_encuesta_visita;
        $respuesta_visita->pregunta_id = $id_pregunta;
        $respuesta_visita->opcion_respuesta_id = $id_opcion_respuesta;
        $respuesta_visita->opcion_respuesta_txt = $txt_opcion_respuesta;
        $respuesta_visita->texto_respuesta = $texto_respuesta;
        $respuesta_visita->calificacion = $calificacion;

        $respuesta_visita->save();

        /* *************************************
            SALVAMOS LA ENCUESTA
        ************************************** */
        if($fin_encuesta == 'S'){
            $encuesta_visita = EncuestaVisitas::with('visita')->where('id',$request->idEncuestaVisita)
                                                ->first();


			$idpdv = $encuesta_visita->visita->idpdv;
            $encuesta_visita->completada = 'S';
            $encuesta_visita->pregunta_actual = $pregunta_siguiente;
            $encuesta_visita->pregunta_anterior = $pregunta_anterior;
            $encuesta_visita->save();

			$f_encuesta = $encuesta_visita->encuesta_id;
			$enc_pend = EncuestaVisitas::where('visita_id',$encuesta_visita->visita_id)->where('completada','N')->get();
			$tengo_encuestas_pendientes = (count($enc_pend) > 0) ? true : false;


			//Si es la encuesta de Cierre la redirigimos a cerrar la visita
			//tener en cuenta que no hace falta ninguna otra encuesta

			if($encuesta_visita->encuesta_id == 1 AND !$tengo_encuestas_pendientes){
				$visita = Visits::find($encuesta_visita->visita->id);
				$visita->activa = 'N';
				$visita->fecha_fin = Carbon::now();
				$visita->save();

				return redirect()->route('gestion.buscar.punto')
                             ->with('success','Visita Cerrada Con Exito!!!');
			}

			//Si es la encuesta Check de salida a ruta cerrar el documento
			if(in_array($encuesta_visita->encuesta_id, [4])){

				$acompañamiento = EncuestaDocumentos::where('id',$doc_acomp)->first();
				$acompañamiento->activa = 'N';
				$acompañamiento->save();
            }

            //Si es la encuesta Tracking Competencia
			if(in_array($encuesta_visita->encuesta_id, [11])){

				$acompañamiento = EncuestaDocumentoGeo::where('id',$doc_acomp)->first();
				$acompañamiento->activa = 'N';
				$acompañamiento->save();
			}

            //Solo para check de salida a ruta
			if($encuesta_visita->encuesta_id == 4){
				return redirect()->route('home')->with('success','Check Salida Ruta, Diligenciado Correctamente!!!!');
			}

			return view('encuestas.finalizada',compact('idpdv','f_encuesta'));


        }else{  // SI AUN NO ES EL FIN DE LA ENCUESTA
            $encuesta_visita = EncuestaVisitas::with('visita')->where('id',$request->idEncuestaVisita)
                                                ->first();
            $encuesta_visita->pregunta_actual = $pregunta_siguiente;
            $encuesta_visita->pregunta_anterior = $pregunta_anterior;
            $encuesta_visita->save();
        }
        /* *****************
         FIN SALVAR ENCUESTA
        ****************** */


        return redirect()->route('encuestas.preguntas.constructor',
                                    [
                                        'encuesta_visita' => $request->idEncuestaVisita,
                                        'encuesta' => $request->idEncuesta,
										'version' => $request->version,
                                        'completada' => $fin_encuesta,
                                        'pregunta' => $pregunta_siguiente,
                                        'anterior' => $pregunta_anterior,
										'doc_acomp' => $doc_acomp
                                    ]
                                );

    }


    public function encuestas_regresar(Request $request){

		$doc_acomp = $request->documentoDigital;

		$actual =  EncuestaRespuestas::where('encuesta_visita_id',$request->idEncuestaVisita)
                                        ->latest()
                                        ->take(2)
                                        ->orderBY('pregunta_id','ASC')
                                        ->get()
                                        ->toArray();


        $id_opc_resp = is_null($actual[0]['opcion_respuesta_id'])? 0 : $actual[0]['opcion_respuesta_id'];
        $txt_opc_resp = $actual[0]['opcion_respuesta_txt'];
        $txt_resp = $actual[0]['texto_respuesta'];
        $calif_resp = $actual[0]['calificacion'];

        $borrar = EncuestaRespuestas::where('encuesta_visita_id',$request->idEncuestaVisita)
                                      ->where('id', '>=', $actual[0]['id'] )
                                      ->get();


        $NumPreguntaActual = encuestaPreguntas::where('id',$actual[0]['pregunta_id'])
                                                ->first('orden_pregunta')
                                                ->toArray();

        if(count($actual) > 1){
            $NumPreguntaAnterior = encuestaPreguntas::where('id',$actual[1]['pregunta_id'])
                                                    ->first('orden_pregunta', 'texto_pregunta')
                                                    ->toArray();
        }else{
            $NumPreguntaAnterior['orden_pregunta'] = 0;
        }

        if(!empty($borrar)){
            foreach($borrar as $item_borrar){
                $borra = EncuestaRespuestas::find($item_borrar->id);
                $borra->delete();
            }
        }

        $encuestaVisita = EncuestaVisitas::where('id',$request->idEncuestaVisita)
                                         ->where('completada','N')
                                         ->first();

        $encuestaVisita->pregunta_actual = $NumPreguntaActual['orden_pregunta'];
        $encuestaVisita->pregunta_anterior = $NumPreguntaAnterior['orden_pregunta'];
        $encuestaVisita->save();

        return redirect()->route('encuestas.preguntas.constructor',
                                    [
                                        'encuesta_visita' => $request->idEncuestaVisita,
                                        'encuesta' => $encuestaVisita->encuesta_id,
										'version' => $encuestaVisita->version_encuesta,
                                        'completada' => $encuestaVisita->completada,
                                        'pregunta' => $NumPreguntaActual['orden_pregunta'],
                                        'anterior' => $NumPreguntaAnterior['orden_pregunta'],
										'doc_acomp' => $doc_acomp,
                                        'atras' => 'T',
                                        'id_opc_resp' => $id_opc_resp,
                                        'txt_opc_resp' => $txt_opc_resp,
                                        'txt_resp' => $txt_resp,
                                        'calif_resp' => $calif_resp,
                                    ]
                                );
    }

    private function html_textoCorto(){
        return 'encuestas.vistas.html_textoCorto';
    }

    private function html_textoLargo(){
        return 'encuestas.vistas.html_textoLargo';
    }

    private function html_Numero(){
        return 'encuestas.vistas.html_Numero';
    }

    private function html_selectSimple(){
        return 'encuestas.vistas.html_selectSimple';
    }

    private function html_selectMultiple(){
        return 'encuestas.vistas.html_selectMultiple';
    }

    private function html_selectBinario(){
        return 'encuestas.vistas.html_selectBinario';
    }

    private function html_selectBinarioNA(){
        return 'encuestas.vistas.html_selectBinarioNA';
    }

    private function html_uploadFile(){
        return 'encuestas.vistas.html_uploadFile';
    }

    private function html_uploadImage(){
        return 'encuestas.vistas.html_uploadImage';
    }

    private function html_buttonClose(){
        return 'encuestas.vistas.html_buttonClose';
    }

    private function tipo_pregunta($tipo_pregunta){
        switch ($tipo_pregunta) {
            case 1: //Texto Corto es <input type='text'>
                $opcion = $this->html_textoCorto();
                break;
            case 2: //Texto largo es <textarea>
                $opcion = $this->html_textoLargo();
                break;
            case 3: //Numero es <input type='number'>
                $opcion = $this->html_Numero();
                break;
            case 4: //SeleccionUnica es <select>
                $opcion = $this->html_selectSimple();
                break;
            case 5: //SeleccionMultiple es <select multiple>
                $opcion = $this->html_selectMultiple();
                break;
            case 6: //SeleccionSi_No es <select>
                $opcion = $this->html_selectBinario();
                break;
            case 7: //Cierre <button>
                $opcion = $this->html_buttonClose();
                break;
            case 8: //Seleccion Si_No_N/A <select>
                $opcion = $this->html_selectBinarioNA();
                break;
            case 9: //Upload Images <input>
                $opcion = $this->html_uploadImage();
                break;
            case 10: //Upload File <input>
                $opcion = $this->html_uploadFile();
                break;
        }

        return $opcion;

    }



	public function ver_check_ruta(){

		$usuario = Auth::user();
		$usuario = $usuario->toArray();
		$ver_enc = EncuestaBase::where('id',4)->first()->toArray();

		$tiene_checks = EncuestaDocumentos::where('encuesta_id',4)
										  ->where('version_encuesta',$ver_enc['version'])
										  ->where('activa','S')
										  ->where('id_user_audit',$usuario['id'])
										  ->first();

		if($tiene_checks){
			$empleado = Employee::where('document',$tiene_checks->cc_watched)->first()->toArray();
			return view('encuestas.documentos.tiene_doc_pendiente',compact('tiene_checks','ver_enc', 'empleado'));
		}else{
			return view('encuestas.documentos.check_ruta',compact('usuario'));
		}

		//dd($ver_enc, $usuario, $tiene_checks, date('HisdmY'));
	}

	public function crear_check_ruta(Request $request){
		$usr_chequea = $request->usr_auth;
		$usr_watched = $request->usr_watch;
		$idpdv = $request->idpdv;
		$id_encuesta = $request->id_encuesta; //4 Check salida a ruta 5 Coaching
		$version = $request->version;

		$tipo_visita = $request->tipo_visita; //2 COACHING

		$documento = EncuestaDocumentos::create([
            'encuesta_id' => $id_encuesta,
			'version_encuesta' => $version,
			'id_user_audit' => $usr_chequea,
			'id_user_watched' => $usr_watched,
			'cc_watched' => $idpdv,
            ]);


		if((int)$id_encuesta != 4) //4-COACHING validar para que sean las de Oficina
		{
			return redirect()->route('gestion.buscar.punto');
		}else{
			$visita = Visits::create([
				'user_id' => $usr_chequea,
				'visit_type_id' => $tipo_visita,
				'idpdv' => $idpdv,
				'planeada' => 'N',
				'fecha_ini' => date('Y-m-d'),
				'longitud' => 0,
				'latitud' => 0,
				]);
		}

		return redirect()->route('encuestas.oficiales',['id_encuesta' => $id_encuesta, 'version' => $version, 'idpdv' => $idpdv, 'doc_acomp' => $documento->id]);

	}

	public function ver_coachings(){

		$usuario = Auth::user();
		$usuario = $usuario->toArray();
		$ver_enc = EncuestaBase::where('id',6)->orWhere('id',7)->first()->toArray();

		$tiene_checks = EncuestaDocumentos::where('activa','S')
										  ->where('id_user_audit',$usuario['id'])
										  ->where(function($q) {
											$q->where('encuesta_id',6)
											  ->orWhere('encuesta_id',7);
											})
										  ->first();

		if($tiene_checks){
			$empleado = Employee::where('document',$tiene_checks->cc_watched)->first()->toArray();
			return view('encuestas.documentos.tiene_doc_pendiente',compact('tiene_checks','ver_enc', 'empleado'));
		}else{
			return view('encuestas.documentos.ver_coachings',compact('usuario'));
		}

		//dd($ver_enc, $usuario, $tiene_checks, date('HisdmY'));

		//return view('encuestas.documentos.ver_coachings',compact('usuario'));


	}



	public function finalizar_coachings($id){

		$acompañamiento = EncuestaDocumentos::where('id',$id)->first();

		$acompañamiento->activa = 'N';
		$acompañamiento->save();


		// LA enuesta de Check de visita inactiva a Encuesta Visita
		if($acompañamiento->encuesta_id == 4){
			$encuestaVisita = EncuestaVisitas::where('documento_digital', $id)->first();
			$encuestaVisita->estado = 'I';
			$encuestaVisita->save();
		}

		// LA enuesta de acompañamiento Cierra la encuesta visita
		if(in_array($acompañamiento->encuesta_id, [6, 7])){
			$encuestaVisita = EncuestaVisitas::where('documento_digital', $id)->first();
			if(!empty($encuestaVisita)){
				$encuestaVisita->completada = 'S';
				$encuestaVisita->save();
			}
		}

		return redirect()->route('home')->with('success','Acompañamiento Cerrado/Finalizado Exitosamente!!!');;

    }



    public function iniciar_tracking(){

		$usuario = Auth::user();
		$usuario = $usuario->toArray();
		$ver_enc = EncuestaBase::where('id',11)->first()->toArray();

		$tiene_trackings = EncuestaDocumentoGeo::where('encuesta_id',11)
										  ->where('version_encuesta',$ver_enc['version'])
										  ->where('activa','S')
										  ->where('id_user_audit',$usuario['id'])
                                          ->first();

        //dd($tiene_trackings);

		if($tiene_trackings){
			return view('encuestas.documentos.tiene_doc_geo_pendiente',compact('tiene_trackings','ver_enc'));
		}else{
			return view('encuestas.documentos.tracking_competencia',compact('usuario','ver_enc'));
		}

		//dd($ver_enc, $usuario, $tiene_checks, date('HisdmY'));
    }


    public function crear_tracking_competencia(Request $request){
		$usr_chequea = $request->usr_auth;
		$id_encuesta = $request->id_encuesta;
		$version = $request->version;
		$tipo_visita = $request->tipo_visita; //11 Check salida a ruta 5 Coaching
		$idpdv = $request->idpdv;
        $regional = $request->regional; //2 COACHING
        $departamento = $request->departamento; //2 COACHING
        $ciudad = $request->ciudad; //2 COACHING

		$documento = EncuestaDocumentoGeo::create([
            'encuesta_id' => $id_encuesta,
			'version_encuesta' => $version,
			'id_user_audit' => $usr_chequea,
			'regional' => $regional,
            'departamento' => $departamento,
            'ciudad' => $ciudad,
            ]);

        $visita = Visits::create([
            'user_id' => $usr_chequea,
            'visit_type_id' => $tipo_visita,
            'idpdv' => $idpdv,
            'planeada' => 'N',
            'fecha_ini' => date('Y-m-d'),
            'longitud' => 0,
            'latitud' => 0,
            ]);


		return redirect()->route('encuestas.oficiales',['id_encuesta' => $id_encuesta, 'version' => $version, 'idpdv' => $idpdv, 'doc_acomp' => $documento->id]);

	}

    /**
     * Reemplaza todos los acentos por sus equivalentes sin ellos
     *
     * @param $string
     *  string la cadena a sanear
     *
     * @return $string
     *  string saneada
     */
    private function sanear_string($string)
    {

        $string = trim($string);

        $string = str_replace(
            array('\t', '\n', '\r'),
            ' ',
            $string
        );

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "-", "~",
                "#", "@", "|", "!", "\"",
                "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "<code>", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ":"),
            '',
            $string
        );


        return $string;
    }

}
