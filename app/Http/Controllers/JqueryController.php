<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


use App\Models\Country;
use App\Models\Regional;
use App\Models\Territory;
use App\Models\Employee;
use App\Models\Infopos_CircuitosDms;
use App\Models\TradeObjetivo;
use App\Models\empleado_role;
use App\Models\ReporteDiego;
use App\Models\InfoposPuntosCve;



class JqueryController extends Controller
{

	public function ajaxPaisRegiones($id){

		$var_pais = Country::find($id);

	  if($var_pais == Null){
	    $mensaje = 'PAIS NO HALLADO';
			$estado = false;

			$regiones = ['id' => 0, 'regional_name' => '--SIN TERRITORIOS--'];

	    return response()->json(['mensaje' => $mensaje, 'estado' => $estado, 'regiones' => $regiones]);
		}

		$pais = $var_pais->toArray();
		$var_regiones = Regional::select('id','regional_name')->DeUnPais($pais['id'])->toArray();
		$mensaje = 'RESULTADO DE REGIONES PARA PAIS';
	  $estado = true;
	  $var_regiones = Arr::prepend($var_regiones, ['id' => 0, 'regional_name' => '--SELECCIONE REGION--']);
	  $regiones = $var_regiones;
	  return response()->json(['mensaje' => $mensaje, 'estado' => $estado, 'regiones' => $regiones]);

  }



    public function ajaxRegionTerritorios($id){

		$var_regional = Regional::find($id);

	    if($var_regional == Null){
	    	$mensaje = 'REGIONAL NO HALLADA';
			$estado = false;
			$territorios = [];
			//$territorios = Arr::prepend($territorios, ['id' => 0, 'territory_name' => '--SIN TERRITORIO--']);

			return response()->json(['mensaje' => $mensaje, 'estado' => $estado, 'territorios' => $territorios]);
		}


		$regional = $var_regional->toArray();
		$var_territorios = Territory::select('id','territory_name')->DeUnaRegional($regional['id'])->toArray();
		$mensaje = 'RESULTADO DE TERRITORIOS PARA REGIONAL';
	    $estado = true;
	    $var_territorios = Arr::prepend($var_territorios, ['id' => 0, 'territory_name' => '--SELECCIONE TERRITORIO--']);
	    $territorios = $var_territorios;
	    return response()->json(['mensaje' => $mensaje, 'estado' => $estado, 'territorios' => $territorios]);

    }


	public function ajaxCedulaEmpleado($cedula){

			$empleado = Employee::with('channels','position','territory','territory.regional')->where('document',$cedula)->first();

			if($empleado == Null){
				$datos['nombre'] = 'N/A';
				$datos['apellido'] = 'N/A';
				$datos['regional'] = 'N/A';
				$datos['canal'] = 'N/A';
				$datos['cargo'] = 'N/A';
				$datos['id_usuario'] = 'N/A';
				$datos['id_empleado'] = 'N/A';
				$datos['id_regional'] = 'N/A';
				$datos['id_canal'] = 'N/A';

				$datos['id_role'] = 'N/A';
				$datos['role'] = 'N/A';

				$estado = false;
				$mensaje = 'NO HAY DATOS';

				return response()->json([
					'estado' => $estado,
					'mensaje' => $mensaje,
					'datos' => $datos
				]);
			}

			$canales = $empleado->channels->first()->toArray();

			$idrole = empleado_role::where('id_empleado',$empleado->id)->first();
			$role_query = "SELECT * FROM roles WHERE id = $idrole->id";
			$role = DB::select($role_query);

			$datos['nombre'] = $empleado->name;
			$datos['apellido'] = $empleado->last_name;
			$datos['regional'] = $empleado->territory->regional->regional_name;
			$datos['canal'] = $canales['channel_name'];
			$datos['cargo'] = $empleado->position->position_name;
			$datos['id_usuario'] = $empleado->user_id;
			$datos['id_empleado'] = $empleado->id;
			$datos['id_regional'] = $empleado->territory->regional->id;
			$datos['id_canal'] = $canales['id'];

			$datos['id_role'] = $idrole->id;
			$datos['role'] = $role[0]->name;

			$estado = true;
			$mensaje = 'EMPLEADO ENCONTRADO EXITOSAMENTE';

			return response()->json([
				'estado' => $estado,
				'mensaje' => $mensaje,
				'datos' => $datos
			]);

        }


        public function ajaxAutocompleteCircuits(Request $request){

            $texto = $request->get('term');
            $resultado = Infopos_CircuitosDms::where('circuito','like','%'.$texto.'%')
                                             ->orderBy('circuito','ASC')
                                             ->get('circuito')->toArray();

            $resultado = Arr::prepend($resultado,['circuito' => 'Circuitos...'] );
            return response()->json($resultado);

      }


      public function ajaxCercanosPuntos($lat, $lng){

        $consulta =
        "SELECT idpdv, nombre_punto, direccion, tel_propietario, lat, lng,
        (acos(sin(radians($lat)) * sin(radians(PUNTO.lat)) +
        cos(radians($lat)) * cos(radians(PUNTO.lat)) *
        cos(radians($lng) - radians(PUNTO.lng))) * 6378) AS distance
        FROM
        api_puntos_cercanos PUNTO
        HAVING DISTANCE < 1.0
        ORDER BY distance ASC";

        $pv_cercanos = DB::select($consulta);

        $estado = true;
		$mensaje = 'INFORMACION DESCARGADA';

				return response()->json([
					'estado' => $estado,
					'mensaje' => $mensaje,
					'datos' => $pv_cercanos
				]);
    }


    public function ajaxTradeObjetivos($fuente){

        $resultado = TradeObjetivo::where('fuente','=',$fuente)
                                    ->select('id','objetivo')
                                    ->orderBy('objetivo')
                                    ->get()
                                    ->toArray();

        //$resultado = Arr::prepend($resultado,['id' => 0, 'objetivo' => 'Objetivos...'] );
        return response()->json(['objetivos' => $resultado]);
        //return response()->json(['mensaje' => $mensaje, 'estado' => $estado, 'regiones' => $regiones]);
  }


	public function ajaxMunicipios_cve($departamento){

			$municipios = ReporteDiego::where('departamento',strtoupper($departamento))->where('municipio_go_cial','ON')->select('municipio')->distinct()->get()->toArray();

			if(empty($municipios)){
				$datos[0]['municipio'] = 'N/A';
				$estado = false;
				$mensaje = 'NO HAY DATOS';

				return response()->json([
					'estado' => $estado,
					'mensaje' => $mensaje,
					'datos' => $datos
				]);
			}

			foreach($municipios as $mcpio){
				$datos[]['municipio'] = $mcpio['municipio'];
			}

			$estado = true;
			$mensaje = 'LISTADO DE MUNICIPIOS DEL DPTO DE '.$departamento.' CON GO COMERCIAL';

			return response()->json([
				'estado' => $estado,
				'mensaje' => $mensaje,
				'datos' => $datos
			]);

    }


	public function ajaxDepartamentos_cve(){

			$departamentos = ReporteDiego::select('departamento')->distinct()->orderBy('departamento','ASC')->get()->toArray();

			if(empty($departamentos)){
				$datos[0]['departamento'] = 'N/A';
				$estado = false;
				$mensaje = 'NO HAY DATOS';

				return response()->json([
					'estado' => $estado,
					'mensaje' => $mensaje,
					'datos' => $datos
				]);
			}

			foreach($departamentos as $dpto){
				$datos[]['departamento'] = $dpto['departamento'];
			}

			$estado = true;
			$mensaje = 'LISTADO DE DEPARTAMENTOS';

			return response()->json([
				'estado' => $estado,
				'mensaje' => $mensaje,
				'datos' => $datos
			]);

    }


	public function ajaxCves_cve($departamento, $municipio){

			$cves = ReporteDiego::where('departamento',strtoupper($departamento))
									->where('municipio',$municipio)
									->select('cve','cve_go_cial')
									->distinct()
									->orderBy('municipio','ASC')
									->get()
									->toArray();

			if(empty($cves)){
				$datos[0]['cve'] = 'N/A';
				$datos[0]['go_cial'] = 'N/A';
				$estado = false;
				$mensaje = 'NO HAY DATOS';

				return response()->json([
					'estado' => $estado,
					'mensaje' => $mensaje,
					'datos' => $datos
				]);
			}

			foreach($cves as $cve){
				$datos[] = ['cve' => $cve['cve'], 'go_cial' => $cve['cve_go_cial']];
			}

			$estado = true;
			$mensaje = 'LISTADO DE CVES ASOCIADOS A '.strtoupper($municipio).' EN '.strtoupper($departamento).' CON/SIN GO COMERCIAL';

			return response()->json([
				'estado' => $estado,
				'mensaje' => $mensaje,
				'datos' => $datos
			]);

    }



    public function ajaxTrackingMunicipio($municipio){

        $qry_municipios = "
            SELECT DISTINCT regional, departamento, municipio ciudad
            FROM base_geografia
            WHERE municipio LIKE '%". strtoupper($municipio) ."%'
            AND regional IN ('COSTA', 'BOGOTA', 'NOROCCIDENTE', 'OCCIDENTE', 'ORIENTE')
            ORDER BY regional, departamento, municipio;
        ";

        $result_municipios = DB::select($qry_municipios);

        if(!empty($result_municipios)){
            $estado = true;
            $mensaje = 'MUNICIPIO '.strtoupper($municipio).' LISTADO CORRECTAMENTE';
            $datos = $result_municipios;
        }else{
            $estado = false;
            $mensaje = 'MUNICIPIO '.strtoupper($municipio).' NO EXISTE';
            $datos[0]['regional'] = 'N.A.';
            $datos[0]['departamento'] = 'N.A.';
            $datos[0]['ciudad'] = 'N.A.';
        }

		return response()->json([
			'estado' => $estado,
			'mensaje' => $mensaje,
			'datos' => $datos
		]);


    }
}
