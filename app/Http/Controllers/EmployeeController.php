<?php

namespace App\Http\Controllers;

use Mail;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;

Use App\User;
Use App\Models\Head;
Use App\Models\Channel;
Use App\Models\Regional;
Use App\Models\Position;
Use App\Models\Employee;
Use App\Models\Territory;
Use App\Models\empleado_role;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class EmployeeController extends Controller
{

    /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_EMPLEADOS', ['only' => ['index','filtros']]);
      $this->middleware('permission:VER_EMPLEADOS', ['only' => ['show']]);
      $this->middleware('permission:CREAR_EMPLEADOS', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_EMPLEADOS', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_EMPLEADOS', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $regional = Employee::Regional();
        $nombre_regional = $regional['nom_regional'];
        $nombre_regional = $nombre_regional == 'TODOS/TODAS' ? '' : $nombre_regional;
        $empleados = Employee::TraerEmpleados($nombre_regional)->StatusA()->paginate(5);

        return view('employees.index',compact('empleados'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $empleado = Employee::with(['territory','territory.regional','position','user'])
                            ->whereId($id)->first();
        $emp_role = empleado_role::where('id_empleado',$id)->first() ;
        if($emp_role){
            $id_role = $emp_role->id_role;
            $a_role = Role::find($id_role);
            $role['name'] = $a_role->name;
        }else{
            $role['name'] = 'Ninguno';
        }
        return view('employees.show',compact('empleado','role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Esta Logica se puede Mejorar
        //pendiente: ver a que modelo trasladar esta logica
        $user = Auth::user();
        if($user->hasRole('SUPER_ADMIN')){
            $roles = Role::all('id','name')->toArray();
        }elseif($user->hasAnyRole(['GERENTE','ADMIN_APP_REGIONAL'])){
            $roles = Role::where('name','<>','SUPER_ADMIN')
                         ->select('id','name')->get()->toArray();
        }elseif($user->hasRole('ADMIN_APP_CANAL')){
            $roles = Role::where('name','<>','SUPER_ADMIN')
                         ->where('name','<>','ADMIN_APP_REGIONAL')
                         ->where('name','<>','ADMIN_APP_CANAL')
                         ->where('name','<>','ANALISTA/ESPECIALISTA')
                         ->select('id','name')->get()->toArray();
        }else{
            $roles = Role::where('name','<>','SUPER_ADMIN')
                         ->where('name','<>','ADMIN_APP_REGIONAL')
                         ->where('name','<>','ADMIN_APP_CANAL')
                         ->where('name','<>','ANALISTA/ESPECIALISTA')
                         ->select('id','name')->get()->toArray();
        }

        $cargos = Position::all('id','position_name')->toArray();
        $emp_roles = empleado_role::where('id_empleado',$id)->first() ;

        $roles = Arr::prepend($roles, ['id' => 0, 'name' => '-- Seleccione --']);
        $cargos = Arr::prepend($cargos, ['id' => 0, 'position_name' => '-- Seleccione --']);

        $role_asig = (! is_null($emp_roles))
                     ? Role::where('id',$emp_roles->id_role)->get()->toArray()
                     : [0 => ['id' => 0, 'name' => '']];

        $empleado = Employee::with(['territory','territory.regional','position','user'])->find($id);

        return view('employees.edit',compact('roles','empleado', 'cargos', 'emp_roles', 'role_asig'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'document' => ['required', 'integer','unique:employees,document,'.$id],
            'position_id' => ['required', 'not_in:0','integer'],
            'role_id' => ['required', 'not_in:0','integer'],
        ]);

        $empleado = Employee::with(['territory','territory.regional','position','user'])->find($id);
        $usuario_asociado = User::find($empleado->user_id);

        $empleado->update([ 'document' => $request->document,
                            'position_id' => $request->position_id]);

        if(! is_null($request->email_verified)){
            $usuario_asociado->email_verified_at = Carbon::now();
            $usuario_asociado->save();
        }else{
            if(! is_null($usuario_asociado->email_verified_at)){
                $usuario_asociado->email_verified_at = null;
                $usuario_asociado->save();
            }
        }

        $ver_empl_roles = empleado_role::where('id_empleado', $id)->first();
        if(!is_null($ver_empl_roles)){
            $old_role_id = $ver_empl_roles->id_role;
            $ver_empl_roles->id_role = $request->role_id;
            $ver_empl_roles->save();

        }else{
            $nuevo_empl_roles = new empleado_role;
            $nuevo_empl_roles->id_empleado = $id;
            $nuevo_empl_roles->id_role = $request->role_id;
            $nuevo_empl_roles->save();
        }

        if($request->role_id <> 9){
            $empleado->status = 'A';
            $empleado->save();
        }

        $usuario_asociado->syncRoles([$request->role_id]);

/*
        //***************************************************************
        // REALIZAMOS LA NOTIFICACION AL USUARIO QUE YA ESTA HABILITADO
        //***************************************************************

        $datosMail = [];
        $datosMail['id_empleado']     = $empleado->id;
        $datosMail['nombre']          = $empleado->name;
        $datosMail['apellido']        = $empleado->last_name;
        $datosMail['documento']       = $empleado->document;
        $datosMail['correo']          = $usuario_asociado->email;
        $datosMail['nombre_completo'] = $usuario_asociado->name;
        $datosMail['regional']        = $n_regional->regional_name;
        $datosMail['territorio']      = $n_territorio->territory_name;
        $datosMail['canal']           = $n_canal->channel_name;
        $datosMail['cargo']           = $n_cargo->position_name;

        $correo_cliente = $usuario_asociado->email;
        $nombre_cliente = $usuario_asociado->name;
        $asunto_correo = 'SU CUENTA INFOPOS HABILITADA';

        // NOTIFICAMOS AL NUEVO USUARIO LA CREACION DE SU CUENTA
        Mail::to($correo_cliente, $nombre_cliente)->send(new NotificacionHabilitadoUsuario($datosMail));
*/

		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'EmployeeController@update()';
		$var_log_datos['valores'] = 
		['empleado_id' => $id,
		  'role' => $request->role_id,
          'documento' => $request->document,
          'position' => $request->position_id];
		$var_log_mensaje = 'Se ha Editado un Empleado:';
		Log::notice($var_log_mensaje,$var_log_datos);
		
        return redirect()->route('empleados.index')
                        ->with('success','Empeado Actualizado Exitosamente->'.$request->role_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $empleado = Employee::find($id);
        $usuario_asociado = User::find($empleado->user_id);

        $usuario_asociado->email_verified_at = null;
        $usuario_asociado->save();

        $empleado->status = 'I';
        $empleado->save();

        $ver_empl_roles = empleado_role::where('id_empleado', $id)->first();
        $ver_empl_roles->id_role = 9;  //9 ES EL ID DE INVITADO SIN ACCESO
        $ver_empl_roles->save();

        $usuario_asociado->syncRoles([9]); //9 ES EL ID DE INVITADO SIN ACCESO



		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'EmployeeController@destroy()';
		$var_log_datos['valores'] = 
		['empleado_id' => $id];
		$var_log_mensaje = 'Se ha Inactivado un Empleado:';
		Log::notice($var_log_mensaje,$var_log_datos);


        return redirect()->route('empleados.index')
                        ->with('success','Empeado Inactivado Exitosamente');

    }

    /**
     * PERMITE REALIZAR UNA SERIE DE FILTROS
     * EN LA PANTALLA INDEX
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function filtros(Request $request)
    {
        //
        $filtro = $request->filtro_empleados;
        $texto = $request->texto_filtro;

        //SI EL FILTRO NO ES SELECCIONADO o NO SE INGRESA TEXTO DE FILTRO
        if(is_null($filtro) || is_null($texto) ){
            return redirect()->route('empleados.index')
                        ->with('error','Filtro/Texto Nulo, debe agregar texto y/o seleccionar filtro');
        }


        $regional = Employee::Regional();
        $nombre_regional = $regional['nom_regional'];
        $nombre_regional = $nombre_regional == 'TODOS/TODAS' ? '' : $nombre_regional;
        $empleados = Employee::TraerEmpleados($nombre_regional)
                             ->Filtros($nombre_regional,$filtro, $texto)
                             ->paginate(5);

        return view('employees.index',compact('empleados'))
            ->with('i', (request()->input('page', 1) - 1) * 5);


    }
	
}
