<?php

namespace App\Http\Controllers;

Use App\Models\TradeClase;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TradeClaseController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_CLASES', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_CLASES', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_CLASES', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_CLASES', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_CLASES', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clases = TradeClase::latest()->paginate(5);

        return view('trade.clases.index',compact('clases'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trade.clases.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre_clase' => ['required', 'max:50','unique:trade_clase_piezas']
        ]);


        TradeClase::create(['nombre_clase' => strtoupper($request->nombre_clase)]);


        return redirect()->route('clases.index')
                        ->with('success','Clase Creada Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $clase = TradeClase::find($id);
        return view('trade.clases.show',compact('clase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $clase = TradeClase::find($id);
        return view('trade.clases.edit',compact('clase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nombre_clase' => ['required', 'max:50','unique:trade_clase_piezas,nombre_clase,'.$id]
        ]);

        $clase = TradeClase::find($id);

        $clase->update(['nombre_clase' => strtoupper($request->nombre_clase)]);

        return redirect()->route('clases.index')
                        ->with('success','Clase Actualizada Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $clase = TradeClase::find($id);
        $clase->delete();

        return redirect()->route('clases.index')
                        ->with('success','Clase Eliminada Exitosamente');
    }
}
