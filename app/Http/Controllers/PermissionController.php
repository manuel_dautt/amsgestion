<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{

    /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:LISTAR_PERMISOS', ['only' => ['index']]);
      $this->middleware('permission:VER_PERMISOS', ['only' => ['show']]);
      $this->middleware('permission:CREAR_PERMISOS', ['only' => ['create','store']]);
      $this->middleware('permission:EDITAR_PERMISOS', ['only' => ['edit','update']]);
      $this->middleware('permission:BORRAR_PERMISOS', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $permisos = Permission::latest()->paginate(5);

        return view('permissions.index',compact('permisos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'permiso_name' => ['required', 'max:255','unique:permissions,name']
        ]);


        Permission::create(['name' => strtoupper($request->permiso_name)]);

        return redirect()->route('permisos.index')
                        ->with('success','Permiso Creado Exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $permiso = Permission::find($id);
        return view('permissions.show',compact('permiso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $permiso = Permission::find($id);
        return view('permissions.edit',compact('permiso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => ['required', 'max:100','unique:permissions,name,'.$id]
        ]);

        $permiso = Permission::find($id);

        $permiso->update(['name' => strtoupper($request->name)]);

        return redirect()->route('permisos.index')
                        ->with('success','Permiso Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $permiso = Permission::find($id);
        $permiso->delete();

        return redirect()->route('permisos.index')
                        ->with('success','Permiso Eliminado Exitosamente');
    }
}
