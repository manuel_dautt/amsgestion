<?php

namespace App\Http\Controllers;

use ArrayObject;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ArchivosController extends Controller
{

    //
    public function verArchivos(){
		
		$var_log_datos['usuario'] = Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'ArchivosController@VerArchivos()';
		$var_log_mensaje = 'Accede a visualizar archivos descargables';
		Log::notice($var_log_mensaje.' ',$var_log_datos);
		
        $directory = '/public/files';
        $files = Storage::files($directory);
        //$files = Storage::allFiles($directory);
        $archivos = [];
		//dd($directory, $files);
        foreach ($files as $file) {

            $fecha_archivo = Carbon::createFromTimestamp(Storage::lastModified($file));
            $valor = explode('/',$file);
            $nombre_corto = $valor[count($valor)-1];
            $nombre_largo = Storage::url($file);
            $url = Storage::url($file);
            $tamano = Storage::size($file);
            $modificado = $fecha_archivo->toDateTimeString();
            $paraHumanos = $fecha_archivo->diffForHumans(Carbon::now());
            $archivos[] = [ 'nombre_corto' => $nombre_corto,
                            'nombre_largo' => $nombre_largo,
                            'url' => $url,
                            'tamano' => $tamano,
                            'modificado' => $modificado,
                            'humanos' => $paraHumanos];
		
        }


        Return view('archivos.verArchivos', compact('archivos'));

    }

    public function descargarArchivos($archivo){
		$var_log_name = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_mensaje = ' Descarga Archivo: '.$archivo;
		Log::notice($var_log_name.$var_log_mensaje);
		
        return response()->download($archivo);
		//Storage::download($archivo);
    }


}
