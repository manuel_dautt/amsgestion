<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Storage;

use App\Models\Employee;
use App\Models\FotosPuntos;
use App\Models\TradeImplementacion;
use App\Models\TradePiezasImplementacion;
use App\Models\TradeColocacionPiezas;
use App\Models\TradePlanFidelizacion;
use App\Models\TradePvFidelizado;
use App\Models\Confidencialidad;
use App\Models\InfoposActualizado;
use App\Models\InfoposArpuPV;
use App\Models\Infopos_Rpoint;
use App\Models\Infopos_Light;
use App\Models\Infopos_Ec;
use App\Models\Infopos_Gross_Peid;
use App\Models\Infopos_Calidad;
use App\Models\Infopos_Composicion;
use App\Models\Infopos_CircuitosDms;
use App\Models\Infopos_Fechas;
use App\Models\Infopos_Paquetes;
use App\Models\Infopos_Periodos;
use App\Models\Infopos_Revenue;
use App\Models\InfoposRutas;
use App\Models\Infopos_TicketRecargas;
use App\Models\Infopos_Estado;
use App\Models\Visits;
use App\Models\VisitsType;
use App\Models\pvEmail;
use App\Models\EncuestaBase;
use App\Models\EncuestaDocumentos;
use App\Models\EncuestaVisitas;

class GestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('permission:VER_DEALERS', ['only' => ['buscarPunto',
																'confidencialidad',
																'foto_punto_venta',
																'traerPunto',
																'traerNombrePunto',
																'traerCedulaPropietario',
																'traerCircuitoEstado',
																'traerPuntosRutaDia'
															   ]
													]);
    }

    /**
     * Buscar Punto de venta.
     *
     * @return
     */
    public function buscarPunto()
    {
        $estados_dms = Infopos_Estado::select('estado_dms')->orderBy('estado_dms','DESC')->get()->toArray();
        $dias_semana = ['DOMINGO','LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO'];
        $dia_hoy = date('w');

        $confidencialidad = Confidencialidad::where('user_id','=',Auth::id())->get()->toArray();
        if(empty($confidencialidad)){
            return view('gestion.confidencialidad');
        }
        return view('gestion.buscar_punto', compact('estados_dms','dias_semana','dia_hoy'));

    }


    public function confidencialidad(Request $request){
        if ($request->accept == 'N'){
            return redirect()->route('home')
                                ->with('error','No Acepta terminos y condiciones de confidencialidad INFOPOS');
        }

        Confidencialidad::create([
            'user_id' => Auth::id(),
            'accept' => $request->accept,
          ]);

          return view('gestion.ok_confidencialidad');

    }

	public function email_pv(Request $request){
		// $request->validate([
            // 'correo' => ['required', 'email','exists:emails_pv,correo'],
        // ]);
		$pv = pvEmail::where('idpdv',$request->idpdv)->first();

		if(!is_null($pv)){
            $pv->correo = $request->correo;
			$pv->save();
        }else{
			$correo_pv = new pvEmail;
            $correo_pv->idpdv = $request->idpdv;
			$correo_pv->correo = $request->correo;
			$correo_pv->save();
        }
		
		$idpdv = $request->idpdv;
		$correo = $request->correo;
		$anterior = $request->anterior;
		
		return view('gestion.ok_correo',compact('idpdv','correo','anterior'));
	
	}


    public function foto_punto_venta(Request $request){
        $request->validate([
            'imagen' => ['required','max:10000'],
        ],[
            'imagen.required' => 'Por favor asocie una imagen a la pieza',
            'imagen.max' => 'La imagen debe ser de maximo 10Mb',
        ]);

        $extension = $request->file('imagen')->extension();

        if(!(strpos('jpg,jpeg,png', $extension) !== false)){
            return view('gestion.foto',
                        [
                            'idpdv' => $request->idpdv,
                            'mensaje'=>'La imagen debe ser JPG, JPEG, PNG',
                            'titulo' => 'ERROR CARGANDO FOTO PUNTO VENTA'
                        ]
            );
        }


        $nombre_foto = $request->prefijo.
                       $request->numero.'_'.
                       $request->idpdv.'.'
                       .$extension;

        $image_resize = Image::make($request->file('imagen')->getRealPath());

        $image_resize->resize(800, null, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
       });

       $image_resize->orientate();


        $valida_fotos = FotosPuntos::where('idpdv','=', $request->idpdv)
                                   ->where('numero',$request->numero)
                                   ->first();

        if($valida_fotos == null){
            $fotos_pv = new FotosPuntos;
            $fotos_pv->idpdv = $request->idpdv;
            $fotos_pv->numero = $request->numero;
        }else{
            $fotos_pv = $valida_fotos;
            Storage::disk('fotos_pv')->delete($fotos_pv->nombre);
        }
        $fotos_pv->nombre = $nombre_foto;

        $image_resize->save(storage_path("app/public/fotos_pv/" . $nombre_foto),60);

        $path = Storage::disk('fotos_pv')->url($fotos_pv->nombre);

        $fotos_pv->url = $path;

        $fotos_pv->save();


        return view('gestion.foto',
                        [
                            'idpdv' => $request->idpdv,
                            'mensaje'=>'Guardada Foto Para el PV',
                            'titulo' => 'FOTO PUNTO VENTA CARGADA CORRECTAMENTE'
                        ]
            );
    }


       /**
     * Buscar Punto de venta.
     *
     * @return
     */
    public function traerPunto(Request $request)
    {
        //$request->validate([
        //    'idpdv' => ['required', 'integer','exists:infopos_rpoint,idpdv'],
        //]);

        $idpdv = $request->input('idpdv');

        $id_usuario = Auth::id();

        $regional = Employee::Regional();

        $empleado = Employee::DatosEmpleado();

        $cc_empleado = $empleado['cedula'];

        $nom_regional = $regional['nom_regional']; // == 'TODOS/TODAS' ? '' : $regional['nom_regional'];
		
        $rpoint =  Infopos_Rpoint::where('idpdv',$idpdv)
                                 ->first();

        if(empty($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','Punto de venta '.$idpdv.' no registra en Infopos_Rpoint!!!');
        }
		
		$rpoint = $rpoint->toArray();
		
		$var_log_datos['usuario'] = 'Usuario: '.Auth::id().'--'.Auth::user()->employee->name.' '.Auth::user()->employee->last_name;
		$var_log_datos['controlador'] = 'GestionController@traerPunto()';
		$var_log_datos['valores'] = ['punto_venta' => $request->input('idpdv'), 'regional' => $regional];
		$var_log_mensaje = 'Usuario Accedio a Consultar Punto de Venta';
		Log::notice($var_log_mensaje,$var_log_datos);

        
        if($rpoint['regional'] != $nom_regional && $nom_regional != 'TODOS/TODAS'){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','Punto de venta no Pertenece a su Regional!!!');
        }
        
		

        $circuito_pv = $rpoint['circuito'];
        $tiene_ruta = InfoposRutas::where('cedula',$cc_empleado)
                             ->where('circuito', $circuito_pv)
                             ->first();
        $ruta = [];

        if(!empty($tiene_ruta)){
            $ruta = InfoposRutas::where('cedula',$cc_empleado)
                    ->where('circuito', $circuito_pv)
                    ->first()->toArray();
        }

        $array_dias_semana = ['dom','lun','mar','mie','jue','vie','sab'];
        $frec_visita = 0;
        $dias_visita = 'No Aplica';
        $ruta_hoy = 'N';
        $msg_ruta_hoy = '';
        $dia_de_hoy = $array_dias_semana[date('w')];

        if(!empty($ruta)){
            $frec_visita = $ruta['lun']+$ruta['mar']+$ruta['mie']+
                            $ruta['jue']+$ruta['vie']+$ruta['sab']+$ruta['dom'];

            if($ruta[$dia_de_hoy] == 1){
                $ruta_hoy = 'S';
                $msg_ruta_hoy = 'PERTENECE A RUTA DE HOY';
            }else{
                $ruta_hoy = 'N';
                $msg_ruta_hoy = 'NO APLICA RUTA DE HOY';
            }

            $dias_visita = ($ruta['lun'] == 1 ? 'Lun-':'').
                           ($ruta['mar'] == 1 ? 'Mar-':'').
                           ($ruta['mie'] == 1 ? 'Mie-':'').
                           ($ruta['jue'] == 1 ? 'Jue-':'').
                           ($ruta['vie'] == 1 ? 'Vie-':'').
                           ($ruta['sab'] == 1 ? 'Sab-':'').
                           ($ruta['dom'] == 1 ? 'Dom-':'');

            $dias_visita = substr($dias_visita,0,strlen($dias_visita)-1);
        }

		/* *******************************************************************
		**   VALIDA SI EL PUNTO DE VENTA PERTENECE A UN PLAN DE FIDELIZACION
		********************************************************************** */
		$esFidel = TradePvFidelizado::where('idpdv',$idpdv)
									->where('status','A')
									->first();
		
		
		if(!empty($esFidel)){
            $PV_Fidel = 'PERTENECE A PLAN DE FIDELIZACION';
        }else{
			$PV_Fidel = '';
		}

		/* *******************************************************************
		**   ENCUESTAS DISPONIBLES PARA MOSTRAR EN EL SEGUIMIENTO
		********************************************************************** */		
		$encuestasDisponibles = EncuestaBase::with('canal')->where('estado','A')
														   ->where('tipo_encuesta_id',1)
														   ->orderBy('canal_id','ASC')
														   ->get()
														   ->toArray();
		/* *******************************************************************
		**   VALIDA SI TENEMOS ALGUN ACOMPAÑAMIENTO PENDIENTE
		********************************************************************** */														   
		$chequeos = EncuestaDocumentos::where('activa','S')
										->where('id_user_audit',$id_usuario)
										->where(function($q) {
											$q->where('encuesta_id',6)
											  ->orWhere('encuesta_id',7);
										})
										->first();
							
		if(!empty($chequeos)){
            $tiene_chequeos['status'] = true;
			$tiene_chequeos['id'] = $chequeos->id;
			$tiene_chequeos['mensaje'] = 'ACOMPAÑAMIENTO ACTIVO';
			$tiene_chequeos['cc'] = $chequeos->cc_watched;
			$emp_cheq = Employee::where('document',$chequeos->cc_watched)->first();
			$tiene_chequeos['nombre'] = $emp_cheq->name.' '.$emp_cheq->last_name;
        }else{
			$tiene_chequeos['status'] = false;
			$tiene_chequeos['id'] = 0;
			$tiene_chequeos['mensaje'] = '';
			$tiene_chequeos['cc'] = '';
			$tiene_chequeos['nombre'] = '';
		}

		/* *****************************************************************************
		**   VALIDA SI HAY ABIERTA ALGUNA VISITA EN ESE PUNTO POR PARTE DE ESE USUARIO
		******************************************************************************** */
        $tipos_visita = VisitsType::select('id','name_type_visit')->orderBy('name_type_visit','ASC')->get()->toArray();

        $find_visita_abierta = Visits::where('user_id', $id_usuario)
                                    ->where('idpdv',$idpdv)
                                    ->where('activa','S')
                                    ->first();

        $validaVisita['hay_visita_pendiente'] = false;
        $validaVisita['id_visita_pendiente'] = 0;
        $validaVisita['id_tipo_visita_pendiente'] = 0;
        $validaVisita['tipo_visita_pendiente'] = 'N/A';
        $validaVisita['fecha_visita_pendiente'] = Carbon::now();
        $validaVisita['tiempo_visita_pendiente'] = '';

        if(!empty($find_visita_abierta)){
            $validaVisita['hay_visita_pendiente'] = true;
            $validaVisita['id_visita_pendiente'] = $find_visita_abierta->id;
            $validaVisita['id_tipo_visita_pendiente'] = $find_visita_abierta->visit_type_id;
            $nm_tipo_visita = VisitsType::select('name_type_visit')->where('id',$validaVisita['id_tipo_visita_pendiente'])->first()->toArray();
            $validaVisita['tipo_visita_pendiente'] = $nm_tipo_visita['name_type_visit'];
            $validaVisita['fecha_visita_pendiente'] = $find_visita_abierta->fecha_ini;
        }
        Carbon::setLocale('es');
        $validaVisita['tiempo_visita_pendiente'] = Carbon::createFromFormat('Y-m-d H:i:s', $validaVisita['fecha_visita_pendiente'], 'America/Bogota')->diffForHumans(Carbon::now());

		/* *******************************************************************
		**   TRAE LAS ULTIMAS 5 VISITAS AL PUNTO
		********************************************************************** */
		$visitas_al_punto = Visits::with('user')->where('idpdv',$idpdv)->latest('fecha_ini')->take(5)->get();
		$visitas_al_punto = $visitas_al_punto->toArray();
		$cant_visitas = count($visitas_al_punto);
		
		//dd($visitas_al_punto, Carbon::parse($visitas_al_punto[1]['fecha_ini'])->floatDiffInMinutes($visitas_al_punto[0]['fecha_fin']));
		/* *******************************************************************
		**   MUESTRA LAS ENCUESTAS ACTIVAS/CERRADAS EN LA VISITA ACTUAL
		********************************************************************** */
	
			$encuestas_activas_visita = EncuestaVisitas::with('encuesta')->where('visita_id',$validaVisita['id_visita_pendiente'])->get();
			
			$tiene_encuestas_activas = (count($encuestas_activas_visita)) ? true : false;
			
			//dd($encuestas_activas_visita, $tiene_encuestas_activas);
										
        $periodos = Infopos_Periodos::first()->toArray();

        $mes_1 = $periodos['mes_1'];
        $mes_2 = $periodos['mes_2'];
        $mes_3 = $periodos['mes_3'];
        $mes_4 = $periodos['mes_4'];

        switch ($rpoint['ultima_tipologia']) {
          case 'PDA':
            $tipo_negocio = 'PDA';
            break;
          case 'PDV':
            $tipo_negocio = 'PDV';
            break;
          case 'MIXTO':
            $tipo_negocio = 'MIXTO';
            break;
          default:
            $tipo_negocio = 'MIXTO';
            break;
        }

        switch ($rpoint['ult_seg_gross']) {
            case 'A. RevenueCero':
                $max_piezas = 2;
                break;
            case 'B. Principiantes.':
                $max_piezas = 3;
                break;
            case 'C. EnDesarrollo':
                $max_piezas = 4;
                break;
            case 'D. Desarrollados':
                $max_piezas = 5;
                break;
            case 'E. Productivos':
                $max_piezas = 7;
                break;
            default:
              $max_piezas = 2;
              break;
          }



        $implementaciones = TradeImplementacion::where('status','A')->get()->toArray();
        $piezas_implementacion = TradePiezasImplementacion::with(['pieza','pieza.objetivo','implementacion'])->where('status','A')->get()->toArray();
        $colocacion_piezas = TradeColocacionPiezas::where('idpdv',$idpdv)->get()->toArray();


        $regional_peid = ($rpoint['regional']);
        $gross_peid = Infopos_Gross_Peid::where('regional',$regional_peid)
                                          ->orderBy('orden','ASC')
                                          ->get()
                                          ->toArray();

        //Trae todo de Fact Ligth
        $fact_ligth = Infopos_Light::where('idpdv',$idpdv)->get()->toArray();

        if(empty($fact_ligth)){
            /*
			$activaciones = [];
            $status['activaciones'] = false;
            $gross_paquetes = [];
            $status['gross_paquetes'] = false;
            $recargas = [];
            $status['recargas'] = false;
            $rev = [];
            $status['rev'] = false;
			*/
			//$activaciones = [];
			$activaciones['brutas'] = [];
            $activaciones['gross_pre'] = [];
            $activaciones['gross_pos'] = [];
            $activaciones['gross_porta'] = [];
            $activaciones['gross_migra'] = [];
            $activaciones['gross_total'] = [];
            $activaciones['gross_mismo_mes'] = [];
            $status['activaciones'] = false;
            
			//$gross_paquetes = [];
			$gross_paquetes['gross_auto'] = [];
            $gross_paquetes['porc_gross_auto'] = [];
            $gross_paquetes['rec_mismo_dia'] = [];
            $gross_paquetes['porc_rec_mismo_dia'] = [];
            $gross_paquetes['rec_mayor_3k'] = [];
            $gross_paquetes['porc_rec_mayor_3k'] = [];
            $gross_paquetes['gross_pq_foco'] = [];
            $gross_paquetes['porc_pq_foco'] = [];
            $gross_paquetes['gross_pq_6k'] = [];
            $gross_paquetes['porc_pq_6k'] = [];
            $gross_paquetes['gross_pq_todo'] = [];
            $gross_paquetes['porc_pq_todo'] = [];			
            $status['gross_paquetes'] = false;
            
			//$recargas = [];
			$recargas['recarga_pura'] = [];
            $recargas['paquetigo'] = [];
            $recargas['precargados'] = [];
            $recargas['bolsas'] = [];
            $recargas['total'] = [];
            $recargas['rec_pq_foco'] = [];
            $recargas['rec_pq_6k'] = [];
            $recargas['rec_pq_todo'] = [];
            $recargas['pdv_rec_200k_pq_foco'] = [];
            $status['recargas'] = false;
			
            //$rev = [];
			$rev['nueva_seg_gross'] = [];
            //$rev['nueva_seg_gross'] = [];
            $rev['arpu_nueva_segmentacion'] = [];
            $rev['rev_nueva_segmentacion'] = [];
            $rev['evolucion_cial'] = [];
			$rev['rank_segmentacion'] = [];
            $rev['icono_segmentacion'] = [];
			$rev['color_evolucion'] = [];
            $rev['icono_evolucion'] = [];
            $status['rev'] = false;
        }else{
            $status['activaciones'] = true;
            $status['gross_paquetes'] = true;
            $status['recargas'] = true;
            $status['rev'] = true;

            foreach( $fact_ligth as $item){
                $periodo = $item['periodo'];

                $activaciones['brutas'][$periodo] = $item['brutas'];
                $activaciones['gross_pre'][$periodo] = $item['gross_pre'];
                $activaciones['gross_pos'][$periodo] = $item['gross_pos'];
                $activaciones['gross_porta'][$periodo] = $item['gross_porta'] + $item['gross_porta_pos'];
                $activaciones['gross_migra'][$periodo] = $item['gross_migra'] + $item['gross_migra_pre'];
                $activaciones['gross_total'][$periodo] = $item['gross_total'];
                $activaciones['gross_mismo_mes'][$periodo] = $item['gross_mismo_mes'];

                $gross_paquetes['gross_auto'][$periodo] = $item['gross_auto'];
                $gross_paquetes['porc_gross_auto'][$periodo] = ($item['gross_pre'] > 0) ? round(($item['gross_auto'] / $item['gross_pre'])*100,1) : 0.0;
                $gross_paquetes['rec_mismo_dia'][$periodo] = $item['rec_mismo_dia'];
                $gross_paquetes['porc_rec_mismo_dia'][$periodo] =  ($item['brutas'] > 0) ? round(($item['rec_mismo_dia'] / $item['brutas'])*100,1) : 0.0;
                $gross_paquetes['rec_mayor_3k'][$periodo] = $item['rec_mayor_3k'];
                $gross_paquetes['porc_rec_mayor_3k'][$periodo] =  ($item['rec_mayor_3k'] > 0) ? round(($item['rec_mayor_3k'] / $item['rec_mismo_dia'])*100,1) : 0.0;
                $gross_paquetes['gross_pq_foco'][$periodo] = $item['gross_pq_foco'];
                $gross_paquetes['porc_pq_foco'][$periodo] =  ($item['gross_pre'] > 0) ? round(($item['gross_pq_foco'] / $item['gross_pre'])*100,1) : 0.0;
                $gross_paquetes['gross_pq_6k'][$periodo] = $item['gross_pq_6k'];
                $gross_paquetes['porc_pq_6k'][$periodo] =  ($item['gross_pre'] > 0) ? round(($item['gross_pq_6k'] / $item['gross_pre'])*100,1) : 0.0;
                $gross_paquetes['gross_pq_todo'][$periodo] = $item['gross_pq_todo'];
                $gross_paquetes['porc_pq_todo'][$periodo] =  ($item['gross_pre'] > 0) ? round(($item['gross_pq_todo'] / $item['gross_pre'])*100,1) : 0.0;

                $rev['nueva_seg_gross'][$periodo] = Str::limit(Str::after($item['nueva_seg_gross'],' '),9,'...');
                //$rev['nueva_seg_gross'][$periodo] = Str::after($item['nueva_seg_gross'],' ');
                $rev['arpu_nueva_segmentacion'][$periodo] = $item['arpu_nueva_segmentacion'];
                $rev['rev_nueva_segmentacion'][$periodo] = $item['rev_nueva_segmentacion'];
                $rev['evolucion_cial'][$periodo] = $item['evolucion_cial'];

                switch ($rev['nueva_seg_gross'][$periodo]) {
                    case 'RevenueCe...':
                        $rev['rank_segmentacion'][$periodo] = 'text-danger';
                        $rev['icono_segmentacion'][$periodo] = 'fas fa-times-circle';
                        break;
                    case 'Principia...':
                        $rev['rank_segmentacion'][$periodo] = 'text-danger';
                        $rev['icono_segmentacion'][$periodo] = 'fas fa-times-circle';
                        break;
                    case 'EnDesarro...':
                        $rev['rank_segmentacion'][$periodo] = 'text-warning';
                        $rev['icono_segmentacion'][$periodo] = 'far fa-times-circle';
                        break;
                    case 'Desarroll...':
                        $rev['rank_segmentacion'][$periodo] = 'text-success';
                        $rev['icono_segmentacion'][$periodo] = 'far fa-check-circle';
                        break;
                    case 'Productiv...':
                        $rev['rank_segmentacion'][$periodo] = 'text-success';
                        $rev['icono_segmentacion'][$periodo] = 'fas fa-check-circle';
                        break;
                }

                switch ($rev['evolucion_cial'][$periodo]) {
                    case 'NIVEL_0':
                        $rev['color_evolucion'][$periodo] = 'text-danger';
                        $rev['icono_evolucion'][$periodo] = 'fas fa-times-circle';
                        break;
                    case 'NIVEL_1':
                    case 'NIVEL_2':
                        $rev['color_evolucion'][$periodo] = 'text-warning';
                        $rev['icono_evolucion'][$periodo] = 'far fa-times-circle';
                        break;
                    case 'NIVEL_3':
                    case 'NIVEL_4':
                        $rev['color_evolucion'][$periodo] = 'text-info';
                        $rev['icono_evolucion'][$periodo] = 'far fa-check-circle';
                        break;
                    case 'NIVEL_5':
                    case 'NIVEL_6':
                        $rev['color_evolucion'][$periodo] = 'text-success';
                        $rev['icono_evolucion'][$periodo] = 'fas fa-check-circle';
                        break;
                }

                $recargas['recarga_pura'][$periodo] = $item['recarga_pura'];
                $recargas['paquetigo'][$periodo] = $item['paquetigo'];
                $recargas['precargados'][$periodo] = $item['precargados'];
                $recargas['bolsas'][$periodo] = $item['bolsas'];
                $recargas['total'][$periodo] = $item['recarga_pura'] + $item['paquetigo'] + $item['bolsas'] + $item['precargados'];
                $recargas['rec_pq_foco'][$periodo] = $item['rec_pq_foco'];
                $recargas['rec_pq_6k'][$periodo] = $item['rec_pq_6k'];
                $recargas['rec_pq_todo'][$periodo] = $item['rec_pq_todo'];
                $recargas['pdv_rec_200k_pq_foco'][$periodo] = $item['pdv_rec_200k_pq_foco'];
            }
        }


        $tipo_rec = Infopos_TicketRecargas::where('idpdv',$idpdv)->get()->toArray();

        if(empty($tipo_rec)){
            $ticket['N/A']['N/A'] = ['cantidad'=>0,'monto'=>0,'ticket'=>0];
            $tipos_recargas = array_keys($ticket);
            //$t_ticket = [];
            $status['ticket'] = false;
        }else{
            $status['ticket'] = true;
            foreach ($tipo_rec as $item) {
                $periodo = $item['periodo'];
                $tipo = $item['tipo'];
                $monto = $item['monto'];
                $cantidad = $item['cantidad'];
                $tiquete = $item['ticket'];

                $ticket[$tipo][$periodo] = ['cantidad'=>$cantidad,'monto'=>$monto,'ticket'=>$tiquete];

                //$t_ticket['cantidad'][$periodo] += $cantidad;
                //$t_ticket['monto'][$periodo] += $monto;
            }
            $tipos_recargas = array_keys($ticket);
			
        }



        $revenue = Infopos_Revenue::where('idpdv',$idpdv)->get()->toArray();

        if(empty($revenue)){
            $rev = [];
            $status['rev'] = false;
        }else{
            $status['rev'] = true;

            foreach ($revenue as $item) {
                $periodo = $item['periodo'];

                $rev['rev_pre'][$periodo] = $item['rev_pre'];
                $rev['rev_pos'][$periodo] = $item['rev_pos'];
                $rev['rev_porta'][$periodo] = $item['rev_porta'];
                $rev['rev_migra'][$periodo] = $item['rev_migra'];
                $rev['rev_total'][$periodo] = $item['rev_total'];
                $rev['seg_x_rev_old'][$periodo] = Str::limit(Str::after($item['seg_x_rev_old'],' '),9,'...');
                //$rev['seg_x_rev_old'][$periodo] = Str::after($item['seg_x_rev_old'],' ');

                switch ($rev['seg_x_rev_old'][$periodo]) {
                    case 'RevenueCe...':
                        $rev['rank_segmentacion_old'][$periodo] = 'text-danger';
                        $rev['icono_segmentacion_old'][$periodo] = 'fas fa-times-circle';
                        break;
                    case 'Principia...':
                        $rev['rank_segmentacion_old'][$periodo] = 'text-danger';
                        $rev['icono_segmentacion_old'][$periodo] = 'fas fa-times-circle';
                        break;
                    case 'EnDesarro...':
                        $rev['rank_segmentacion_old'][$periodo] = 'text-warning';
                        $rev['icono_segmentacion_old'][$periodo] = 'far fa-times-circle';
                        break;
                    case 'Desarroll...':
                        $rev['rank_segmentacion_old'][$periodo] = 'text-success';
                        $rev['icono_segmentacion_old'][$periodo] = 'far fa-check-circle';
                        break;
                    case 'Productiv...':
                        $rev['rank_segmentacion_old'][$periodo] = 'text-success';
                        $rev['icono_segmentacion_old'][$periodo] = 'fas fa-check-circle';
                        break;
                }
            }
        }

        $paquetes = Infopos_Paquetes::where('id_pdv',$idpdv)->get()->toArray();

        if(empty($paquetes)){
            $mix_rec_paquetes = [];
            $status['mix_rec_paquetes'] = false;
            $paquetes_recargas = [];
            $status['paquetes_recargas'] = false;
        }else{
            $status['mix_rec_paquetes'] = true;
            $status['paquetes_recargas'] = true;

            foreach ($paquetes as $item) {
                $periodo = $item['periodo'];

                $paquete = $item['tipo_paquete'];
                $monto = $item['monto'];
                $cantidad = $item['cantidad'];

                $mix_rec_paquetes[$paquete][$periodo] = ['cantidad'=>$cantidad,'monto'=>$monto];
            }
            $paquetes_recargas = array_keys($mix_rec_paquetes);
        }


        $composicion = Infopos_Composicion::where('idpdv',$idpdv)->orderBy('periodo')->get()->toArray();

        if(empty($composicion)){
            $composicion_gross = [];
            $status['composicion_gross'] = false;
            $paquetes_composicion = [];
            $status['paquetes_composicion'] = false;
        }else{
            $status['composicion_gross'] = true;
            $status['paquetes_composicion'] = true;

            foreach ($composicion as $item) {
                $per = $item['periodo'];
                $paq = $item['paquete'];
                $gross = $item['gross'];

                $composicion_gross[$paq][$per] = $gross;
            }
            $paquetes_composicion = array_keys($composicion_gross);
        }

        $calidad = Infopos_Calidad::where('idpdv',$idpdv)->get()->toArray();
        $status['calidad'] = true;
        if(empty($calidad)){
            $calidad = [];
            $status['calidad'] = false;
        }
        $fechas = Infopos_Fechas::all()->toArray();
        $status['fechas'] = true;
        if(empty($fechas)){
            $fechas = [];
            $status['fechas'] = false;
        }

        $fechas_cubos = Infopos_Fechas::where('periodo',$mes_4)->first()->toArray();

        $ec = Infopos_ec::where('idpdv',$idpdv)->get()->toArray();

        $status['ec'] = true;
        if(empty($ec)){
            $ec = [];
            $status['ec'] = false;
        }

        $fecha_infoposActualizado = InfoposActualizado::first()->toArray();
        $infoposActualizado = Carbon::parse($fecha_infoposActualizado['fecha'],'UTC')->locale('es');
        $fecha_actualizacion_infopos = $infoposActualizado->isoFormat('ddd DD MMMM/YYYY');
        $error_actualizacion_infopos = $fecha_infoposActualizado['errores'];
        $obs_actualizacion_infopos = $fecha_infoposActualizado['observacion'];


        $fotos = FotosPuntos::where('idpdv',$idpdv)->get()->toArray();

        if(count($fotos)){
            foreach ($fotos as $foto) {
                $imagen_pv[$foto['numero']]['nombre'] = $foto['nombre'];
                $imagen_pv[$foto['numero']]['url'] = $foto['url'];
            }
        }else{
            $imagen_pv = [];
        }

        $arpupv = InfoposArpuPV::where('idpdv',$idpdv)->first();
        if($arpupv){
            $arpu_pv['mes_1'] = $arpupv->mes_1;
            $arpu_pv['mes_2'] = $arpupv->mes_2;
            $arpu_pv['mes_3'] = $arpupv->mes_3;
            $arpu_pv['mes_actual'] =  $arpupv->mes_actual;
        }else{
            $arpu_pv['mes_1'] = 0;
            $arpu_pv['mes_2'] = 0;
            $arpu_pv['mes_3'] = 0;
            $arpu_pv['mes_actual'] =  0;
        }
		
		
		$email = pvEmail::where('idpdv',$idpdv)->first();
        if($email){
            $pv_correo = $email->correo;
        }else{
            $pv_correo = 'Sin Correo Electronico';
        }


/*
        dd(
            'activ',$activaciones, 'Gross_Peid', $gross_peid, 'gross_pq',$gross_paquetes, 'revenue',$rev, 'recargas',$recargas,
            'm1',$mes_1, 'm2',$mes_2, 'm3',$mes_3, 'm4',$mes_4, 'mix_pq_rec',$mix_rec_paquetes, 'paq_rec',$paquetes_recargas,
            'composicion',$composicion_gross, 'paquetes',$paquetes_composicion, 'calidad',$calidad, 'fechas',$fechas,
            'tickets',$ticket, 'tipos de recargas',$tipo_rec,'Fechas Cubos', $fechas_cubos, 'EC',$ec, 'idpdv',$idpdv, 'rpoint',$rpoint, 'status',$status
            );
*/

        return view('gestion.ver_punto',compact(
                                                'activaciones',
                                                'gross_paquetes',
                                                'gross_peid',
                                                'rev',
                                                'recargas',
                                                'mes_1',
                                                'mes_2',
                                                'mes_3',
                                                'mes_4',
                                                'mix_rec_paquetes',
                                                'paquetes_recargas',
                                                'ticket',
                                            //    '$t_ticket',
                                                'tipos_recargas',
                                                'composicion_gross',
                                                'paquetes_composicion',
                                                'calidad',
                                                'fechas',
                                                'fechas_cubos',
                                                'ec',
                                                'idpdv',
                                                'rpoint',
                                                'status',
                                                'tipos_visita',
                                                'validaVisita',
                                                'ruta',
                                                'frec_visita',
                                                'dias_visita',
                                                'ruta_hoy',
                                                'msg_ruta_hoy',
                                                'tipo_negocio',
                                                'max_piezas',
                                                'implementaciones',
                                                'piezas_implementacion',
                                                'colocacion_piezas',
                                                'fecha_actualizacion_infopos',
                                                'error_actualizacion_infopos',
                                                'obs_actualizacion_infopos',
                                                'imagen_pv',
                                                'arpu_pv',
												'pv_correo',
												'PV_Fidel',
												'visitas_al_punto',
												'cant_visitas',
												'encuestasDisponibles',
												'tiene_chequeos',
												'encuestas_activas_visita', 
												'tiene_encuestas_activas'
                                                )
                    );

    }

    public function traerNombrePunto(Request $request)
    {
        $request->validate([
            'nombre' => ['required'],
        ]);

        $nombre = str_replace(' ','%',trim($request->input('nombre')));

        $id_usuario = Auth::id();

        $regional = Employee::Regional();

        $nom_regional = ($regional['nom_regional'] != 'TODOS/TODAS') ? $regional['nom_regional'] : '';


        $rpoint =  Infopos_Rpoint::where('nombre_punto','like',"%$nombre%")
                                /*    ->where('regional','like',"%$nom_regional%") */
                                    ->orderBy('estado_dms','DESC')
                                    ->orderBy('circuito','ASC')
                                    ->get()->toArray();
									

        if(empty($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','No Existen Coincidencias con ese nombre en la regional!!!');
        }
        $mensaje_resultado = ' el nombre de punto de venta *'.$nombre.'* ';
        return view('gestion.ver_nombre_puntos',compact('rpoint','mensaje_resultado'));
    }

    public function traerCedulaPropietario(Request $request)
    {
        $request->validate([
            'cc_owner' => ['required'],
        ]);

        $cc_owner = $request->input('cc_owner');

        $id_usuario = Auth::id();

        $regional = Employee::Regional();

        $nom_regional = ($regional['nom_regional'] != 'TODOS/TODAS') ? $regional['nom_regional'] : '';

       // $nom_regional = ($nom_regional == 'SUROCCIDENTE' || $nom_regional == 'EJE CAFETERO') ? 'OCCIDENTE' : $nom_regional;

        $rpoint =  Infopos_Rpoint::where('ced_propietario','like',"%$cc_owner%")
                                    ->where('regional','like',"%$nom_regional%")
                                    ->orderBy('estado_dms','DESC')
                                    ->orderBy('circuito','ASC')
                                    ->get()->toArray();

        if(empty($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','No Existen Coincidencias con CC Propietario!!!');
        }
        $mensaje_resultado = ' el documento de propietario *'.$cc_owner.'* ';
        return view('gestion.ver_nombre_puntos',compact('rpoint','mensaje_resultado'));
    }


    public function traerCodigoPos(Request $request)
    {
        $request->validate([
            'poscode' => ['required'],
        ]);

        $poscode = $request->input('poscode');

        $rpoint =  Infopos_Rpoint::where('pos_code', $poscode)
								   ->orWhere('pos_padre', $poscode)
                                   ->orderBy('estado_dms','DESC')
                                   ->orderBy('circuito','ASC')
                                   ->get()->toArray();

        if(empty($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','No Existen Coincidencias con CodigoSub / CodigoPos!!!');
        }
        $mensaje_resultado = ' el Codigo Sub/Pos *'.$poscode.'* ';
        return view('gestion.ver_nombre_puntos',compact('rpoint','mensaje_resultado'));
    }

    public function traerCircuitoEstado(Request $request)
    {

        $request->validate([
            'circuito' => ['required','exists:infopos_rpoint,circuito'],
            'estado'  => ['required','not_in:X'],
        ]);

        $circuito = $request->input('circuito');
        $estado = $request->input('estado');

        $rpoint =  Infopos_Rpoint::where('circuito','like',"%$circuito%")
                                 ->where('estado_dms','=',$estado)
                                 ->orderBy('estado_dms','DESC')
                                 ->orderBy('circuito','ASC')
                                 ->orderBy('nombre_punto','ASC')
                                 ->get()
                                 ->toArray();

        if(empty($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','No Existen Puntos de venta en el circuito!!!');
        }

        $mensaje_resultado = ' Los puntos de venta hallados en el/los circuito(s) *'.$circuito.'* ';
        return view('gestion.ver_nombre_puntos',compact('rpoint','mensaje_resultado'));

    }


    public function traerPuntosRutaDia(Request $request)
    {

		$dia_semana = ($request->input('dia_semana') == 'X')?$request->input('dia_hoy') : $request->input('dia_semana');

        switch($dia_semana){
            case 0:
                $criterio_dia_semana = 'dom = 1';
                break;
            case 1:
                $criterio_dia_semana = 'lun = 1';
                break;
            case 2:
                $criterio_dia_semana = 'mar = 1';
                break;
            case 3:
                $criterio_dia_semana = 'mie = 1';
                break;
            case 4:
                $criterio_dia_semana = 'jue = 1';
                break;
            case 5:
                $criterio_dia_semana = 'vie = 1';
                break;
            case 6:
                $criterio_dia_semana = 'sab = 1';
                break;
        }

        $id_usuario = Auth::id();
        $empleado = Employee::DatosEmpleado();
        $cedula = $empleado['cedula'];

        $consulta_rutas =
            "SELECT R.*
             FROM infopos_rpoint R
                   INNER JOIN
                    (SELECT circuito
                     FROM infopos_rutas_dms
                     WHERE cedula = '$cedula' AND $criterio_dia_semana) C
                    ON R.circuito = C.circuito
            WHERE estado_dms = 'VENDE'
            ORDER BY lat, lng
            ";

        $rpoint = DB::select($consulta_rutas);

        if(!count($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','No tiene asignada una Ruta para este dia!!!');
        }
		
		// Si vamos a mostrar el mapa de la RUTA, aplica este codigo
		$v_geojson = [];
		$lat_prom = 0.0;
		$lng_prom = 0.0;
		
		$lat_max = 0.0;
		$lat_min = 100.0;
		$lng_max = -100.0;
		$lng_min = 0.0;
		
		if($request->has('ruta')){
			$s_lat = 0.0;
			$s_lng = 0.0;
			
			$c_lat = 0.0;
			$c_lng = 0.0;
			
			$a_geojson = [];
			
			foreach($rpoint as $pv){
				
				if( ( ($pv->lat >= -5.0 AND $pv->lat < 0.0) OR $pv->lat > 0.0) AND $pv->lng < 0.0 ){
					
					//Suma las Latitudes y Longitudes
					$s_lat += $pv->lat;
					$s_lng += $pv->lng;
					
					//Cuenta las Latitudes y Longitudes
					$c_lat++;
					$c_lng++;
					
					if($pv->lat > $lat_max){
						$lat_max = $pv->lat;
					}
					if($pv->lat < $lat_min){
						$lat_min = $pv->lat;
					}
					
					if($pv->lng > $lng_max){
						$lng_max = $pv->lng;
					}
					if($pv->lng < $lng_min){
						$lng_min = $pv->lng;
					}
					
					array_push($a_geojson,
						array(
							'type' => 'Feature',
							'geometry' =>
								array(
										'type' => 'Point',
										'coordinates' => [ (float) $pv->lng , (float) $pv->lat ]
								),
							'properties' =>
								array (
									'idpdv' => $pv->idpdv,
									'nombre' => $pv->nombre_punto,
									'circuito' => $pv->circuito,
									'propietario' => $pv->nom_propietario,
									'categoria_dms' => $pv->cat_dms,
									'categoria_trade' => $pv->categoria_homologada,
									'segmentacion' => $pv->ult_seg_gross,
									'cve' => $pv->nombre_cve,
									'estado_dms' => $pv->estado_dms,
									'tipologia' => $pv->ultima_tipologia,
									'direccion' => $pv->direccion
								)
						)

					);	
				}
			}
			
			$pv_geojson = ['type' => 'FeatureCollection','features' => $a_geojson];
			
			$lat_prom = $s_lat / $c_lat;
			$lng_prom = $s_lng / $c_lng;
			
			return view('mapas.rutas.ver_ruta_dia', ['dia_semana' => $dia_semana, 'lat_prom' => $lat_prom, 'lng_prom' => $lng_prom, 'pv_geojson' => $pv_geojson, 'lat_max' => $lat_max, 'lat_min' => $lat_min, 'lng_max' => $lng_max, 'lng_min' => $lng_min] );
		}	
			$mensaje_resultado = ' Los puntos de venta hallados en el/los circuito(s) De la Ruta del Dia ';
			return view('gestion.ver_puntos_ruta',compact('rpoint','mensaje_resultado', 'dia_semana', 'lat_prom','lng_prom', 'v_geojson'));

/*
        $rpoint =  Infopos_Rpoint::where('circuito','like',"%$circuito%")
                                 ->where('estado_dms','=',$estado)
                                 ->orderBy('estado_dms','DESC')
                                 ->orderBy('circuito','ASC')
                                 ->orderBy('nombre_punto','ASC')
                                 ->get()
                                 ->toArray();

        if(empty($rpoint)){
            return redirect()->route('gestion.buscar.punto')
                             ->with('error','No Existen Puntos de venta en el circuito!!!');
        }

        $mensaje_resultado = ' Los puntos de venta hallados en el/los circuito(s) *'.$circuito.'* ';
        return view('gestion.ver_nombre_puntos',compact('rpoint','mensaje_resultado'));
*/
    }

}
