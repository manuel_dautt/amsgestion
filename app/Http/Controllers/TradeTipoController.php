<?php

namespace App\Http\Controllers;

Use App\Models\TradeClase;
Use App\Models\TradeTipo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TradeTipoController extends Controller
{

   /**
    * Constructor para controller
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('permission:TRADE_LISTAR_TIPOS', ['only' => ['index']]);
      $this->middleware('permission:TRADE_VER_TIPOS', ['only' => ['show']]);
      $this->middleware('permission:TRADE_CREAR_TIPOS', ['only' => ['create','store']]);
      $this->middleware('permission:TRADE_EDITAR_TIPOS', ['only' => ['edit','update']]);
      $this->middleware('permission:TRADE_BORRAR_TIPOS', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tipos = TradeTipo::with(['clase'])->latest()->paginate(5);
        return view('trade.tipos.index',compact('tipos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clases = TradeClase::where('status','=','A')->get();
        return view('trade.tipos.create', compact('clases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'clase_pieza_id' => ['required'],
            'nombre_pieza' => ['required', 'max:20','unique:trade_tipo_piezas']
        ]);


        TradeTipo::create(['nombre_pieza' => strtoupper($request->nombre_pieza),
                           'clase_pieza_id' => $request->clase_pieza_id]);


        return redirect()->route('tipos.index')
                        ->with('success','Tipo Pieza Creado Exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tipo = TradeTipo::find($id);
        return view('trade.tipos.show',compact('tipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $clases = TradeClase::where('status','=','A')->get();
        $tipo = TradeTipo::find($id);
        return view('trade.tipos.edit',compact('tipo','clases'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nombre_pieza' => ['required', 'max:20','unique:trade_tipo_piezas,nombre_pieza,'.$id],
            'clase_pieza_id' => ['required']
        ]);

        $tipo = TradeTipo::find($id);

        $tipo->update(['nombre_pieza' => strtoupper($request->nombre_pieza),
                       'clase_pieza_id' => $request->clase_pieza_id]);

        return redirect()->route('tipos.index')
                        ->with('success','Tipo Pieza Actualizado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tipo = TradeTipo::find($id);
        $tipo->delete();

        return redirect()->route('tipos.index')
                        ->with('success','Tipo PIeza Eliminado Exitosamente');
    }
}
