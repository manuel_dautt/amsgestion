$(document).ready(function(){

	if(window.location.pathname == "/login" || window.location.pathname == "/register") {
		$("body").removeClass("app");
		$("body").addClass("login");
	} else {
		$("body").removeClass("login");
		$("body").addClass("app");
	}

});